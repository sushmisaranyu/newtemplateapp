//
//  ResetPasswordAPI.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 02/12/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class ResetPasswordAPI:ResetPasswordAPIProtocol
{

    public init(){}

    public func resetPassword(code:String,newPassword: String, response: @escaping ((DataLoader<Bool>) -> Void)) {
        UserService.ResetPasswordForMobileNo(otpCode: code, newpassword: newPassword, confirmPassword: newPassword, responseHandler: response)
    }

    
}
