//
//  ResetPasswordViewModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 02/12/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//


import Foundation
import RxCocoa
import RxSwift


public class ResetPasswordViewModel
{
    public struct Input
    {

        let newPasswordDriver:Driver<String>
        let confirmPasswordDriver:Driver<String>

        let newPasswordSubject:BehaviorSubject<String>
        let confirmPasswordSubject:BehaviorSubject<String>

        public init(newPassword:Driver<String>, confirmPassword:Driver<String>)
        {

            self.newPasswordDriver = newPassword
            self.confirmPasswordDriver  = confirmPassword

            self.newPasswordSubject = BehaviorSubject(value: "")
            self.confirmPasswordSubject = BehaviorSubject(value: "")
        }
    }

    public struct Output
    {

        public let validatedNewPassword:Driver<PasswordValidationResult>
        public let validatedConfirmPassword:Driver<ConfirmPasswordValidationResult>
        public let isLoading:Driver<Bool>

        public let changePasswordResponse:Driver<DataLoader<Bool>>

        let validatedNewPasswordSubject:BehaviorSubject<PasswordValidationResult>
        let validatedConfirmPasswordSubject: BehaviorSubject<ConfirmPasswordValidationResult>
        let isLoadingSubject:BehaviorSubject<Bool>

        let changePasswordResponseSubject:BehaviorSubject<DataLoader<Bool>>

        public init()
        {
            validatedNewPasswordSubject     = BehaviorSubject(value: .None)
            validatedConfirmPasswordSubject = BehaviorSubject(value: .None)
            isLoadingSubject                = BehaviorSubject(value: false)
            changePasswordResponseSubject   = BehaviorSubject(value: .None)

            validatedNewPassword            = validatedNewPasswordSubject.asDriver(onErrorJustReturn: .None)
            validatedConfirmPassword        = validatedConfirmPasswordSubject.asDriver(onErrorJustReturn: .None)
            isLoading                       = isLoadingSubject.asDriver(onErrorJustReturn: false)
            changePasswordResponse          = changePasswordResponseSubject.asDriver(onErrorJustReturn: .None)

        }


    }

    public struct Dependancy
    {
        let api:ResetPasswordAPIProtocol
        let validationService:ChangePasswordValidationServiceProtocol

        public init(api:ResetPasswordAPIProtocol,validationService:ChangePasswordValidationServiceProtocol)
        {
            self.api = api
            self.validationService = validationService
        }
    }

    let input:Input
    public let output:Output
    let dependancy:Dependancy
    let disposeBag = DisposeBag()
    let code:String

    public init( input:Input, dependancy:Dependancy,code:String)
    {
        self.input = input
        self.output = Output()
        self.dependancy = dependancy
        self.code = code

        self.setupInputBehaviourSubjects()
    }


    func setupInputBehaviourSubjects()
    {
        self.input.newPasswordDriver.distinctUntilChanged().drive(onNext: {[weak self] updatedText in
            guard let strongSelf = self  else { return }
            strongSelf.resetNewPasswordValidationState()
            strongSelf.input.newPasswordSubject.onNext(updatedText)
        }).disposed(by: disposeBag)

        self.input.confirmPasswordDriver.distinctUntilChanged().drive(onNext: {[weak self] updatedText in
            guard let strongSelf = self  else { return }
            strongSelf.resetConfirmPasswordValidationState()
            strongSelf.input.confirmPasswordSubject.onNext(updatedText)
        }).disposed(by: disposeBag)


    }


    public func validateNewPassword() -> Bool
    {
        guard let newPassword = try? input.newPasswordSubject.value() else { return false }

        let result:PasswordValidationResult = self.dependancy.validationService.validateNewPassword(newPassword)
        self.output.validatedNewPasswordSubject.onNext(result)

        return result.isValid
    }

    func resetNewPasswordValidationState()
    {
        self.output.validatedNewPasswordSubject.onNext(.None)
    }

    public func validateConfirmPassword() -> Bool
    {
        guard let confirmPassword = try? input.confirmPasswordSubject.value() else { return false }
        guard let newpassword = try? input.newPasswordSubject.value() else { return false }


        _ = validateNewPassword()

        let result:ConfirmPasswordValidationResult = self.dependancy.validationService.validateConfirmPassword(newpassword, confirmPassword: confirmPassword)
        self.output.validatedConfirmPasswordSubject.onNext(result)

        return result.isValid
    }

    func resetConfirmPasswordValidationState()
    {
        self.output.validatedConfirmPasswordSubject.onNext(.None)
    }

    public func changePassword()
    {

        if self.isBusy() == true {return}


        guard let newdPassword = try? self.input.newPasswordSubject.value() else {return}
        guard let contifmPassword = try? self.input.confirmPasswordSubject.value() else {return}

        if validateNewPassword() == false || validateConfirmPassword() == false
        {
            return
        }

        self.output.isLoadingSubject.onNext(true)

        self.dependancy.api.resetPassword(code: code, newPassword: newdPassword) { [weak self] (response) in
            self?.output.isLoadingSubject.onNext(false)
            self?.output.changePasswordResponseSubject.onNext(response)
        }
    }

    func isBusy() -> Bool
    {
        guard let isBusy = try? self.output.isLoadingSubject.value() else  {return false}

        return isBusy
    }

}
