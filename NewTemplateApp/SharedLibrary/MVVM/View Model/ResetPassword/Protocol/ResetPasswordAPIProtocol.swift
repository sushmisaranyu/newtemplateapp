//
//  ResetPasswordAPIProtocol.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 02/12/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
public protocol ResetPasswordAPIProtocol:class
{
    func resetPassword(code:String,newPassword:String,response: @escaping ((DataLoader<Bool>)->Void))
}
