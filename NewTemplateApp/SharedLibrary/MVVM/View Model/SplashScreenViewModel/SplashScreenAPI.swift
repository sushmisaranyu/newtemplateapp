//
//  SplashScreenAPI.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 31/08/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class SplashScreenAPI:SplashScreenAPIProtocol
{
    deinit {
        AppUtilities.printLog(message: "deinit")
    }

    public init()
    {
        
    }

    var hasGotRegion = false
    var hasUpdatedServerURLs = true

    var hasConfigBeenUpdated = false
    var hasMenuList = false
    var hasGotCountryCodeList = false

    var currentAppUpdateState:AppUpdateInfo = AppUpdateInfo()

    var hasGotUserDetails = false

    public func getSplashScreen(response: @escaping ((DataLoader<AppUpdateInfo>) -> Void)) {
        getDataFromServer(response: response)
    }


    func getDataFromServer(response: @escaping ((DataLoader<AppUpdateInfo>) -> Void))
    {
        let group = DispatchGroup()

        if hasGotRegion == false
        {
            group.enter()
            AppUtilities.printLog(message:"Group Enter Region")
            DefaultService().GetRegion { [weak self] (response) in
                switch response
                {
                    case .success: self?.hasGotRegion = true
                    default:  break
                }
                print("Group Leave Region")
                group.leave()
            }

        }

        if hasMenuList == false
        {
            group.enter()
           AppUtilities.printLog(message:"Group Enter Menu")
            CatalogService().getHomeScreen(listID: "catalog-tabs") { [weak self] (response) in
                switch response
                {
                case .success(let list) : self?.hasMenuList = true
                    MenuModel.menuList = list
                default: break

                }
               AppUtilities.printLog(message:"Group Leave Menu")
                group.leave()
            }
        }


        if hasUpdatedServerURLs == false
        {
            group.enter()
            AppUtilities.printLog(message:"Group Enter Server")
            DefaultService().GetServerURl { [weak self] (response) in
            switch response
            {
            case .success: self?.hasUpdatedServerURLs = true
            default: break
            }
                AppUtilities.printLog(message:"Group Leave Server")
            group.leave()
        }

        }

        if hasConfigBeenUpdated == false
        {
         var appver = ""
            if let version  = Bundle.main.versionNumber
            {
                appver = version
            }
            group.enter()
           AppUtilities.printLog(message:"Group Enter Config")
            DefaultService().GetConfig(version: appver) { [weak self] (response) in

                switch response
                {
                case .success(let updateStatus): self?.currentAppUpdateState = updateStatus
                                                 self?.hasConfigBeenUpdated = true
                default: break
                }
                AppUtilities.printLog(message:"Group Leave Config")
                group.leave()
            }

        }

        if hasGotCountryCodeList == false
        {
            group.enter()
           AppUtilities.printLog(message:"Group Enter Country Code")
            DefaultService().getCountryCodeList { [weak self]  (response) in
                switch response
                {
                case .success(let updateStatus): self?.hasGotCountryCodeList = true

                default: break
                }
                print("Group Leave Country Code")
                group.leave()
            }
        }

        if OTTUser.hasLoggedInUser() == false
        {
            hasGotUserDetails = true
        }


        if hasGotUserDetails == false
        {
            group.enter()
            AppUtilities.printLog(message:"Group Enter User Details")

            UserService.getUserDetails { [weak self]  (response) in
                switch response
                {
                case .success(let updateStatus): self?.hasGotUserDetails = true

                default: break
                }
                print("Group Leave Enter User Details")
                group.leave()
            }
        }



        group.notify(queue: DispatchQueue.main, execute: { [weak self] in

            guard let strongSelf = self else {return}

            let valid = strongSelf.hasUpdatedServerURLs && strongSelf.hasGotRegion && strongSelf.hasConfigBeenUpdated && strongSelf.hasMenuList && strongSelf.hasGotUserDetails

            if valid == true
            {
               response(DataLoader.success(response: strongSelf.currentAppUpdateState))
            }
            else
            {
                response(DataLoaderErrorHelper.getDefaultErrorDataLoader())
            }

        })
    }



}
