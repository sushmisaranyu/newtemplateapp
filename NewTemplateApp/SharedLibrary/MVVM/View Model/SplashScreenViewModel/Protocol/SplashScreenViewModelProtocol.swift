//
//  SplashScreenViewModelProtocol.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 31/08/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public protocol SplashScreenAPIProtocol:class {
    func getSplashScreen(response: @escaping ((DataLoader<AppUpdateInfo>)->Void))
}
