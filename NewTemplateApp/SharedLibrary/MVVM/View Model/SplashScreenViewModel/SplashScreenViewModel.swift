//
//  SplashScreenViewModel.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 11/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

public class SplashScreenViewModel
{
    deinit {
        AppUtilities.printLog(message: "Deinit")
    }

    public struct Output
    {
        init()
        {
            isLoadingSubject = BehaviorSubject(value: false)
            isLoadingDriver = isLoadingSubject.asDriver(onErrorJustReturn: false)

            viewStateErrorSubject = BehaviorSubject(value:ViewStateError.NoError)
            viewStateErrorDriver = viewStateErrorSubject.asDriver(onErrorJustReturn: ViewStateError.NoError)

            updateStatusSubject = PublishSubject<AppUpdateInfo>()
            updateStatusDriver = updateStatusSubject.asDriver(onErrorJustReturn: AppUpdateInfo())

        }

        public let updateStatusDriver:Driver<AppUpdateInfo>
        public let isLoadingDriver: Driver<Bool>
        public let viewStateErrorDriver: Driver<ViewStateError>

        let updateStatusSubject:PublishSubject<AppUpdateInfo>
        let isLoadingSubject:BehaviorSubject<Bool>
        let viewStateErrorSubject:BehaviorSubject<ViewStateError>


    }

    public struct Dependancy
    {
        let api:SplashScreenAPIProtocol

        public init(api:SplashScreenAPIProtocol)
        {
            self.api = api
        }
    }


    public let output:Output
    public let dependancy:Dependancy


    public init(dependancy:Dependancy)
    {
        self.output = Output()
        self.dependancy = dependancy
    }


    public func getDataFromServer() {
        guard let isBusy = try? output.isLoadingSubject.value() else {return}
        if isBusy == true {return}

        output.isLoadingSubject.onNext(true)
        output.viewStateErrorSubject.onNext(.NoError)

        self.dependancy.api.getSplashScreen(response: {[weak self] response in
            guard let strongSelf = self else {return}

            strongSelf.output.isLoadingSubject.onNext(false)

            switch response
            {
            //TODO: No item check
            case .success(let response): strongSelf.output.updateStatusSubject.onNext(response)
            case .connectivityError: strongSelf.output.viewStateErrorSubject.onNext(ViewStateError.InternetConnectivityError);
            case .serverErrorWithCode(let code, let message): strongSelf.output.viewStateErrorSubject.onNext(ViewStateError.ErrorFromServer(title: "Opps", message: "Some thing went wrong")); break
            default: break
            }
        })
    }

}
