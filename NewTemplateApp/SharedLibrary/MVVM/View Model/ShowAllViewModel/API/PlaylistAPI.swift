//
//  PlaylistAPI.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 27/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class PlaylistAPI:ShowAllAPIProtocol
{
    public init()
    {

    }

    public func isCatalogListNeeded() -> Bool {
        return false
    }

    public func getVideoItemsFor(params: Any, response: @escaping ((DataLoader<[OTTVideoItem]>) -> Void)) {

        if let cp = params as? CatalogParams
        {
            PlaylistService().GetPlayListFor(playListId: cp.listID, responseHandler: response)
        }
    }

    public func getCatalogItemFor(params: Any, response: @escaping ((DataLoader<CatalogListItem>) -> Void)) {
        return
    }


}
