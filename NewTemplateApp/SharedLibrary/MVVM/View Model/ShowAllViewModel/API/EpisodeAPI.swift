//
//  EpisodeAPI.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 26/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class EpisodeAPI:ShowAllAPIProtocol
{
    public init()
    {

    }
    
    public func isCatalogListNeeded() -> Bool {
        return false
    }

    public func getVideoItemsFor(params: Any, response: @escaping ((DataLoader<[OTTVideoItem]>) -> Void)) {

        if let showID = params as? String
        {
            TVShowService.getEpisodesForShow(showId: showID, responseHandler: response)
        }
    }

    public func getCatalogItemFor(params: Any, response: @escaping ((DataLoader<CatalogListItem>) -> Void)) {
        return
    }


}
