//
//  TrendingShowAllAPI.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 27/12/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class  TrendingShowAllAPI: ShowAllAPIProtocol
{

    var isCatalogNeeded:Bool

    public init(isCatalogNeeded:Bool)
    {
        self.isCatalogNeeded = isCatalogNeeded
    }

    public func isCatalogListNeeded() -> Bool {
        return isCatalogNeeded
    }

    public func getCatalogItemFor(params: Any, response: @escaping ((DataLoader<CatalogListItem>) -> Void)) {

        guard  let params = params as? CatalogParams
            else {
                response(DataLoaderErrorHelper.getBadResponse())
                return
        }

        CatalogService.getCatalogListItemWithKidsFilter(listID: params.listID, responseHandler: response)

    }

    public func getVideoItemsFor(params: Any, response: @escaping ((DataLoader<[OTTVideoItem]>) -> Void)) {

        guard  let params = params as? CatalogParams
            else {
                response(DataLoaderErrorHelper.getBadResponse())
                return
        }

        if let genre = params.genre
        {
            CatalogService.getItemsInCatalogListWithGenreFiltre(listID: params.listID, genre: genre, responseHandler: response)
        }
        else
        {
            CatalogService.getItemsInCatalogListWith(listID: params.listID, responseHandler: response)
        }

    }
}
