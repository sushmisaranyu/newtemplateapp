//
//  AllTVChannelAPI.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 24/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class AllTVChannelAPI:ShowAllAPIProtocol
{
    public init()
    {

    }

    public func isCatalogListNeeded() -> Bool {
        return true
    }

    public func getVideoItemsFor(params: Any, response: @escaping ((DataLoader<[OTTVideoItem]>) -> Void)) {


    }

    public func getCatalogItemFor(params: Any, response: @escaping ((DataLoader<CatalogListItem>) -> Void)) {
        guard let catalog = params as? CatalogParams else {return}
     
        CatalogService.getCatalogListItemWithKidsFilter(listID: catalog.listID, responseHandler: response)
    }


}
