//
//  StaticVideoItemAPI.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 06/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class StaticVideoItemAPI:ShowAllAPIProtocol
{

    var items:[OTTVideoItem]
    public  init(items:[OTTVideoItem]) {
        self.items = items
    }

    public func getVideoItemsFor(params: Any, response: @escaping ((DataLoader<[OTTVideoItem]>) -> Void)) {
        response(DataLoader.success(response: items))
    }

    public func isCatalogListNeeded() -> Bool {
        return false
    }

    public func getCatalogItemFor(params: Any, response: @escaping ((DataLoader<CatalogListItem>) -> Void)) {
        response(DataLoader.None)
    }

}
