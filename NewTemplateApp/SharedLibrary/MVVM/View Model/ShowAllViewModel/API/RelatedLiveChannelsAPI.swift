//
//  RelatedLiveChannelsAPI.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 01/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class  RelatedLiveChannelsAPI:ShowAllAPIProtocol
{

    var category:String

    public init(category:String)
    {
        self.category = category
    }

    public func isCatalogListNeeded() -> Bool {
        return true
    }

    public func getVideoItemsFor(params: Any, response: @escaping ((DataLoader<[OTTVideoItem]>) -> Void)) {


    }

    public func getCatalogItemFor(params: Any, response: @escaping ((DataLoader<CatalogListItem>) -> Void)) {
        guard let catalog = params as? CatalogParams else {return}
       CatalogService.getCatalogListItemWithContentCategory(listID: catalog.listID, content_category: category, responseHandler: response)
    }


}
