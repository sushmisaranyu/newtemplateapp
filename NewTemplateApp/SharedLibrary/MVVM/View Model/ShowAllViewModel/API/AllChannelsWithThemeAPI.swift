//
//  AllChannelsWithThemeAPI.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 30/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class  AllChannelsWithThemeAPI:ShowAllAPIProtocol
{

    var theme:String

    public init(theme:String)
    {
        self.theme = theme
    }

    public func isCatalogListNeeded() -> Bool {
        return true
    }

    public func getVideoItemsFor(params: Any, response: @escaping ((DataLoader<[OTTVideoItem]>) -> Void)) {


    }

    public func getCatalogItemFor(params: Any, response: @escaping ((DataLoader<CatalogListItem>) -> Void)) {
        guard let catalog = params as? CatalogParams else {return}
        //CatalogService.getVideoFromParentCatalogForTV(catalogID: catalog.listID, responseHandler: response)
        CatalogService.getCatalogListItemWithTheme(listID: catalog.listID, theme: theme, responseHandler: response)
    }


}
