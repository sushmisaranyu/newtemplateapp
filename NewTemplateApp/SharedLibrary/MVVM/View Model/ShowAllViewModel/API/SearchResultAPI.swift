//
//  SearchAPI.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 09/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class SearchResultAPI: ShowAllAPIProtocol
{
    public init()
    {

    }

    public func isCatalogListNeeded() -> Bool {
        return false
    }

    public func getCatalogItemFor(params: Any, response: @escaping ((DataLoader<CatalogListItem>) -> Void)) {
    }

    public func getVideoItemsFor(params: Any, response: @escaping ((DataLoader<[OTTVideoItem]>) -> Void)) {
        guard let searchTerm = params as? String else {return}
        SearchService.getSearchResult(text: searchTerm, responseHandler: response)
    

    }
}
