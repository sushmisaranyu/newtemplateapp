//
//  EpisodeListViewModel.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 26/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public struct ShowParams
{
    let contentID:String
    let catalogID:String
}

public class EpisodeListViewModel:BaseShowAllViewModel
{
    var showContentId:String
    var showCatalogId:String


    public init(showContentId:String,showCatalogId:String,dependancy:Dependancy)
    {
        self.showContentId = showContentId
        self.showCatalogId = showCatalogId
        super.init(dependancy: dependancy)

    }

    public override func getDataFromServer()
    {
        getEpisodeList()
    }

    public func getEpisodeList() {
        guard let isBusy = try? defaultOutput.isLoadingSubject.value() else {return}
        if isBusy == true {return}

        defaultOutput.isLoadingSubject.onNext(true)
        defaultOutput.viewStateErrorSubject.onNext(.NoError)

        self.dependancy.api.getVideoItemsFor(params: self.showContentId, response: {[weak self] response in
            guard let strongSelf = self else {return}

            strongSelf.defaultOutput.isLoadingSubject.onNext(false)

            switch response
            {
            //TODO: No item check
            case .success(let response):

                print("Episode Item count : \(response.count)")
                if response.count == 0
                {
                    strongSelf.defaultOutput.viewStateErrorSubject.onNext(ViewStateError.NoData);
                }
                else
                {
                    if strongSelf.hasShowAll {
                        var itemsshowall = response
                        itemsshowall.append(OTTVideoItem.getShowAllItem())
                        strongSelf.default_Output.videoListSubject.onNext(itemsshowall)
                    }else {
                        strongSelf.default_Output.videoListSubject.onNext(response)
                    }
                    // strongSelf.default_Output.videoListSubject.onNext(response)
                }
            case .connectivityError: strongSelf.defaultOutput.viewStateErrorSubject.onNext(ViewStateError.InternetConnectivityError);
            case .serverErrorWithCode(let code, let message): strongSelf.defaultOutput.viewStateErrorSubject.onNext(ViewStateError.ErrorFromServer(title: "Opps", message: "Some thing went wrong")); break
            default: break
            }
        })
    }

    public func getFirstEpisode() -> OTTVideoItem?
    {
        guard let videoList = try? self.defaultOutput.videoListSubject.value() else {return nil}

        if videoList.count == 0 {return nil}

        return videoList.first
    }



    public override func getNextPageofItemsFromServer() {

    }

    public override func updateViewModel(updatedProperty:Any)
    {

    }
}
