//
//  ShowAllViewModel.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 06/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa




public class ShowAllViewModel:BaseShowAllViewModel
{

    var listId:String
    var genre:String?


    public init(listId:String,genre:String? = nil,dependancy:Dependancy)
    {
        self.listId = listId
        self.genre = genre
        super.init(dependancy: dependancy)
        
    }

    public override func getDataFromServer()
    {
        if dependancy.api.isCatalogListNeeded()
        {
            getVideoListWithCatalog()
        }
        else
        {
            getVideoList()
        }
    }

    public func getVideoList() {
        guard let isBusy = try? defaultOutput.isLoadingSubject.value() else {return}
        if isBusy == true {return}

        defaultOutput.isLoadingSubject.onNext(true)
        defaultOutput.viewStateErrorSubject.onNext(.NoError)

        let params = CatalogParams(listID: listId, genre: genre)

        self.dependancy.api.getVideoItemsFor(params:params, response: {[weak self] response in
            guard let strongSelf = self else {return}

            strongSelf.defaultOutput.isLoadingSubject.onNext(false)

            switch response
            {
            //TODO: No item check
            case .success(let response):

                strongSelf
                strongSelf.updateVideoList(list: response)
            case .connectivityError: strongSelf.defaultOutput.viewStateErrorSubject.onNext(ViewStateError.InternetConnectivityError);
            case .serverErrorWithCode(let code, let message): strongSelf.defaultOutput.viewStateErrorSubject.onNext(ViewStateError.ErrorFromServer(title: "Opps", message: "Some thing went wrong")); break
            default: break
            }
        })
 }

    public func getVideoListWithCatalog()
    {
        guard let isBusy = try? defaultOutput.isLoadingSubject.value() else {return}
        if isBusy == true {return}

        defaultOutput.isLoadingSubject.onNext(true)
        defaultOutput.viewStateErrorSubject.onNext(.NoError)

        let params = CatalogParams(listID: listId, genre: genre)

        self.dependancy.api.getCatalogItemFor(params: params, response: {[weak self] response in
            guard let strongSelf = self else {return}

            strongSelf.defaultOutput.isLoadingSubject.onNext(false)

            switch response
            {
            //TODO: No item check
            case .success(let response):

                guard let items = response.items else
                {
                    strongSelf.defaultOutput.viewStateErrorSubject.onNext(ViewStateError.NoData);
                    return;
                }

                strongSelf.catalogItem = response

                print("Catalog Layout Type: \(response.layout_type)")
                strongSelf.updateVideoList(list: items)
            case .connectivityError: strongSelf.defaultOutput.viewStateErrorSubject.onNext(ViewStateError.InternetConnectivityError);
            case .serverErrorWithCode(let code, let message): strongSelf.defaultOutput.viewStateErrorSubject.onNext(ViewStateError.ErrorFromServer(title: "Opps", message: "Some thing went wrong")); break
            default: break
            }
        })
    }


    func updateVideoList(list:[OTTVideoItem])
    {
        self.default_Output.videoListSubject.onNext(list)
        if list.count == 0
        {
            self.default_Output.viewStateErrorSubject.onNext(.NoData)
        }


    }

    public override func getNextPageofItemsFromServer() {
        
    }

    public override func updateViewModel(updatedProperty:Any)
     {

    }

}
