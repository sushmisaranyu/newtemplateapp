//
//  SearchViewModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 09/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

public class SearchViewModel:BaseShowAllViewModel
{

    var searchString:String = ""
    public let customOutput: CustomOutput

    let bag = DisposeBag()

    public struct CustomOutput
    {
        public init()
        {
            itemCountSubject = BehaviorSubject(value:0)
            itemCountDriver = itemCountSubject.asDriver(onErrorJustReturn: 0)
        }
    public let itemCountDriver: Driver<Int>

    let itemCountSubject:BehaviorSubject<Int>
    }

    public init()
    {
        customOutput = CustomOutput()
        super.init(dependancy: Dependancy(api: SearchResultAPI()))

        self.default_Output.videoListDriver.drive(onNext: { [weak self] items in
            self?.customOutput.itemCountSubject.onNext(items.count)
        }).disposed(by: bag)

    }

    public override func getDataFromServer()
    {
        getSearchResult(searchStr: searchString)
    }

    public func getSearchResult(searchStr:String) {
//        guard let isBusy = try? defaultOutput.isLoadingSubject.value() else {return}
//        if isBusy == true {return}


        defaultOutput.viewStateErrorSubject.onNext(.NoError)

        //Search result after 2nd letter
        if searchStr.count <= 1 {
            self.default_Output.videoListSubject.onNext([])
            return

        }
         defaultOutput.isLoadingSubject.onNext(true)

        self.dependancy.api.getVideoItemsFor(params:searchStr.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed), response: {[weak self] response in
            guard let strongSelf = self else {return}

            strongSelf.defaultOutput.isLoadingSubject.onNext(false)

            if searchStr != strongSelf.searchString {return}

            switch response
            {
            //TODO: No item check
            case .success(let response):

                if response.count == 0
                {
                    strongSelf.default_Output.videoListSubject.onNext([])
                    strongSelf.defaultOutput.viewStateErrorSubject.onNext(ViewStateError.NoData)
                }
                else
                {
                    print("Show All Item count : \(response.count) : \(searchStr)")
                    strongSelf.default_Output.videoListSubject.onNext(response)
                }


            case .connectivityError: strongSelf.defaultOutput.viewStateErrorSubject.onNext(ViewStateError.InternetConnectivityError);
            case .serverErrorWithCode(let code, let message): strongSelf.defaultOutput.viewStateErrorSubject.onNext(ViewStateError.ErrorFromServer(title: "Opps", message: "Some thing went wrong")); break
            default: break
            }
        })
    }


    public  func getCurrentSearchTerm() -> String
    {
        return self.searchString
    }


    public override func getNextPageofItemsFromServer() {

    }

    public override func updateViewModel(updatedProperty:Any)
    {
        guard let string = updatedProperty as? String else {return}

        let trimedStr = string.trimmingCharacters(in: .whitespacesAndNewlines)

        if trimedStr.isEmpty == true
        {
            self.searchString = trimedStr
            self.default_Output.videoListSubject.onNext([OTTVideoItem]())
            return
        }

        if trimedStr == self.searchString { return }

        self.searchString = trimedStr
        getDataFromServer()
    }

}
