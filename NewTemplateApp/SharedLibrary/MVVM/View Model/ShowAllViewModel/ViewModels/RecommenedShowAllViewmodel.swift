//
//  RecommenedShowAllViewmodel.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 13/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class RecommenedShowAllViewmodel: ShowAllViewModel
{

    var  filterID:String

    public init(listId:String,genre:String? = nil,dependancy:Dependancy,filterID:String)
    {
        self.filterID = filterID
        super.init(listId: listId, genre: genre, dependancy: dependancy)
    }

    public override func updateVideoList(list: [OTTVideoItem]) {
        let items = list.filter({$0.content_id != filterID})

        print("old: \(list.count) :: New \(items.count) :: filter :: \(filterID)")


        if items.count == 0
        {
            self.defaultOutput.viewStateErrorSubject.onNext(ViewStateError.NoData)
        }
        
        if hasShowAll {
            var itemsshowall = items
            itemsshowall.append(OTTVideoItem.getShowAllItem())
            default_Output.videoListSubject.onNext(itemsshowall)
        }else {
            default_Output.videoListSubject.onNext(items)
        }
       
          //self.default_Output.videoListSubject.onNext(items)
    }

    
}

