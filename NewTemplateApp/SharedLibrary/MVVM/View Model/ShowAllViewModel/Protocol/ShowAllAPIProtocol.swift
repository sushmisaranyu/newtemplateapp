//
//  ShowAllAPIProtocol.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 06/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation



public protocol ShowAllAPIProtocol:class {
    func isCatalogListNeeded() -> Bool
    func getVideoItemsFor(params:Any, response: @escaping ((DataLoader<[OTTVideoItem]>)->Void))
    func getCatalogItemFor(params:Any, response: @escaping ((DataLoader<CatalogListItem>)->Void))
}
