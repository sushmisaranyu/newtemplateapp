//
//  BaseShowAllViewModel.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 24/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

public struct DefaultOutput
{

    init()
    {
        isLoadingSubject = BehaviorSubject(value: false)
        isLoadingDriver = isLoadingSubject.asDriver(onErrorJustReturn: false)

        viewStateErrorSubject = BehaviorSubject(value:ViewStateError.NoError)
        viewStateErrorDriver = viewStateErrorSubject.asDriver(onErrorJustReturn: ViewStateError.NoError)

        videoListSubject = BehaviorSubject(value: [])
        videoListDriver = videoListSubject.asDriver(onErrorJustReturn: [])
    }

    public let isLoadingDriver: Driver<Bool>
    public let viewStateErrorDriver: Driver<ViewStateError>

    let isLoadingSubject:BehaviorSubject<Bool>
    let viewStateErrorSubject:BehaviorSubject<ViewStateError>

    public let videoListDriver: Driver<[OTTVideoItem]>
    let videoListSubject: BehaviorSubject<[OTTVideoItem]>

}

public protocol ShowAllViewModelDependancy:class
{
    var defaultOutput:DefaultOutput {get}
    var catalogItem:CatalogListItem? {get set}

    func getDataFromServer()
    func getItemAtIndex(index:Int) -> OTTVideoItem?
    func getNextPageofItemsFromServer()
    func updateItemList(items:[OTTVideoItem])
    func updateViewModel(updatedProperty:Any)
    func getItemCount() -> Int

    func isShowAllEnabled(enable : Bool)
}

public class BaseShowAllViewModel:ShowAllViewModelDependancy
{

    public var defaultOutput: DefaultOutput {get {return default_Output}}

    public var catalogItem: CatalogListItem? {
        get {return catalog_Item}

        set {
            self.catalog_Item = newValue
        }
    }

    weak public var catalog_Item:CatalogListItem?
    var hasShowAll : Bool = false
    let default_Output:DefaultOutput


    public struct Dependancy
    {
        let api:ShowAllAPIProtocol

        public init(api:ShowAllAPIProtocol)
        {
            self.api = api
        }
    }



    public let dependancy:Dependancy

    public init(dependancy:Dependancy)
    {
        self.default_Output = DefaultOutput()
        self.dependancy = dependancy
    }

    public func getDataFromServer() {
        guard let isBusy = try? defaultOutput.isLoadingSubject.value() else {return}
        if isBusy == true {return}

        defaultOutput.isLoadingSubject.onNext(true)
        defaultOutput.viewStateErrorSubject.onNext(.NoError)

        self.dependancy.api.getVideoItemsFor(params: "", response: {[weak self] response in
            guard let strongSelf = self else {return}

            strongSelf.defaultOutput.isLoadingSubject.onNext(false)

            switch response
            {
            //TODO: No item check
            case .success(let response):

                if response.count == 0
                {

                    strongSelf.default_Output.videoListSubject.onNext([])
                    strongSelf.defaultOutput.viewStateErrorSubject.onNext(ViewStateError.NoData)
                }
                else
                {
                    
                    strongSelf.default_Output.videoListSubject.onNext(response)
                }

            case .connectivityError: strongSelf.defaultOutput.viewStateErrorSubject.onNext(ViewStateError.InternetConnectivityError);
            case .serverErrorWithCode(let code, let message): strongSelf.defaultOutput.viewStateErrorSubject.onNext(ViewStateError.ErrorFromServer(title: "Opps", message: "Some thing went wrong")); break
            default: break
            }
        })
    }

    public func getItemAtIndex(index: Int) -> OTTVideoItem? {
        guard let items = try? self.default_Output.videoListSubject.value() else {return nil}

        if items.count < index {return nil}

        return items[index]
    }

    public func getNextPageofItemsFromServer() {

    }

    public func updateItemList(items: [OTTVideoItem]) {
        
        if hasShowAll {
            var itemsshowall = items
            itemsshowall.append(OTTVideoItem.getShowAllItem())
            default_Output.videoListSubject.onNext(itemsshowall)
        }else {
            default_Output.videoListSubject.onNext(items)
        }
    }

    public func updateViewModel(updatedProperty: Any) {

    }

    public func getItemCount() -> Int
    {
          guard let items = try? self.default_Output.videoListSubject.value() else {return 0}

        return items.count
    }

    public func isShowAllEnabled(enable: Bool) {
        hasShowAll = enable
    }
}
