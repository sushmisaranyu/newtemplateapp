//
//  UserDetailsAPIProtocol.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 24/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public protocol UserDetailsAPIProtocol:class
{
    func getUserDetails(response: @escaping ((DataLoader<OTTUserDetails>)->Void))

    func logout(response: @escaping ((DataLoader<Bool>)->Void))
}
