//
//  UserDetailsViewModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 24/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift


public class UserDetailsViewModel
{
    public struct Output {

        init()
        {
            self.isLoadingSubject      = BehaviorSubject(value: false)
            self.userResponseSubject   = BehaviorSubject(value: DataLoader.None)
            self.logoutResponseSubject = BehaviorSubject(value: DataLoader.None)

            //public Driver Setup
            self.isLoading             = isLoadingSubject.asDriver(onErrorJustReturn: false)
            self.userResponse          = userResponseSubject.asDriver(onErrorJustReturn: .None)
            self.logoutResponse        = logoutResponseSubject.asDriver(onErrorJustReturn: DataLoader.None)
        }

        let isLoadingSubject:BehaviorSubject<Bool>
        let userResponseSubject: BehaviorSubject<DataLoader<OTTUserDetails>>
        let logoutResponseSubject: BehaviorSubject<DataLoader<Bool>>

        // Is signing process in progress
        public let isLoading: Driver<Bool>

        // Has user signed in
        public let userResponse: Driver<DataLoader<OTTUserDetails>>
        public let logoutResponse: Driver<DataLoader<Bool>>
    }


    public struct Dependancy
    {
        let api:UserDetailsAPIProtocol

        public init(api:UserDetailsAPIProtocol)
        {
            self.api = api
        }
    }


    public let output:Output
    let dependancy:Dependancy
 

    public init(api:UserDetailsAPIProtocol) {
        self.dependancy = Dependancy(api: api)
        self.output = Output()
    }


    public func getDataFromServer()
    {
        guard let isLoading = try? self.output.isLoadingSubject.value() else {return}

        if isLoading == true {return}
        self.output.isLoadingSubject.onNext(true)
        self.output.userResponseSubject.onNext(.None)
        self.dependancy.api.getUserDetails(response: {[weak self] response in
            self?.output.isLoadingSubject.onNext(false)
            self?.output.userResponseSubject.onNext(response)
        })
    }


    public func logout()
    {
        guard let isLoading = try? self.output.isLoadingSubject.value() else {return}

        if isLoading == true {return}
        self.output.isLoadingSubject.onNext(true)
        self.dependancy.api.logout(response: {[weak self] response in
            self?.output.isLoadingSubject.onNext(false)
            self?.output.logoutResponseSubject.onNext(response)
        })
    }

}
