//
//  UserDetailsAPI.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 24/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class UserDetailsAPI:UserDetailsAPIProtocol
{
    public func logout(response: @escaping ((DataLoader<Bool>) -> Void)) {
        UserService.Logout(responseHandler: response)
    }


    public init() {}

    public func getUserDetails(response: @escaping ((DataLoader<OTTUserDetails>) -> Void)) {
        UserService.getUserDetails(responseHandler: response)
    }


}
