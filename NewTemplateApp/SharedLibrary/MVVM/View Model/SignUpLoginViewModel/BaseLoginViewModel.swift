//
//  BaseLoginViewModel.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 17/08/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa


public class BaseLoginViewModel:ViewModelProtocol
{

    public  struct Input
    {
        public init(username:Driver<String>, password: Driver<String> ) {
            self.username = username
            self.password = password
            self.loginTaps = PublishSubject<Void>()
            self.emailValidate = PublishSubject<Void>()
            self.passwordValidate = PublishSubject<Void>()
        }

        let username: Driver<String>
        let password: Driver<String>
        let loginTaps: PublishSubject<()>
        let emailValidate: PublishSubject<()>
        let passwordValidate: PublishSubject<()>
        //let usernameExit:Signal<()>
    }

    public struct Output {

        init(input:Input, dependancy:Dependancy)
        {
            self.validatedUsernameSubject = BehaviorSubject(value: .None)
            self.validatedPasswordSubject = BehaviorSubject(value: .None)

          //  self.validatedPassword = self.validatedPasswordSubject.asDriver(onErrorJustReturn: .None)
          //  self.validatedUsername = self.validatedUsernameSubject.asDriver(onErrorJustReturn: .None)

            let activityIndicator = ActivityIndicator()
            self.isLoading = activityIndicator.asDriver()

            let usernameAndPassword = Driver.combineLatest(input.username, input.password) { (username: $0, password: $1) }

            self.validatedPassword = input.passwordValidate.withLatestFrom(input.password).flatMapLatest{(password)  in
                return dependancy.validationService.validatePassword(password)
            }.asDriver(onErrorJustReturn: PasswordValidationResult.None).distinctUntilChanged()

            self.validatedUsername = input.emailValidate.withLatestFrom(input.username).flatMapLatest{(username)  in
                return dependancy.validationService.valiateUserName(username)
            }.asDriver(onErrorJustReturn: EmailValidationResult.None).distinctUntilChanged()

            signInResponse = input.loginTaps.asObservable().withLatestFrom(usernameAndPassword).flatMapLatest {(pair)  in

                

                return dependancy.api.signIn(email: pair.username, password: pair.password)
                    .trackActivity(activityIndicator)
                    .asDriver(onErrorJustReturn: DataLoader.complete)

            }.asDriver(onErrorJustReturn:  DataLoader.complete)

            self.signinEnabled = Driver.combineLatest(
                validatedUsername,
                validatedPassword,
                isLoading
            )   { username, password, isLoading in
                    username.isValid &&
                    password.isValid &&
                    !isLoading
                }
                .distinctUntilChanged()
        }

        let validatedUsernameSubject: BehaviorSubject<EmailValidationResult>
        let validatedPasswordSubject: BehaviorSubject<PasswordValidationResult>
        //let activityIndicator:ActivityIndicator
        public let validatedUsername: Driver<EmailValidationResult>
        public let validatedPassword: Driver<PasswordValidationResult>
        public let signinEnabled: Driver<Bool>
        // Is signing process in progress
        public var isLoading: Driver<Bool>

        // Has user signed in
        public var signInResponse: Driver<DataLoader<Bool>>

    }

    public struct Dependancy
    {
        public init(api:LoginAPIProtocol,validationService: LoginValidationServiceProtocol)
        {
            self.api = api
            self.validationService = validationService
        }

        let api: LoginAPIProtocol
        let validationService: LoginValidationServiceProtocol
    }

    let input: Input

    public let output: Output

    let  dependency:Dependancy

    // Is signup button enabled

    public init(input: Input,
         dependency: Dependancy)
    {

        self.dependency = dependency
        self.input = input
        self.output = Output(input:input, dependancy: dependency)

    }


    func validateEmail()
    {
        input.emailValidate.onNext(())
    }

    func validatePassword()
    {
        input.passwordValidate.onNext(())
    }

    func login()
    {
        input.loginTaps.onNext(())
    }

}

