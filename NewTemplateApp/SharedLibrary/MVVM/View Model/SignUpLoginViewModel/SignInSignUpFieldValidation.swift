//
//  SignInSignUpFieldValidation.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 16/08/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class SignInSignUpFieldValidation
{

    public class func ValidateEmail(emailId:String?,validEmail:(()->Void),invalidEmail:((EmailValidationError)->Void)) ->Bool
    {
        if StringHelper.isNilOrEmpty(string: emailId)
        {
            invalidEmail(.EmailIdIsNilOrEmpty)
            return false
        }

        if AppUtilities.isValidEmail(emailId) == false
        {
            invalidEmail(.InvalidEmailId)
            return false
        }


        if StringHelper.hasSpace(string: emailId)
        {
            invalidEmail(.EmailIdHasSpaces)
            return false
        }


        validEmail()
        return true
    }

    public class func ValidateMobileNos(mobileNos:String?,valid:(()->Void),invalidMobileNo:((MobileNosValidationError)->Void)) ->Bool
    {
        if StringHelper.isNilOrEmpty(string: mobileNos)
        {
            invalidMobileNo(.MobileNosIsNilOrEmpty)
            return false
        }

        if AppUtilities.isMobile(string: mobileNos) == false
        {
            invalidMobileNo(.InvalidMobileNos)
            return false
        }


        if StringHelper.hasSpace(string: mobileNos)
        {
            invalidMobileNo(.MobileNosIsNilOrEmpty)
            return false
        }


        valid()
        return true
    }


    public class func ValidateCountryCode(countrycode:String,valid:(()->Void),invalidCountryCode:((CountryCodeValidationError) ->Void))-> Bool
    {
        if StringHelper.isNilOrEmpty(string:countrycode)
        {
            invalidCountryCode(.CountrycodeIsNilOrEmpty)
            return false
        }

        if AppUtilities.isValidCountryCode(code:countrycode) == false
        {
            invalidCountryCode(.InvalidCountryCode)
            return false
        }


        if StringHelper.hasSpace(string: countrycode)
        {
            invalidCountryCode(.CountryCodeHasSpaces)
            return false
        }


        valid()
        return true
    }


    public class func isTextMobileNumber(emailOrMobile:String,isMobileNo:(() -> Void),notAMobileNumber:()-> Void) -> Bool
    {
        if AppUtilities.textContainsOlyDigits(code: emailOrMobile) == false
        {
            notAMobileNumber()
            return false
        }
        isMobileNo()
        return true

    }


    public class func ValidateOtp(otpCode:String,validOtp:(()->Void),invalidOtp:((OtpValidationError)->Void))->Bool
    {
        if StringHelper.isNilOrEmpty(string: otpCode)
        {
            invalidOtp(.OtpIsNilOrEmpty)
            return false
        }
        if StringHelper.hasSpace(string: otpCode)
        {
            invalidOtp(.OtpHasSpaces)
            return false
        }
        validOtp()
        return true
    }

    public class func ValidateEmailCharset(emailId:String,validCharset:(()->Void),invalidCharset:((EmailCharsetError)->Void)) -> Bool
    {
        if StringHelper.hasSpace(string: emailId)
        {
            invalidCharset(.SpaceNotAllowedInEmail)
            return false
        }

        validCharset()
        return true
    }


    public class func ValidateMobileCharset(mobile:String,validCharset:(()->Void),invalidCharset:((PhoneCharsetError)->Void)) -> Bool
    {
        if StringHelper.hasSpace(string: mobile)
        {
            invalidCharset(.SpaceNotAllowedInMobile)
            return false
        }


        if(AppUtilities.isMobile(string: mobile) ==  false)
        {
            invalidCharset(.hasCharactersOtherThanNumbers)
            return false
        }

        validCharset()
        return true
    }



    public class func ValidatePasswordForSignUp(password:String?,minCharCount:Int=6,canHaveSpace:Bool=true,validPassword:(()->Void),invalidPassword:((PasswordValidationError)->Void)) ->Bool
    {

        if StringHelper.isNilOrEmpty(string: password)
        {
            invalidPassword(.EmptyOrNil)
            return false
        }

        if (password?.characters.count)! < minCharCount
        {
            invalidPassword(.SizeLessThanMiniumCharCount)
            return false
        }

        if StringHelper.hasSpace(string: password) && canHaveSpace == false
        {
            invalidPassword(.HasSpace)
            return false
        }

        validPassword()
        return true
    }

    public class func ValidatePasswordForSignIn(password:String,validPassword:(()->Void),invalidPassword:((PasswordValidationError)->Void)) -> Bool
    {

        if StringHelper.isNilOrEmpty(string: password)
        {
            invalidPassword(.EmptyOrNil)
            return false
        }


        validPassword()
        return true
    }

    public class func testIsMobile(string:String) ->Bool
    {
        return   AppUtilities.isMobile(string: string)
    }
}
