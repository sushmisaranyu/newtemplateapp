//
//  OTPVerificationAPI.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 05/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class OTPVerificationAPI:OTPVerificationAPIProtocol
{

    public init() {}

    public func otpVerification(mobileno: String, OTP: String, response: @escaping ((DataLoader<String>) -> Void)) {
        UserService.validateOTP(otpString: OTP, mobileNo: mobileno, responseHandler: response)
    }

    public func resendOTP(mobileno: String, response: @escaping ((DataLoader<Bool>) -> Void)) {
        UserService.resendOTPForVerification(mobileNos: mobileno, responseHandler: response)
    }


}
