//
//  OTPVerificationViewModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 05/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

public class OTPVerificationViewModel
{

    var mobileNo:String
    var user:UserModel

    public struct Output
    {
        init()
        {
            isLoadingSubject = BehaviorSubject(value: false)
            isLoadingDriver = isLoadingSubject.asDriver(onErrorJustReturn: false)

            resendOTPStatusSubject = BehaviorSubject(value: DataLoader.None)
            resendOTPStatusDriver = resendOTPStatusSubject.asDriver(onErrorJustReturn: DataLoader.None)

            verificationOTPStatusSubject = BehaviorSubject(value: DataLoader.None)
            verificationOTPStatusDriver = verificationOTPStatusSubject.asDriver(onErrorJustReturn: DataLoader.None)
        }


        public let isLoadingDriver: Driver<Bool>
        public let resendOTPStatusDriver: Driver<DataLoader<Bool>>
        public let verificationOTPStatusDriver: Driver<DataLoader<String>>


        let resendOTPStatusSubject: BehaviorSubject<DataLoader<Bool>>
        let verificationOTPStatusSubject: BehaviorSubject<DataLoader<String>>
        let isLoadingSubject:BehaviorSubject<Bool>
    }

    public struct Dependancy
    {
        let api:OTPVerificationAPIProtocol

        public init(api:OTPVerificationAPIProtocol)
        {
            self.api = api
        }
    }


    public let output:Output
    public let dependancy:Dependancy


    public init(mobileNo:String,user:UserModel,dependancy:Dependancy)
    {
        self.output = Output()
        self.dependancy = dependancy
        self.mobileNo = mobileNo
        self.user = user
    }


    public func validateOTP(otp:String)
    {
        guard let isBusy = try? output.isLoadingSubject.value() else {return}
        if isBusy == true {return}

        output.isLoadingSubject.onNext(true)

        self.dependancy.api.otpVerification(mobileno: mobileNo, OTP: otp) {[weak self] (response) in
            guard let strongSelf = self else {return}

            strongSelf.output.isLoadingSubject.onNext(false)
            strongSelf.output.verificationOTPStatusSubject.onNext(response)
        }
    }

    public func resendOTP() {
        guard let isBusy = try? output.isLoadingSubject.value() else {return}
        if isBusy == true {return}

        output.isLoadingSubject.onNext(true)

        self.dependancy.api.resendOTP(mobileno: mobileNo) {[weak self] response in
            guard let strongSelf = self else {return}
            strongSelf.output.isLoadingSubject.onNext(false)
            strongSelf.output.resendOTPStatusSubject.onNext(response)
        }
    }

    public func setupAndLoginUser(sessionID:String)
    {
        user.logInUser(sessionID: sessionID)
          AppUserDefaults.login_type.setValue(LoginType.Mobile.key)
    }

}
