//
//  VerifyOTPForgotPassworAPI.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 02/12/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation

public class VerifyOTPForgotPassworAPI:OTPVerificationAPIProtocol
{

    public init() {}

    public func otpVerification(mobileno: String, OTP: String, response: @escaping ((DataLoader<String>) -> Void)) {
        UserService.verifyOTPForgotPassword(otpString: OTP, responseHandler: response)
    }

    public func resendOTP(mobileno: String, response: @escaping ((DataLoader<Bool>) -> Void)) {
         UserService.ForgotPasswordMobile(mobileNo: mobileno, responseHandler: response)
    }


}
