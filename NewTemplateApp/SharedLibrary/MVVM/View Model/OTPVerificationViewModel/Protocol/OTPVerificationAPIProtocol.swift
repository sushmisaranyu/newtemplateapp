//
//  OTPVerificationAPIProtocol.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 05/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation

public protocol OTPVerificationAPIProtocol:class {
    func otpVerification(mobileno:String,OTP:String, response: @escaping ((DataLoader<String>)->Void))
    func resendOTP(mobileno:String, response: @escaping ((DataLoader<Bool>)->Void))
}
