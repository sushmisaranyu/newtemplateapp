//
//  MovieDetailsAPIProtocol.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 07/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation

public protocol MovieDetailsAPIProtocol
{
    func getVideoItem(contentId:String,catalogId:String,response: @escaping ((DataLoader<OTTVideoItem>)->Void))

    func getItemDetailsState(contentID:String,catalogId:String,category:String ,response: @escaping ((DataLoader<DetailsScreenStateModel>)->Void) )
}
