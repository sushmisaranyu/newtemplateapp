//
//  MovieDetailsAPI.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 07/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class MovieDetailsAPI:MovieDetailsAPIProtocol
{
    public func getItemDetailsState(contentID: String, catalogId: String, category: String, response: @escaping ((DataLoader<DetailsScreenStateModel>) -> Void)) {
        UserService.GETConsolidatedItemState(catalogId: catalogId, contentId: contentID, category: category, responseHandler: response)
    }


    public init()
    {
        
    }

    public func getVideoItem(contentId: String, catalogId: String, response: @escaping ((DataLoader<OTTVideoItem>) -> Void)) {
        CatalogService.getVideoItem(contentId: contentId, catalogId: catalogId, responseHandler: response)
    }


}
