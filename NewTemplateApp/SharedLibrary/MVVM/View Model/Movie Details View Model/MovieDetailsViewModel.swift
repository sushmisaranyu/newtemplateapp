//
//  MovieDetailsViewModel.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 07/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

public class MovieDetailsViewModel
{
    var catalogListId:String
    var contentId:String


    public var selectedItemDetail:DetailsModel?

    var hasUpdatedDetailsStatus:Bool = false

   // public var movie:OTTVideoItem?




    public struct Output
    {
        init()
        {
            videoItemSubject = BehaviorSubject(value: nil)
            videoItemDriver = videoItemSubject.asDriver(onErrorJustReturn: nil)

        //    selectedItemDetailSubject = BehaviorSubject(value: nil)
         //   selectedItemDetailDriver  = selectedItemDetailSubject.asDriver(onErrorJustReturn: nil)

            isLoadingSubject = BehaviorSubject(value: false)
            isLoadingDriver = isLoadingSubject.asDriver(onErrorJustReturn: false)

            viewStateErrorSubject = BehaviorSubject(value:ViewStateError.NoError)
            viewStateErrorDriver = viewStateErrorSubject.asDriver(onErrorJustReturn: ViewStateError.NoError)
        }

        public let videoItemDriver: Driver<DetailsModel?>
      //  public let selectedItemDetailDriver:Driver<DetailsModel?>

        public let isLoadingDriver: Driver<Bool>
        public let viewStateErrorDriver: Driver<ViewStateError>

        let videoItemSubject: BehaviorSubject<DetailsModel?>
        let isLoadingSubject:BehaviorSubject<Bool>
        let viewStateErrorSubject:BehaviorSubject<ViewStateError>
       // let selectedItemDetailSubject:BehaviorSubject<DetailsModel?>


    }

    public struct Dependancy
    {
        let api:MovieDetailsAPIProtocol

        public init(api:MovieDetailsAPIProtocol)
        {
            self.api = api
        }
    }


    public let output:Output
    public let dependancy:Dependancy


    public init(catalogListId:String,contentId:String,dependancy:Dependancy)
    {
        self.catalogListId = catalogListId
        self.contentId = contentId
        self.output = Output()
        self.dependancy = dependancy
    }


    public func getDataFromServer() {
        guard let isBusy = try? output.isLoadingSubject.value() else {return}
        if isBusy == true {return}

        //Check if media item has been downloaded
        if selectedItemDetail?.mediaitem != nil
        {
            //Get item state if already downloaded Item
            self.getDetailsStateModelFor(item: (selectedItemDetail?.mediaitem)!)
            return
        }


        output.isLoadingSubject.onNext(true)
        output.viewStateErrorSubject.onNext(.NoError)


        self.dependancy.api.getVideoItem(contentId: contentId, catalogId: catalogListId) {[weak self] (response) in
            guard let strongSelf = self else {return}
            strongSelf.output.isLoadingSubject.onNext(false)
            switch response
            {
            //TODO: No item check
            case .success(let response):   // strongSelf.movie = response
                                            strongSelf.selectedItemDetail = DetailsModel(item: response)
                                            strongSelf.getDetailsStateModelFor(item: response)

            case .connectivityError: strongSelf.output.viewStateErrorSubject.onNext(ViewStateError.InternetConnectivityError);
            case .serverErrorWithCode(let code, let message): strongSelf.output.viewStateErrorSubject.onNext(ViewStateError.ErrorFromServer(title: "Opps", message: "Some thing went wrong")); break
            default: break
            }
        }
    }

    public func updateMovie(item:OTTVideoItem)
    {
       // self.movie = item
       // self.output.videoItemSubject.onNext(item)

        self.selectedItemDetail = DetailsModel(item: item)
        self.getDetailsStateModelFor(item: item)
    }

    public func showError()
    {
        self.output.viewStateErrorSubject.onNext(ViewStateError.InternetConnectivityError);
    }

    func getDetailsStateModelFor(item:OTTVideoItem)
    {

        guard let content = item.content_id else {showError(); return;}
        guard let catalog = item.catalog_id else {showError(); return;}
        guard let cat     = item.catalog_object?.layout_scheme  else {showError(); return;}

        guard let isBusy = try? output.isLoadingSubject.value() else {return}
        if isBusy == true {return}



        if OTTUser.hasLoggedInUser() == false
        {
            guard let itemDetails = self.selectedItemDetail else {return}
            self.output.videoItemSubject.onNext(itemDetails)
            return
        }

        if self.selectedItemDetail?.itemState != nil
        {
            return
        }

        output.isLoadingSubject.onNext(true)
        output.viewStateErrorSubject.onNext(.NoError)


        self.dependancy.api.getItemDetailsState(contentID: content, catalogId: catalog, category: cat, response: { [weak self] response in
            guard let strongSelf = self else {return}
            strongSelf.output.isLoadingSubject.onNext(false)
            switch response
            {
            //TODO: No item check
            case .success(let response):
                strongSelf.selectedItemDetail?.setItemState(state: response)

                guard let mitem = strongSelf.selectedItemDetail else {return}
                strongSelf.output.videoItemSubject.onNext(mitem)
                                          
            case .connectivityError: strongSelf.output.viewStateErrorSubject.onNext(ViewStateError.InternetConnectivityError);
            case .serverErrorWithCode(let code, let message): strongSelf.output.viewStateErrorSubject.onNext(ViewStateError.ErrorFromServer(title: "Opps", message: "Some thing went wrong")); break
            default: break
            }

        })
    }

    public func resetItemState()
    {
        self.selectedItemDetail?.itemState = nil
    }


}
