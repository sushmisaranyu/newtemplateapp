//
//  ForgotPasswordValidationService.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 01/12/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public protocol ForgotPasswordValidationServiceProtocol:class
{
    func valiateUsername(_ username: String) -> UserNameValidationResult
    func validateUsernameCharset(_ string: String,text:String) -> Bool

}
