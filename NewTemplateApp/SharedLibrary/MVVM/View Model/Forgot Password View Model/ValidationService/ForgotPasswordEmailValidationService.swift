//
//  ForgotPasswordValidationServiceProtocol.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 01/12/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class ForgotPasswordEmailValidationService:ForgotPasswordValidationServiceProtocol
{
    public init() {}


    public func valiateUsername(_ username: String) -> UserNameValidationResult
    {
        var  result = UserNameValidationResult.None
        if StringHelper.isNilOrEmpty(string: username)
        {
            result = .UserNameIsNilOrEmpty
        }
        else if AppUtilities.isValidEmail(username) == false
        {
            result = .InvalidUserName
        }
        else if StringHelper.hasSpace(string: username)
        {
            result = .UserNameHasSpaces
        }
        else
        {
            result = .Ok
        }

        return result
    }
    public func validateUsernameCharset(_ string: String,text:String) -> Bool
    {
        return true
    }
}
