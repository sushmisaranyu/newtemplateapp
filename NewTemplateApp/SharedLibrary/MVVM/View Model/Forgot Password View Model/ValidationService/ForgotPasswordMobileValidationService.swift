//
//  ForgotPasswordMobileValidationService.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 01/12/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation



public class ForgotPasswordMobileValidationService:ForgotPasswordValidationServiceProtocol
{
    public init() {}


    public func valiateUsername(_ username: String) -> UserNameValidationResult
    {
        var  result = UserNameValidationResult.None
        if StringHelper.isNilOrEmpty(string: username)
        {
            result = .UserNameIsNilOrEmpty
        }
        else if AppUtilities.isMobile(string: username) == false || username.count != 10
        {
            result = .InvalidUserName
        }
        else if StringHelper.hasSpace(string: username)
        {
            result = .UserNameHasSpaces
        }
        else
        {
            result = .Ok
        }

        return result
    }
    public func validateUsernameCharset(_ string: String,text:String) -> Bool
    {

        if (string+text).count > 10 {return false}

        return AppUtilities.isMobile(string: text+string)
    }
}
