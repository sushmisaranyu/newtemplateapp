//
//  ForgotPasswordMobileAPI.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 01/12/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class ForgotPasswordMobileAPI:ForgotPasswordAPIProtocol
{

    public init(){}

    public func sendForgotPassword(username: String, response: @escaping ((DataLoader<Bool>) -> Void)) {
        UserService.ForgotPasswordMobile(mobileNo: username, responseHandler: response)
    }


}
