//
//  ForgotPasswordViewModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 01/12/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

public class ForgotPasswordViewModel
{
    public struct Input
    {
        let userNameDriver:Driver<String>
        public let userNameSubject:BehaviorSubject<String>

        public init(username:Driver<String>)
        {
            self.userNameDriver = username
            self.userNameSubject = BehaviorSubject(value: "")

        }
    }

    public struct Output
    {
        public let validatedUserName:Driver<UserNameValidationResult>
        let validatedUsernameSubject:BehaviorSubject<UserNameValidationResult>

        public let isLoading:Driver<Bool>
        let isLoadingSubject:BehaviorSubject<Bool>

        public let verifiedForgotPasswordResponse:Driver<DataLoader<Bool>>
        let verifiedForgotPasswordResponseSubject:BehaviorSubject<DataLoader<Bool>>

        public init()
        {
            self.validatedUsernameSubject = BehaviorSubject(value: .None)
            self.validatedUserName = validatedUsernameSubject.asDriver(onErrorJustReturn: .None)

            self.isLoadingSubject = BehaviorSubject(value: false)
            self.isLoading = isLoadingSubject.asDriver(onErrorJustReturn: false)

            self.verifiedForgotPasswordResponseSubject = BehaviorSubject(value: .None)
            self.verifiedForgotPasswordResponse = verifiedForgotPasswordResponseSubject.asDriver(onErrorJustReturn: .None)
        }


    }

    public  struct Dependancy
    {
        let api:ForgotPasswordAPIProtocol
        let validationService:ForgotPasswordValidationServiceProtocol

        public init(api:ForgotPasswordAPIProtocol,validationService:ForgotPasswordValidationServiceProtocol)
        {
            self.api = api
            self.validationService = validationService
        }
    }

    public let output:Output
    let input:Input
    let dependancy:Dependancy

    let disposeBag = DisposeBag()

    public init(input:Input,api:ForgotPasswordAPIProtocol,validationService:ForgotPasswordValidationServiceProtocol)
    {
        self.input = input
        self.output = Output()
        self.dependancy = Dependancy(api: api, validationService: validationService)
        self.setupInputBehaviourSubjects()
    }

    func setupInputBehaviourSubjects()
    {

        //Setting up OnNext listener to check for changes in password
        self.input.userNameDriver.distinctUntilChanged().drive(onNext: {[weak self] updatedText in
            guard let strongSelf = self  else { return }
            strongSelf.resetUserNameValidationState()
            strongSelf.input.userNameSubject.onNext(updatedText)
        }).disposed(by: disposeBag)


    }


    func resetUserNameValidationState()
    {
        self.output.validatedUsernameSubject.onNext(.None)
    }

    public func validateUsername() -> Bool
    {
        guard let name = try? input.userNameSubject.value() else { return false}
        let result:UserNameValidationResult = self.dependancy.validationService.valiateUsername(name)
        self.output.validatedUsernameSubject.onNext(result)

        return result.isValid
    }

    public func validateUserName(string:String,text:String) -> Bool
    {
        return self.dependancy.validationService.validateUsernameCharset(string,text:text)
    }

    public func sendForgotPassword(countryCode:String)
    {

        guard let name = try? self.input.userNameSubject.value() else {return}

        if validateUsername() == false { return }

        if isBusy() == true {return}

        self.output.isLoadingSubject.onNext(true)

        self.dependancy.api.sendForgotPassword(username: countryCode+name, response: {[weak self] result in
            self?.output.isLoadingSubject.onNext(false)
            self?.output.verifiedForgotPasswordResponseSubject.onNext(result)
        })
   }

    func isBusy() -> Bool
    {
        guard let isBusy = try? self.output.isLoadingSubject.value() else  {return false}

        return isBusy
    }


}
