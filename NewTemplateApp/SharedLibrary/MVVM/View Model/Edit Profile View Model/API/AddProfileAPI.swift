//
//  AddProfileAPI.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 19/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation

public class AddProfileAPI:EditProfileAPIProtocol
{

    public init(){}

    public func getProfileFor(id: String, response: @escaping ((DataLoader<ProfileUser>) -> Void)) {

    }

    public func saveProfile(profileId: String, firstName: String, isChild: Bool, response: @escaping ((DataLoader<Bool>) -> Void)) {
        ProfileService.addProfile(firstName: firstName, lastName: "", age: "", isChild: isChild, responseHandler: response)
    }

    public func deleteProfile(profileId: String, response: @escaping ((DataLoader<Bool>) -> Void)) {

    }


}
