//
//  EditProfileViewModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 19/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa


public class EditProfileViewModel
{
    public  struct Input
    {
        public init(name:Driver<String>, isChild:Driver<Bool>) {
            nameDriver = name
            //Initlize Behaviour subjects to listen to change in login text fields
            nameSubject = BehaviorSubject(value: "")

            isChildDriver = isChild
            isChildSubject = BehaviorSubject(value:false)
        }

        let nameDriver: Driver<String>
        let nameSubject: BehaviorSubject<String>

        let isChildDriver: Driver<Bool>
        let isChildSubject: BehaviorSubject<Bool>
    }


    public struct Output {

        init(input:Input, dependancy:Dependancy)
        {
            self.validatedProfileNameSubject  = BehaviorSubject(value: .None)
            self.isLoadingSubject             = BehaviorSubject(value: false)
            self.saveProfileResponseSubject   = BehaviorSubject(value: DataLoader.None)
            self.deleteProfileResponseSubject = BehaviorSubject(value: DataLoader.None)

            //public Driver Setup
            self.validatedProfileName  = validatedProfileNameSubject.asDriver(onErrorJustReturn: .None)
            self.isLoading             = isLoadingSubject.asDriver(onErrorJustReturn: false)
            self.saveProfileResponse   = saveProfileResponseSubject.asDriver(onErrorJustReturn: .None)
            self.deleteProfileResponse = deleteProfileResponseSubject.asDriver(onErrorJustReturn: .None)
        }

        let validatedProfileNameSubject:    BehaviorSubject<NameValidationResult>
        let isLoadingSubject:               BehaviorSubject<Bool>
        let saveProfileResponseSubject:     BehaviorSubject<DataLoader<Bool>>
        let deleteProfileResponseSubject:   BehaviorSubject<DataLoader<Bool>>

        public let validatedProfileName: Driver<NameValidationResult>
        public let isLoading: Driver<Bool>
        public let saveProfileResponse: Driver<DataLoader<Bool>>
        public let deleteProfileResponse: Driver<DataLoader<Bool>>

    }

    public struct Dependancy
    {
        public init(api:EditProfileAPIProtocol,validationService: ProfileValidationServiceProtocol)
        {
            self.api = api
            self.validationService = validationService
        }

        let api: EditProfileAPIProtocol
        let validationService: ProfileValidationServiceProtocol
    }

    let input:Input
    public let output:Output
    let dependancy:Dependancy
    let bag  = DisposeBag()
    public let profile:ProfileUser

    public init(input:Input,dependancy:Dependancy,profile:ProfileUser)
    {
        self.input      = input
        self.output     = Output(input: input, dependancy: dependancy)
        self.dependancy = dependancy
        self.profile    = profile
        self.setupInputBehaviourSubjects()
        self.setupInputSubject()
    }

    public func setupInputSubject()
    {
        if let firstname = profile.firstName
        {
            self.input.nameSubject.onNext(firstname)
        }
    }

     func setupInputBehaviourSubjects()
     {
        self.input.nameDriver.distinctUntilChanged().drive(onNext: {[weak self] updatedText in
            guard let strongSelf = self  else { return }
            strongSelf.resetNameValidationState()
            strongSelf.input.nameSubject.onNext(updatedText)
        }).disposed(by: bag)

        self.input.isChildDriver.distinctUntilChanged().drive(onNext: {[weak self] state in
            guard let strongSelf = self  else { return }
            strongSelf.input.isChildSubject.onNext(state)
        }).disposed(by: bag)
    }




    func resetNameValidationState()
    {
        self.output.validatedProfileNameSubject.onNext(.None)
    }


    func isBusy() -> Bool
    {
        guard let isBusy = try? self.output.isLoadingSubject.value() else  {return false}

        return isBusy
    }


    public func validateProfileName() -> Bool
    {

        self.output.validatedProfileNameSubject.onNext(.None)
         guard let firstName =   try? self.input.nameSubject.value() else {return false}

        let result = dependancy.validationService.validateProfileName(name: firstName)

        self.output.validatedProfileNameSubject.onNext(result)

        return result.isValid
    }

    public func saveProfile(isChild:Bool)
    {

        if isBusy() == true {return}

        if validateProfileName() == false {return }

        guard let firstName =   try? self.input.nameSubject.value() else {return}
        guard let pid    =   self.profile.profileId else {return}
        self.output.isLoadingSubject.onNext(true)

        self.dependancy.api.saveProfile(profileId:pid, firstName: firstName, isChild: isChild) {[weak self] (response) in
            guard let strongSelf = self else {return}
            strongSelf.output.isLoadingSubject.onNext(false)
            strongSelf.output.saveProfileResponseSubject.onNext(response)
        }
    }

    public func validateProfileNameCharacterSet(string:String,text:String?) -> Bool
    {

        guard let txt = text else {return false}

        return dependancy.validationService.validateProfileNameCharset(string: string, text: txt)
    }

   public func deleteProfile()
    {

        if isBusy() == true {return}


        guard let firstName =   try? self.input.nameSubject.value() else {return}
        guard let isChild   =   try? self.input.isChildSubject.value() else {return}
        guard let pid       =   self.profile.profileId else {return}
        self.output.isLoadingSubject.onNext(true)

        self.dependancy.api.deleteProfile(profileId: pid) {[weak self] (response) in
            guard let strongSelf = self else {return}
            strongSelf.output.isLoadingSubject.onNext(false)
            strongSelf.output.deleteProfileResponseSubject.onNext(response)
        }
    }



}
