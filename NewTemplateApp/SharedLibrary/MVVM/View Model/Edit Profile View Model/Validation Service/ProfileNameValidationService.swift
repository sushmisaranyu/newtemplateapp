//
//  ProfileNameValidationService.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 19/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class ProfileNameValidationService: ProfileValidationServiceProtocol
{

     public init(){}

    public func validateProfileNameCharset(string: String, text: String) -> Bool {
        if string == "" {return true}

        if (string + text).count > 30 {return false}

        return true
    }

    public func validateProfileName(name: String) -> NameValidationResult {
        var  result = NameValidationResult.None
        if StringHelper.isNilOrEmpty(string: name.trimmingCharacters(in: .whitespacesAndNewlines))
        {
            result = .EmptyOrNil
        }
        else
        {
            result = .Ok
        }

        return result
    }


}
