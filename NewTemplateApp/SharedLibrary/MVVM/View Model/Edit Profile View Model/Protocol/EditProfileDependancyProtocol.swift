//
//  EditProfileDependancyProtocol.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 19/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public protocol EditProfileAPIProtocol:class
{
    func getProfileFor(id:String, response: @escaping ((DataLoader<ProfileUser>)->Void))
    func saveProfile(profileId:String,firstName:String,isChild:Bool, response: @escaping ((DataLoader<Bool>)->Void))
    func deleteProfile(profileId:String, response: @escaping ((DataLoader<Bool>) -> Void))
}


public protocol ProfileValidationServiceProtocol: class
{
    func validateProfileName(name:String) -> NameValidationResult
    func validateProfileNameCharset(string:String,text:String) -> Bool
}
