//
//  CatalogListShowAllViewModel.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 10/08/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class CatalogListShowAllViewModel:DefaultShowAllViewModel
{
    var listId:String

    public init(listId:String) {
        self.listId = listId
    }


    override public func makeAPIRequest(successHandler: @escaping (([OTTVideoItem]) -> Void), errorHandler: @escaping ((String, String) -> Void), internetHandler: @escaping ((String, String) -> Void)) {

        CatalogService.getVideosFor(listId: listId,isNoLimit: true, successHandler: successHandler, errorHandler: errorHandler, internetErrorHandler: internetHandler)
    }
}
