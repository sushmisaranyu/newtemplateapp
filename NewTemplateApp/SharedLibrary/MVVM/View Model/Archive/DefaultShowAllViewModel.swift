//
//  BaseShowAllVM.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 10/08/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift

public class DefaultShowAllViewModel: BaseViewModel, ViewModelProtocol
{

    public struct Input {

    }

    public struct Output
    {

    }

    public struct Dependancy
    {

    }

    public let input:Input
    public let output:Output
    public let dependancy:Dependancy


    public override init()
    {
        input = Input()
        output = Output()
        dependancy = Dependancy()
    }

    public var items = BehaviorSubject<[OTTVideoItem]>(value: [])

    // public var items = BehaviorSubject<[OTTVideoItem]>(value: [])

    public func makeAPIRequest(successHandler: @escaping (([OTTVideoItem]) -> Void), errorHandler: @escaping ((String, String) -> Void), internetHandler: @escaping ((String, String) -> Void)) {

        
    }

    public func getDataFromServer() {

        isLoading.onNext(true)
        viewStateError.onNext(.NoError)
        makeAPIRequest(successHandler: { [weak self] (videoItems) in

            guard let strongSelf = self else {return}
            strongSelf.isLoading.onNext(false)
            strongSelf.items.onNext(videoItems)

            }, errorHandler: { [weak self]  (message, title ) in

                guard let strongSelf = self else {return}

                strongSelf.viewStateError.onNext(ViewStateError.ErrorFromServer(title: "Opps", message: "Something went wrong. Please try again") )
                strongSelf.isLoading.onNext(false)

            }, internetHandler: { [weak self]  (message, title ) in

                guard let strongSelf = self else {return}
                strongSelf.viewStateError.onNext(ViewStateError.InternetConnectivityError)
                strongSelf.isLoading.onNext(false)

        })
    }


}
