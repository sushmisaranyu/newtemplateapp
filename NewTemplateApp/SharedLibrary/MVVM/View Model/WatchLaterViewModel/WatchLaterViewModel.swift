//
//  WatchLaterViewModel.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 26/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

public class WatchLaterViewModel
{

    static let watchLaterKey = "watchlater"

    public struct Output
    {
        public let watchLaterState:Driver<Bool>
        public let viewErrorState:Driver<ViewStateError>


        let isLoadingSubject:BehaviorSubject<Bool>
        let watchLaterStateSubject:BehaviorSubject<Bool>
        let viewErrorStateSubject:BehaviorSubject<ViewStateError>

        public init()
        {
            self.watchLaterStateSubject = BehaviorSubject(value: false)
            self.isLoadingSubject       = BehaviorSubject(value: false)
            self.viewErrorStateSubject  = BehaviorSubject(value: .NoError)

           self.watchLaterState         = watchLaterStateSubject.asDriver(onErrorJustReturn: false)
            self.viewErrorState         = viewErrorStateSubject.asDriver(onErrorJustReturn: .NoError)
        }
    }

    public struct Dependancy
    {
        let api :WatchLaterAPIProtocol

        public init(api:WatchLaterAPIProtocol)
        {
            self.api = api
        }
    }

    public let output:Output
    let dependancy:Dependancy

    var itemState:DetailsScreenStateModel?

    public init(api:WatchLaterAPIProtocol,itemState:DetailsScreenStateModel?)
    {
        self.output = Output()
        self.dependancy = Dependancy(api: api)

        self.itemState = itemState

        if let state = itemState
        {
            self.updateItemState(newState: state)
        }

    }


    public func updateItemState(newState:DetailsScreenStateModel)
    {
        self.itemState = newState

        changeWatchLaterState()
    }

    func changeWatchLaterState()
    {

        guard let state = self.itemState else {
            self.output.watchLaterStateSubject.onNext(false)
            return
        }

        if state.hasWatchListItem() == true
        {
            self.output.watchLaterStateSubject.onNext(true)
        }
        else
        {
            self.output.watchLaterStateSubject.onNext(false)
        }
    }

    public func updateWatchLaterState()
    {

        guard let state = itemState else {return}

        if itemState?.hasWatchListItem() == true
        {
            removeFromWatchLater()
        }
        else
        {
            addToWatchLater()
        }
    }

    public func addToWatchLater()
    {
        if isBusy() == true {return}
        

        guard let contentID = itemState?.contentId else {return}
        guard let catalogId = itemState?.catalogId else {return}

        self.output.isLoadingSubject.onNext(true)
        self.dependancy.api.addToWatchLater(playlistId: WatchLaterViewModel.watchLaterKey, contentId: contentID, catalogId: catalogId, response: {[weak self] response in
                 self?.output.isLoadingSubject.onNext(false)
            switch response
            {

            case .success(let response):

                self?.itemState?.watchLaterListItemId = response
                  self?.changeWatchLaterState()
                self?.output.viewErrorStateSubject.onNext(ViewStateError.NoError)

            case .serverErrorWithCode(let code, let message):
            self?.output.viewErrorStateSubject.onNext(ViewStateError.ErrorFromServer(title: "", message: "Opps something went wrong. Failed To add item from Watch Later list.") )
            case .connectivityError:
                 self?.output.viewErrorStateSubject.onNext(ViewStateError.InternetConnectivityError)
            case .None:
                break
            }

        })
    }

    public func removeFromWatchLater()
    {
        if isBusy() == true {return}

         guard let listId = itemState?.watchLaterListItemId else {return}
             self.output.isLoadingSubject.onNext(true)
        self.dependancy.api
        .removeFromWatchLater(playlistId:  WatchLaterViewModel.watchLaterKey, listItemId:  listId, response: {[weak self] response in
                 self?.output.isLoadingSubject.onNext(false)
            switch response
            {
            case .success(let response):
                self?.itemState?.watchLaterListItemId = nil
                self?.changeWatchLaterState()
                self?.output.viewErrorStateSubject.onNext(ViewStateError.NoError)

            case .serverErrorWithCode(let code, let message):
                self?.output.viewErrorStateSubject.onNext(ViewStateError.ErrorFromServer(title: "", message: "Opps something went wrong. Failed To remove item from Watch Later list.") )
            case .connectivityError:
                self?.output.viewErrorStateSubject.onNext(ViewStateError.InternetConnectivityError)
            case .None:
                break
            }

        })
    }

    func isBusy() -> Bool
    {
        guard let isBusy = try? self.output.isLoadingSubject.value() else  {return false}

        return isBusy
    }

}
