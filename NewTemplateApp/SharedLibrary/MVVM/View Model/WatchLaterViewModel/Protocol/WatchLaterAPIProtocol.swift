//
//  WatchLaterAPIProtocol.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 26/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public protocol WatchLaterAPIProtocol
{
    func addToWatchLater(playlistId:String,contentId:String,catalogId:String,response: @escaping ((DataLoader<String>)->Void))
    func removeFromWatchLater(playlistId:String,listItemId:String,response: @escaping ((DataLoader<Bool>)->Void))
}
