//
//  WatchLaterAPI.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 26/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class WatchLaterAPI:WatchLaterAPIProtocol
{

    public init(){}

    public func addToWatchLater(playlistId: String, contentId: String, catalogId: String, response: @escaping ((DataLoader<String>) -> Void)) {
        PlaylistService.AddItemsToPlayList(playListId: playlistId, contentId: contentId, catalogId: catalogId, responseHandler: response)
    }

    public func removeFromWatchLater(playlistId: String, listItemId: String, response: @escaping ((DataLoader<Bool>) -> Void)) {
        PlaylistService.RemoveItemsToPlayList(playListId: playlistId, listItemID: listItemId, responseHandler: response)
    }


}
