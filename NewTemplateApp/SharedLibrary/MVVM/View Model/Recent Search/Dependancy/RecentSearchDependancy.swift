//
//  RecentSearchDependancy.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 23/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class RecentSearchDependancy:RecentSearchDependancyProtocol
{
    public func clearAllResults() {
        AppUserDefaults.recentSearches.setValue([String]())
    }


    static let maxRecentSearchCount = 10

    public init(){}

    public func saveSearchTerm(string: String) {
        var currentList = getSavedRecentSearched()

        if currentList.contains(string) {return}

        currentList.insert(string, at: 0)

        if currentList.count > RecentSearchDependancy.maxRecentSearchCount
        {
            currentList.removeLast(currentList.count - RecentSearchDependancy.maxRecentSearchCount)
        }

        AppUserDefaults.recentSearches.setValue(currentList)
    }

    public func getSavedRecentSearched() -> [String] {
        var result = [String]()

        guard let savedSearchKeys = AppUserDefaults.recentSearches.value as? [String] else {return result}

        return savedSearchKeys

    }


}
