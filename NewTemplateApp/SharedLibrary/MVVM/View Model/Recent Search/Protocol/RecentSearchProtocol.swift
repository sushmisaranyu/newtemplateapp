//
//  RecentSearchProtocol.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 23/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public protocol RecentSearchDependancyProtocol
{
    func saveSearchTerm(string:String)
    func getSavedRecentSearched() -> [String]
    func clearAllResults()
}
