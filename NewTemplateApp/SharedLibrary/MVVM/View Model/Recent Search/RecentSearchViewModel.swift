//
//  RecentSearchViewModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 23/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

public class RecentSearchViewModel
{
    public struct Output
    {
        public init()
        {
            searchTermSubject = BehaviorSubject(value: [])
            searchTermDriver  = searchTermSubject.asDriver(onErrorJustReturn: [])
        }

        public let searchTermDriver: Driver<[String]>
        let searchTermSubject: BehaviorSubject<[String]>
    }

    public struct Dependancy
    {

        public init(dependancy:RecentSearchDependancyProtocol)
        {
            self.recentSearchDependancy = dependancy
        }

        let recentSearchDependancy:RecentSearchDependancyProtocol
    }

    let dependancy: Dependancy
    public let output:Output

    public init(dependancy:Dependancy)
    {
        self.output = Output()
        self.dependancy = dependancy
    }

    public func saveRecentSearch(string:String)
    {
        self.dependancy.recentSearchDependancy.saveSearchTerm(string: string)
        self.updateRecentSearches()
    }

    public func updateRecentSearches()
    {
        let result = self.dependancy.recentSearchDependancy.getSavedRecentSearched()
        self.output.searchTermSubject.onNext(result)
    }

    public func getItemAtIndex(index:Int) -> String?
    {
        guard let items = try? self.output.searchTermSubject.value() else {return nil}

        if items.count <= index {return nil}

        return items[index]
    }

    public func clearAllRecentSearches()
    {
        self.dependancy.recentSearchDependancy.clearAllResults()
        let result = self.dependancy.recentSearchDependancy.getSavedRecentSearched()
        self.output.searchTermSubject.onNext(result)

    }

   
}
