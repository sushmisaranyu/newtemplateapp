//
//  HoneScreenConentViewModel.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 03/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation



public protocol HomeScreenContentAPIProtocol:class {
    func getHomeScreenWith(listId:String, response: @escaping ((DataLoader<[CatalogListItem]>)->Void))
    func getContinueWatchingList(playListId:String, response: @escaping ((DataLoader<[OTTVideoItem]>)->Void))
}
