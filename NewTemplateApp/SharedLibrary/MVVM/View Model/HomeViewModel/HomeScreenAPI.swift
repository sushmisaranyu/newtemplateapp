//
//  HomeScreenAPI.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 03/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class HomeScreenAPI:HomeScreenContentAPIProtocol
{
    public func getContinueWatchingList(playListId: String, response: @escaping ((DataLoader<[OTTVideoItem]>) -> Void)) {
        PlaylistService().GetPlayListFor(playListId: playListId, responseHandler: response)
    }

    public init() {}
    public func getHomeScreenWith(listId: String, response: @escaping ((DataLoader<[CatalogListItem]>) -> Void)) {
        CatalogService().getHomeScreen(listID: listId, responseHandler: response)
    }


}
