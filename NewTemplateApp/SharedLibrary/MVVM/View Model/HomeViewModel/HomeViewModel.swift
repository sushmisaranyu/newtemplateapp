//
//  HomeViewModel.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 03/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa





public class HomeScreenViewModel: BaseViewModel
{
    var catalogListId:String

    var holderList:[CatalogListItem]?
    var continueWatchingList:[OTTVideoItem]?

    deinit {
        AppUtilities.printLog(message: "Deinit")
        self.continueWatchingList = nil
        self.holderList = nil
    }

    public struct Output
    {
        init()
        {
            catalogListSubject = BehaviorSubject(value: [])
            catalogListDriver = catalogListSubject.asDriver(onErrorJustReturn: [])

            isLoadingSubject = BehaviorSubject(value: false)
            isLoadingDriver = isLoadingSubject.asDriver(onErrorJustReturn: false)

            viewStateErrorSubject = BehaviorSubject(value:ViewStateError.NoError)
            viewStateErrorDriver = viewStateErrorSubject.asDriver(onErrorJustReturn: ViewStateError.NoError)
        }

        public let catalogListDriver: Driver<[CatalogListItem]>
        public let isLoadingDriver: Driver<Bool>
        public let viewStateErrorDriver: Driver<ViewStateError>

        let catalogListSubject: BehaviorSubject<[CatalogListItem]>
        let isLoadingSubject:BehaviorSubject<Bool>
        let viewStateErrorSubject:BehaviorSubject<ViewStateError>


    }

    public struct Dependancy
    {
        let api:HomeScreenContentAPIProtocol

        public init(api:HomeScreenContentAPIProtocol)
        {
            self.api = api
        }
    }


    public let output:Output
    public let dependancy:Dependancy
    let watchHistoryKey:String
    let continueWatchingLayoutSchema:String


    public init(catalogListId:String,dependancy:Dependancy,watchHistoryKey:String,continueWatchingLayoutSchema:String)
    {
        self.catalogListId = catalogListId
        self.output = Output()
        self.dependancy = dependancy
        self.watchHistoryKey = watchHistoryKey
        self.continueWatchingLayoutSchema = continueWatchingLayoutSchema
    }


    public func getDataFromServer() {
        guard let isBusy = try? output.isLoadingSubject.value() else {return}
        if isBusy == true {return}

        output.isLoadingSubject.onNext(true)
        output.viewStateErrorSubject.onNext(.NoError)

        self.dependancy.api.getHomeScreenWith(listId: catalogListId, response: {[weak self] response in
            guard let strongSelf = self else {return}

            strongSelf.output.isLoadingSubject.onNext(false)

            switch response
            {
            //TODO: No item check
            case .success(let response):
                strongSelf.filterList(list:response)
                if strongSelf.hasContinueWatchingSchema(list: response)
                {
                    strongSelf.getWatchHistory()
                }
            case .connectivityError: strongSelf.output.viewStateErrorSubject.onNext(ViewStateError.InternetConnectivityError);
            case .serverErrorWithCode(let code, let message): strongSelf.output.viewStateErrorSubject.onNext(ViewStateError.ErrorFromServer(title: "Opps", message: "Some thing went wrong")); break
            default: break
            }
        })

    }


    func getWatchHistory()
    {
        if OTTUser.hasLoggedInUser() == false {return}

        self.dependancy.api.getContinueWatchingList(playListId: watchHistoryKey, response: {[weak self] response in
            guard let strongSelf = self else {return}


            switch response
            {
            //TODO: No item check
            case .success(let response): strongSelf.addContinueWatchingToFilteredList(items: response)
            default: break
            }
        })
    }

    public func updateContinueWatchingList()
    {
        guard let isBusy = try? output.isLoadingSubject.value() else {return}
        if isBusy == true {return}

        if OTTUser.hasLoggedInUser() == false {return}

        self.getWatchHistory()

    }

    func hasContinueWatchingSchema(list:[CatalogListItem]) -> Bool
    {
        if let index = list.firstIndex(where: {$0.layout_type == continueWatchingLayoutSchema})
        {
            return true
        }
        else
        {
            return false
        }
    }

    func getContineWatchingListIndex() -> Int?
    {
        guard let list =  self.holderList else {return nil}

        if let index = list.firstIndex(where: {$0.layout_type == continueWatchingLayoutSchema})
        {
            return index
        }
        else
        {
            return nil
        }
    }

    func addContinueWatchingToFilteredList(items:[OTTVideoItem])
    {
        guard let index = getContineWatchingListIndex() else {return}

        guard let list = self.holderList else { return }

        list[index].items = items

        filterList(list: list)
    }

    public func filterList(list:[CatalogListItem])
    {
        self.holderList = list

        let filteredList = list.filter{$0.items != nil && $0.items!.count > 0}

        self.output.catalogListSubject.onNext(filteredList)

        //t_continue_watching
    }


    public func getItemAtIndex(index:Int) -> CatalogListItem?
    {
        guard let items = try? self.output.catalogListSubject.value() else {return nil}

        if items.count < index {return nil}

        return items[index]
    }

    public func updateCatalogListID(listId:String)
    {
        self.catalogListId = listId
    }

}
