//
//  DummyHomeScreenAPI.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 05/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class DummyHomeScreenAPI:HomeScreenContentAPIProtocol
{
    public func getContinueWatchingList(playListId: String, response: @escaping ((DataLoader<[OTTVideoItem]>) -> Void)) {
        
    }


    let items:[CatalogListItem]

    public init( items:[CatalogListItem]) {
        self.items = items
    }

    public func getHomeScreenWith(listId: String, response: @escaping ((DataLoader<[CatalogListItem]>) -> Void)) {
        response(DataLoader.success(response: self.items))
      //  response(DataLoader.serverErrorWithCode(code: "1212", message: "Opps something went wrong"))
        // response(DataLoader.None)
    }
}
