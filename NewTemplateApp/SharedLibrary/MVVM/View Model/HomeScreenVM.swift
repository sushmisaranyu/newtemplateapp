//
//  HomeScreenVM.swift
//  SaraynuLibrary
//
//  Created by Ashley Dsouza on 16/05/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation
import CoreData


public class HomeScreenVM: BaseViewModel
{
    public var managedObjectContext = NSManagedObjectContext.newObjectContext()
    private var fetchedResultsController: NSFetchedResultsController<OTTCatalogItem>?
    
    public let continueWatchingKey = "watchhistory"
    public var downloadedHomeScreen = false
    
    var continueWatchingList:[OTTVideoItem]?
    
    public override init()
    {
        super.init()
        SetupFetchResultController()
        managedObjectContext = NSManagedObjectContext.newObjectContext()
    }
    
    public func getHomescreen(successHandler:@escaping (()->Void),errorHandler:@escaping ((String,String)->Void),internetErrorHandler:@escaping ((String,String)->Void))
    {
        
        self.downloadedHomeScreen = false
        CatalogService.getHomeScreen(successHandler:{
            self.downloadedHomeScreen = true
            self.getContinueWatching(successHandler: successHandler, errorHandler: errorHandler, internetErrorHandler: internetErrorHandler)
        },errorHandler:errorHandler,internetErrorHandler:internetErrorHandler)
    }

   
    func SetupFetchResultController()
    {
        
        fetchedResultsController = (OTTCatalogItem.fetchedResultsController(managedObjectContext: managedObjectContext,sortKeys: ["sortId"], isAscending: true, predicate: NSPredicate(format: "isMenuItem == true AND catalogType !='\(OTTCatalogType.Empty.key)'") ) as! NSFetchedResultsController<OTTCatalogItem>)
    }
    
    
    //Get count of OTTCatalog Items for homescreen
    public func getCatalogCount()->Int
    {
        guard let list = fetchedResultsController?.fetchedObjects else
        {
            return 0
        }
        
        return list.count
    }
    
    //Gets the OTTCatalogItem at index
    public func getCatalogForIndex(index:Int) -> OTTCatalogItem?
    {
        if let item = fetchedResultsController?.fetchedObjects?[index]
        {
            return item
        }
        
        return nil
    }
    
    //Reloads OTTCatalogItems from DB
    public func reloadData()
    {
        try? self.fetchedResultsController?.performFetch()
    }
    
    
    func getContinueWatching(successHandler:@escaping (()->Void),errorHandler:@escaping ((String,String)->Void),internetErrorHandler:@escaping ((String,String)->Void))
    {
        
        let catalog = createContinueWatchingCatalog()
        
        if OTTUser.hasLoggedInUser() == false
        {
            self.continueWatchingList = nil
            catalog.catalogType = OTTCatalogType.Empty.key
            successHandler()
            return
        }
        
        PlaylistService.GetPlayListFor(playListId:continueWatchingKey, successHandler: { videos in
            self.continueWatchingList = videos
            
            if self.continueWatchingList?.count == 0
            {
                catalog.catalogType = OTTCatalogType.Empty.key
            }
            else
            {
                catalog.catalogType = OTTCatalogType.ContinueWatching.key
            }
            
            self.managedObjectContext.Save()
            successHandler()
            
        }, errorHandler: errorHandler, internetErrorHandler: internetErrorHandler)
    }
    
    func cleanUpVideosList(list:[OTTVideoItem]) -> [OTTVideoItem]
    {
        var newList = [OTTVideoItem]()
        
        for item in list
        {
            let watchTimeSeconds = item.getWatchTimeForVideo(managedObjectContext: managedObjectContext)
               
            let duration = item.getDurationInSeconds()
            
            
            if watchTimeSeconds == 0 || duration == 0
            {
                newList.append(item)
                continue
            }
            
            if duration - watchTimeSeconds > 20
            {
                newList.append(item)
            }
            
        }
        
        return newList
    }
    
    func createContinueWatchingCatalog() -> OTTCatalogItem
    {
        if let item = OTTCatalogItem.GetOTTCatalogItemWith(listId: continueWatchingKey, managedObjectContext: managedObjectContext)
        {
            return item
        }
        
        let catalog =  OTTCatalogItem.managedObject(managedObjectContext: managedObjectContext) as! OTTCatalogItem
        
        catalog.catalogItemList     = nil
        catalog.catalogType         = OTTCatalogType.Empty.key
        catalog.displayTitle        = "Continue Watching"
        catalog.friendlyId          = continueWatchingKey
        catalog.isMenuItem          = true
        catalog.layout_type         = "OTTVideos"
        catalog.listId              = continueWatchingKey
        catalog.listType            = "standard"
        catalog.sortId              = 0
        catalog.theme               = "catalog list"
        catalog.total_items_count   = 20
        
        return catalog
        
        
    }
    
    public func reloadContinueWatching(successHandler:@escaping (()->Void),errorHandler:@escaping ((String,String)->Void),internetErrorHandler:@escaping ((String,String)->Void))
    {
        getContinueWatching(successHandler: successHandler, errorHandler: errorHandler, internetErrorHandler: internetErrorHandler)
    }
    
    public func getVideoForContinueWatchingAt(index:Int) -> OTTVideoItem
    {
        return continueWatchingList![index]
    }
    
    
    public func continueWatchingItemCount() -> Int
    {
        guard let list = continueWatchingList
        else
        {
            return 0
        }
        return list.count
    }
    
    public func isContinueWatchingAt(index:Int) -> Bool
    {
       guard let item =  getCatalogForIndex(index:index)
        else
       {
            return false
       }
        
        if item.listId == continueWatchingKey
        {
            return true
        }
        
        return false
    }
    
}
