//
//  SetParentalViewModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 27/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

public class ParentalPinViewModel
{
    public struct Output
    {
        public let isLoading: Driver<Bool>
        public let verifyPinResponse: Driver<DataLoader<Bool>>
        public let changePinResponse: Driver<DataLoader<Bool>>

        let isLoadingSubject: BehaviorSubject<Bool>
        let verifyPinResponseSubject :BehaviorSubject<DataLoader<Bool>>
        let changePinResponseSubject :BehaviorSubject<DataLoader<Bool>>


        public init()
        {
            self.isLoadingSubject = BehaviorSubject(value: false)
            self.changePinResponseSubject = BehaviorSubject(value: .None)
            self.verifyPinResponseSubject = BehaviorSubject(value: .None)

            self.isLoading = isLoadingSubject.asDriver(onErrorJustReturn: false)
            self.verifyPinResponse = verifyPinResponseSubject.asDriver(onErrorJustReturn: .None)
            self.changePinResponse = changePinResponseSubject.asDriver(onErrorJustReturn: .None)

        }
    }

    public struct Dependancy {
        let api:ParentalPinAPIProtocol

        public init(api:ParentalPinAPIProtocol)
        {
            self.api = api
        }
    }

    public let output:Output
    let dependancy:Dependancy

    public init(api:ParentalPinAPIProtocol)
    {
        self.dependancy = Dependancy(api: api)
        self.output = Output()
    }



    func isBusy() -> Bool
    {
        guard let isBusy = try? self.output.isLoadingSubject.value() else  {return false}

        return isBusy
    }

    public func setPin(pin:String)
    {
        if isBusy() == true {return}


        self.output.isLoadingSubject.onNext(true)

        self.dependancy.api.updatePin(isParentalPinEnabled: true, pin: pin) {[weak self] (response) in
             self?.output.isLoadingSubject.onNext(false)
            switch response
            {
            case .success: AppUserDefaults.parentalControlFlag.setValue(true)
            default: break
            }
           self?.output.changePinResponseSubject.onNext(response)
        }
    }


    public func validatePin(pin:String)
    {
        if isBusy() == true {return}


        self.output.isLoadingSubject.onNext(true)

        self.dependancy.api.getUserDetails {[weak self] (response) in
        self?.output.isLoadingSubject.onNext(false)
            switch response
            {

            case .success(let response):
                guard let parentalPin = response.parental_pin else
                {
                    self?.output.changePinResponseSubject.onNext(DataLoader.connectivityError)
                    return
                }

                if pin == parentalPin
                {
                    self?.output.verifyPinResponseSubject.onNext(DataLoader.success(response: true))
                }
                else
                {
                    self?.output.verifyPinResponseSubject.onNext(DataLoader.success(response: false))
                }


            case .serverErrorWithCode(let code, let message):
                self?.output.changePinResponseSubject.onNext(DataLoader.serverErrorWithCode(code: code, message: message))
            case .connectivityError:
                 self?.output.changePinResponseSubject.onNext(DataLoader.connectivityError)
            case .None:
                self?.output.changePinResponseSubject.onNext(DataLoader.None)

            }
        }
    }


    public func validatePinAndBindProfile(pin:String,profileId:String)
    {
        if isBusy() == true {return}

        self.output.isLoadingSubject.onNext(true)

        self.dependancy.api.getUserDetails {[weak self] (response) in
            self?.output.isLoadingSubject.onNext(false)
            switch response
            {

            case .success(let response):
                guard let parentalPin = response.parental_pin else
                {
                    self?.output.changePinResponseSubject.onNext(DataLoader.connectivityError)
                    return
                }

                if pin == parentalPin
                {
                    self?.bindProfile(profileId: profileId)
                }
                else
                {
                    self?.output.verifyPinResponseSubject.onNext(DataLoader.success(response: false))
                }


            case .serverErrorWithCode(let code, let message):
                self?.output.changePinResponseSubject.onNext(DataLoader.serverErrorWithCode(code: code, message: message))
            case .connectivityError:
                self?.output.changePinResponseSubject.onNext(DataLoader.connectivityError)
            case .None:
                self?.output.changePinResponseSubject.onNext(DataLoader.None)

            }
        }
    }

    func bindProfile(profileId:String)
    {
        if isBusy() == true {return}


            self.output.isLoadingSubject.onNext(true)

            self.dependancy.api.bindProfile(profileId: profileId) {[weak self] (response) in
                self?.output.isLoadingSubject.onNext(false)
                switch response
                {
                case .success: self?.output.verifyPinResponseSubject.onNext(DataLoader.success(response: true))
                case .serverErrorWithCode(let code, let message):
                    self?.output.changePinResponseSubject.onNext(DataLoader.serverErrorWithCode(code: code, message: message))
                case .connectivityError:
                    self?.output.changePinResponseSubject.onNext(DataLoader.connectivityError)
                case .None:
                    self?.output.changePinResponseSubject.onNext(DataLoader.None)
                }

            }
        }


   
}
