//
//  ParentalProfileAPIProtocol.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 27/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public protocol ParentalPinAPIProtocol:class
{
    func getUserDetails(response: @escaping ((DataLoader<OTTUserDetails>)->Void))
    func updatePin(isParentalPinEnabled:Bool,pin:String,response: @escaping ((DataLoader<Bool>)->Void))

    func bindProfile(profileId:String,response: @escaping ((DataLoader<Bool>)->Void) )

}
