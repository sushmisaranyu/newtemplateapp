//
//  ParentalPinAPI.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 27/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation

public class ParentalPinAPI:ParentalPinAPIProtocol
{
    public func bindProfile(profileId: String, response: @escaping ((DataLoader<Bool>) -> Void)) {
        ProfileService.bindProfile(profileID: profileId, responseHandler: response)
        
    }

    public init(){}

    public func getUserDetails(response: @escaping ((DataLoader<OTTUserDetails>) -> Void)) {
        UserService.getUserDetails(responseHandler: response)
    }

    public func updatePin(isParentalPinEnabled: Bool, pin: String, response: @escaping ((DataLoader<Bool>) -> Void)) {
        UserService.ChangeParentalPin(isParentalPinEnabled: isParentalPinEnabled, pin: pin, responseHandler: response)
    }


}
