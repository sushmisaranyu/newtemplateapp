//
//  ActivePlansAPI.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 07/12/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class ActivePlansAPI:ActivePlansAPIProtocol
{
    public init(){}
    public func getActivePlans(response: @escaping ((DataLoader<[PurchasedPlanModel]>) -> Void)) {
        UserService.getPurchasedPlans(responseHandler: response)
    }


}
