//
//  ActivePlansViewModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 07/12/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift
import  RxCocoa



public class ActivePlansViewModel
{
    public static let Apple_Store_PaymentGatewayKey = "apple_store"
    public static let AllAccessKey = "all_access"
    var activePlans:[PurchasedPlanModel] = []
    var inactivePlans:[PurchasedPlanModel] = []

public struct Output
{
    init()
    {
        plansSubject = BehaviorSubject(value: .None)
        plansDriver  = plansSubject.asDriver(onErrorJustReturn: .None)

        isLoadingSubject = BehaviorSubject(value: false)
        isLoadingDriver = isLoadingSubject.asDriver(onErrorJustReturn: false)



    }

    public let plansDriver: Driver<DataLoader<Bool>>
    public let isLoadingDriver: Driver<Bool>


    let plansSubject: BehaviorSubject<DataLoader<Bool>>
    let isLoadingSubject:BehaviorSubject<Bool>




}

public struct Dependancy
{
    let api:ActivePlansAPIProtocol

    public init(api:ActivePlansAPIProtocol)
    {
        self.api = api
    }
}

public init(api:ActivePlansAPIProtocol)
{
    self.output = Output()
    self.dependancy = Dependancy(api: api)
}

public let output:Output
let dependancy:Dependancy

public func getDataFromServer() {

    if OTTUser.hasLoggedInUser() == false {true}

    guard let isBusy = try? output.isLoadingSubject.value() else {return}
    if isBusy == true {return}

    output.isLoadingSubject.onNext(true)
    output.plansSubject.onNext(.None)


    self.dependancy.api.getActivePlans{[weak self] (response) in
        guard let strongSelf = self else {return}
        strongSelf.output.isLoadingSubject.onNext(false)
        switch response
        {
        //TODO: No item check
        case .success(let response):
            strongSelf.updatePlans(plans: response)
            strongSelf.output.plansSubject.onNext(DataLoader.success(response: true))
        case .connectivityError: strongSelf.output.plansSubject.onNext(DataLoader.connectivityError);
        case .serverErrorWithCode(let code, let message): strongSelf.output.plansSubject.onNext(DataLoader.serverErrorWithCode(code: code, message: message)); break
        default: break
        }
    }
    }

    public func updatePlans(plans:[PurchasedPlanModel])
    {
        let activeList = plans.filter({$0.plan_status == true})
        let inaciveList = plans.filter({$0.plan_status == false})

        self.activePlans = activeList
        self.inactivePlans = inaciveList
    }

    public func getNosOfSections() -> Int
    {
        if self.inactivePlans.count == 0 {return 1}

        return 2
    }

    public func getNosOfItemsInActiveList() -> Int
    {
        return self.activePlans.count
    }

    public func getNosOfInactivePlans() -> Int
    {
        return self.inactivePlans.count
    }

    public func hasActivePlans() -> Bool
    {
        if self.activePlans.count == 0 {
            return false
        }

        return true
    }

    public func hasPreviousPlans() -> Bool
    {
        if self.inactivePlans.count == 0 {
            return false
        }

        return true
    }

    public func getActivePlansFor(index:Int) -> PurchasedPlanModel
    {
        return self.activePlans[index]
    }

    public func getInActivePlansFor(index:Int) -> PurchasedPlanModel
    {
        return self.inactivePlans[index]
    }

    public func canModifyPlans() -> Bool
    {
        let list = self.activePlans.filter({$0.payment_gateway != ActivePlansViewModel.Apple_Store_PaymentGatewayKey})

        if list.count > 0 {return false}

        return true
    }


    public func canAddPack() -> Bool
    {
        if hasActivePlans() == false {return true}

        let list = self.activePlans.filter({$0.category?.hasPrefix(ActivePlansViewModel.AllAccessKey) ?? false})

        if list.count > 0 { return false }

        return true
    }


}



