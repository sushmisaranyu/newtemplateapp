//
//  ActivePlansAPIProtocol.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 07/12/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation

public protocol ActivePlansAPIProtocol:class
{
    func getActivePlans(response: @escaping ((DataLoader<[PurchasedPlanModel]>)->Void))
}
