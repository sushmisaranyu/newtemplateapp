//
//  PlaylistEditViewModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 27/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

public class PlaylistEditViewModel
{
    public struct Output
    {
        let isLoadingSubject: BehaviorSubject<Bool>
        let playlistResponseSubject : BehaviorSubject<DataLoader<Bool>>

        public let isLoading : Driver<Bool>
        public let playlistResponse: Driver<DataLoader<Bool>>

        public init()
        {
            isLoadingSubject = BehaviorSubject(value: false)
            playlistResponseSubject = BehaviorSubject(value: .None)
            isLoading = isLoadingSubject.asDriver(onErrorJustReturn: false)
            playlistResponse = playlistResponseSubject.asDriver(onErrorJustReturn: .None)
        }
    }

    public struct Dependancy
    {
        let api: WatchLaterAPIProtocol

        public init(api:WatchLaterAPIProtocol)
        {
            self.api = api
        }
    }

    public let output:Output
    let dependancy:Dependancy

    let playListId:String

    public init(api:WatchLaterAPIProtocol, playlsitId:String)
    {
        self.output = Output()
        self.dependancy = Dependancy(api: api)
        self.playListId = playlsitId
    }

    public func removeItemFromWatchLater(listItemID:String)
    {
        guard let isBusy = try? output.isLoadingSubject.value() else {return}
        if isBusy == true {return}

        self.output.isLoadingSubject.onNext(true)
        self.dependancy.api.removeFromWatchLater(playlistId: playListId, listItemId: listItemID, response: {[weak self] response in
             self?.output.isLoadingSubject.onNext(false)
            self?.output.playlistResponseSubject.onNext(response)

        })
    }
}
