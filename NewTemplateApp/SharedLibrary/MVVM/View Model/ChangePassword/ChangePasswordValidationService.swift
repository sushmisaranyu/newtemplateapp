//
//  ChangePasswordValidationService.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 25/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class ChangePasswordValidationService:ChangePasswordValidationServiceProtocol
{

    public init(){}

    public func validateOldPassword(_ password: String) -> PasswordValidationResult {
        if StringHelper.isNilOrEmpty(string: password) {
            return .EmptyOrNil
        }

        return .Ok
    }

    public func validateNewPassword(_ password: String) -> PasswordValidationResult {
        if StringHelper.isNilOrEmpty(string: password) {
            return .EmptyOrNil
        }

        if password.count < 6 {
            return .SizeLessThanMiniumCharCount
        }

        return .Ok

    }

    public func validateConfirmPassword(_ password: String, confirmPassword: String) -> ConfirmPasswordValidationResult {
        if password != confirmPassword
        {
            return .notSameAsPassword
        }
        else
        {
            return .Ok
        }
    }



}
