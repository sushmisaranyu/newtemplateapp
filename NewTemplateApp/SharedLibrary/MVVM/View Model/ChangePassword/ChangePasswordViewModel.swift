//
//  ChangePasswordViewModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 25/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift


public class ChangePasswordViewModel
{
    public struct Input
    {
        let oldPasswordDriver:Driver<String>
        let newPasswordDriver:Driver<String>
        let confirmPasswordDriver:Driver<String>

        let oldPasswordSubject:BehaviorSubject<String>
        let newPasswordSubject:BehaviorSubject<String>
        let confirmPasswordSubject:BehaviorSubject<String>

        public init(oldPassword:Driver<String>,newPassword:Driver<String>, confirmPassword:Driver<String>)
        {
            self.oldPasswordDriver = oldPassword
            self.newPasswordDriver = newPassword
            self.confirmPasswordDriver  = confirmPassword

            self.newPasswordSubject = BehaviorSubject(value: "")
            self.oldPasswordSubject = BehaviorSubject(value: "")
            self.confirmPasswordSubject = BehaviorSubject(value: "")
        }
    }

    public struct Output
    {
        public let validatedOldPassword:Driver<PasswordValidationResult>
        public let validatedNewPassword:Driver<PasswordValidationResult>
        public let validatedConfirmPassword:Driver<ConfirmPasswordValidationResult>
        public let isLoading:Driver<Bool>

        public let changePasswordResponse:Driver<DataLoader<Bool>>

        let validatedOldPasswordSubject:BehaviorSubject<PasswordValidationResult>
        let validatedNewPasswordSubject:BehaviorSubject<PasswordValidationResult>
        let validatedConfirmPasswordSubject: BehaviorSubject<ConfirmPasswordValidationResult>
        let isLoadingSubject:BehaviorSubject<Bool>

        let changePasswordResponseSubject:BehaviorSubject<DataLoader<Bool>>

        public init()
        {
            validatedOldPasswordSubject     = BehaviorSubject(value: .None)
            validatedNewPasswordSubject     = BehaviorSubject(value: .None)
            validatedConfirmPasswordSubject = BehaviorSubject(value: .None)
            isLoadingSubject                = BehaviorSubject(value: false)
            changePasswordResponseSubject   = BehaviorSubject(value: .None)

            validatedOldPassword            = validatedOldPasswordSubject.asDriver(onErrorJustReturn: .None)
            validatedNewPassword            = validatedNewPasswordSubject.asDriver(onErrorJustReturn: .None)
            validatedConfirmPassword        = validatedConfirmPasswordSubject.asDriver(onErrorJustReturn: .None)
            isLoading                       = isLoadingSubject.asDriver(onErrorJustReturn: false)
            changePasswordResponse          = changePasswordResponseSubject.asDriver(onErrorJustReturn: .None)

        }


    }

    public struct Dependancy
    {
        let api:ChangePasswordAPIProtocol
        let validationService:ChangePasswordValidationServiceProtocol

        public init(api:ChangePasswordAPIProtocol,validationService:ChangePasswordValidationServiceProtocol)
        {
            self.api = api
            self.validationService = validationService
        }
    }

    let input:Input
    public let output:Output
    let dependancy:Dependancy
    let disposeBag = DisposeBag()

    public init( input:Input, dependancy:Dependancy)
    {
        self.input = input
        self.output = Output()
        self.dependancy = dependancy

         self.setupInputBehaviourSubjects()
    }


    func setupInputBehaviourSubjects()
    {

        //Setting up OnNext listener to check for changes in password
        self.input.oldPasswordDriver.distinctUntilChanged().drive(onNext: {[weak self] updatedText in
            guard let strongSelf = self  else { return }
            strongSelf.resetOldPasswordValidationState()
            strongSelf.input.oldPasswordSubject.onNext(updatedText)
        }).disposed(by: disposeBag)

        self.input.newPasswordDriver.distinctUntilChanged().drive(onNext: {[weak self] updatedText in
            guard let strongSelf = self  else { return }
            strongSelf.resetNewPasswordValidationState()
            strongSelf.input.newPasswordSubject.onNext(updatedText)
        }).disposed(by: disposeBag)

        self.input.confirmPasswordDriver.distinctUntilChanged().drive(onNext: {[weak self] updatedText in
            guard let strongSelf = self  else { return }
            strongSelf.resetConfirmPasswordValidationState()
            strongSelf.input.confirmPasswordSubject.onNext(updatedText)
        }).disposed(by: disposeBag)


    }

    public func validateOldPassword() -> Bool
    {
        guard let password = try? input.oldPasswordSubject.value() else { return false}
        let result:PasswordValidationResult = self.dependancy.validationService.validateOldPassword(password)
        self.output.validatedOldPasswordSubject.onNext(result)

        return result.isValid
    }

    public func validateNewPassword() -> Bool
    {
        guard let newPassword = try? input.newPasswordSubject.value() else { return false }
        guard let password = try? input.oldPasswordSubject.value() else { return false }


        _ = validateOldPassword()

        let result:PasswordValidationResult = self.dependancy.validationService.validateNewPassword(newPassword)
        self.output.validatedNewPasswordSubject.onNext(result)

        return result.isValid
    }

     func resetOldPasswordValidationState()
    {
        self.output.validatedOldPasswordSubject.onNext(.None)
    }

    func resetNewPasswordValidationState()
    {
        self.output.validatedNewPasswordSubject.onNext(.None)
    }

    public func validateConfirmPassword() -> Bool
    {
        guard let confirmPassword = try? input.confirmPasswordSubject.value() else { return false }
        guard let newpassword = try? input.newPasswordSubject.value() else { return false }


        _ = validateNewPassword()

        let result:ConfirmPasswordValidationResult = self.dependancy.validationService.validateConfirmPassword(newpassword, confirmPassword: confirmPassword)
        self.output.validatedConfirmPasswordSubject.onNext(result)

        return result.isValid
    }

    func resetConfirmPasswordValidationState()
    {
        self.output.validatedConfirmPasswordSubject.onNext(.None)
    }

    public func changePassword()
    {

         if self.isBusy() == true {return}

        guard let oldPassword = try? self.input.oldPasswordSubject.value() else {return}
        guard let newdPassword = try? self.input.newPasswordSubject.value() else {return}
        guard let contifmPassword = try? self.input.confirmPasswordSubject.value() else {return}

        if validateOldPassword() == false || validateNewPassword() == false || validateConfirmPassword() == false
        {
            return
        }

          self.output.isLoadingSubject.onNext(true)

        self.dependancy.api.changePassword(oldPassword: oldPassword, newPassword: newdPassword) { [weak self] (response) in
              self?.output.isLoadingSubject.onNext(false)
            self?.output.changePasswordResponseSubject.onNext(response)
        }
    }

    func isBusy() -> Bool
    {
        guard let isBusy = try? self.output.isLoadingSubject.value() else  {return false}

        return isBusy
    }

}
