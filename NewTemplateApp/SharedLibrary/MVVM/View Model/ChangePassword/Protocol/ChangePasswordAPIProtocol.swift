//
//  ChangePasswordAPIProtocol.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 25/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

public protocol ChangePasswordAPIProtocol:class
{
    func changePassword(oldPassword:String,newPassword:String,response: @escaping ((DataLoader<Bool>)->Void))
}

