//
//  ChangePasswordValidationServiceProtocol.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 25/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public protocol ChangePasswordValidationServiceProtocol:class
{
    func validateOldPassword(_ password: String) -> PasswordValidationResult
     func validateNewPassword(_ password: String) -> PasswordValidationResult
      func validateConfirmPassword(_ password: String,confirmPassword:String) -> ConfirmPasswordValidationResult
}
