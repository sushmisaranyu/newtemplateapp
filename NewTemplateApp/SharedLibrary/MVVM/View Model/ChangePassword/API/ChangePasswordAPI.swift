//
//  ChangePasswordAPI.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 25/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class  ChangePasswordAPI:ChangePasswordAPIProtocol
{
    public init()
    {
        
    }

    public func changePassword(oldPassword:String,newPassword:String,response: @escaping ((DataLoader<Bool>)->Void)){
        UserService.ChangePassword(oldPassword: oldPassword, newPassword: newPassword, responseHandler: response)
    }
}
