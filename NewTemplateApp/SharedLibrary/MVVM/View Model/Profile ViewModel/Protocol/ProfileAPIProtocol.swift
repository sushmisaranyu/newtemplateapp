//
//  ProfileAPIProtocol.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 19/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation



public protocol ProfileAPIProtocol:class
{
    func getAllprofiles(response: @escaping ((DataLoader<[ProfileUser]>)->Void))
    func bindProfile(profileId:String,response: @escaping ((DataLoader<Bool>)->Void) )
}
