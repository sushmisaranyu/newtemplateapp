//
//  ProfileAPI.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 19/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class ProfileAPI:ProfileAPIProtocol
{
    public func bindProfile(profileId:String,response: @escaping ((DataLoader<Bool>) -> Void)) {
        ProfileService.bindProfile(profileID: profileId, responseHandler: response)
    }



    public init(){}

    public func getAllprofiles(response: @escaping ((DataLoader<[ProfileUser]>) -> Void)) {
        ProfileService.geAllProfiles(responseHandler: response)
    }

}
