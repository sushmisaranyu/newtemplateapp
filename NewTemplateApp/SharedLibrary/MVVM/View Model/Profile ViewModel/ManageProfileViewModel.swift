//
//  ManageProfileViewModel.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 19/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class ManageProfileViewModel:ProfileViewModel
{

    var maxProfile = 5
    public init(api: ProfileAPI , maxProfile:Int) {
        super.init(api: api)

        if maxProfile > 0
        {
            self.maxProfile = maxProfile
        }
    }

    override func updateProfileList(profiles:[ProfileUser])
    {


        let addProfile = ProfileUser(profileId: "", firstName: "New Profile", lastName: "", age: "", isChild: false, default_user: false)
        addProfile.adduserFlag = true

        var updatedList = profiles

        if profiles.count < maxProfile
        {
            updatedList.append(addProfile)
        }

        self.output.profilesSubject.onNext(updatedList)
    }
}
