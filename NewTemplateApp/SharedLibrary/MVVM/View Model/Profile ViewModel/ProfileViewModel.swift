//
//  ProfileViewModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 19/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift
import  RxCocoa


public class ProfileViewModel
{
    public struct Output
    {
        init()
        {
            profilesSubject = BehaviorSubject(value: [])
            profilesDriver  = profilesSubject.asDriver(onErrorJustReturn: [])

            isLoadingSubject = BehaviorSubject(value: false)
            isLoadingDriver = isLoadingSubject.asDriver(onErrorJustReturn: false)

            viewStateErrorSubject = BehaviorSubject(value:ViewStateError.NoError)
            viewStateErrorDriver = viewStateErrorSubject.asDriver(onErrorJustReturn: ViewStateError.NoError)

            bindProfileResponseSubject = BehaviorSubject(value: DataLoader.None)
            bindProfileResponseDriver = bindProfileResponseSubject.asDriver(onErrorJustReturn: DataLoader.None)

        }

        public let profilesDriver: Driver<[ProfileUser]>
        public let isLoadingDriver: Driver<Bool>
        public let viewStateErrorDriver: Driver<ViewStateError>
        public let bindProfileResponseDriver: Driver<DataLoader<Bool>>

        let profilesSubject: BehaviorSubject<[ProfileUser]>
        let isLoadingSubject:BehaviorSubject<Bool>
        let viewStateErrorSubject:BehaviorSubject<ViewStateError>

        let bindProfileResponseSubject:BehaviorSubject<DataLoader<Bool>>

    }

    public struct Dependancy
    {
        let api:ProfileAPI

        public init(api:ProfileAPI)
        {
            self.api = api
        }
    }

    public init(api:ProfileAPI)
    {
        self.output = Output()
        self.dependancy = Dependancy(api: api)
    }

    public let output:Output
    let dependancy:Dependancy

    public func getDataFromServer() {

        if OTTUser.hasLoggedInUser() == false {true}

        guard let isBusy = try? output.isLoadingSubject.value() else {return}
        if isBusy == true {return}

        output.isLoadingSubject.onNext(true)
        output.viewStateErrorSubject.onNext(.NoError)


        self.dependancy.api.getAllprofiles{[weak self] (response) in
            guard let strongSelf = self else {return}
            strongSelf.output.isLoadingSubject.onNext(false)
            switch response
            {
            //TODO: No item check
            case .success(let response):
                strongSelf.updateProfileList(profiles: response)
            case .connectivityError: strongSelf.output.viewStateErrorSubject.onNext(ViewStateError.InternetConnectivityError);
            case .serverErrorWithCode(let code, let message): strongSelf.output.viewStateErrorSubject.onNext(ViewStateError.ErrorFromServer(title: "Opps", message: "Some thing went wrong")); break
            default: break
            }
        }
    }



    public func bindProfile(profile:ProfileUser) {
        guard let isBusy = try? output.isLoadingSubject.value() else {return}
        guard let profileID = profile.profileId else {return}
        if isBusy == true {return}

//        if let boundProfileID = AppUserDefaults.boundProfileId
//        {
//            if boundProfileID == profileID
//            {
//                return
//            }
//        }

        output.isLoadingSubject.onNext(true)
        output.viewStateErrorSubject.onNext(.NoError)
        output.bindProfileResponseSubject.onNext(DataLoader.None)

        self.dependancy.api.bindProfile(profileId: profileID){[weak self] (response) in
            guard let strongSelf = self else {return}
            strongSelf.output.isLoadingSubject.onNext(false)
            switch response
            {
            //TODO: No item check
            case .success(let response): AppUserDefaults.profile_ID.setValue(profileID)
            default: break
            }
            strongSelf.output.bindProfileResponseSubject.onNext(response)
        }
    }


    public func bindToDefaultProfile()
    {
        guard  let list = try? self.output.profilesSubject.value() else {
            return
        }

        let defaultProfile = list.first(where: {$0.default_user == true})

        if let profile:ProfileUser = defaultProfile
        {
            self.bindProfile(profile: profile)
        }
    }

    public func numberOfProfiles() -> Int
    {
        guard let profiles = try? self.output.profilesSubject.value() else {return 0}
        return profiles.count
    }

    func updateProfileList(profiles:[ProfileUser])
    {
        self.output.profilesSubject.onNext(profiles)
    }

    public func getProfileCellFor(indexPath:IndexPath) -> ProfileUser?
    {
        guard  let profiles = try? output.profilesSubject.value() else {return nil}

        if profiles.count < indexPath.row - 1
        {
            return nil
        }

        return profiles[indexPath.row]
    }
}
