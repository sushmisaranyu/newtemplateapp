//
//  ClearWatchHistoryDependancy.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 19/12/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public protocol ClearWatchHistoryAPIProtocol
{
    func clearWatchHistory(response: @escaping ((DataLoader<Bool>) -> Void))
}
