//
//  ClearWatchHistoryViewModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 19/12/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa


public class ClearWatchHistoryViewModel
{
    public struct Output
    {
        init()
        {
            clearHistorySubject = BehaviorSubject(value: .None)
            clearHistoryDriver = clearHistorySubject.asDriver(onErrorJustReturn: .None)

            isLoadingSubject = BehaviorSubject(value: false)
            isLoadingDriver = isLoadingSubject.asDriver(onErrorJustReturn: false)
        }

        public let clearHistoryDriver: Driver<DataLoader<Bool>>
        public let isLoadingDriver: Driver<Bool>


        let clearHistorySubject: BehaviorSubject<DataLoader<Bool>>
        let isLoadingSubject:BehaviorSubject<Bool>
    }

    public struct Dependancy
    {
        public let api:ClearWatchHistoryAPIProtocol

        init(api:ClearWatchHistoryAPIProtocol) {
            self.api = api
        }
    }

    public var output:Output
    var dependancy:Dependancy

    public init(api:ClearWatchHistoryAPIProtocol)
    {
        self.output = Output()
        self.dependancy = Dependancy(api:api)
    }

   public func clearWatchHistory()
    {

        guard let isBusy = try? output.isLoadingSubject.value() else {return}
        if isBusy == true {return}

        output.isLoadingSubject.onNext(true)
        output.clearHistorySubject.onNext(.None)


        self.dependancy.api.clearWatchHistory{[weak self] (response) in
            guard let strongSelf = self else {return}
            strongSelf.output.isLoadingSubject.onNext(false)
            switch response
            {
            //TODO: No item check
            case .success(let response):
                strongSelf.output.clearHistorySubject.onNext(DataLoader.success(response: true))
            case .connectivityError: strongSelf.output.clearHistorySubject.onNext(DataLoader.connectivityError);
            case .serverErrorWithCode(let code, let message): strongSelf.output.clearHistorySubject.onNext(DataLoader.serverErrorWithCode(code: code, message: message)); break
            default: break
            }
        }
    }

}
