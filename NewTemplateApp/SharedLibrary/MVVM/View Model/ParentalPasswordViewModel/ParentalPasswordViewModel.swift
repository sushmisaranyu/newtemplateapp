//
//  ParentalPasswordViewModel.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 27/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

public class ParentalPasswordViewModel
{
    public struct Input
    {
        let passworDriver:Driver<String>
        public let passwordSubject:BehaviorSubject<String>

        public init(password:Driver<String>)
        {
            self.passworDriver = password
            self.passwordSubject = BehaviorSubject(value: "")

        }
    }

    public struct Output
    {
        public let validatedPassword:Driver<PasswordValidationResult>
        let validatedPasswordSubject:BehaviorSubject<PasswordValidationResult>

        public let isLoading:Driver<Bool>
        let isLoadingSubject:BehaviorSubject<Bool>

        public let verifiedPasswordResponse:Driver<DataLoader<Bool>>
        let verifiedPasswordResponseSubject:BehaviorSubject<DataLoader<Bool>>

        public init()
        {
            self.validatedPasswordSubject = BehaviorSubject(value: .None)
            self.validatedPassword = validatedPasswordSubject.asDriver(onErrorJustReturn: .None)

            self.isLoadingSubject = BehaviorSubject(value: false)
            self.isLoading = isLoadingSubject.asDriver(onErrorJustReturn: false)

            self.verifiedPasswordResponseSubject = BehaviorSubject(value: .None)
            self.verifiedPasswordResponse = verifiedPasswordResponseSubject.asDriver(onErrorJustReturn: .None)
        }


    }

    public  struct Dependancy
    {
        let api:ParentalPasswordAPIProtocol
        let validationService:ParentalPasswordValidationServiceProtocol

        public init(api:ParentalPasswordAPIProtocol,validationService:ParentalPasswordValidationServiceProtocol)
        {
            self.api = api
            self.validationService = validationService
        }
    }

    public let output:Output
    let input:Input
    let dependancy:Dependancy

    let disposeBag = DisposeBag()

    public init(input:Input,api:ParentalPasswordAPIProtocol,validationService:ParentalPasswordValidationServiceProtocol)
    {
        self.input = input
        self.output = Output()
        self.dependancy = Dependancy(api: api, validationService: validationService)
        self.setupInputBehaviourSubjects()
    }

    func setupInputBehaviourSubjects()
    {

        //Setting up OnNext listener to check for changes in password
        self.input.passworDriver.distinctUntilChanged().drive(onNext: {[weak self] updatedText in
            guard let strongSelf = self  else { return }
            strongSelf.resetPasswordValidationState()
            strongSelf.input.passwordSubject.onNext(updatedText)
        }).disposed(by: disposeBag)


    }


    func resetPasswordValidationState()
    {
        self.output.validatedPasswordSubject.onNext(.None)
    }

    public func validatePassword() -> Bool
    {
        guard let password = try? input.passwordSubject.value() else { return false}
        let result:PasswordValidationResult = self.dependancy.validationService.validatePassword(password)
        self.output.validatedPasswordSubject.onNext(result)

        return result.isValid
    }

    public func verifyPassword()
    {

        guard let password = try? self.input.passwordSubject.value() else {return}

        if validatePassword() == false { return }

        if isBusy() == true {return}

        self.output.isLoadingSubject.onNext(true)

        self.dependancy.api.verifyPassword(password: password, response: {[weak self] result in
            self?.output.isLoadingSubject.onNext(false)
            self?.output.verifiedPasswordResponseSubject.onNext(result)
        })

    }

    public func resetPinFlowVerifyPassword()
    {
        guard let password = try? self.input.passwordSubject.value() else {return}

        if validatePassword() == false { return }

        if isBusy() == true {return}

        self.output.isLoadingSubject.onNext(true)

        self.dependancy.api.verifyPassword(password: password, response: {[weak self] result in
            self?.output.isLoadingSubject.onNext(false)


            switch result
            {
            case .success: self?.resetPin()
            default:  self?.output.verifiedPasswordResponseSubject.onNext(result)
            }


        })
    }

    func resetPin()
    {

        if isBusy() == true {return}

        self.output.isLoadingSubject.onNext(true)

        self.dependancy.api.updatePin(isParentalPinEnabled: false, pin: "", response: {[weak self] result in
            self?.output.isLoadingSubject.onNext(false)

            switch result
            {
            case .success: AppUserDefaults.parentalControlFlag.setValue(false)
            default: break
            }

            self?.output.verifiedPasswordResponseSubject.onNext(result)
        })
    }
    


    func isBusy() -> Bool
    {
        guard let isBusy = try? self.output.isLoadingSubject.value() else  {return false}

        return isBusy
    }


}
