//
//  ParentalPasswordAPI.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 27/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class ParentalPasswordAPI:ParentalPasswordAPIProtocol
{
    public func updatePin(isParentalPinEnabled: Bool, pin: String, response: @escaping ((DataLoader<Bool>) -> Void)) {
        UserService.ChangeParentalPin(isParentalPinEnabled: isParentalPinEnabled, pin: pin, responseHandler: response)
    }


    public init()
    {}

    public func verifyPassword(password: String, response: @escaping ((DataLoader<Bool>) -> Void)) {
        UserService.VerifyPassword(password: password, responseHandler: response)
    }


}
