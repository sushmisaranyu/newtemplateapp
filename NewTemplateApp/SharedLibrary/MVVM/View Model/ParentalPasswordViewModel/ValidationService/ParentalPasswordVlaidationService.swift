//
//  ParentalPasswordVlaidationService.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 27/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class ParentalPasswordValidationService:ParentalPasswordValidationServiceProtocol
{

    public init(){}

    public func validatePassword(_ password: String) -> PasswordValidationResult {

            if StringHelper.isNilOrEmpty(string: password) {
                return .EmptyOrNil
            }

            return .Ok
    
    }


}
