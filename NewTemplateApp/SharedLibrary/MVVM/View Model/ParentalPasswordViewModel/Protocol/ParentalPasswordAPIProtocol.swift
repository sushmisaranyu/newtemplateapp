//
//  ParentalPasswordAPIProtocol.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 27/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public protocol ParentalPasswordAPIProtocol:class
{
    func verifyPassword(password:String,response: @escaping ((DataLoader<Bool>)->Void))
    func updatePin(isParentalPinEnabled:Bool,pin:String,response: @escaping ((DataLoader<Bool>)->Void))
}


public protocol ParentalPasswordValidationServiceProtocol:class
{
    func validatePassword(_ password: String) -> PasswordValidationResult
}
