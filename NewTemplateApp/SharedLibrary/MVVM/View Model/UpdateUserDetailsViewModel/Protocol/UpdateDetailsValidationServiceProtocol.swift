//
//  UpdateDetailsValidationServiceProtocol.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 24/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public protocol UpdateDetailsValidationServiceProtocol:class
{
    func validateName(_ name: String) -> NameValidationResult
    func valiateEmail(_ email: String) -> UserNameValidationResult
    func validateMobileNo(_ mobileno: String) -> UserNameValidationResult

}
