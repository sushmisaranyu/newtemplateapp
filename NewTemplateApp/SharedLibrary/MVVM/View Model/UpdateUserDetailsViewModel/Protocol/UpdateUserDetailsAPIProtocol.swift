//
//  UpdateUserDetailsAPIProtocol.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 24/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public protocol UpdateUserDetailsAPIProtocol:class {

    func updateEmailUserDetails(name:String,lastName:String,mobileNos:String,email:String,birthDate:String,response: @escaping ((DataLoader<Bool>)->Void))

     func updateMobileUserDetails(name:String,lastName:String,mobileNos:String,email:String,birthDate:String,response: @escaping ((DataLoader<Bool>)->Void))
}


