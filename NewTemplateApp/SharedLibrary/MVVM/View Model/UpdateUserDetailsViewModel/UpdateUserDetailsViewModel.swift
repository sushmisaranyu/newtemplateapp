//
//  UpdateUserDetailsViewModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 24/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

public class UpdateUserDetailsViewModel
{
    public struct Input
    {
        let nameDriver: Driver<String>
        let mobileDriver: Driver<String>
        let emailDriver: Driver<String>
        let dateDriver:Driver<String>


        let nameSubject: BehaviorSubject<String>
        let mobileSubject: BehaviorSubject<String>
        let emailSubject:BehaviorSubject<String>
        let dateSubject:BehaviorSubject<String>

        public init(name:Driver<String>,mobile:Driver<String>,email:Driver<String>,date:Driver<String>,user:OTTUserDetails)
        {
            self.nameDriver     = name
            self.mobileDriver   = mobile
            self.emailDriver    = email
            self.dateDriver     = date

            self.nameSubject = BehaviorSubject(value: Input.getNotNilString(string: user.firstName))
            self.emailSubject = BehaviorSubject(value: Input.getNotNilString(string: StringHelper.isNilOrEmpty(string: user.email_id) == false ?  user.email_id : user.user_email_id))
            self.mobileSubject = BehaviorSubject(value: Input.getNotNilString(string: user.mobile_number))
            self.dateSubject = BehaviorSubject(value: Input.getNotNilString(string: user.birthdate))
        }

        static func getNotNilString(string:String?) -> String
        {
            guard let value = string else {return ""}

            if value.isEmpty == true {return ""}

            return value
        }
    }




    public struct Output
    {

        let validatedNameSubject: BehaviorSubject<NameValidationResult>
        let validatedEmailSubject: BehaviorSubject<UserNameValidationResult>
        let validatedMobileSubject: BehaviorSubject<UserNameValidationResult>
        let isLoadingSubject:BehaviorSubject<Bool>
        let updateResponseSubject: BehaviorSubject<DataLoader<Bool>>

        public let validatedName: Driver<NameValidationResult>
        public let validatedEmail: Driver<UserNameValidationResult>
        public let validatedMobile: Driver<UserNameValidationResult>
        public let updateResponse: Driver<DataLoader<Bool>>
        public let isLoadingDriver: Driver<Bool>

        public init() {
            validatedNameSubject    = BehaviorSubject(value: .None)
            validatedEmailSubject   = BehaviorSubject(value: .None)
            validatedMobileSubject  = BehaviorSubject(value: .None)
            isLoadingSubject        = BehaviorSubject(value: false)
            updateResponseSubject   = BehaviorSubject(value: .None)

            validatedName           = validatedNameSubject.asDriver(onErrorJustReturn: .None)
            validatedEmail          = validatedEmailSubject.asDriver(onErrorJustReturn: .None)
            validatedMobile         = validatedMobileSubject.asDriver(onErrorJustReturn: .None)
            isLoadingDriver         = isLoadingSubject.asDriver(onErrorJustReturn: false)
            updateResponse          = updateResponseSubject.asDriver(onErrorJustReturn: .None)
        }
    }

    public struct Dependancy
    {
        let api: UpdateUserDetailsAPIProtocol
        let validationService : UpdateDetailsValidationServiceProtocol

        public init(api:UpdateUserDetailsAPIProtocol,validationService:UpdateDetailsValidationServiceProtocol)
        {
            self.api = api
            self.validationService = validationService
        }

    }

    let input:Input
    public let output:Output
    let dependancy:Dependancy
    let disposeBag  = DisposeBag()

    public init(input:Input,dependancy:Dependancy)
    {
        self.input = input
        self.dependancy = dependancy
        self.output = Output()

        setupInputBehaviourSubjects()
    }


    func setupInputBehaviourSubjects()
    {
        //Setting up OnNext listener to check for changes in username
        self.input.nameDriver.distinctUntilChanged().drive(onNext: {[weak self] updatedText in
            guard let strongSelf = self  else { return }
            strongSelf.resetNameValidationState()
            strongSelf.input.nameSubject.onNext(updatedText)
        }).disposed(by: disposeBag)

        self.input.emailDriver.distinctUntilChanged().drive(onNext: {[weak self] updatedText in
            guard let strongSelf = self  else { return }
            strongSelf.resetEmailValidationState()
            strongSelf.input.emailSubject.onNext(updatedText)
        }).disposed(by: disposeBag)

        //Setting up OnNext listener to check for changes in password
        self.input.mobileDriver.distinctUntilChanged().drive(onNext: {[weak self] updatedText in
            guard let strongSelf = self  else { return }
            strongSelf.resetMobileValidationState()
            strongSelf.input.mobileSubject.onNext(updatedText)
        }).disposed(by: disposeBag)

        self.input.dateDriver.distinctUntilChanged().drive(onNext: {[weak self] updatedText in
            guard let strongSelf = self  else { return }
            strongSelf.input.dateSubject.onNext(updatedText)
        }).disposed(by: disposeBag)
    }

    func resetNameValidationState()
    {
        output.validatedNameSubject.onNext(.None)
    }

    func resetEmailValidationState()
    {
        output.validatedEmailSubject.onNext(.None)
    }

    func resetMobileValidationState()
    {
        output.validatedMobileSubject.onNext(.None)
    }

    public func validateName() -> Bool
    {
        guard let name = try? input.nameSubject.value() else { return false}
        let result = self.dependancy.validationService.validateName(name)
        self.output.validatedNameSubject.onNext(result)

        return result.isValid
    }

    public func validateMobile() -> Bool
    {
        guard let mobile = try? input.mobileSubject.value() else { return false}
        let result = self.dependancy.validationService.validateMobileNo(mobile)
        self.output.validatedMobileSubject.onNext(result)

        return result.isValid
    }

    public func validateEmail() -> Bool
    {
        guard let email = try? input.emailSubject.value() else { return false}
        let result = self.dependancy.validationService.valiateEmail(email)
        self.output.validatedEmailSubject.onNext(result)

        return result.isValid
    }


    func updateDetails(loginType:LoginType)
    {
        if loginType == .Email
        {

        }

        if loginType == .Mobile
        {

        }

    }

    public func updateDate(date:String)
    {
        input.dateSubject.onNext(date)
    }


    public func updateUserDetails(isMobile:Bool)
    {

        if isBusy() == true{return}

        guard let email = try? input.emailSubject.value() else {return}
        guard var mobile = try? input.mobileSubject.value() else {return}
        guard let name = try? input.nameSubject.value() else {return}
        guard let date = try? input.dateSubject.value() else {return}

        if isMobile == true
        {
            if validateName() == false || validateEmail() == false{
                return
            }



              self.output.isLoadingSubject.onNext(true)
            self.dependancy.api.updateMobileUserDetails(name: name, lastName: "", mobileNos:  mobile, email: email, birthDate: date, response: {[weak self] response in
                  self?.output.isLoadingSubject.onNext(false)
                self?.output.updateResponseSubject.onNext(response)

            })

        }
        else
        {
            if validateName() == false || validateMobile() == false{
                return
            }

            if AppUserDefaults.detectedRegionForApp == "US"
            {
                mobile  = "1" + mobile
            }
            self.output.isLoadingSubject.onNext(true)

            self.dependancy.api.updateEmailUserDetails(name: name, lastName: "", mobileNos: mobile, email: email, birthDate: date ,response: {[weak self] response in
                self?.output.isLoadingSubject.onNext(false)
                self?.output.updateResponseSubject.onNext(response)

            })
        }
    }





    func isBusy() -> Bool
    {
        guard let isBusy = try? self.output.isLoadingSubject.value() else  {return false}

        return isBusy
    }
}
