//
//  UpdateUserDetailsAPI.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 24/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class UpdateUserDetailsAPI:UpdateUserDetailsAPIProtocol
{
    public func updateEmailUserDetails(name: String, lastName: String, mobileNos: String, email: String, birthDate: String, response: @escaping ((DataLoader<Bool>) -> Void)) {
        UserService.UpdateEmailUserDetails(name: name, lastName: "", mobileNos: mobileNos, birthDate: birthDate, responseHandler: response)
    }

    public func updateMobileUserDetails(name: String, lastName: String, mobileNos: String, email: String, birthDate: String, response: @escaping ((DataLoader<Bool>) -> Void)) {
        UserService.UpdateMobileUserDetails(name: name, lastName: "", email: email, birthDate: birthDate, responseHandler: response)
    }

    public init(){}


}
