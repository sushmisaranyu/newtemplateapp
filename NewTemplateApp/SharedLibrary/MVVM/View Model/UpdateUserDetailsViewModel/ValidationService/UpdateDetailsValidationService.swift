//
//  UpdateDetailsValidationService.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 24/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class UpdateDetailsValidationService:UpdateDetailsValidationServiceProtocol
{

    public init() {}

    public func validateName(_ name: String) -> NameValidationResult {
        var  result = NameValidationResult.None
        if StringHelper.isNilOrEmpty(string: name.trimmingCharacters(in: .whitespacesAndNewlines))
        {
            result = .EmptyOrNil
        }
        else
        {
            result = .Ok
        }

        return result
    }

    public func valiateEmail(_ email: String) -> UserNameValidationResult {
        var  result = UserNameValidationResult.None
        if StringHelper.isNilOrEmpty(string: email)
        {
            result = .Ok
        }
        else if AppUtilities.isValidEmail(email) == false
        {
            result = .InvalidUserName
        }
        else if StringHelper.hasSpace(string: email)
        {
            result = .UserNameHasSpaces
        }
        else
        {
            result = .Ok
        }

        return result
    }

    public func validateMobileNo(_ mobileno: String) -> UserNameValidationResult {
        var  result = UserNameValidationResult.None
        if StringHelper.isNilOrEmpty(string: mobileno)
        {
            result = .Ok
        }
        else if AppUtilities.isMobile(string: mobileno) == false || mobileno.count != 10
        {
            result = .InvalidUserName
        }
        else if StringHelper.hasSpace(string: mobileno)
        {
            result = .UserNameHasSpaces
        }
        else
        {
            result = .Ok
        }

        return result
    }

    
}
