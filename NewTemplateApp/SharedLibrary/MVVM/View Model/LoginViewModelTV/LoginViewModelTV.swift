//
//  LoginViewModelTV.swift
//  SharedLibraryAppleTV
//
//  Created by MAC on 28/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

public class LoginViewModelTV
{
   
    public var token : String?
    public var timer = Timer()
    
    public struct Output
    {
       
        public let isLoading:Driver<Bool>
        
        public let generateCodeResponse:Driver<DataLoader<String>>
        public let verifyTokenResponse:Driver<DataLoader<Bool>>
        public let getUserDetailsResponse:Driver<DataLoader<Bool>>

      
        let isLoadingSubject:BehaviorSubject<Bool>
        
        let generateCodeResponseSubject:BehaviorSubject<DataLoader<String>>
        let verifyTokenResponseSubject:BehaviorSubject<DataLoader<Bool>>
        let getUserDetailsResponseSubject:BehaviorSubject<DataLoader<Bool>>

        
        public init()
        {
         
            isLoadingSubject                 = BehaviorSubject(value: false)
            generateCodeResponseSubject      = BehaviorSubject(value: .None)
            verifyTokenResponseSubject       = BehaviorSubject(value: .None)
            getUserDetailsResponseSubject       = BehaviorSubject(value: .None)

            isLoading                        = isLoadingSubject.asDriver(onErrorJustReturn: false)
            generateCodeResponse             = generateCodeResponseSubject.asDriver(onErrorJustReturn: .None)
            verifyTokenResponse              = verifyTokenResponseSubject.asDriver(onErrorJustReturn: .None)
            getUserDetailsResponse           = getUserDetailsResponseSubject.asDriver(onErrorJustReturn: .None)

        }
        
        
    }
    
    public struct Dependancy
    {
        let api:LoginAPIProtocolTV
        
        public init(api:LoginAPIProtocolTV)
        {
            self.api = api
            
        }
    }
    
    public let output:Output
    let dependancy:Dependancy
    let disposeBag = DisposeBag()
    
    public init(dependancy:Dependancy)
    {
        self.output = Output()
        self.dependancy = dependancy
        
    }
    
   
    public func generateLoginCode()
    {
        
        if self.isBusy() == true {return}
        
        self.output.isLoadingSubject.onNext(true)
        
        self.dependancy.api.GenerateLoginCode { [weak self] (response) in
            self?.output.isLoadingSubject.onNext(false)
            
            switch response {
                case .success(let response): self?.token = response
                //start timer and call verify
                self?.timer = Timer.scheduledTimer(timeInterval: 10 , target: self,   selector: (#selector(self!.verifyToken)), userInfo: nil, repeats: true)
                
                default: break
            }
            
            self?.output.generateCodeResponseSubject.onNext(response)
        }
      
    }
    
    @objc public func verifyToken()
    {
        
        if self.isBusy() == true {return}
        
        self.output.isLoadingSubject.onNext(true)
        
        self.dependancy.api.VerifyLoginToken(token: self.token!) {[weak self] (response) in
         
            self?.output.isLoadingSubject.onNext(false)
           
            switch response {
                case .success(let response):
            
                    if response == true {
                       
                        self?.invalidateTimer()

                        self?.getDetails()
                    }
                
                default: break
            }
            self?.output.verifyTokenResponseSubject.onNext(response)
        }
        
        
    }
    
     public func getDetails()
    {
        
        if self.isBusy() == true {return}
        
        self.output.isLoadingSubject.onNext(true)
        
        self.dependancy.api.GetUserDetails { [weak self] (response) in
            self?.output.isLoadingSubject.onNext(false)
            
            switch response {
                
            case .success(let response):
                break
            case .serverErrorWithCode(let code, let message):
                
                self?.getDetails()
                
            case .connectivityError:
                
                self?.getDetails()
                
            case .None: break
                
            }
            self?.output.getUserDetailsResponseSubject.onNext(response)
        }
        
        
    }
    func isBusy() -> Bool
    {
        guard let isBusy = try? self.output.isLoadingSubject.value() else  {return false}
        
        return isBusy
    }
    
    public func invalidateTimer(){
        
        timer.invalidate()
    }
}
