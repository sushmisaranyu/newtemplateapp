//
//  LoginAPITV.swift
//  SharedLibraryAppleTV
//
//  Created by MAC on 28/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

public protocol LoginAPIProtocolTV:class
{
    func GenerateLoginCode(response: @escaping ((DataLoader<String>)->Void))
    func VerifyLoginToken(token : String, response: @escaping ((DataLoader<Bool>)->Void))
    func GetUserDetails(response: @escaping ((DataLoader<Bool>)->Void))
}


public class LoginAPITV :LoginAPIProtocolTV
{
    public init()
    {
        
    }
    public func GetUserDetails(response: @escaping ((DataLoader<Bool>)->Void)){
        UserService.getUserDetailsTV(responseHandler: response)
    }
    
    public func GenerateLoginCode(response: @escaping ((DataLoader<String>)->Void)){
        UserService.GenerateLoginCode(responseHandler: response)
    }
    
    public func VerifyLoginToken(token : String, response: @escaping ((DataLoader<Bool>)->Void)){
   
        UserService.VerifyLoginToken(token: token, responseHandler: response)

    }
}


