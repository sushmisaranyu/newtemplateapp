//
//  LinearAndLiveAPI.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 25/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class LinearAndLiveAPI: LinearAndLiveAPIProtocol
{
    public func getItemDetailsState(contentID: String, catalogId: String, category: String, response: @escaping ((DataLoader<DetailsScreenStateModel>) -> Void)) {
         UserService.GETConsolidatedItemState(catalogId: catalogId, contentId: contentID, category: category, responseHandler: response)
    }


    public init(){}

    public func getLinearAndLiveDetails(contentId: String, catalogId: String, response: @escaping ((DataLoader<LinearAndLiveModel>) -> Void)) {
        CatalogService.getForListofPrograms(catalogID: catalogId, contentId: contentId, responseHandler: response)

    }


}
