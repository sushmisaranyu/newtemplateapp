//
//  LinearAndLiveApiProtocol.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 25/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public protocol LinearAndLiveAPIProtocol:class
{
    func getLinearAndLiveDetails(contentId:String,catalogId:String,response: @escaping ((DataLoader<LinearAndLiveModel>)->Void))


     func getItemDetailsState(contentID:String,catalogId:String,category:String ,response: @escaping ((DataLoader<DetailsScreenStateModel>)->Void) )

}
