//
//  LinearLiveViewModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 25/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa


public enum LinearLiveContentType
{
    case live
    case linear

    public var isLive:Bool
    {
        switch self
        {
        case .linear : return false
        case .live : return true
        }
    }
}


public class LinearLiveViewModel
{

    var selectItemDetail:DetailsModel?

    public struct Output
    {
        init()
        {
            selectedItemSubject = PublishSubject()
            selectedItemDriver = selectedItemSubject.asDriver(onErrorJustReturn: nil)

            programListSubject = BehaviorSubject(value: [])
            programListDriver = programListSubject.asDriver(onErrorJustReturn: [])

            isLoadingSubject = BehaviorSubject(value: false)
            isLoadingDriver = isLoadingSubject.asDriver(onErrorJustReturn: false)

            viewStateErrorSubject = BehaviorSubject(value:ViewStateError.NoError)
            viewStateErrorDriver = viewStateErrorSubject.asDriver(onErrorJustReturn: ViewStateError.NoError)
        }

        public let selectedItemDriver: Driver<DetailsModel?>
        public let programListDriver:Driver<[OTTVideoItem]>

        public let isLoadingDriver: Driver<Bool>
        public let viewStateErrorDriver: Driver<ViewStateError>

        let selectedItemSubject: PublishSubject<DetailsModel?>
        let programListSubject:BehaviorSubject<[OTTVideoItem]>
        let isLoadingSubject:BehaviorSubject<Bool>
        let viewStateErrorSubject:BehaviorSubject<ViewStateError>


    }

    public struct Dependancy
    {
        let api:LinearAndLiveAPIProtocol

        public init(api:LinearAndLiveAPIProtocol)
        {
            self.api = api
        }
    }


    public let output:Output
    public let dependancy:Dependancy

    let contentId:String
    let catalogId:String

    let contentType:LinearLiveContentType


    public init(contentId:String,catalogId:String,contentType:LinearLiveContentType ,dependancy:Dependancy)
    {

        self.contentType = contentType
        self.contentId = contentId
        self.catalogId = catalogId

        self.output = Output()
        self.dependancy = dependancy
    }

    public func getDataFromServer(silent:Bool = false)
    {
        guard let isBusy = try? output.isLoadingSubject.value() else {return}
        if isBusy == true {return}

        if silent == false
        {
            output.isLoadingSubject.onNext(true)
        }
        output.viewStateErrorSubject.onNext(.NoError)

        self.dependancy.api.getLinearAndLiveDetails(contentId: self.contentId, catalogId: self.catalogId){[weak self] (response) in
            guard let strongSelf = self else {return}
            strongSelf.output.isLoadingSubject.onNext(false)
            switch response
            {
            //TODO: No item check
            case .success(let response):

                if response.item == nil
                {
                    strongSelf.output.viewStateErrorSubject.onNext(ViewStateError.NoData);
                }

                strongSelf.getState(item: response.item)

              //  strongSelf.output.selectedItemSubject.onNext(response.item)
                if let list = response.list
                {
                    strongSelf.output.programListSubject.onNext(list)
                }
            case .connectivityError:
                if silent == false
                {
                    strongSelf.output.viewStateErrorSubject.onNext(ViewStateError.InternetConnectivityError);
                }
            case .serverErrorWithCode(let code, let message):
                if silent == false
                {
                    strongSelf.output.viewStateErrorSubject.onNext(ViewStateError.NoData)
                }
                break
            default: break
            }
        }
    }


    public func getState(item:OTTVideoItem?)
    {
        self.selectItemDetail = nil
        guard let tItem = item else {return}
        selectItemDetail = DetailsModel(item: tItem)
        getDetailsStateModelFor(item: tItem)

    }

    public func updateMovie(item:OTTVideoItem)
    {
      //  self.movie = item
        
        getState(item: item)
      
       // self.output.selectedItemSubject.onNext(item)
    }

    var lastProgramListUpdate = 0.0
    public func updateProgramList()
    {
        guard let list = try? self.output.programListSubject.value() else {return}

        let filteredList = list.filter({self.isValidProgram(item: $0)})

        if filteredList.count > 0 {
            self.output.programListSubject.onNext(filteredList)
        }

        if filteredList.count < 5 && Date().timeIntervalSince1970 - lastProgramListUpdate > (15 * 60)
        {
            self.lastProgramListUpdate = Date().timeIntervalSince1970 
            self.getDataFromServer(silent: true)
        }
    }

    public func isValidProgram(item:OTTVideoItem) -> Bool
    {
        guard let itemStopTime = AppUtilities.getLocalDateForInterval(interval: item.stop_time_uts) else {return false}

        if itemStopTime.timeIntervalSince1970 > Date().timeIntervalSince1970 {
            return true
        }

        return false

    }


    public func showError()
    {
        self.output.viewStateErrorSubject.onNext(ViewStateError.InternetConnectivityError);
    }

    func getDetailsStateModelFor(item:OTTVideoItem)
    {

        guard let content = item.content_id else {showError(); return;}
        guard let catalog = item.catalog_object?.listId else {showError(); return;}
        guard let cat     = item.catalog_object?.plan_category_type  else {showError(); return;}

        guard let isBusy = try? output.isLoadingSubject.value() else {return}
        if isBusy == true {return}


        if OTTUser.hasLoggedInUser() == false
        {
            guard let itemDetails = self.selectItemDetail else {return}
            self.output.selectedItemSubject.onNext(itemDetails)
            return
        }

        output.isLoadingSubject.onNext(true)
        output.viewStateErrorSubject.onNext(.NoError)


        self.dependancy.api.getItemDetailsState(contentID: content, catalogId: catalog, category: cat, response: { [weak self] response in
            guard let strongSelf = self else {return}
            strongSelf.output.isLoadingSubject.onNext(false)
            switch response
            {
            //TODO: No item check
            case .success(let response):
                strongSelf.selectItemDetail?.setItemState(state: response)

                guard let item =  strongSelf.selectItemDetail else {return}
                strongSelf.output.selectedItemSubject.onNext(item)

            case .connectivityError: strongSelf.output.viewStateErrorSubject.onNext(ViewStateError.InternetConnectivityError);
            case .serverErrorWithCode(let code, let message): strongSelf.output.viewStateErrorSubject.onNext(ViewStateError.ErrorFromServer(title: "Opps", message: "Some thing went wrong")); break
            default: break
            }

        })
    }

    public func resetItemState()
    {
        self.selectItemDetail?.itemState = nil
    }



}
