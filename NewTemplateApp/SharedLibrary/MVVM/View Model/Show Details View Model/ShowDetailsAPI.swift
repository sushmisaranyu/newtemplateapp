//
//  ShowDetailsAPI.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 17/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class ShowDetailsAPI:ShowDetailsAPIProtocol
{
    public init()
    {
        
    }

    public func getShowItem(contentId: String, catalogId: String, response: @escaping ((DataLoader<OTTVideoItem>) -> Void)) {
        CatalogService.getVideoItem(contentId: contentId, catalogId: catalogId, responseHandler: response)
    }

    public func getEpisodeItem(contentId: String, showId: String, response: @escaping ((DataLoader<OTTVideoItem>) -> Void)) {
        CatalogService.getEpisodeItem(contentId: contentId, subcategory_ID: nil, showID: showId, responseHandler: response)

      
    }

    public func getItemDetailsState(contentID: String, catalogId: String, category: String, response: @escaping ((DataLoader<DetailsScreenStateModel>) -> Void)) {
        UserService.GETConsolidatedItemState(catalogId: catalogId, contentId: contentID, category: category, responseHandler: response)
    }


}
