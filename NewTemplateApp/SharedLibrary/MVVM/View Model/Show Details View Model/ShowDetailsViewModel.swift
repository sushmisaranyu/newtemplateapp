//
//  ShowDetailsViewModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 07/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//


import Foundation
import RxSwift
import RxCocoa


public enum ContentType
{
    case show(show:DetailsModel)
    case episode(episode:DetailsModel,forcePlay:Bool)
    case noData
}

public class ShowDetailsViewModel
{
    var showId:String
    var catalogId:String
    var episodeId:String?
    var isShow:Bool = false
    var isUpdatedFromStaticData:Bool = false

    public var show:OTTVideoItem?

    var selectedEpisode:OTTVideoItem?
    var selectedItemDetail:DetailsModel?


    public struct Output
    {
        init()
        {
            selectedItemSubject = PublishSubject()
            selectedItemDriver = selectedItemSubject.asDriver(onErrorJustReturn: .noData)

            isLoadingSubject = BehaviorSubject(value: false)
            isLoadingDriver = isLoadingSubject.asDriver(onErrorJustReturn: false)

            viewStateErrorSubject = BehaviorSubject(value:ViewStateError.NoError)
            viewStateErrorDriver = viewStateErrorSubject.asDriver(onErrorJustReturn: ViewStateError.NoError)
        }

        public let selectedItemDriver: Driver<ContentType>

        public let isLoadingDriver: Driver<Bool>
        public let viewStateErrorDriver: Driver<ViewStateError>

        let selectedItemSubject: PublishSubject<ContentType>

        let isLoadingSubject:BehaviorSubject<Bool>
        let viewStateErrorSubject:BehaviorSubject<ViewStateError>


    }

    public struct Dependancy
    {
        let api:ShowDetailsAPIProtocol

        public init(api:ShowDetailsAPIProtocol)
        {
            self.api = api
        }
    }


    public let output:Output
    public let dependancy:Dependancy


    public init(showId:String,episodeID:String?,catalogId:String,dependancy:Dependancy)
    {
        self.showId = showId
        self.catalogId = catalogId

        if StringHelper.isNilOrEmpty(string: episodeID) == false
        {
            self.isShow = false
            self.episodeId = episodeID
        }
        else
        {
            self.isShow = true
        }

        self.output = Output()
        self.dependancy = dependancy
    }


    public func getDataFromServer()
    {
        if isUpdatedFromStaticData == false
        {
             getShowDataFromServer()
        }
        else
        {
            guard let item = selectedItemDetail?.mediaitem else {return}
            self.updateSelectedEpisode(item: item, forcePlay: false)
        }

    }


    func getShowDataFromServer() {
        guard let isBusy = try? output.isLoadingSubject.value() else {return}
        if isBusy == true {return}

        if self.show != nil
        {
            self.getEpisdodeIfNeeded(show:self.show!)
        }

        output.isLoadingSubject.onNext(true)
        output.viewStateErrorSubject.onNext(.NoError)


        self.dependancy.api.getShowItem(contentId: self.showId, catalogId: self.catalogId) {[weak self] (response) in
            guard let strongSelf = self else {return}
            strongSelf.output.isLoadingSubject.onNext(false)
            switch response
            {
            //TODO: No item check
            case .success(let response): strongSelf.getEpisdodeIfNeeded(show:response)
            case .connectivityError: strongSelf.output.viewStateErrorSubject.onNext(ViewStateError.InternetConnectivityError);
            case .serverErrorWithCode(let code, let message): strongSelf.output.viewStateErrorSubject.onNext(ViewStateError.ErrorFromServer(title: "Opps", message: "Some thing went wrong")); break
            default: break
            }
        }
    }

    func getEpisdodeIfNeeded(show:OTTVideoItem)
    {
        self.show = show


        if self.isShow == true
        {
            self.selectedItemDetail = DetailsModel(item: show)
            self.getDetailsStateModelFor(item: show, showType: true, forcePlay: false)
            return
        }
        else
        {
            self.getEpisodeFromServer()
        }
    }

    public func getEpisodeFromServer() {
        guard let isBusy = try? output.isLoadingSubject.value() else {return}
        if isBusy == true {return}

        guard let episodeID = self.episodeId
            else
        {
            output.viewStateErrorSubject.onNext(ViewStateError.ErrorFromServer(title: "Opps", message: "Something went wrong"))
            return
        }

        //Check if media item has been downloaded
        if selectedItemDetail?.mediaitem != nil
        {
            //Get item state if already downloaded Item
            self.getDetailsStateModelFor(item: (selectedItemDetail?.mediaitem)!, showType: false)
            return
        }


        output.isLoadingSubject.onNext(true)
        output.viewStateErrorSubject.onNext(.NoError)

        self.dependancy.api.getEpisodeItem(contentId: episodeID, showId: self.showId) { [weak self] (response) in
            guard let strongSelf = self else {return}

            strongSelf.output.isLoadingSubject.onNext(false)
            switch response
            {
            //TODO: No item check
            case .success(let response):
                strongSelf.updateSelectedEpisode(item: response)
            case .connectivityError: strongSelf.output.viewStateErrorSubject.onNext(ViewStateError.InternetConnectivityError);
            case .serverErrorWithCode(let code, let message): strongSelf.output.viewStateErrorSubject.onNext(ViewStateError.ErrorFromServer(title: "Opps", message: "Some thing went wrong")); break
            default: break
            }
        }
    }



    public func updateSelectedEpisode(item:OTTVideoItem,forcePlay:Bool = false)
    {
        if item == self.selectedEpisode && self.selectedItemDetail?.itemState != nil {return}
        self.isUpdatedFromStaticData = true
        self.selectedEpisode = item
        self.isShow = false
        self.selectedItemDetail = DetailsModel(item: item)
        self.getDetailsStateModelFor(item: item, showType: false, forcePlay: forcePlay)


    }

    public func resetItemState()
    {
        self.selectedItemDetail?.itemState = nil
    }

    public func showError()
    {
        self.output.viewStateErrorSubject.onNext(ViewStateError.InternetConnectivityError);
    }

    func getDetailsStateModelFor(item:OTTVideoItem,showType:Bool, forcePlay:Bool = false)
    {


        if OTTUser.hasLoggedInUser() == false
        {
            guard let itemDetails = self.selectedItemDetail else {return}
            if self.isShow == true
            {
                self.output.selectedItemSubject.onNext(ContentType.show(show: itemDetails))
            }
            else
            {
                self.output.selectedItemSubject.onNext(ContentType.episode(episode:  itemDetails, forcePlay: forcePlay))
            }

            return

        }

        guard let content = item.content_id else {showError(); return;}
        guard let catalog = item.catalog_id else {showError(); return;}
        guard let cat     = item.catalog_object?.layout_scheme  else {showError(); return;}

        guard let isBusy = try? output.isLoadingSubject.value() else {return}
        if isBusy == true {return}

        output.isLoadingSubject.onNext(true)
        output.viewStateErrorSubject.onNext(.NoError)


        self.dependancy.api.getItemDetailsState(contentID: content, catalogId: catalog, category: cat, response: { [weak self] response in
            guard let strongSelf = self else {return}
            strongSelf.output.isLoadingSubject.onNext(false)
            switch response
            {
            //TODO: No item check
            case .success(let response):
                strongSelf.selectedItemDetail?.setItemState(state: response)

                guard let itemDetails = strongSelf.selectedItemDetail else {return}

                if strongSelf.isShow == true
                {
                     strongSelf.output.selectedItemSubject.onNext(ContentType.show(show: itemDetails))
                }
                else
                {
                    strongSelf.output.selectedItemSubject.onNext(ContentType.episode(episode:  itemDetails, forcePlay: forcePlay))
                }

            case .connectivityError: strongSelf.showError();
            case .serverErrorWithCode(let code, let message): strongSelf.showError();  break
            default: break
            }

        })
    }
}

