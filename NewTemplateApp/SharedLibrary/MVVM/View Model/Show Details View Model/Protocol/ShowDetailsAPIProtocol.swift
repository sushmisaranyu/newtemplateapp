//
//  ShowDetailsAPIProtocol.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 07/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public protocol ShowDetailsAPIProtocol
{
    func getShowItem(contentId:String,catalogId:String,response: @escaping ((DataLoader<OTTVideoItem>)->Void))
    func getEpisodeItem(contentId:String,showId:String,response: @escaping ((DataLoader<OTTVideoItem>)->Void))
    func getItemDetailsState(contentID:String,catalogId:String,category:String ,response: @escaping ((DataLoader<DetailsScreenStateModel>)->Void) )
}
