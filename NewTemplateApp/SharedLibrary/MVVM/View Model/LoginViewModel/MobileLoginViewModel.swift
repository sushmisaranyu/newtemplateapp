//
//  MobileLoginViewModel.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 01/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//


import Foundation
import RxSwift
import RxCocoa


public class MobileLoginViewModel:BaseLoginViewModel
{
    // Is signup button enabled

    var countryCode:CountryCode

    public init(input: Input, countryCode:CountryCode)
    {
        self.countryCode = countryCode
        let mobileDependency = Dependancy(api: MobileLoginAPI(), validationService: MobileLoginValidationService())
        super.init(input: input, dependency: mobileDependency)
    }



    public override func login()
    {
        if self.isBusy() == true {return}

        if validateUserName() == false ||  validatePassword() == false { return }

        guard let selectedCountryCode = countryCode.callingCode else { return }

        guard let username = try? self.input.usernameSubject.value() else {return}
        guard let password = try? self.input.passwordSubject.value() else {return}

        self.output.isLoadingSubject.onNext(true)

        self.dependency.api.signIn(username: selectedCountryCode + username, password: password) {[weak self] (response) in
            guard let strongSelf = self else {return}

            strongSelf.output.isLoadingSubject.onNext(false)

            strongSelf.output.signInResponseSubject.onNext(response)
        }

    }





}

