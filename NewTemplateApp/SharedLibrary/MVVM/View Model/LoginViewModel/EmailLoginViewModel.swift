//
//  EmailLoginViewModel.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 03/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa


public class EmailLoginViewModel:BaseLoginViewModel
{

    public init(input: Input)
    {
        let mobileDependency = Dependancy(api: EmailLoginAPI(), validationService: LoginValidationService())
        super.init(input: input, dependency: mobileDependency)
    }



    public override func login()
    {
        if self.isBusy() == true {return}

        if validateUserName() == false ||  validatePassword() == false { return }

        guard let username = try? self.input.usernameSubject.value() else {return}
        guard let password = try? self.input.passwordSubject.value() else {return}

        self.output.isLoadingSubject.onNext(true)

        self.dependency.api.signIn(username: username, password: password) {[weak self] (response) in
            guard let strongSelf = self else {return}

            strongSelf.output.isLoadingSubject.onNext(false)

            strongSelf.output.signInResponseSubject.onNext(response)
        }

    }

}
