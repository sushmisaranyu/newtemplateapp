//
//  LoginViewModelProtocols.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 17/08/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift

public enum UserNameValidationResult
{
    case InvalidUserName
    case UserNameIsNilOrEmpty
    case UserNameHasSpaces
    case Ok
    case None

    var isValid:Bool
    {
        if self == .Ok
        {
            return true
        }

        return false
    }
}

public enum PasswordValidationResult
{
    case EmptyOrNil
    case SizeLessThanMiniumCharCount
    case HasSpace
    case Ok
    case None

    var isValid:Bool
    {
        if self == .Ok
        {
            return true
        }

        return false
    }
}


public protocol LoginAPIProtocol:class {
    func signIn(username:String,password:String, response: @escaping ((DataLoader<Bool>)->Void))
}

public protocol LoginValidationServiceProtocol:class
{

//    func valiateUserName(_ userName: String) -> Observable<EmailValidationResult>
//    func validatePassword(_ password: String) -> Observable<PasswordValidationResult>

    func valiateUserName(_ userName: String) -> UserNameValidationResult
    func validatePassword(_ password: String) -> PasswordValidationResult

}
