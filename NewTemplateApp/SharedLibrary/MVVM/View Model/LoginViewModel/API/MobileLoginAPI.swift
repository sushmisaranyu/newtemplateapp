//
//  MobileLoginAPI.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 03/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class MobileLoginAPI:LoginAPIProtocol
{
    public init(){}

    public func signIn(username: String, password: String, response: @escaping ((DataLoader<Bool>) -> Void)) {
        UserService.MobileSignIn(mobile: username, password: password, responseHandler: response)
    }
}

