//
//  LoginAPI.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 17/08/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift


public class EmailLoginAPI:LoginAPIProtocol
{
    public init(){}
    
    public func signIn(username: String, password: String, response: @escaping ((DataLoader<Bool>) -> Void)) {
        UserService.EmailSignIn(emailId: username, password: password, responseHandler: response)
    }
}
