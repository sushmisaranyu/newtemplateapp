//
//  LoginValidationService.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 17/08/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import  RxSwift

public class LoginValidationService:LoginValidationServiceProtocol
{
    public func valiateUserName(_ userName: String) -> UserNameValidationResult {
        var  result = UserNameValidationResult.None
        if StringHelper.isNilOrEmpty(string: userName)
        {
            result = .UserNameIsNilOrEmpty
        }
        else if AppUtilities.isValidEmail(userName) == false
        {
            result = .InvalidUserName
        }
        else if StringHelper.hasSpace(string: userName)
        {
            result = .UserNameHasSpaces
        }
        else
        {
            result = .Ok
        }

        return result
    }

    public func validatePassword(_ password: String) -> PasswordValidationResult {
        var  result = PasswordValidationResult.None
        if StringHelper.isNilOrEmpty(string: password)
        {
            result = .EmptyOrNil
        }
        else
        {
            result = .Ok
        }
        return result
    }


    public init()
    {

    }

    public func valiateUserName(_ userName: String) -> Observable<UserNameValidationResult> {
        return Observable<UserNameValidationResult>.create({ [weak self] (observer) -> Disposable in

            guard let strongSelf = self  else {  observer.onCompleted()
                return Disposables.create()

            }

            var  result = UserNameValidationResult.None
            result = strongSelf.valiateUserName(userName)
            observer.onNext(result)
            observer.onCompleted()
            return Disposables.create()
        })
    }


    public func validatePassword(_ password: String) -> Observable<PasswordValidationResult> {

        return Observable<PasswordValidationResult>.create({ [weak self] (observer) -> Disposable in

            guard let strongSelf = self  else {  observer.onCompleted()
                return Disposables.create()

            }

            var  result = PasswordValidationResult.None
            result = strongSelf.validatePassword(password)

            observer.onNext(result)
            observer.onCompleted()
            return Disposables.create()
        })
    }



}
