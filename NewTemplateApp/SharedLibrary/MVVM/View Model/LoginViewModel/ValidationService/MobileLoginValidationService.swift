//
//  MobileLoginValidationService.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 03/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


import Foundation
import  RxSwift

public class  MobileLoginValidationService:LoginValidationService
{
    public override func valiateUserName(_ userName: String) -> UserNameValidationResult {
        var  result = UserNameValidationResult.None
        if StringHelper.isNilOrEmpty(string: userName)
        {
            result = .UserNameIsNilOrEmpty
        }
        else if AppUtilities.isMobile(string: userName) == false || userName.count < 10 || userName.count > 10
        {
            result = .InvalidUserName
        }
        else if StringHelper.hasSpace(string: userName)
        {
            result = .UserNameHasSpaces
        }
        else
        {
            result = .Ok
        }

        return result
    }

}
