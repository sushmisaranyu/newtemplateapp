//
//  LoginViewModel2.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 17/08/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa


public class BaseLoginViewModel:ViewModelProtocol
{
    var disposeBag = DisposeBag()

    public  struct Input
    {
        public init(username:Driver<String>, password: Driver<String> ) {
            usernameDriver = username
            passwordDriver = password

            //Initlize Behaviour subjects to listen to change in login text fields
            usernameSubject = BehaviorSubject(value: "")
            passwordSubject = BehaviorSubject(value: "")
        }

        let usernameDriver: Driver<String>
        let passwordDriver: Driver<String>

        let usernameSubject: BehaviorSubject<String>
        let passwordSubject: BehaviorSubject<String>

    }

    public struct Output {

        init(input:Input, dependancy:Dependancy)
        {
            self.validatedUsernameSubject = BehaviorSubject(value: .None)
            self.validatedPasswordSubject = BehaviorSubject(value: .None)
            self.isLoadingSubject         = BehaviorSubject(value: false)
            self.signInResponseSubject    = BehaviorSubject(value: DataLoader.None)

            //public Driver Setup
            self.validatedUsername  = validatedUsernameSubject.asDriver(onErrorJustReturn: .None)
            self.validatedPassword  = validatedPasswordSubject.asDriver(onErrorJustReturn: .None)
            self.isLoading          = isLoadingSubject.asDriver(onErrorJustReturn: false)
            self.signInResponse     = signInResponseSubject.asDriver(onErrorJustReturn: .None)

            self.signinEnabled = Driver.combineLatest(
                input.usernameDriver,
                input.passwordDriver,
                isLoading
            )   { username, password, isLoading in
                    let usernameResult = dependancy.validationService.valiateUserName(username)
                    let passwordResult = dependancy.validationService.validatePassword(password)

                    return usernameResult.isValid && passwordResult.isValid && !isLoading

                }
                .distinctUntilChanged()
        }

        let validatedUsernameSubject: BehaviorSubject<UserNameValidationResult>
        let validatedPasswordSubject: BehaviorSubject<PasswordValidationResult>
        let isLoadingSubject:BehaviorSubject<Bool>
        let signInResponseSubject: BehaviorSubject<DataLoader<Bool>>


        //let activityIndicator:ActivityIndicator
        public let validatedUsername: Driver<UserNameValidationResult>
        public let validatedPassword: Driver<PasswordValidationResult>
        public let signinEnabled: Driver<Bool>

        // Is signing process in progress
        public let isLoading: Driver<Bool>

        // Has user signed in
        public let signInResponse: Driver<DataLoader<Bool>>

    }

    public struct Dependancy
    {
        public init(api:LoginAPIProtocol,validationService: LoginValidationServiceProtocol)
        {
            self.api = api
            self.validationService = validationService
        }

        let api: LoginAPIProtocol
        let validationService: LoginValidationServiceProtocol
    }

    let input: Input
    public let output: Output
    let  dependency:Dependancy

    // Is signup button enabled

    public init(input: Input,
                dependency: Dependancy)
    {

        self.dependency = dependency
        self.input = input
        self.output = Output(input:input, dependancy: dependency)
        self.setupInputBehaviourSubjects()
    }

    func setupInputBehaviourSubjects()
    {
        //Setting up OnNext listener to check for changes in username
        self.input.usernameDriver.distinctUntilChanged().drive(onNext: {[weak self] updatedText in
            guard let strongSelf = self  else { return }
            strongSelf.resetUserNameValidationState()
            strongSelf.input.usernameSubject.onNext(updatedText)
        }).disposed(by: disposeBag)


        //Setting up OnNext listener to check for changes in password
        self.input.passwordDriver.distinctUntilChanged().drive(onNext: {[weak self] updatedText in
            guard let strongSelf = self  else { return }
            strongSelf.resetPasswordValidationState()
            strongSelf.input.passwordSubject.onNext(updatedText)
        }).disposed(by: disposeBag)
    }


    public func validateUserName() -> Bool
    {
        guard let email = try? input.usernameSubject.value() else { return false}
        let result:UserNameValidationResult = self.dependency.validationService.valiateUserName(email)
        self.output.validatedUsernameSubject.onNext(result)

        return result.isValid
    }

    public func validatePassword() -> Bool
    {
        guard let password = try? input.passwordSubject.value() else { return false}
        let result:PasswordValidationResult = self.dependency.validationService.validatePassword(password)
        self.output.validatedPasswordSubject.onNext(result)

        return result.isValid
    }

    func resetUserNameValidationState()
    {
        self.output.validatedUsernameSubject.onNext(.None)
    }

    func resetPasswordValidationState()
    {
        self.output.validatedPasswordSubject.onNext(.None)
    }

    func isBusy() -> Bool
    {
        guard let isBusy = try? self.output.isLoadingSubject.value() else  {return false}

        return isBusy
    }

    public func login()
    {
        if self.isBusy() == true {return}

        if validateUserName() == false ||  validatePassword() == false { return }

        guard let username = try? self.input.usernameSubject.value() else {return}
        guard let password = try? self.input.passwordSubject.value() else {return}

        self.output.isLoadingSubject.onNext(true)

        self.dependency.api.signIn(username: username, password: password) {[weak self] (response) in
            guard let strongSelf = self else {return}

            strongSelf.output.isLoadingSubject.onNext(false)

            strongSelf.output.signInResponseSubject.onNext(response)
        }

    }


    public func resetErrorMessageState()
    {
        self.output.validatedUsernameSubject.onNext(.None)
        self.output.validatedPasswordSubject.onNext(.None)
    }

}

