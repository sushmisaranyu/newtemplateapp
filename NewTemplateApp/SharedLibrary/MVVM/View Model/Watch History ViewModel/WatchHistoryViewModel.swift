//
//  WatchHistoryViewModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 21/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

public class WatchHistoryViewModel
{

    public static let watchHistoryPingInterval = 30.0
    public static let watchHistoryStartTime = 30.0

    var selectedItem:DetailsModel?
    var currentWatchTime:Double = 0.0
    public var hasUserWatchedSomething:Bool = false

    public var lastRecordedWatchHistory:Double = 0.0

    var lastWatchHistoryAPIPingTime:Double = 0.0

    public struct Output
    {
        public init()
        {
            watchHistoryProgressSubject = BehaviorSubject(value: 0.0)
            watchHistoryProgressDriver = watchHistoryProgressSubject.asDriver(onErrorJustReturn: 0.0)
        }

        public var watchHistoryProgressDriver:Driver<Double>
        var watchHistoryProgressSubject:BehaviorSubject<Double>

    }

    public struct Dependancy
    {
        public init(api:WatchHistoryAPIProtocol)
        {
            self.api = api
        }

        var api:WatchHistoryAPIProtocol
    }

    var output:Output
    var dependancy:Dependancy

    public init(api:WatchHistoryAPIProtocol)
    {
        self.output = Output()
        self.dependancy = Dependancy(api: api)
    }

    public func setupViewModelData(item:DetailsModel)
    {
        resetWatchHistoryTracker()
        self.selectedItem = item
    }

    public func getItemWatchHistory()
    {

    }

    public func resetWatchHistoryTracker()
    {
        if selectedItem != nil  && lastRecordedWatchHistory > 0.0
        {
            self.updateWatchHistory()
        }

        self.currentWatchTime = 0.0
        self.lastRecordedWatchHistory = 0.0
        self.lastWatchHistoryAPIPingTime = 0.0
        self.hasUserWatchedSomething = false


    }

    public func userHasWatchedSomething()
    {
        hasUserWatchedSomething = true
    }


    public func updateWatchHistoryTime(currentPlaybacktime:Double , isForced:Bool)
    {

        if OTTUser.hasLoggedInUser() == false {return}

        self.currentWatchTime = currentPlaybacktime

        var diff = self.currentWatchTime - self.lastRecordedWatchHistory

        if diff < 0
        {
            diff = -diff
        }

     //   AppUtilities.printLog(message: "Watch History \(currentWatchTime) - \(lastRecordedWatchHistory) : \(diff)")

        if diff <  WatchHistoryViewModel.watchHistoryPingInterval && isForced == false
        {
            return
        }

        if self.currentWatchTime == self.lastRecordedWatchHistory
        {
            return
        }

        let currentTime = Date().timeIntervalSince1970

//        if currentTime - lastWatchHistoryAPIPingTime > WatchHistoryViewModel.watchHistoryPingInterval
//        {
//            return
//        }

        self.lastWatchHistoryAPIPingTime = currentTime
        self.lastRecordedWatchHistory = currentWatchTime
        self.updateWatchHistory()

    }


    func updateWatchHistory()
    {
        guard let contentId = selectedItem?.mediaitem.content_id else {return}
        guard let catlogId  = selectedItem?.mediaitem.catalog_id else {return}

        self.dependancy.api.updateWatchHistory(contentId: contentId, catalogId: catlogId, play_back_time: currentWatchTime, response: {[weak self] response in

            print("Watch History Updated")

        })


    }

    public func getUpdateWatchHistoryPos() -> Double
    {
        return currentWatchTime
    }

    
}
