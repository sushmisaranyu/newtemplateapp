//
//  WatchHistoryAPIProtocol.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 21/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation



public protocol WatchHistoryAPIProtocol:class
{
    func updateWatchHistory(contentId:String,catalogId:String,play_back_time:Double, response: @escaping ((DataLoader<Bool>)->Void))
}
