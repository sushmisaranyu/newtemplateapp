//
//  WatchHistoryAPI.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 21/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class WatchHistoryAPI: WatchHistoryAPIProtocol
{

    public init(){}

    public func updateWatchHistory(contentId: String, catalogId: String, play_back_time: Double, response: @escaping ((DataLoader<Bool>) -> Void)) {
        PlaylistService.UpdateWatchHistory(catalogId: catalogId, contentId: contentId, playbackTimeInSeconds: play_back_time, responseHandler: response)
    }



}
