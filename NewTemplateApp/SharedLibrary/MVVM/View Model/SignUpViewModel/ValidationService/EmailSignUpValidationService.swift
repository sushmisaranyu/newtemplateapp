//
//  EmailSignUpValidationService.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 03/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation

public class EmailSignUpValidationService:SignUpValidationServiceProtocol
{
    public func validateName(_ name: String) -> NameValidationResult {
        var  result = NameValidationResult.None
        if StringHelper.isNilOrEmpty(string: name.trimmingCharacters(in: .whitespacesAndNewlines))
        {
            result = .EmptyOrNil
        }
        else
        {
            result = .Ok
        }

          return result

    }

    public func validateConfirmPassword(_ password: String, confirmPassword: String) -> ConfirmPasswordValidationResult {
        if password != confirmPassword
        {
            return .notSameAsPassword
        }
        else
        {
            return .Ok
        }
    }



    public func valiateUserName(_ userName: String) -> UserNameValidationResult {
        var  result = UserNameValidationResult.None
        if StringHelper.isNilOrEmpty(string: userName)
        {
            result = .UserNameIsNilOrEmpty
        }
        else if AppUtilities.isValidEmail(userName) == false
        {
            result = .InvalidUserName
        }
        else if StringHelper.hasSpace(string: userName)
        {
            result = .UserNameHasSpaces
        }
        else
        {
            result = .Ok
        }

        return result
    }

    public func validatePassword(_ password: String) -> PasswordValidationResult {
        var  result = PasswordValidationResult.None
        if StringHelper.isNilOrEmpty(string: password)
        {
            result = .EmptyOrNil
        }
        else if password.count < 6
        {
            result = .SizeLessThanMiniumCharCount
        }
        else if StringHelper.hasSpace(string: password)
        {
            result = .HasSpace
        }
        else
        {
            result = .Ok
        }

        return result
    }


}
