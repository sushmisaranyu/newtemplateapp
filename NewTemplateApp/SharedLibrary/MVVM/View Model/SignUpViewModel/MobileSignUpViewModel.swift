//
//  MobileSignUpViewModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 04/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class MobileSignUpViewModel:BaseSignUpViewModel
{


    var countryCode:CountryCode?

    public init(input: Input, countryCode:CountryCode)
    {
        self.countryCode = countryCode
        super.init(input: input, dependency: Dependancy(api: MobileSignUpAPI(), validationService: MobileSignUpValidationService()))
    }


    public override func signUp()
    {
        if self.isBusy() == true {return}

         if validateName() == false || validateUserName() == false ||  validatePassword() == false ||  validateConfirmPassword() ==  false || validateIsTOCCheckboxSelected() == false { return }

        guard let username = try? self.input.usernameSubject.value() else {return}
        guard let password = try? self.input.passwordSubject.value() else {return}
        guard let name = try? self.input.nameSubject.value() else {return}
        guard let countryCode:String = self.countryCode?.callingCode else {return}


        self.output.isLoadingSubject.onNext(true)


        self.dependency.api.signUp(username: countryCode + username, password: password, name: name, response:  {[weak self] (response) in
            guard let strongSelf = self else {return}

            strongSelf.output.isLoadingSubject.onNext(false)

            strongSelf.output.signUpResponseSubject.onNext(response)
        })
    }
    
}
