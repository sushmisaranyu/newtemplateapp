//
//  EmailSignUpViewModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 03/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class EmailSignUpViewModel:BaseSignUpViewModel
{
    public init(input: Input)
    {
        super.init(input: input, dependency: Dependancy(api: EmailSignUpAPI(), validationService: EmailSignUpValidationService()))
    }
}
