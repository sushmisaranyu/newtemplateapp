//
//  SignUpViewModelProtocol.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 03/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation

public enum NameValidationResult
{
    case EmptyOrNil
    case hasSpecialCharacter
    case hasNumbers
    case Ok
    case None

    var isValid:Bool
    {
        if self == .Ok
        {
            return true
        }

        return false
    }
}

public enum ConfirmPasswordValidationResult
{
    case EmptyOrNil
    case notSameAsPassword
    case Ok
    case None

    var isValid:Bool
    {
        if self == .Ok
        {
            return true
        }

        return false
    }
}




public protocol SignUpAPIProtocol:class {
    func signUp(username:String,password:String,name:String, response: @escaping ((DataLoader<UserModel>)->Void))
}

public protocol SignUpValidationServiceProtocol:LoginValidationServiceProtocol
{
    func validateName(_ name: String) -> NameValidationResult
    func validateConfirmPassword(_ password: String,confirmPassword:String) -> ConfirmPasswordValidationResult
}
