//
//  EmailSignUpAPI.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 03/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class EmailSignUpAPI:SignUpAPIProtocol
{
    public func signUp(username: String, password: String, name: String, response: @escaping ((DataLoader<UserModel>) -> Void)) {
        UserService.EmailSignUp(emailId: username, name: name, password: password, responseHandler: response)
    }

}
