//
//  BaseSignUpViewModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 03/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//


import Foundation
import RxSwift
import RxCocoa


public class BaseSignUpViewModel:ViewModelProtocol
{
    var disposeBag = DisposeBag()

    public  struct Input
    {
        public init(name:Driver<String>,username:Driver<String>, password: Driver<String>, confirmPassword:Driver<String>) {
            usernameDriver           = username
            passwordDriver           = password
            nameDriver               = name
            confirmPasswordDriver    = confirmPassword
            //Initlize Behaviour subjects to listen to change in login text fields
            usernameSubject             = BehaviorSubject(value: "")
            passwordSubject             = BehaviorSubject(value: "")
            nameSubject                 = BehaviorSubject(value: "")
            confirmPasswordSubject      = BehaviorSubject(value: "")
            isCheckBoxSelectedSubject   = BehaviorSubject(value: false)

        
        }

        let usernameDriver: Driver<String>
        let passwordDriver: Driver<String>
        let nameDriver: Driver<String>
        let confirmPasswordDriver: Driver<String>


        let usernameSubject: BehaviorSubject<String>
        let passwordSubject: BehaviorSubject<String>
        let nameSubject:BehaviorSubject<String>
        let confirmPasswordSubject:BehaviorSubject<String>

        let isCheckBoxSelectedSubject:BehaviorSubject<Bool>


    }

    public struct Output {

        init(input:Input, dependancy:Dependancy)
        {
            self.validatedUsernameSubject           = BehaviorSubject(value: .None)
            self.validatedPasswordSubject           = BehaviorSubject(value: .None)
            self.validatedNameSubject               = BehaviorSubject(value: .None)
            self.validatedConfirmPasswordSubject    = BehaviorSubject(value: .None)

            self.isLoadingSubject                   = BehaviorSubject(value: false)
            self.signUpResponseSubject              = BehaviorSubject(value: DataLoader.None)
            self.validateIsTOCSelectedSubject       = PublishSubject()


            //public Driver Setup
            self.validatedUsername          = validatedUsernameSubject.asDriver(onErrorJustReturn: .None)
            self.validatedPassword          = validatedPasswordSubject.asDriver(onErrorJustReturn: .None)
            self.validatedName              = validatedNameSubject.asDriver(onErrorJustReturn: .None)
            self.validatedConfirmPassword   = validatedConfirmPasswordSubject.asDriver(onErrorJustReturn: .None)

            self.isLoading                  = isLoadingSubject.asDriver(onErrorJustReturn: false)
            self.signUpResponse             = signUpResponseSubject.asDriver(onErrorJustReturn: .None)
            self.validateIsTOCSelected      = validateIsTOCSelectedSubject.asDriver(onErrorJustReturn: false)
            self.isCheckBoxSelectedDriver   = input.isCheckBoxSelectedSubject.asDriver(onErrorJustReturn: false)

            self.signupEnabled = Driver.combineLatest(
                input.usernameDriver,
                input.passwordDriver,
                input.nameDriver,
                input.confirmPasswordDriver,
                isCheckBoxSelectedDriver,
                isLoading
            )   { username, password,name,confirmPassword,isCheckBoxSelectedSubject, isLoading in
                let usernameResult = dependancy.validationService.valiateUserName(username)
                let passwordResult = dependancy.validationService.validatePassword(password)
                let nameResult = dependancy.validationService.validateName(name)
                let confirmPasswordResult = dependancy.validationService.validateConfirmPassword(password, confirmPassword: confirmPassword)

                return usernameResult.isValid && passwordResult.isValid && !isLoading && confirmPasswordResult.isValid && nameResult.isValid && isCheckBoxSelectedSubject

                }
                .distinctUntilChanged()
        }

        let validatedUsernameSubject: BehaviorSubject<UserNameValidationResult>
        let validatedPasswordSubject: BehaviorSubject<PasswordValidationResult>
        let validatedNameSubject: BehaviorSubject<NameValidationResult>
        let validatedConfirmPasswordSubject: BehaviorSubject<ConfirmPasswordValidationResult>

        let isLoadingSubject:BehaviorSubject<Bool>
        let signUpResponseSubject: BehaviorSubject<DataLoader<UserModel>>
        let validateIsTOCSelectedSubject:PublishSubject<Bool>


        public let validatedUsername: Driver<UserNameValidationResult>
        public let validatedPassword: Driver<PasswordValidationResult>
        public let validatedName: Driver<NameValidationResult>
        public let validatedConfirmPassword: Driver<ConfirmPasswordValidationResult>
        public let validateIsTOCSelected:Driver<Bool>
        public  let isCheckBoxSelectedDriver:Driver<Bool>

        public let signupEnabled: Driver<Bool>

        // Is signing process in progress
        public let isLoading: Driver<Bool>

        // Has user signed in
        public let signUpResponse: Driver<DataLoader<UserModel>>

    }

    public struct Dependancy
    {
        public init(api:SignUpAPIProtocol,validationService: SignUpValidationServiceProtocol)
        {
            self.api = api
            self.validationService = validationService
        }

        let api: SignUpAPIProtocol
        let validationService: SignUpValidationServiceProtocol
    }

    let input: Input

    public let output: Output

    let  dependency:Dependancy

    // Is signup button enabled

    public init(input: Input,
                dependency: Dependancy)
    {

        self.dependency = dependency
        self.input = input
        self.output = Output(input:input, dependancy: dependency)
        self.setupInputBehaviourSubjects()
    }

    func setupInputBehaviourSubjects()
    {
        //Setting up OnNext listener to check for changes in username
        self.input.usernameDriver.distinctUntilChanged().drive(onNext: {[weak self] updatedText in
            guard let strongSelf = self  else { return }
            strongSelf.resetUserNameValidationState()
            strongSelf.input.usernameSubject.onNext(updatedText)
        }).disposed(by: disposeBag)

        self.input.nameDriver.distinctUntilChanged().drive(onNext: {[weak self] updatedText in
            guard let strongSelf = self  else { return }
            strongSelf.resetNameValidationState()
            strongSelf.input.nameSubject.onNext(updatedText)
        }).disposed(by: disposeBag)

        //Setting up OnNext listener to check for changes in password
        self.input.passwordDriver.distinctUntilChanged().drive(onNext: {[weak self] updatedText in
            guard let strongSelf = self  else { return }
            strongSelf.resetPasswordValidationState()
            strongSelf.input.passwordSubject.onNext(updatedText)
        }).disposed(by: disposeBag)

        self.input.confirmPasswordDriver.distinctUntilChanged().drive(onNext: {[weak self] updatedText in
            guard let strongSelf = self  else { return }
            strongSelf.resetConfirmPasswordValidationState()
            strongSelf.input.confirmPasswordSubject.onNext(updatedText)
        }).disposed(by: disposeBag)
    }

    //MARK: Validation
    public func validateUserName() -> Bool
    {
        guard let email = try? input.usernameSubject.value() else { return false}
        let result:UserNameValidationResult = self.dependency.validationService.valiateUserName(email)
        self.output.validatedUsernameSubject.onNext(result)

        return result.isValid
    }

    public func validatePassword() -> Bool
    {
        guard let password = try? input.passwordSubject.value() else { return false}
        let result:PasswordValidationResult = self.dependency.validationService.validatePassword(password)
        self.output.validatedPasswordSubject.onNext(result)

        return result.isValid
    }

    public func validateName() -> Bool
    {
        guard let name = try? input.nameSubject.value() else { return false }
        print("Name: \(name)")
        let result:NameValidationResult = self.dependency.validationService.validateName(name.trimmingCharacters(in: .whitespacesAndNewlines))
        self.output.validatedNameSubject.onNext(result)

         return result.isValid
    }

    public func validateConfirmPassword() -> Bool
    {
        guard let confirmPassword = try? input.confirmPasswordSubject.value() else { return false }
        guard let password = try? input.passwordSubject.value() else { return false }


        _ = validatePassword()

        let result:ConfirmPasswordValidationResult = self.dependency.validationService.validateConfirmPassword(password, confirmPassword: confirmPassword)
        self.output.validatedConfirmPasswordSubject.onNext(result)

        return result.isValid
    }


    //MARK: Reset Validation
    func resetUserNameValidationState()
    {
        self.output.validatedUsernameSubject.onNext(.None)
    }

    func resetPasswordValidationState()
    {
        self.output.validatedPasswordSubject.onNext(.None)
    }

    func resetNameValidationState()
    {
        self.output.validatedNameSubject.onNext(.None)
    }

    func resetConfirmPasswordValidationState()
    {
        self.output.validatedConfirmPasswordSubject.onNext(.None)
    }

    func validateIsTOCCheckboxSelected() -> Bool
    {

         print("Is Check box selected")

        guard let checkBoxState = try? input.isCheckBoxSelectedSubject.value() else {
            output.validateIsTOCSelectedSubject.onNext(false)
            return false

        }

        print("Is Check box selected \(checkBoxState)")

        output.validateIsTOCSelectedSubject.onNext(checkBoxState)


        return checkBoxState
    }

    func isBusy() -> Bool
    {
        guard let isBusy = try? self.output.isLoadingSubject.value() else  {return false}

        return isBusy
    }

    public func updateIsCheckBoxSelected(state:Bool)
    {
        print("Check box updated \(state)")
        input.isCheckBoxSelectedSubject.onNext(state)
    }

    public func signUp()
    {
        if self.isBusy() == true {return}

        if validateName() == false || validateUserName() == false ||  validatePassword() == false ||  validateConfirmPassword() ==  false || validateIsTOCCheckboxSelected() == false { return }

        guard let username = try? self.input.usernameSubject.value() else {return}
        guard let password = try? self.input.passwordSubject.value() else {return}
        guard let name = try? self.input.nameSubject.value() else {return}


        self.output.isLoadingSubject.onNext(true)


        self.dependency.api.signUp(username: username, password: password, name: name, response:  {[weak self] (response) in
            guard let strongSelf = self else {return}

            strongSelf.output.isLoadingSubject.onNext(false)

            strongSelf.output.signUpResponseSubject.onNext(response)
        })
    }

}

