//
//  DetailsModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 23/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class DetailsModel
{
    public var mediaitem:OTTVideoItem
    public var itemState:DetailsScreenStateModel?

    public init(item:OTTVideoItem)
    {
        self.mediaitem = item
    }

    public func setItemState(state:DetailsScreenStateModel)
    {
        self.itemState = state
    }

    public var hasItemState:Bool
    {
        if self.itemState == nil
        {
            return false
        }

        return true
    }


    
}
