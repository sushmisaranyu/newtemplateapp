//
//  StaticCatalogListData.swift
//  SharedLibrary
//
//  Created by MAC on 05/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import UIKit

public class StaticCatalogListData {
    
    public class func getStaticCatalogListData() -> [CatalogListItem]?{
        
        return [
            self.getCatalogListItem(displayTitle: "PageBanner", layoutType: "t_16_9_banner")!,
            self.getCatalogListItem(displayTitle: "Continue Watching", layoutType: "t_continue_watching")!,
            self.getCatalogListItem(displayTitle: "Classic Movies", layoutType: "t_2_3_movie")!,
            self.getCatalogListItem(displayTitle: "Classic Shows", layoutType: "t_16_9_small")!,
            self.getCatalogListItem(displayTitle: "Classic Songs", layoutType: "t_1_1_play")!,
            self.getCatalogListItem(displayTitle: "Bollywood Plus", layoutType: "t_2_3_movie")!,
            self.getCatalogListItem(displayTitle: "Featured Shows", layoutType: "t_comb_16_9_list")!,
            self.getCatalogListItem(displayTitle: "Best of Kishore Kumar", layoutType: "t_comb_1_1_list")!,
            self.getCatalogListItem(displayTitle: "Gujarathi", layoutType: "t_16_9_big")!,
            self.getCatalogListItem(displayTitle: "Kids", layoutType: "t_2_3_movie")!,
            self.getCatalogListItem(displayTitle: "Bakti", layoutType: "t_16_9_big")!,
            self.getCatalogListItem(displayTitle: "Panjabi", layoutType: "t_1_1_plain")!
            
        ]
    }
    
    public class func getCatalogListItem(displayTitle : String, layoutType: String) -> CatalogListItem?
    {
        let friendlyId:String = ""
        
        let object = CatalogListItem()
        
        object.displayTitle = displayTitle
        
        object.friendlyId           = friendlyId
        object.listId               = friendlyId
        object.listType             = ""
        object.tags                 = [""]
        
        
        object.total_items_count    = 6
        
        
        object.theme                = ""
        object.layout_type          = layoutType
        object.home_link            = ""
        object.layout_scheme        = ""
        
        object.items = [self.StaticDataOfOTTVideoItem(),
                        self.StaticDataOfOTTVideoItem(),
                        self.StaticDataOfOTTVideoItem(),
                        self.StaticDataOfOTTVideoItem(),
                        self.StaticDataOfOTTVideoItem(),
                        self.StaticDataOfOTTVideoItem()]
        
        
        
        
        return object
        
    }
    public class func StaticDataOfOTTVideoItem() -> OTTVideoItem
    {
        var object = OTTVideoItem()
        
        object.catalog_id = "595492e8345cf207e2000007"
        
        object.catalog_name = "TV Shows"
        object.content_id = "5954d945345cf207e200003d"
        object.description_string = ""
        object.duration_string = "2821"
        object.genres = [""]

        object.language = "hindi"
        object.like_count = 1
        object.no_of_user_rated = 1
        object.listitem_id = ""
        object.episode_type = ""
        
        
        
        let  thumbnailjson:[String:Any] = [
            "l_large" : [ "url" : "https://st1.epicon.epicchannel.com/EpicON-Production/TV-Shows/shows/Rakkt/subcategories/Season-1/episodes/S1-E7-Alauddin-Khilji/images/S1:E7-Alauddin-Khilji-794x447.jpg"],
            "l_medium" : [ "url" : "https://st1.epicon.epicchannel.com/EpicON-Production/TV-Shows/shows/Rakkt/subcategories/Season-1/episodes/S1-E7-Alauddin-Khilji/images/S1:E7-Alauddin-Khilji-410x231.jpg"],
            "p_small" : [ "url" : "https://st1.epicon.epicchannel.com/EpicON-Production/TV-Shows/shows/Rakkt/subcategories/Season-1/episodes/S1-E7-Alauddin-Khilji/images/S1:E7-Alauddin-Khilji-410x231.jpg"] ]
        
        object.thumbnails   = OTTImageURLs.Parse(jsonDictionary: thumbnailjson)
        
        
        
        object.title = "S1:E7 Alauddin Khilji"
        object.view_count = 5
        
        if object.title == "S1:E7 Alauddin Khilji"
        {
            print("List")
        }
        return object
        
    }
}



