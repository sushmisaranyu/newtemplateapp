//
//  Config.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 31/08/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class  OTTConfig
{

    public init(termsOfConditionURL:String,privacyURL:String,aboutURL:String,feedbackCatalogID:String)
    {
        AppUserDefaults.feedBackID.setValue(feedbackCatalogID)
        AppUserDefaults.aboutUsURl.setValue(aboutURL)
        AppUserDefaults.termsOfConditionURL.setValue(termsOfConditionURL)
        AppUserDefaults.privacyURL.setValue(privacyURL)
    }

    public class func Parse(json:[String:Any]) -> Bool
    {
        guard let feedbackjson:[String:Any] = json.valueFor(key: "feedback", isMandatory: false)
            else
        {
            return false
        }

        guard let feedbackCatalogID:String = feedbackjson.valueFor(key:"catalog_id",isMandatory: false)
            else
        {
            return false
        }

        guard let web_pagesJson:[String:Any] = json.valueFor(key: "web_pages", isMandatory: false)
            else
        {
            return false
        }

        guard let toc:String = web_pagesJson.valueFor(key: "terms_conditions", isMandatory: false)
            else
        {
            return false
        }

        guard let privacy_policy:String = web_pagesJson.valueFor(key: "privacy_policy", isMandatory: false)
            else
        {
             return false
        }

        guard let about_us:String = web_pagesJson.valueFor(key: "about_us", isMandatory: false)
            else
        {
            return false
        }

        _ = OTTConfig(termsOfConditionURL: toc, privacyURL: privacy_policy, aboutURL: about_us, feedbackCatalogID: feedbackCatalogID)
        return true

    }
}
