//
//  AppUpdateConfig.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 31/08/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
public class AppUpdateInfo
{

    public var force_upgrade              = false
    public var recomended_upgrade         = false
    public var message                    = ""
    public var title                      = ""
    public var updateState:AppUpdateState = .noUpdate
    public var cancel_btn                 = "Cancel"
    public var proceed_btn                = "Proceed"

    public class func Parse(json:[String:Any]) -> AppUpdateInfo
    {
        var updateInfo = AppUpdateInfo()

        if let value:Bool = json.valueFor(key: "force_upgrade", isMandatory: false)
        {
            updateInfo.force_upgrade = value
        }

        if let value:Bool = json.valueFor(key: "recomended_upgrade", isMandatory: false)
        {
            updateInfo.recomended_upgrade = value
        }

        if let value:String = json.valueFor(key: "title", isMandatory: false)
        {
            updateInfo.title = value
        }

        if let value:String = json.valueFor(key: "message", isMandatory: false)
        {
            updateInfo.message = value
        }

        if let value:String = json.valueFor(key: "proceed_btn", isMandatory: false)
        {
            updateInfo.proceed_btn = value
        }

        if let value:String = json.valueFor(key: "cancel_btn", isMandatory: false)
        {
            updateInfo.cancel_btn = value
        }

        if updateInfo.force_upgrade == true
        {
            updateInfo.updateState = .forcedUpdate
        }
        else if updateInfo.recomended_upgrade == true
        {
            updateInfo.updateState = .softUpdate
        }
        else
        {
            updateInfo.updateState = .noUpdate
        }


        return updateInfo
    }

}


public class AppUpdateConfig
{
    let status:AppUpdateState

    public init(status:AppUpdateState){
        self.status = status
    }

    public class func Parse(version:String, json:[String:Any]) -> AppUpdateConfig
    {
        var minversion = ""

        var maxVersion = ""


        if let value:String = json.valueFor(key: "ios_min_version", isMandatory: false)
        {
            minversion = value
        }

        if let value:String = json.valueFor(key: "ios_max_version", isMandatory: false)
        {
            maxVersion = value
        }

        let appUpdateState = AppUtilities.versionChecker(appVersion: version, minVersion: minversion, maxVersion: maxVersion)

        return AppUpdateConfig(status: appUpdateState)

    }

    
}
