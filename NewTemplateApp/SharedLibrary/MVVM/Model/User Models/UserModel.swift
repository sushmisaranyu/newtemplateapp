//
//  UserModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 03/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation

public class UserModel
{
   public var emailId: String?
   public var session: String?
   public var loginType: String?
   public var firstName: String?
   public var lastName: String?
   public var profileImage: String?
   public var userId: String?
   public var ext_account_email_id: String?
   public var mobile_number:String?
   public var first_time_login:Bool = false
    public var profile_id:String?
    public var email_verification_flag:Bool = false
    public var birthdate:String?

    public init()
    {

    }

    public  class func Parse(jsonDictionary:[String:Any], loginType:LoginType) -> UserModel?
    {

        var addProfile = false
        var first_time_login = false
        guard let data:[String:Any] = jsonDictionary.valueFor(key:"data", isMandatory: false) else
        {
            return nil
        }

        guard let session:String = data.valueFor(key:"session_id", isMandatory: false) else
        {
            return nil
        }

        guard let userId:String = data.valueFor(key:"user_id", isMandatory: false) else
        {
            return nil
        }

        if let value:Bool = data.valueFor(key: "first_time_login", isMandatory: false)
        {
            first_time_login = value
        }

        let user                = UserModel()
        user.userId             = userId
        user.session            = session
        user.first_time_login   = first_time_login


        if let value:String  = data.valueFor(key:"email_id", isMandatory: false)
        {
            user.emailId = value
        }

        if let value:String = data.valueFor(key:"ext_account_email_id",isMandatory: false)
        {
            user.ext_account_email_id = value
        }

        if let value:String  = data.valueFor(key:"login_type", isMandatory: false)
        {
            user.loginType = value
        }
        else
        {
            if let value:String  = data.valueFor(key:"registered_using", isMandatory: false)
            {
                user.loginType = value
            }
        }

        if let value:String  = data.valueFor(key:"firstname", isMandatory: false)
        {
            user.firstName = value
        }

        if let value:String  = data.valueFor(key:"lastname", isMandatory: false)
        {
            user.lastName = value
        }


        if loginType == .Mobile
        {
            if let value:String  = data.valueFor(key:"email_id", isMandatory: false)
            {
                user.mobile_number = value
            }
        }
        else
        {
            if let value:String  = data.valueFor(key:"mobile_number", isMandatory: false)
            {
                user.mobile_number = value
            }
        }


        if let value:String  = data.valueFor(key:"profile_pic", isMandatory: false)
        {
            user.profileImage = value
        }

        if let value:Bool  = data.valueFor(key:"add_profile", isMandatory: false)
        {
            addProfile = value
        }

        user.birthdate = data.valueFor(key: "birthdate", isMandatory: false)

        if let value:Bool  = data.valueFor(key:"add_profile", isMandatory: false)
        {
           user.email_verification_flag = value
        }

        if let value:[String:Any] =  data.valueFor(key:"profile_obj", isMandatory: false)
        {
            user.profile_id = value.valueFor(key: "profile_id", isMandatory: false)
            print("User Model Profile ID = \(user.profile_id)")
        }
         print("User Model User = \(user)")


        return user

    }

    public func logInUser(sessionID:String)
    {

        OTTUser.InitUserWithUserModel(userModel: self, sessionId: sessionID)
        AppUserDefaults.session_ID.setValue(sessionID)
    }
}
