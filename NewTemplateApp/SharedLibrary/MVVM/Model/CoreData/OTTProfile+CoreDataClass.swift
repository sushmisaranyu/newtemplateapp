//
//  OTTProfile+CoreDataClass.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 06/04/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData

@objc(OTTProfile)
public class OTTProfile: NSManagedObject {
    
    @NSManaged public var profileId: String?
    @NSManaged public var firstName: String?
    @NSManaged public var lastName: String?
    @NSManaged public var age: String?
    @NSManaged public var isChild: Bool
    @NSManaged public var sortId:NSNumber?
    @NSManaged public var default_profile:Bool
    
    
    public class func ParseArrays(jsonDictionary:[String:Any]) -> Bool
    {

        guard let dataArray:[String:Any] = jsonDictionary.valueFor(key:"data", isMandatory: false) else
        {
            return false
        }

        guard let profileArray:[[String:Any]] = dataArray.valueFor(key:"profiles", isMandatory: false) else
        {
            return false
        }
        
        DeleteAllProfiles()
        
        var index = 0
        for profile in profileArray
        {
            Parse(jsonDictionary: profile, sortId:index)
            index+=1
        }
        return true
    }
    
    public  class func Parse(jsonDictionary:[String:Any],sortId:Int) -> OTTProfile
    {
        let managedObjectContext = NSManagedObjectContext.newObjectContext()
        let profile              = InitOTTProfile(managedObjectContext:managedObjectContext)

        profile.profileId        = jsonDictionary.valueFor(key:"profile_id", isMandatory: false)
        profile.firstName        = jsonDictionary.valueFor(key:"firstname", isMandatory: false)
        profile.lastName         = jsonDictionary.valueFor(key:"lastname", isMandatory: false)
        profile.age              = jsonDictionary.valueFor(key:"age", isMandatory: false)
        profile.sortId           = NSNumber(integerLiteral: sortId)



        if let value:String     = jsonDictionary.valueFor(key:"default_profile", isMandatory: false)
        {
            if value == true.description
            {
                profile.default_profile = true
            }
            else
            {
                profile.default_profile = false
            }
        }
        else
        {
            profile.default_profile   = false
        }


        if let value:Bool   = jsonDictionary.valueFor(key:"child", isMandatory: false)
        {
            profile.isChild   = value
        }
        else
        {
            profile.isChild   = false
        }
        
        
        managedObjectContext.Save()
        
        return profile
        
    }

    public class func getAllProfileUsers() -> [ProfileUser]?
    {
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()

        let list = OTTProfile.getManagedObjects(managedObjectContext: managedObjectContext) as? [OTTProfile]

        guard let tempList = list else{ return nil}

        var profileUsers = [ProfileUser]()

        for profile in tempList
        {
            guard let id = profile.profileId else {continue}
            let user = ProfileUser(profileId:id, firstName: profile.firstName, lastName: profile.lastName, age: profile.age, isChild: profile.isChild, default_user: profile.default_profile)

            profileUsers.append(user)
        }

        if profileUsers.count == 0 {return nil}

        return profileUsers
    }


    
  
}
