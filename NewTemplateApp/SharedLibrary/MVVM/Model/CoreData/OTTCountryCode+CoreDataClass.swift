//
//  OTTCountryCode+CoreDataClass.swift
//  SaraynuLibrary
//
//  Created by Apple  on 22/02/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import CoreData

@objc(OTTCountryCodeList)
public class OTTCountryCodeList:NSManagedObject
{
    public class func ParseCountryCodeDictionary(jsonDictionary:[String:Any])
    {
        guard let _:String = jsonDictionary.valueFor(key:"country_code", isMandatory: true) else {
            return
        }
        guard let _:String = jsonDictionary.valueFor(key: "calling_code", isMandatory: true)
            else {
            return
        }
        let managedObjectContext = NSManagedObjectContext.newObjectContext()
        let object = OTTCountryCodeList.managedObject(managedObjectContext: managedObjectContext) as! OTTCountryCodeList
        
        object.countryRegion = jsonDictionary.valueFor(key: "region")
        object.callingCode =  jsonDictionary.valueFor(key: "calling_code")
         AppUtilities.printLog(message: "\(object.callingCode)")
        object.countryCode = jsonDictionary.valueFor(key: "country_code")
        object.maxDigits = jsonDictionary.valueFor(key: "max_digits")
        object.minDigits = jsonDictionary.valueFor(key: "min_digits")
        if let value:String = jsonDictionary.valueFor(key:"service_under_maintainance", isMandatory: false)
        {
            if value == "yes"
            {
                object.seviceUnderMaintanance = true
            }
            else{
                object.seviceUnderMaintanance = true

            }
            
        }
        if let value:String = jsonDictionary.valueFor(key:"is_valid", isMandatory: false)
        {
            if value == "yes"
            {
                object.isValid = true
            }
            else{
                object.isValid = true
                
            }
            
        }
  
        
          managedObjectContext.Save()
    }
    
    public class func fetchCountryCode() -> [OTTCountryCodeList]? {
        
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
        
        let countryCodeArray = OTTCountryCodeList.getManagedObjects(managedObjectContext: managedObjectContext) as? [OTTCountryCodeList]
        
        return countryCodeArray
        
    }

    public func getCountryCodeForRegion(region:String) -> CountryCode?
    {
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()

        let countryCodeArray = OTTCountryCodeList.getManagedObjects(managedObjectContext: managedObjectContext, predicate: NSPredicate(format:"countryRegion = '\(region)'")) as? [OTTCountryCodeList]

        guard let  cc = countryCodeArray?.first else { return nil }

        let countryCode = CountryCode()
        countryCode.callingCode = cc.callingCode
        countryCode.countryCode = cc.countryCode
        countryCode.countryRegion = cc.countryRegion
        countryCode.maxDigits = cc.maxDigits
        countryCode.minDigits = cc.minDigits


        return countryCode
    }
    
    public class func DeleteAllCodes() {
        
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
        
        let list = OTTCatalog.getManagedObjects(managedObjectContext: managedObjectContext) as? [OTTCatalog]
        
        guard let tempList = list else{ return }
        
        for item in tempList
        {
            managedObjectContext.delete(item)
        }
        
        managedObjectContext.Save()
    }
    
}
