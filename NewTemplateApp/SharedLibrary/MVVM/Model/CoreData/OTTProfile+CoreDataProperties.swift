//
//  OTTProfile+CoreDataProperties.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 06/04/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension OTTProfile {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<OTTProfile> {
        return NSFetchRequest<OTTProfile>(entityName: "OTTProfile");
    }

    
    class func InitOTTProfile(managedObjectContext:NSManagedObjectContext) -> OTTProfile {
       
        return  OTTProfile.managedObject(managedObjectContext: managedObjectContext) as! OTTProfile
    }
    
    public class func DeleteAllProfiles() {
        
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
        
        let list = OTTProfile.getManagedObjects(managedObjectContext: managedObjectContext) as? [OTTProfile]
        
        guard let tempList = list else{ return }
        
        for item in tempList
        {
            managedObjectContext.delete(item)
        }
        
          managedObjectContext.Save()
    }


    public class func isBoundProfileAChild() -> Bool
    {
        if OTTUser.hasLoggedInUser() == false {return false }

        guard let profileId = AppUserDefaults.boundProfileId else {return false}

        let managedObjectContext    = NSManagedObjectContext.newObjectContext()

        guard let list = OTTProfile.getManagedObjects(managedObjectContext: managedObjectContext, predicate: NSPredicate(format:"profileId = '\(profileId)'")) as? [OTTProfile]
        else
        {
            return false
        }

        if list.count  <= 0
        {
            return false
        }

        guard let state = list.first?.isChild
        else
        {
            return false
        }

        return state

    }


    public class func isBoundProfileTheDefaultProfile() -> Bool
    {
        if OTTUser.hasLoggedInUser() == false {return false}

        guard let profileId = AppUserDefaults.boundProfileId else {return false}

        let managedObjectContext    = NSManagedObjectContext.newObjectContext()

        guard let list = OTTProfile.getManagedObjects(managedObjectContext: managedObjectContext, predicate: NSPredicate(format:"profileId = '\(profileId)'")) as? [OTTProfile]
            else
        {
            return false
        }

        if list.count  <= 0
        {
            return false
        }

        guard let state = list.first?.default_profile
            else
        {
            return false
        }

        return state

    }

}
