//
//  OTTCountryCode+CoreDataProperties.swift
//  SaraynuLibrary
//
//  Created by Apple  on 22/02/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import CoreData
extension OTTCountryCodeList
{
    @nonobjc public class func fetchRequest() -> NSFetchRequest<OTTCountryCodeList> {
        return NSFetchRequest<OTTCountryCodeList>(entityName: "OTTCountryCode")
    }
    @NSManaged public var countryRegion: String?
    @NSManaged public var countryCode: String?
    @NSManaged public var callingCode: String?
    @NSManaged public var seviceUnderMaintanance: Bool
    @NSManaged public var maxDigits: NSNumber?
    @NSManaged public var minDigits: NSNumber?
    @NSManaged public var isValid: Bool 







}
