//
//  PreviewModel.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 11/12/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class PreviewModel:NSObject,NSCoding
{
    public var ext_previews:String?
    public var preview_available:Bool? = false
    public var preview_end:Double?     = 0
    public var preview_start:Double?   = 0


    override init()
    {

    }

    init(preview_available:Bool,preview_start:Double,preview_end:Double,ext_preview:String?) {

        self.preview_available = preview_available
        self.preview_end       = preview_end
        self.preview_start     = preview_start
        self.ext_previews      = ext_preview
    }

    public func encode(with aCoder: NSCoder){

        aCoder.encode(preview_available, forKey: "preview_available")
        aCoder.encode(preview_end, forKey: "preview_end")
        aCoder.encode(preview_start, forKey: "preview_start")
        aCoder.encode(ext_previews, forKey: "ext_previews")
    }

    required convenience public init(coder aDecoder: NSCoder) {

        self.init()
        self.preview_end                 = aDecoder.decodeObject(forKey: "preview_end") as? Double
        self.preview_available           = aDecoder.decodeObject(forKey: "preview_available") as? Bool

        self.preview_start               = aDecoder.decodeObject(forKey: "preview_start") as? Double
        self.preview_available           = aDecoder.decodeObject(forKey: "preview_available") as? Bool


    }

    public class func Parse(json:[String:Any]) -> PreviewModel?
    {
        var preview_available = false
        var preview_end = 0.0
        var preview_start = 0.0
        var ext_Preview:String?

        if let value:Bool = json.valueFor(key: "preview_available")
        {
            preview_available = value
        }


        if let value:String = json.valueFor(key: "preview_start")
        {
            preview_start = getDurationInSeconds(duration_string: value)
        }

        if let value:String = json.valueFor(key: "preview_end")
        {
            preview_end = getDurationInSeconds(duration_string: value)
        }

        if let value:String = json.valueFor(key: "ext_preview_url")
        {
            if StringHelper.isNilOrEmpty(string: value) == false
            {
                ext_Preview = value
            }
        }

        if preview_end < preview_start && StringHelper.isNilOrEmpty(string: ext_Preview) == true
        {
            preview_available = false
        }



//        if let extJson:[[String:Any]] = json.valueFor(key: "ext_previews")
//        {
//            var gotExtUrl = false
//
//            for item in extJson
//            {
//
//                if gotExtUrl == true {continue}
//
//                guard let playURL:[String:Any] = item.valueFor(key: "play_url") else {continue}
//
//                guard let saranyu:[String:Any] = playURL.valueFor(key: "saranyu") else {continue}
//
//                guard let url:String = saranyu.valueFor(key: "url") else {continue}
//
//                gotExtUrl = true
//                ext_Preview = url
//
//            }
//        }


        if preview_end == 0 && preview_start == 0 && StringHelper.isNilOrEmpty(string: ext_Preview) == true
        {
            preview_available = false
        }

        return PreviewModel(preview_available: preview_available, preview_start: preview_start, preview_end: preview_end,ext_preview: ext_Preview)

    }


    public class func getDurationInSeconds(duration_string:String?) -> Double
    {

        guard let duration = duration_string else {return 0}

        var durationArray = duration.components(separatedBy: ":")


        if durationArray.count == 4
        {
            durationArray.removeLast()
        }

        var index = 3

        var totalSeconds = 0

        for item in durationArray
        {
            var itemNum = 0
            if let value = Int(item)
            {
                itemNum = value
            }

            if index == 3 {totalSeconds = totalSeconds + itemNum * 60 * 60 }
            if index == 2 {totalSeconds = totalSeconds + itemNum * 60 }
            if index == 1 {totalSeconds = totalSeconds + itemNum }

            index = index - 1
        }

        return Double(totalSeconds)

    }

}
