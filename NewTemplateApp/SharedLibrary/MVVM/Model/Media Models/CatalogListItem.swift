//
//  CatalogListItem.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 10/08/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class CatalogListItem:NSObject,NSCoding
{
    public var displayTitle: String?
    public var friendlyId: String?

    public var listId: String?
    public var listType: String?

    public var total_items_count:Int = 0
    public var theme: String?
    public var thumbnails: OTTImageURLs?
    public var catalogType:String?
    public var tags:[String]?

    public var layout_type:String?
    public var layout_scheme:String?
    public var plan_category_type:String?

    public var home_link:String?
    public var menu_link:String?

    public var items:[OTTVideoItem]?

    public func encode(with aCoder: NSCoder){
        aCoder.encode(displayTitle, forKey: "displayTitle")
        aCoder.encode(friendlyId, forKey: "friendlyId")
        aCoder.encode(listId, forKey: "listId")
        aCoder.encode(listType, forKey: "listType")
        aCoder.encode(total_items_count, forKey: "total_items_count")
        aCoder.encode(theme, forKey: "theme")
        aCoder.encode(thumbnails, forKey: "thumbnails")
        aCoder.encode(catalogType, forKey: "catalogType")
        aCoder.encode(tags, forKey: "tags")
        aCoder.encode(layout_type, forKey: "layout_type")
        aCoder.encode(layout_scheme, forKey: "layout_scheme")
        aCoder.encode(home_link, forKey: "home_link")
        aCoder.encode(items, forKey: "items")
        aCoder.encode(menu_link, forKey: "menu_link")
        aCoder.encode(plan_category_type, forKey: "plan_category_type")


    }

    required convenience public init(coder aDecoder: NSCoder) {
        self.init()
        self.displayTitle       = aDecoder.decodeObject(forKey: "displayTitle") as? String
        self.friendlyId         = aDecoder.decodeObject(forKey: "friendlyId") as? String
        self.listId             = aDecoder.decodeObject(forKey: "listId") as? String
        self.listType           = aDecoder.decodeObject(forKey: "listType") as? String
        self.total_items_count  = aDecoder.decodeObject(forKey: "total_items_count") as! Int
        self.theme              = aDecoder.decodeObject(forKey: "theme") as? String
        self.thumbnails         = aDecoder.decodeObject(forKey: "thumbnails") as? OTTImageURLs
        self.catalogType        = aDecoder.decodeObject(forKey: "catalogType") as? String
        self.tags               = aDecoder.decodeObject(forKey: "tags") as? [String]
        self.layout_type        = aDecoder.decodeObject(forKey: "layout_type") as? String
        self.layout_scheme      = aDecoder.decodeObject(forKey: "layout_scheme") as? String
        self.home_link          = aDecoder.decodeObject(forKey: "home_link") as? String
        self.items              = aDecoder.decodeObject(forKey: "items") as? [OTTVideoItem]
        self.menu_link          = aDecoder.decodeObject(forKey: "menu_link") as? String
        self.plan_category_type          = aDecoder.decodeObject(forKey: "plan_category_type") as? String
    }

    public class func Parse(jsonDictionary:[String:Any]) -> CatalogListItem?
    {
        var catalogID:String? = nil

        let friendlyId:String?  = jsonDictionary.valueFor(key: "friendly_id", isMandatory: false)
        let list_Id:String?     = jsonDictionary.valueFor(key: "list_id", isMandatory: false)
        let id:String?          = jsonDictionary.valueFor(key: "id", isMandatory: false)

        if friendlyId != nil
        {
            catalogID = friendlyId
        }

        if catalogID == nil && list_Id != nil
        {
            catalogID = list_Id
        }

        if catalogID == nil && id != nil
        {
            catalogID = id
        }

        if catalogID == nil {return nil}

        let object = CatalogListItem()
        object.displayTitle = jsonDictionary.valueFor(key:"display_title", isMandatory: false)

        object.displayTitle         = jsonDictionary.valueFor(key:"display_title", isMandatory: false)
        object.friendlyId           = catalogID
        object.listId               = catalogID
        object.listType             = jsonDictionary.valueFor(key:"list_type", isMandatory: false)
        object.tags                 = jsonDictionary.valueFor(key:"tags", isMandatory: false)


        if let count:Int =  jsonDictionary.valueFor(key:"total_items_count", isMandatory: false)
        {
            object.total_items_count    = count
        }

        object.theme                = jsonDictionary.valueFor(key: "theme", isMandatory: false)
        object.layout_type          = jsonDictionary.valueFor(key: "layout_type", isMandatory: false)
        object.home_link            = jsonDictionary.valueFor(key: "home_link", isMandatory: false)
        object.menu_link            = jsonDictionary.valueFor(key: "menu_link", isMandatory: false)
        object.layout_scheme        = jsonDictionary.valueFor(key: "layout_scheme", isMandatory: false)
        object.plan_category_type   = jsonDictionary.valueFor(key: "plan_category_type", isMandatory: false)

        object.items = [OTTVideoItem]()



        var tempVideoList: [[String:Any]]?

        if let value:[[String:Any]] = jsonDictionary.valueFor(key: "catalog_list_items", isMandatory: false)
        {
            tempVideoList = value
        }
        else
        {
            if let value:[[String:Any]] = jsonDictionary.valueFor(key: "items", isMandatory: false)
            {
                tempVideoList = value
            }
        }


        guard let videoList: [[String:Any]] = tempVideoList
            else
        {
            return object
        }

        for item in videoList
        {
            guard let videoItem:OTTVideoItem = OTTVideoItem.Parse(jsonDictionary: item) else {continue}

            object.items?.append(videoItem)
        }

        return object

    }



}







