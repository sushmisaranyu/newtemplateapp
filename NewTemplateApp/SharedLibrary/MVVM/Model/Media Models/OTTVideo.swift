//
//  OTTEpisodes.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 03/05/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation
import CoreData


public enum ItemType
{
    case movie
    case tv_show
    case episode
    case album
    case album_song
    case live
    case linear
    case linear_program
    case standalone_song
    case video


 

    public static func GetItemType(theme:String?,layout_type:String?) -> ItemType?
    {
        guard let thm = theme, let layout = layout_type else {return nil}

        switch thm
        {
        case "movie" : return .movie
        case "show" :
            if layout == "album"
            {
                return .album
            }
            else
            {
                return .tv_show
            }

        case "episode","show_episode":
            if layout == "album"
            {
                return .album_song
            }
            else
            {
                return .episode
            }
        case "live":
            if layout == "linear"
            {
                return .linear
            }
            else if layout == "program"
            {
                return .linear_program
            }
            else
            {
                return .live
            }
        case "video":
            if layout == "song"
            {
                return .standalone_song
            }
            else
            {
                return .video
            }
        case "linear":
            if layout == "channels"
            {
                return .linear
            }
            else{
                return linear_program
            }
        default : return nil

        }
    }
}

public class AdditionMetaData:NSObject,NSCoding
{
    public var key: String?
    public var values:[String]?


    public override init() {

    }


    public init(key:String,values:[String])
    {
        self.key = key
        self.values = values
    }

    public func encode(with aCoder: NSCoder){

        aCoder.encode(key, forKey: "key")
        aCoder.encode(values, forKey: "values")
    }

    required convenience public init(coder aDecoder: NSCoder) {
        self.init()
        self.key                    = aDecoder.decodeObject(forKey: "key") as? String
        self.values                 = aDecoder.decodeObject(forKey: "values") as? [String]
    }

    class func Parse(dict:[String:[Any]]) -> [AdditionMetaData]
    {
        var list =  [AdditionMetaData]()
        for item in dict
        {

            if StringHelper.isNilOrEmpty(string: item.key) || item.value.count <= 0  { continue }
            guard let dictValues = item.value as? [String] else { continue }

            //print("Saved Values = \(item.key) :: \(dictValues) ")
            let additionalMeta = AdditionMetaData(key: item.key, values: dictValues)
            list.append(additionalMeta)
        }

        return list
    }
}

public class OTTVideoItem:NSObject,NSCoding
{
    public var title: String?
    public var content_id: String?
    public var catalog_id: String?
    public var theme: String?
    public var sequence_no:NSNumber? = 0
    public var language: String?
    public var genres: [String]?
    public var description_string: String?
    public var play_url:OTTPlayURL?
    public var play_url_type:String?
    public var thumbnails: OTTImageURLs?
    public var subcategory_id:String?
    public var show_theme_id:String?

    public var rating:NSNumber?
    public var like_count:NSNumber?
    public var view_count:NSNumber?
    public var no_of_user_rated:NSNumber?
    public var catalog_name:String?
    public var show_name:String?
    public var subcategory_name:String?
    public var short_description:String?

    public var release_date_string:String?

    public var duration_string:String?
    public var share_url:String?
    public var listitem_id:String?
    public var isSelected:Bool = false
    public var episode_count:NSNumber? = 0
    public var episode_type:String?
    public var episodetype_tags:[String]?
    public var people:[OTTPeople]?
    public var access_control: OTTAccessControl?

    public var media_type:String?
    public var release_date_uts:Double?
    public var catalog_object:CatalogListItem?

    public var item_caption:String?
    public var item_additional_data:[AdditionMetaData]?

    public var itemType:ItemType?

    public var start_time_uts:Double?   = 0
    public var stop_time_uts:Double?    = 0
    public var channel_logo:OTTImageURLs?
    public var items:[OTTVideoItem]?

    public var play_back_time: String?
    public var total_percentage:Double?

    public var preview:PreviewModel?

    public var episode_number:String?

    public var isShowAllItem: Bool = false

    public func encode(with aCoder: NSCoder){
        
        aCoder.encode(title, forKey: "title")
        aCoder.encode(content_id, forKey: "content_id")
        aCoder.encode(catalog_id, forKey:"catalog_id")
        aCoder.encode(theme, forKey: "theme")
        aCoder.encode(sequence_no, forKey: "sequence_no")
        aCoder.encode(genres, forKey:"genres")
        aCoder.encode(language, forKey: "language")
        aCoder.encode(description_string, forKey:"description_string")
        aCoder.encode(play_url, forKey: "play_url")
        aCoder.encode(play_url_type, forKey: "play_url_type")
        aCoder.encode(thumbnails, forKey:"thumbnails")
        aCoder.encode(subcategory_id, forKey: "subcategory_id")
        aCoder.encode(show_theme_id, forKey: "show_theme_id")
        aCoder.encode(rating, forKey: "rating")
        aCoder.encode(like_count, forKey: "like_count")
        aCoder.encode(view_count, forKey:"view_count")
        aCoder.encode(no_of_user_rated, forKey: "no_of_user_rated")
        aCoder.encode(catalog_name, forKey: "catalog_name")
        aCoder.encode(show_name, forKey: "show_name")
        aCoder.encode(subcategory_name,forKey:"subcategory_name")
        aCoder.encode(short_description,forKey:"short_description")
        aCoder.encode(release_date_string,forKey:"release_date_string")
        aCoder.encode(duration_string,forKey:"duration_string")
        aCoder.encode(share_url,forKey:"share_url")
        aCoder.encode(listitem_id,forKey:"listitem_id")
        aCoder.encode(isSelected,forKey:"isSelected")
        aCoder.encode(episode_count,forKey:"episode_count")
        aCoder.encode(episode_type,forKey:"episode_type")
        aCoder.encode(people,forKey:"people")
        aCoder.encode(access_control,forKey:"access_control")
        aCoder.encode(media_type,forKey:"media_type")
        aCoder.encode(release_date_uts,forKey:"release_date_uts")
        aCoder.encode(catalog_object,forKey:"catalog_object")
        aCoder.encode(item_caption,forKey:"item_caption")
        aCoder.encode(item_additional_data,forKey:"item_additional_data")
        aCoder.encode(itemType,forKey:"itemType")

        aCoder.encode(start_time_uts,forKey:"start_time_uts")
        aCoder.encode(stop_time_uts,forKey:"stop_time_uts")
        aCoder.encode(channel_logo,forKey:"channel_logo")
        aCoder.encode(items,forKey:"items")
        aCoder.encode(preview,forKey:"preview")
        aCoder.encode(episode_number,forKey:"episode_number")
        aCoder.encode(isShowAllItem,forKey:"isShowAllItem")

        
    }
    
    required convenience public init(coder aDecoder: NSCoder) {
        
        self.init()
        self.title                  = aDecoder.decodeObject(forKey: "title") as? String
        self.content_id             = aDecoder.decodeObject(forKey: "content_id") as? String
        self.catalog_id             = aDecoder.decodeObject(forKey: "catalog_id") as? String
        self.theme                  = aDecoder.decodeObject(forKey: "theme") as? String
        self.sequence_no            = aDecoder.decodeObject(forKey: "sequence_no") as? NSNumber
        self.language               = aDecoder.decodeObject(forKey: "language") as? String
        self.genres                 = aDecoder.decodeObject(forKey: "genres") as? [String]
        self.description_string     = aDecoder.decodeObject(forKey: "description_string") as? String
        self.play_url               = aDecoder.decodeObject(forKey: "play_url") as? OTTPlayURL
        self.thumbnails             = aDecoder.decodeObject(forKey: "thumbnails") as? OTTImageURLs
        self.subcategory_id         = aDecoder.decodeObject(forKey: "subcategory_id") as? String
        self.show_theme_id          = aDecoder.decodeObject(forKey: "show_theme_id") as? String
        self.rating                 = aDecoder.decodeObject(forKey: "rating") as? NSNumber
        self.like_count             = aDecoder.decodeObject(forKey:"like_count") as? NSNumber
        self.view_count             = aDecoder.decodeObject(forKey: "view_count") as? NSNumber
        self.no_of_user_rated       = aDecoder.decodeObject(forKey: "no_of_user_rated") as? NSNumber
        self.catalog_name           = aDecoder.decodeObject(forKey: "catalog_name") as? String
        self.show_name              = aDecoder.decodeObject(forKey: "show_name") as? String
        self.subcategory_name       = aDecoder.decodeObject(forKey: "subcategory_name") as? String
        self.short_description      = aDecoder.decodeObject(forKey: "short_description") as? String
        self.release_date_string    = aDecoder.decodeObject(forKey: "release_date_string") as? String
        self.duration_string        = aDecoder.decodeObject(forKey: "duration_string") as? String
        self.share_url              = aDecoder.decodeObject(forKey: "share_url") as? String
        self.listitem_id            = aDecoder.decodeObject(forKey: "listitem_id") as? String
        self.isSelected             = false
        self.episode_count          = aDecoder.decodeObject(forKey: "episode_count") as? NSNumber
        self.episodetype_tags       = aDecoder.decodeObject(forKey: "episodetype_tags") as? [String]
        self.people                 = aDecoder.decodeObject(forKey: "people") as? [OTTPeople]
        self.access_control         = aDecoder.decodeObject(forKey: "access_control") as? OTTAccessControl
        self.media_type             = aDecoder.decodeObject(forKey: "media_type") as? String
        self.release_date_uts       = aDecoder.decodeObject(forKey: "release_date_uts") as? Double
        self.catalog_object         = aDecoder.decodeObject(forKey: "catalog_object") as? CatalogListItem

        self.item_caption           = aDecoder.decodeObject(forKey: "item_caption") as? String
        self.item_additional_data   = aDecoder.decodeObject(forKey: "item_additional_data") as? [AdditionMetaData]
        self.itemType               = aDecoder.decodeObject(forKey: "itemType") as? ItemType

        self.start_time_uts         = aDecoder.decodeObject(forKey: "start_time_uts") as? Double
        self.stop_time_uts          = aDecoder.decodeObject(forKey: "stop_time_uts") as? Double
        self.channel_logo           = aDecoder.decodeObject(forKey: "channel_logo") as? OTTImageURLs
        self.items                  = aDecoder.decodeObject(forKey: "items") as? [OTTVideoItem]

        self.play_back_time         = aDecoder.decodeObject(forKey: "play_back_time") as? String
        self.total_percentage       = aDecoder.decodeObject(forKey: "total_percentage") as? Double
        self.preview                = aDecoder.decodeBool(forKey: "preview") as? PreviewModel
        self.episode_number                = aDecoder.decodeBool(forKey: "episode_number") as? String
        self.isShowAllItem                = aDecoder.decodeBool(forKey: "isShowAllItem") as Bool


   }
    
    public class func getShowAllItem() -> OTTVideoItem {
    
    let item  = OTTVideoItem()
    item.isShowAllItem = true
    return item
    }
    
    
    public class func Parse(jsonDictionary:[String:Any]) -> OTTVideoItem?
    {
        var object = OTTVideoItem()
        
        object.catalog_id         = jsonDictionary.valueFor(key: "catalog_id", isMandatory: false)

        object.catalog_name       = jsonDictionary.valueFor(key: "catalog_name", isMandatory: false)
        object.content_id         = jsonDictionary.valueFor(key: "content_id", isMandatory: false)
        object.description_string = jsonDictionary.valueFor(key: "description", isMandatory: false)
        object.duration_string    = jsonDictionary.valueFor(key: "duration_string", isMandatory: false)
        object.genres             = jsonDictionary.valueFor(key: "genres", isMandatory: false)
        object.language           = jsonDictionary.valueFor(key: "language", isMandatory: false)
        object.like_count         = jsonDictionary.valueFor(key: "like_count", isMandatory: false)
        object.no_of_user_rated   = jsonDictionary.valueFor(key: "no_of_user_rated", isMandatory: false)
        object.listitem_id        = jsonDictionary.valueFor(key: "listitem_id", isMandatory: false)
        object.episode_type       = jsonDictionary.valueFor(key: "episode_type", isMandatory: false)
        object.media_type         = jsonDictionary.valueFor(key: "media_type", isMandatory: false)
        object.release_date_uts   = jsonDictionary.valueFor(key: "release_date_uts", isMandatory: false)
        object.episode_number     = jsonDictionary.valueFor(key: "episode_number", isMandatory: false)

        object.item_caption       = jsonDictionary.valueFor(key: "item_caption", isMandatory: false)

        object.start_time_uts     = jsonDictionary.valueFor(key: "start_time_uts", isMandatory: false)
        object.stop_time_uts      = jsonDictionary.valueFor(key: "stop_time_uts", isMandatory: false)

        if let value:[String:[Any]] = jsonDictionary.valueFor(key: "item_additional_data", isMandatory: false)
        {
            object.item_additional_data = AdditionMetaData.Parse(dict: value)
            AppUtilities.printLog(message:"Additional Meta Data: \(object.title) :: \(object.item_additional_data)")
        }


        if let value:[String:Any]  = jsonDictionary.valueFor(key: "access_control", isMandatory: false)
        {
            object.access_control       = OTTAccessControl.Parse(json: value)
        }
        
         if let value:[[String:Any]] = jsonDictionary.valueFor(key: "people", isMandatory: false)
         {
            var pArray = [OTTPeople]()
            var index = 0
            for item in value
            {
                let p = OTTPeople.Parse(jsonDictionary: item, sortid: Int64(index))
                index+=1
                pArray.append(p)
            }
            
            object.people = pArray
         }
        
        
        if let value:[[String:Any]] = jsonDictionary.valueFor(key: "episodetype_tags", isMandatory: false)
        {
            var list = [String]()
            for item in value
            {
                if let name:String = item.valueFor(key: "name", isMandatory: false)
                {
                    list.append(name)
                }
            }
            
            object.episodetype_tags = list
        }

        object.rating               = jsonDictionary.valueFor(key: "rating", isMandatory: false)
        object.release_date_string  = jsonDictionary.valueFor(key: "release_date_string", isMandatory: false)
        
        if let value:NSNumber = jsonDictionary.valueFor(key: "sequence_no", isMandatory: false)
        {
             object.sequence_no  = value
        }

        object.share_url            = jsonDictionary.valueFor(key: "share_url", isMandatory: false)
        object.short_description    = jsonDictionary.valueFor(key: "short_description", isMandatory: false)
        object.show_name            = jsonDictionary.valueFor(key: "show_name", isMandatory: false)
        object.show_theme_id        = jsonDictionary.valueFor(key: "show_theme_id", isMandatory: false)
        object.subcategory_id       = jsonDictionary.valueFor(key: "subcategory_id", isMandatory: false)

        if let value:[String:Any] = jsonDictionary.valueFor(key: "catalog_object", isMandatory: false)
        {
              object.catalog_object       = CatalogListItem.Parse(jsonDictionary: value)
        }

        if let count:NSNumber = jsonDictionary.valueFor(key: "episode_count", isMandatory:false)
            {
                object.episode_count = count

        }
        
        object.subcategory_name = jsonDictionary.valueFor(key: "subcategory_name", isMandatory: false)
        object.theme            = jsonDictionary.valueFor(key: "theme", isMandatory: false)
        
       var theme = ""
        
        if let thm = object.theme
        {
            theme = thm
        }


        
        if let value:String = jsonDictionary.valueFor(key:"play_url_type", isMandatory: false)
        {
            object.play_url_type  = value
            if let dict:[String:Any] = jsonDictionary.valueFor(key:"play_url", isMandatory: false)
            {
                object.play_url = OTTPlayURL.Parse(jsonDictionary: dict, playURLTypeString: value)
            }
        }

        
        
        if let thumbnailjson:[String:Any] = jsonDictionary.valueFor(key:"thumbnails", isMandatory: false)
        {
            object.thumbnails   = OTTImageURLs.Parse(jsonDictionary: thumbnailjson)
        }

        if let thumbnailjson:[String:Any] = jsonDictionary.valueFor(key:"channel_logo", isMandatory: false)
        {
            object.channel_logo   = OTTImageURLs.Parse(jsonDictionary: thumbnailjson)
        }
        
        object.title        = jsonDictionary.valueFor(key: "title", isMandatory: false)
        object.view_count   = jsonDictionary.valueFor(key: "view_count", isMandatory: false)

        object.itemType = ItemType.GetItemType(theme: object.theme, layout_type: object.catalog_object?.layout_type)

        if object.itemType == nil
        {
            print("Bad Item Type = Title : \(object.title) :: \(object.catalog_id) :: \(object.content_id) :: \(object.theme) :: \(object.catalog_object?.layout_type)")
        }

        object.play_back_time   = jsonDictionary.valueFor(key: "play_back_time", isMandatory: false)
        object.total_percentage = jsonDictionary.valueFor(key: "total_percentage", isMandatory: false)

        if let type = object.itemType
        {
            if type == .linear
            {
                object.items = commonVideoListParser(jsonDictionary:jsonDictionary,key: "programs")
            }

        }

        if let value:[String:Any] = jsonDictionary.valueFor(key: "preview")
        {
            object.preview = PreviewModel.Parse(json: value)
        }

        return object
        
    }

    public class func commonVideoListParser(jsonDictionary:[String:Any],key:String) -> [OTTVideoItem]?
    {

        var videoItems = [OTTVideoItem]()

        guard  let videoList:[[String:Any]] = jsonDictionary.valueFor(key: key, isMandatory: false)
        else{
           return nil
        }

        for item in videoList
        {
            guard let videoItem:OTTVideoItem = OTTVideoItem.Parse(jsonDictionary: item) else {continue}

            videoItems.append(videoItem)
        }

        return videoItems

    }

    
    public func getReleaseDateYear() -> String?
    {
        
        guard let releaseDate = self.release_date_string else {
            return nil
        }
        
        guard releaseDate.characters.count >= 4 else { return nil }
        
        let startIndex  = releaseDate.index(releaseDate.startIndex, offsetBy: 4)
        let year        = releaseDate.substring(to: startIndex)
        
        return year
    }
    
    public  func getDurationInSeconds() -> Int
    {
        
        guard let duration = duration_string else {return 0}

        let durationArray = duration.components(separatedBy: ":")

        var index = 3

        var totalSeconds = 0

        for item in durationArray
        {
            var itemNum = 0
            if let value = Int(item)
            {
                itemNum = value
            }

            if index == 3 {totalSeconds = totalSeconds + itemNum * 60 * 60 }
            if index == 2 {totalSeconds = totalSeconds + itemNum * 60 }
            if index == 1 {totalSeconds = totalSeconds + itemNum }

            index = index - 1
        }
        
       return totalSeconds
        
    }


    
    
    
    public  func getDurationInMinutes() -> Int
    {
        
        let durationInSeconds = getDurationInSeconds()
         return durationInSeconds / 60

        
    }
    
    public  func getDurationInHHmm() -> (hr:Int,min:Int)
    {
        
        guard let durationinMinutes = duration_string else {return (0,0)}
        
        guard let durationInt = Double(durationinMinutes)
            else
        {
            return (0,0)
        }
        
        let minutes = Int(durationInt)/60
        
        let hr = minutes/60
        let min = minutes%60
        
        return (hr,min)
    }
    
    public func getWatchTimeForVideo(managedObjectContext:NSManagedObjectContext?) -> Int
    {
        guard let moc  = managedObjectContext
        else {
            return 0
        }
        
        guard let catalogid = self.catalog_id else{
            return 0
        }
        
        guard let contentid = self.content_id else{
            return 0
        }
        
        var watchTime = 0
        if let watchHistoryItem                = OTTPlaylist.getPlayListItemFor(contentId: contentid, catalogId: catalogid,playlistType: PlayListManagerConfig.watchHistoryPlaylistType,managedObjectContext:moc)
        {
            watchTime = watchHistoryItem.getWatchHistory()
            
        }
        return watchTime
    }

    public  func getFirstGenre() -> String?
    {
        guard  let genresValue = self.genres else {
            return nil
        }

        if genresValue.count == 0 {return nil}

        return genresValue.first
    }
    
  
   
}
