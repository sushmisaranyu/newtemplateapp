//
//  CountryCode.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 01/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class CountryCode
{
   public var countryRegion: String?
   public var countryCode: String?
   public var callingCode: String?
   public var maxDigits: NSNumber?
   public var minDigits: NSNumber?


    public class func getIndiaCountryCode() -> CountryCode
    {
        let item = CountryCode()
        item.callingCode    = "91"
        item.countryCode    = "IN"
        item.countryRegion  = "India"
        item.minDigits      = 10
        item.maxDigits      = 10
        return item
    }

}
