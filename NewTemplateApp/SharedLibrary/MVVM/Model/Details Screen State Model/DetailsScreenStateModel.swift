//
//  DetailsScreenStateModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 23/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class DetailsScreenStateModel
{


    static let  watchHistoryKey = "Watch History"
    static let  watchLaterKey   = "Watch Later"


    public var contentId:String
    public var catalogId:String


    public var is_subscribed:Bool
    public var watchHistoryposition:Double = 0.0
    public var watchLaterListItemId:String? = ""

    public var gender:String = ""
    public var user_period:String = ""
    public var analytics_user_id:String = ""
    public var user_state:String = ""
    public var age:String = ""


    //TODO
    public var user_plan:String = ""
    public var user_plan_type: String = ""


    public init(contentId:String,catalogId:String)
    {
        is_subscribed = false
        watchHistoryposition = 0.0

        self.contentId = contentId
        self.catalogId = catalogId
    }

    public class func Parse(json:[String:Any],contentId:String,catalogId:String) -> DetailsScreenStateModel
    {
        var model = DetailsScreenStateModel(contentId: contentId,catalogId:catalogId)

        guard let data:[String:Any] = json.valueFor(key: "data", isMandatory: false) else {return model}


        if let value:Bool = data.valueFor(key: "is_subscribed", isMandatory: false)
        {
            model.is_subscribed = value
        }


        guard let user_info:[String:Any] = data.valueFor(key: "user_info", isMandatory: false) else {return model}

        if let value:String = user_info.valueFor(key: "user_period", isMandatory: false)
        {
            model.user_period = value
        }

        if let value:String = user_info.valueFor(key: "analytics_user_id", isMandatory: false)
        {
            model.analytics_user_id = value
        }


        if let value:String = user_info.valueFor(key: "gender", isMandatory: false)
        {
            model.gender = value
        }

        if let value:String = user_info.valueFor(key: "user_state", isMandatory: false)
        {
            model.user_state = value
        }

        if let value:String = user_info.valueFor(key: "age", isMandatory: false)
        {
            model.age = value
        }

        if let value:String = user_info.valueFor(key: "user_pack_name", isMandatory: false)
        {
            model.user_plan = value
        }

        if let value:String = user_info.valueFor(key: "user_plan_type", isMandatory: false)
        {
            model.user_plan_type = value
        }

//        if let plan_info:[[String:Any]] = user_info.valueFor(key: "user_plan_info", isMandatory: false)
//        {
//            if let firstItem = plan_info.first{
//
//
//            }
//        }


        guard let items:[[String:Any]] = data.valueFor(key: "playlists", isMandatory: false) else { return model}


        for item in items
        {
            guard let name:String = item.valueFor(key: "name", isMandatory: false) else {continue}


            if name == DetailsScreenStateModel.watchHistoryKey
            {
                guard let pos:String = item.valueFor(key: "pos", isMandatory: false) else {continue}

                model.watchHistoryposition = Double(AppUtilities.getSecondsFrom_HH_MM_SS(timeString: pos))
            }

            if name == DetailsScreenStateModel.watchLaterKey
            {
                model.watchLaterListItemId =  item.valueFor(key: "listitem_id", isMandatory: false)
            }
        }



        return model
    }

    public func checkIfSame(contentId:String?,catalogId:String?) -> Bool
    {
        guard let content = contentId else {return false}
        guard let catalog = catalogId else {return false}

        if self.contentId == content && self.catalogId == catalog {return true}

        return false 
    }

    public func hasWatchListItem() -> Bool
    {
        return !StringHelper.isNilOrEmpty(string: self.watchLaterListItemId)
    }
}
