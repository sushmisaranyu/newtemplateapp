//
//  PurchasedPlanModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 07/12/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class PurchasedPlanModel
{
    public var id: String?
    public var plan_id: String?
    public var category: String?
    public var transaction_id:String?
    public var price_charged:String?
    public var currency:String?
    public var valid_till_uts:Double = 0
    public var subscription_title:String?
    public var plan_title:String?
    public var plan_status:Bool = false
    public var auto_renew:Bool = false
    public var payment_gateway:String?
    public var device_info:String?
    public var plan_categories:[String]?
    public var layout_scheme:String?

    public var start_date:String?
    public var valid_till:String?


    public class func Parse(response:[String:Any]) -> [PurchasedPlanModel]
    {

        var planList = [PurchasedPlanModel]()

//        guard  let data:[[String:Any]] = response.valueFor(key: "data") else {
//            return planList
//        }

        var itemList:[[String:Any]] =  [[String:Any]]()

        if let active:[[String:Any]] = response.valueFor(key: "current_active_plans")
        {
            itemList.append(contentsOf: active)
        }

        if let previous:[[String:Any]] = response.valueFor(key: "previous_plans")
        {
            itemList.append(contentsOf:previous)
        }

        for item in itemList
        {
            let plan = PurchasedPlanModel()

            plan.auto_renew = false
            if let value:String =  item.valueFor(key: "auto_renew")
            {
                if value == true.description
                {
                    plan.auto_renew = true
                }
            }

            plan.plan_status = false
            if let value:String =  item.valueFor(key: "plan_status")
            {
                if value == "active"
                {
                    plan.plan_status = true
                }
            }

            plan.layout_scheme = "all"

            if let value:[String] =  item.valueFor(key: "plan_categories")
            {
                plan.plan_categories = value
                
                if let first = value.first
                {
                    plan.layout_scheme = first

                    if first == "all_access_pack" || first == "all"
                    {
                        plan.layout_scheme = "all"
                    }
                }
            
            }
            

            plan.category           = item.valueFor(key: "category")
            plan.currency           = item.valueFor(key: "currency")
            plan.device_info        = item.valueFor(key: "device_info")
            plan.id                 = item.valueFor(key: "id")

            plan.payment_gateway    = item.valueFor(key: "payment_gateway")
            plan.plan_id            = item.valueFor(key: "plan_id")
            plan.id                 = item.valueFor(key: "id")

            plan.plan_title         = item.valueFor(key: "plan_title")
            plan.price_charged      = item.valueFor(key: "price_charged")

            plan.subscription_title = item.valueFor(key: "subscription_title")
            plan.transaction_id     = item.valueFor(key: "transaction_id")


            if let value:Double     = item.valueFor(key: "valid_till_uts")
            {
                plan.valid_till_uts     = value
            }

            plan.start_date         = item.valueFor(key: "start_date")
            plan.valid_till         = item.valueFor(key: "valid_till")

            planList.append(plan)
        }

        return planList
    }

    public func getPlanDuration() -> String
    {
        guard  let start = self.start_date else {return ""}
        let startDate = AppUtilities.convertDateFormater(date: start)
         guard  let end = self.valid_till else {return ""}
        let validTill = AppUtilities.convertDateFormater(date: end)

         let s = AppUtilities.epocTimeToNormalDateForSubscription(timeInterval: startDate)
         let e = AppUtilities.epocTimeToNormalDateForSubscription(timeInterval: validTill)

        return s + " - " + e
    }

}


