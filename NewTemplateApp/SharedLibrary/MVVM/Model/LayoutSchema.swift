//
//  LayoutSchema.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 11/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import UIKit

public class GradientColors
{
    public var startColor:UIColor = UIColor.black
    public var endColor:UIColor = UIColor.black
    public var image:String?

}

public class LayoutSchema
{
    static var schemas:[String:GradientColors] = [String:GradientColors]()



    public class func Parse(json:[[String:Any]])
    {
        schemas = [String:GradientColors]()
        for item in json
        {
            guard let name:String = item.valueFor(key: "scheme") else {continue}
            guard let start_color:String = item.valueFor(key: "start_color") else {continue}
            guard let end_color:String = item.valueFor(key: "end_color") else {continue}




            let gColor = GradientColors()

            if let imageURL:String = item.valueFor(key: "image_url")
            {
                gColor.image = imageURL
            }

            if let value = UIColor(hexString: start_color)
            {
                gColor.startColor = value
            }

            if let value = UIColor(hexString: end_color)
            {
                gColor.endColor = value
            }

            if StringHelper.isNilOrEmpty(string: name) == false
            {
                schemas[name] = gColor
            }


            
        }
    }

    public  class func getGradientFor(schemaKey:String?) -> GradientColors
    {
        var color = GradientColors()


        guard let key = schemaKey else {return color}


        if let value = schemas[key]
        {
            color = value
        }

        return color

    }

    public class func Save()
    {
        UserDefaults.standard.set(schemas, forKey: "LayoutSchema")
        UserDefaults.standard.synchronize()
    }

    public class func getLayoutSchema()
    {
        let value = UserDefaults.standard.object(forKey: "LayoutSchema") as Any?

        if let schValue = value as? [String:GradientColors]
        {
            schemas = schValue
        }
    }

    public static func remove() {
        UserDefaults.standard.removeObject(forKey: "LayoutSchema")
        UserDefaults.standard.synchronize()
    }

}
