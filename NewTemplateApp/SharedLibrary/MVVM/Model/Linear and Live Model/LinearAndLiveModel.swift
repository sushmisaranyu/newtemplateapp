//
//  LinearAndLiveModel.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 25/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class LinearAndLiveModel
{

    public var item:OTTVideoItem?
    public var list:[OTTVideoItem]?

    public init(item:OTTVideoItem?, list:[OTTVideoItem]?)
    {
        self.item = item
        self.list = list
        
    }
}
