//
//  ProfileUser.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 19/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class ProfileUser
{
     public var age: String?
     public var firstName: String?
     public var isChild: Bool
     public var lastName: String?
     public var profileId: String?
     public var default_user:Bool = false

    public var adduserFlag:Bool = false

    public init(profileId:String, firstName:String?, lastName:String?, age:String?, isChild:Bool,default_user:Bool)
    {
        self.age            = age
        self.firstName      = firstName
        self.isChild        = isChild
        self.lastName       = lastName
        self.profileId      = profileId
        self.default_user   = default_user

    }

    public class func defaultUser() -> ProfileUser
    {
        let user = ProfileUser(profileId: "", firstName: "", lastName: "", age: "", isChild: false, default_user: false)

        return user
    }

  
}
