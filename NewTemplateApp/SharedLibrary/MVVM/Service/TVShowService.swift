//
//  TVShowService.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 26/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class TVShowService
{

    public static let episodeThemeKey = "episode"

    public  class func getEpisodesForShow(showId:String, responseHandler: @escaping ((_ response: DataLoader<[OTTVideoItem]>) -> Void))
    {
        let service = CatalogAPI(api: .GetEpisodesForShow(showId: showId))

        service.apiRequest(responseHandler: { response in

            switch response
            {
            case .success(let responseJson):

                if let episodeList = CommonEpisodeParser(response: responseJson)
                {
                    responseHandler(DataLoader.success(response: episodeList))
                }
                else
                {
                    responseHandler(DataLoaderErrorHelper.getBadResponse())
                    return
                }
            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }

        })
    }

    public class func CommonEpisodeParser(response:[String:Any]) -> [OTTVideoItem]?
    {
        //Basic Json Validation
        guard let dataJson:[String:Any] = response.valueFor(key: "data", isMandatory: false) else { return nil  }

        guard let itemJson:[[String:Any]] = dataJson.valueFor(key: "items", isMandatory: false) else { return nil }

        guard let catalogItemJson:[String:Any] = dataJson.valueFor(key: "catalog_object", isMandatory: false) else {return nil}

        guard let catalogObject = CatalogListItem.Parse(jsonDictionary: catalogItemJson) else {return nil}

        //Parsing Video Item
        var videoList = [OTTVideoItem]()
        for item in itemJson
        {
            guard let _:String = item.valueFor(key: "content_id", isMandatory: false)
                else
            {
                continue
            }

            guard let videoItem = OTTVideoItem.Parse(jsonDictionary: item) else { continue }

            videoItem.theme = TVShowService.episodeThemeKey
            videoItem.catalog_object = catalogObject
            videoItem.itemType = ItemType.GetItemType(theme: videoItem.theme, layout_type: videoItem.catalog_object?.layout_type)
            videoList.append(videoItem)
        }


        //Sorting Video Items by Sequence No
        let sortedArray = videoList.sorted(by: { ($0.sequence_no?.intValue)! < ($1.sequence_no?.intValue)! })
        return sortedArray
    }
}
