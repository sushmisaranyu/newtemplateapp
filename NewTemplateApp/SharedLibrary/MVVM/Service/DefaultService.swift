//
//  DefaultService.swift
//  SaraynuLibrary
//
//  Created by Ashley Dsouza on 26/06/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation


public enum AppUpdateState
{
    case forcedUpdate
    case softUpdate
    case noUpdate


    public static func Parse(appVersion:String,json:[String:Any]) -> AppUpdateState?
    {
        guard let data:[String:Any] = json.valueFor(key: "data", isMandatory: false) else {return nil}

        guard let params_hash2:[String:Any] = data.valueFor(key: "params_hash2", isMandatory: false) else {return nil}

        guard let config_params:[String:Any] = params_hash2.valueFor(key: "config_params", isMandatory: false)  else {return nil}

        let configStatus = OTTConfig.Parse(json: config_params)

        if configStatus == false
        {
            return nil
        }

        let appConfigUpdate = AppUpdateConfig.Parse(version: appVersion, json: config_params)

        return appConfigUpdate.status
    }
}

public class DefaultService
{

    public  func GetRegion(responseHandler:@escaping ((_ response: DataLoader<Void>) -> Void))
    {
        let service = DefaultAPI(api: .GetRegion)

        service.apiRequest(responseHandler: { [weak self]  response in
            switch response
            {
            case .success(let responseJson):  let state = AppUserDefaults.ParseRegionJson(json: responseJson)

            if state == true
            {
                responseHandler(DataLoader.success(response: ()))
            }
            else
            {
                responseHandler(DataLoaderErrorHelper.getBadResponse())
                }

            default : DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }
        })

    }


    public  func GetServerURl(responseHandler:@escaping ((_ response: DataLoader<Void>) -> Void))
    {
        let service = DefaultAPI(api: .GetServerURLs)
        service.apiRequest(responseHandler: { [weak self]  response in
            switch response
            {
            case .success(let responseJson): let state = ServerUserDefaults.Parse(json:responseJson)
            if state == true
            {
                responseHandler(DataLoader.success(response: ()))
            }
            else
            {
                responseHandler(DataLoaderErrorHelper.getBadResponse())
                }
            default : DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }
        })

    }


    public  func GetConfig(version:String, responseHandler:@escaping ((_ response: DataLoader<AppUpdateInfo>) -> Void))
    {
        let service = DefaultAPI(api: .GetConfig(version:version))

        service.apiRequest(responseHandler: {[weak self]  response in

            switch response
            {
            case .success(let responseJson):
                guard let data:[String:Any] = responseJson.valueFor(key: "data", isMandatory: false)
                    else
                {
                    responseHandler(DataLoaderErrorHelper.getBadResponse())
                    return
                }




                guard let params_hash2:[String:Any] = data.valueFor(key: "params_hash2", isMandatory: false)
                    else
                {
                    responseHandler(DataLoaderErrorHelper.getBadResponse())
                    return
                }

                guard let config_params:[String:Any] = params_hash2.valueFor(key: "config_params", isMandatory: false)
                    else
                {
                    responseHandler(DataLoaderErrorHelper.getBadResponse())
                    return
                }

                if let analytics:[String:Any] = config_params.valueFor(key: "analytics", isMandatory: false)
                {
                    if let clevertap_mediaevents:Bool = analytics.valueFor(key: "clevertap_mediaevents", isMandatory: false)
                    {
                        AppUserDefaults.isCleverTapEventEnabled.setValue(clevertap_mediaevents)
                    }
                }



                guard let layoutSchema:[[String:Any]] = config_params.valueFor(key: "layout_scheme", isMandatory: false)
                    else
                {
                    responseHandler(DataLoaderErrorHelper.getBadResponse())
                    return
                }


                guard let webPages:[String:Any] = config_params.valueFor(key: "web_pages") else{ responseHandler(DataLoaderErrorHelper.getBadResponse())
                    return

                }

                if let value:String = webPages.valueFor(key: "faq")
                {
                    AppUserDefaults.faq.setValue(value)
                }

                if let value:String = webPages.valueFor(key: "terms_conditions")
                {
                    AppUserDefaults.termsOfConditionURL.setValue(value)
                }

                if let value:String = webPages.valueFor(key: "privacy_policy")
                {
                    AppUserDefaults.privacyURL.setValue(value)
                }

                if let value:String = webPages.valueFor(key: "contact_us")
                {
                    AppUserDefaults.contactUSURL.setValue(value)
                }



                let configStatus = OTTConfig.Parse(json: config_params)
                LayoutSchema.Parse(json: layoutSchema)
                let appConfigUpdate = AppUpdateConfig.Parse(version: version, json: config_params)


                guard let app_version:[String:Any] = data.valueFor(key: "app_version", isMandatory: false) else {
                    responseHandler(DataLoaderErrorHelper.getBadResponse())
                    return
                }

                let updateInfo = AppUpdateInfo.Parse(json: app_version)
                responseHandler(DataLoader.success(response: updateInfo))




            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }

        })
    }


    public  func getCountryCodeList(responseHandler:@escaping ((_ response: DataLoader<Void>) -> Void))
    {
        let service = DefaultAPI(api: .CountryCode)

        service.apiRequest(responseHandler: {[weak self] response in

            switch response
            {
            case .success(let responseJson):
                guard let data:[[String:Any]] = responseJson.valueFor(key: "data", isMandatory: false)
                    else
                {
                    responseHandler(DataLoaderErrorHelper.getBadResponse())
                    return
                }
                
                OTTCountryCodeList.DeleteAllCodes()
                for item in data
                {
                    OTTCountryCodeList.ParseCountryCodeDictionary(jsonDictionary: item)
                }

            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }

        })
    }


    //Archive
    public class func GetRegion(successHandler:@escaping (()->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        let service = DefaultAPI(api: .GetRegion)
        
        service.call(successHandler: { response in


            
            guard let region:[String:Any] = response.valueFor(key: "region", isMandatory: false)
                else {
                    errorHandler("","Bad response")
                    return
            }

            guard let countryRegion:String = region.valueFor(key: "country_code2", isMandatory: false)
                else
            {
                errorHandler("","Bad response")
                return
            }
            AppUserDefaults.detectedRegion.setValue(countryRegion)

            guard let countriCode:String = region.valueFor(key: "calling_code")
                else
            {
                errorHandler("","Bad response")
                return
                
            }
            AppUserDefaults.callingCode.setValue(countriCode)

            guard let countryName:String = region.valueFor(key: "country_name")
                else{
                    errorHandler("","Bad response")
                    return
            }
            AppUserDefaults.countryName.setValue(countryName)
            
            
            successHandler()
        }, errorHandler: { code , message in
            
            
            errorHandler("",message)
            
        } , internetErrorHandler: {code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
        
        
    }

    public class func GetServerURl(successHandler:@escaping (()->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        let service = DefaultAPI(api: .GetServerURLs)
        
        service.call(successHandler: { response in
            
            //TODO: Parsing URl
            guard let data:[String:Any] = response.valueFor(key: "data", isMandatory: false)
                else
            {
                errorHandler("","Bad response from server")
                return
            }
            
            guard let services:[[String:Any]] = data.valueFor(key: "services", isMandatory: false)
                else
            {
                errorHandler("","Bad response from server")
                return
            }
            
            for item in services
            {
                guard let name:String = item.valueFor(key: "name", isMandatory: false)
                    else
                {
                    errorHandler("","Bad Response")
                    return
                }
                
                guard let dns:String = item.valueFor(key: "dns", isMandatory: false)
                    else
                {
                    errorHandler("","Bad Response")
                    return
                }
                
                switch name
                {
                case "catalog": ServerUserDefaults.catalogs_ServerURL.setValue(dns)
                case "users": ServerUserDefaults.users_ServerURl.setValue(dns)
                case "subscriptions": ServerUserDefaults.subscriptions_ServerURL.setValue(dns)
                case "search": ServerUserDefaults.search_ServerURL.setValue(dns)
                default: break
                }
            }
            
            
            
            successHandler()
        }, errorHandler: { code , message in
            
            
            errorHandler("",message)
            
        } , internetErrorHandler: {code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
        
        
    }



    
    public  class func fetchCountryCode(successHandler:@escaping(()->Void),successHandlerWithArray:@escaping((_ responseArray: [[String:Any]])->Void),errorHandler:@escaping((_ title:String,_ message:String)->Void),internetErrorHandler:@escaping((_ title:String,_ message:String)->Void))
    {
        let defaultService = DefaultAPI(api: .CountryCode)
        
        defaultService.callforCountryCode(successHandler: { (response) in
            AppUtilities.printLog(message: "success")
            
        }, successHandlerWithArray: { (responseArray) in
            
            successHandlerWithArray(responseArray)
        }, errorHandler: { (title, message) in
            errorHandler("error loading country code",message)

        }, internetErrorHandler: { (code) in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)

        })
    }



    //To Delete

    public class func GetConfig(version:String, successHandler:@escaping ((_ updateState:AppUpdateInfo)->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        let service = DefaultAPI(api: .GetConfig(version:version))

        service.call(successHandler: { response in


            guard let data:[String:Any] = response.valueFor(key: "data", isMandatory: false)
                else
            {
                errorHandler("","Bad response from server")
                return
            }

            guard let params_hash2:[String:Any] = data.valueFor(key: "params_hash2", isMandatory: false)
                else
            {
                errorHandler("","Bad response from server")
                return
            }

            guard let config_params:[String:Any] = params_hash2.valueFor(key: "config_params", isMandatory: false)
                else
            {
                errorHandler("","Bad response from server")
                return
            }

            guard let app_version:[String:Any] = data.valueFor(key: "app_version", isMandatory: false) else {
                errorHandler("","Bad response from server")
                return
            }


            let configStatus = OTTConfig.Parse(json: config_params)
         //   let appUpdateState = AppUpdateConfig.Parse(version: version, json: config_params)

            let updateInfo = AppUpdateInfo.Parse(json: app_version)



            successHandler(updateInfo)

        }, errorHandler: { code , message in


            errorHandler("",message)

        } , internetErrorHandler: {code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })


    }
    
}
