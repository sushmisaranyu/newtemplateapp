//
//  PlaylistService.swift
//  cinestaan
//
//  Created by Ash Dz on 24/04/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation

import CoreData

public class PlaylistService
{
    public class func UpdateWatchHistory(catalogId:String,contentId:String,playbackTimeInSeconds:Double,responseHandler: @escaping ((_ response: DataLoader<Bool>) -> Void))
    {
        let playbackString = AppUtilities.formatSecondsToString(playbackTimeInSeconds)

        var service = UserAPI(api:.UpdateWatchHistory(catalogId: catalogId, contentId: contentId, playbackTime: playbackString))

        service.apiRequest { (response) in
            switch response
            {
            case .success(let responseJson):
                guard let data:[[String:Any]] = responseJson.valueFor(key: "data", isMandatory: false) else {
                    responseHandler(DataLoaderErrorHelper.getBadResponse())
                    return
                }

                for item in data
                {
                    guard  let contentid:String = item.valueFor(key: "content_id", isMandatory: false) else {continue}

                    guard  let catalogid:String = item.valueFor(key: "catalog_id", isMandatory: false) else {continue}

                    if contentid == contentId && catalogId == catalogId{

                        DispatchQueue.main.async {
                            let  managedObjectContext = NSManagedObjectContext.newObjectContext()
                            OTTPlaylist.updatePlayListItemFromWatchHistory(type:PlayListManagerConfig.watchHistoryPlaylistType,contentId:contentid,catalogId:catalogid,watchTime:playbackString,response:item,managedObjectContext:managedObjectContext)
                        }
                    }
                }

               responseHandler(DataLoader.success(response: true))

            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }
        }
    }

    public func GetPlayListFor(playListId:String,responseHandler: @escaping ((_ response: DataLoader<[OTTVideoItem]>) -> Void))
    {
        let service = UserAPI(api:.GetPlaylistWith(playlistId: playListId))

        service.apiRequest { [weak self] (response) in
            CommonCatalogResponseManager.CommonVideoItemListReseponseManager(response: response, listItemKey: "items", responseHandler: responseHandler)
        }


        

    }

    public class func AddItemsToPlayList(playListId:String,contentId:String,catalogId:String,responseHandler:@escaping ((_ response: DataLoader<String>) -> Void))
    {
        let service = UserAPI(api:.AddItemToList(playlistId: playListId, contentId: contentId, catalogId: catalogId))

        service.apiRequest { (response) in


             switch response
            {
            case .success(let responseJson):
                if let data:[[String:Any]] = responseJson.valueFor(key: "data", isMandatory: false) {
                    for item in data
                    {
                        if let listItemId: String = item.valueFor(key: "listitem_id", isMandatory: false ){
                            responseHandler(DataLoader.success(response: listItemId))
                            return
                        }
                    }
                }
                else
                {
                    if let data:[String:Any] = responseJson.valueFor(key: "data", isMandatory: false) {
                        if let listItemId: String = data.valueFor(key: "listitem_id", isMandatory: false ){
                                responseHandler(DataLoader.success(response: listItemId))
                                return
                        }

                    }
                }


                responseHandler(DataLoaderErrorHelper.getBadResponse())


            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }


        }
    }

    public class func RemoveItemsToPlayList(playListId:String,listItemID:String,responseHandler:@escaping ((_ response: DataLoader<Bool>) -> Void))
    {
        let service = UserAPI(api:.RemoveItemFromPlayList(playlistId: playListId, playListItemId: listItemID))

        service.apiRequest { (response) in
            DefaultResponseHandler.defaultBoolResponseHandeler(response: response, responseHandler: responseHandler)
        }
    }


    public class func GetPlayListFor(playListId:String,successHandler:@escaping (([OTTVideoItem])->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        let service = UserAPI(api:.GetPlaylistWith(playlistId: playListId))

        service.call(successHandler: { response in

            var videolist = [OTTVideoItem]()
            guard let data:[String:Any] = response.valueFor(key: "data", isMandatory: false)
                else
            {
                successHandler(videolist)
                return
            }

            guard let items:[Any] = data.valueFor(key: "items", isMandatory: false)
                else
            {
                successHandler(videolist)
                return
            }

            for json in items
            {
                if let itemJson:[String:Any] = json as? [String : Any]
                {
                    guard let contentID:String = itemJson.valueFor(key: "content_id", isMandatory: false)
                        else
                    {
                        continue
                    }

                    guard let video = OTTVideoItem.Parse(jsonDictionary: itemJson) else {continue}
                    videolist.append(video)
                }
            }

            successHandler(videolist)

        } ,
                     errorHandler: { code , message in
                        errorHandler("Profile",message)

        } , internetErrorHandler: {code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })



    }




    //MARK: Archive


    public class func GetAllPlayList(managedObjectContext:NSManagedObjectContext,successHandler:@escaping (()->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        
        let service = UserAPI(api:.GetAllPlayLists)
        
        service.call(successHandler: { response in
           
            
            guard let data:[String:Any] = response.valueFor(key: "data", isMandatory: false)
                else
            {
                let (title,message) = ErrorMessageManager.getDefaultErrorMessage()
                errorHandler(title,message)
                return
            }
            
            guard let items:[[String:Any]] = data.valueFor(key: "items", isMandatory: false)
                else
            {
                let (title,message) = ErrorMessageManager.getDefaultErrorMessage()
                errorHandler(title,message)
                return
            }
            
            OTTPlaylist.DeleteAllPlaylist()
            for item in items{
                OTTPlaylist.Parse(jsonDictionary: item,managedObjectContext: managedObjectContext)
            }
            
            //Parse response
            successHandler()
        }, errorHandler: { code , message in
            errorHandler("Profile",message)
            
        } , internetErrorHandler: {code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
            
        })
    }
    

    
    public class func CreatePlayList(playlistName:String, playlistType:String,managedObjectContext:NSManagedObjectContext,successHandler:@escaping (()->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        let service = UserAPI(api:.CreatePlayList(playListName: playlistName, playListType: playlistType))
        

        service.call(successHandler: { response in
            
            guard let data:[String:Any] = response.valueFor(key: "data", isMandatory: false)
                else
            {
                AppUtilities.printLog(message: "Error Creating playList : \(response)")
                let (title,message) = ErrorMessageManager.getDefaultErrorMessage()
                errorHandler(title,message)
                return
            }
            
            
            OTTPlaylist.Parse(jsonDictionary: data,managedObjectContext:managedObjectContext)
            successHandler()

            
        } ,
           errorHandler: { code , message in
                        errorHandler("Profile",message)
                        
        } , internetErrorHandler: {code in
             ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
        
        
    }
    
   
   public class func AddItemsToPlayList(playListId:String,contentId:String,catalogId:String,managedObjectContext:NSManagedObjectContext,successHandler:@escaping (()->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        let service = UserAPI(api:.AddItemToList(playlistId: playListId, contentId: contentId, catalogId: catalogId))
        
        service.call(successHandler: { response in
            
            //TODO: Update PlayList
            if let data:[String:Any] = response.valueFor(key: "data", isMandatory: false)
            {
               let item = OTTPlayListItem.Parse(jsonDictionary: data)
                
                OTTPlaylist.updatePlayListItemFromPlaylistwith(playlistId: playListId, item: item,managedObjectContext:managedObjectContext)
                

                //AppUtilities.printLog(message: "Added Item PlaylistId: \(playListId) : \(item)")
               successHandler()
               return
            }
            
            //Fallback when array is sent to device
            if let data:[[String:Any]] = response.valueFor(key: "data", isMandatory: false)
            {
                for item in data
                {
                    if let listId:String = item.valueFor(key: "listitem_id", isMandatory: false)
                    {
                        let item = OTTPlayListItem.Parse(jsonDictionary: item)
                        DispatchQueue.main.async {
                            OTTPlaylist.updatePlayListItemFromPlaylistwith(playlistId: playListId, item: item,managedObjectContext:managedObjectContext)
                                managedObjectContext.Save()
                            AppUtilities.printLog(message: "Added fallback Item PlaylistId: \(playListId) : \(item)")
                            successHandler()
                        }
                       
                        
                       
                    }
                }
            }
            else
            {
                 ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
                return
            }
            
            
           successHandler()
        }, errorHandler: { code , message in
            errorHandler("Profile",message)
            
        } , internetErrorHandler: {code in
             ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
            
        })
    }
    
  public class func RemoveItemFromPlayList(playListId:String,playListItemId:String,managedObjectContext:NSManagedObjectContext,successHandler:@escaping (()->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        var service = UserAPI(api:.RemoveItemFromPlayList(playlistId: playListId, playListItemId: playListItemId))
        service.call(successHandler: { response in
            
            DispatchQueue.main.async {
                
            OTTPlaylist.removePlayListItemFromPlaylistwith(playlistId: playListId, itemId: playListItemId,managedObjectContext:managedObjectContext)
                  managedObjectContext.Save()
                successHandler()
            }

        }, errorHandler: { code , message in
            errorHandler("Profile",message)
            
        } , internetErrorHandler: {code in
             ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
            
        })
    }
    
    

    
    public class func UpdateWatchHistorySlimWithoutDBSave(catalogId:String,contentId:String,playbackTimeInSeconds:Double,successHandler:@escaping (()->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        
        let playbackString = AppUtilities.formatSecondsToString(playbackTimeInSeconds)
        
        var service = UserAPI(api:.UpdateWatchHistory(catalogId: catalogId, contentId: contentId, playbackTime: playbackString))
        service.call(successHandler: { response in
            
            successHandler()
            
        }, errorHandler: { code , message in
            errorHandler("Watch History",message)
            
        } , internetErrorHandler: {code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
            
        })
    }
    
    
    public class func UpdateUserRatingSlimWithoutDBSave(catalogId:String,contentId:String,rating:Double,playlistType:String,successHandler:@escaping (()->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        
        
        
        var service = UserAPI(api:.UpdateUserRating(catalogId: catalogId, contentId: contentId, rating: String(rating), playListType: playlistType))
        service.call(successHandler: { response in
            
            successHandler()
            
        }, errorHandler: { code , message in
            errorHandler("Watch History",message)
            
        } , internetErrorHandler: {code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
            
        })
    }
    
    public class func UpdatePlaylistWithType(catalogId:String,contentId:String,playListType:String,managedObjectContext:NSManagedObjectContext,successHandler:@escaping (()->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        
       
        
        var service = UserAPI(api:.UpdatePlaylistWithType(catalogId: catalogId, contentId: contentId, playlistType: playListType))
        service.call(successHandler: { response in
            
            
            if let data:[String:Any] = response.valueFor(key: "data", isMandatory: false)
            {
                //print(data)
            }
            
            
            if let data:[[String:Any]] = response.valueFor(key: "data", isMandatory: false)
            {
                //print(data)
                
                for item in data{
                    
                    guard  let contentid:String = item.valueFor(key: "content_id", isMandatory: false)
                        else
                    {
                        continue
                    }
                    
                    guard  let catalogid:String = item.valueFor(key: "catalog_id", isMandatory: false)
                        else
                    {
                        continue
                    }
                    
                    if contentid == contentId && catalogId == catalogId{
                        OTTPlaylist.updatePlayListItemWithType(type:playListType,contentId:contentid,catalogId:catalogid,response:item,managedObjectContext:managedObjectContext)
                        
                        
                    }
                }
            }
            successHandler()
        }, errorHandler: { code , message in
            errorHandler("Watch History",message)
            
        } , internetErrorHandler: {code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
            
        })
    }
    
    public class func DeletePlaylist(playlistID:String,managedObjectContext:NSManagedObjectContext,successHandler:@escaping (()->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {

        var service = UserAPI(api:.DeletePlaylist(playListID: playlistID))
        service.call(successHandler: { response in
            if let playlist = OTTPlaylist.GetPlayListWith(playListId: playlistID, managedObjectContext: managedObjectContext)
            {
                managedObjectContext.delete(playlist)
                managedObjectContext.Save()
                
            }
            
            successHandler()
            
        }, errorHandler: { code , message in
            errorHandler("",message)
        } , internetErrorHandler: {code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
            
        })
    }

    
    public class func RenamePlayList(playlistName:String, playlistType:String,playlistId:String,successHandler:@escaping (()->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        let service = UserAPI(api:.Renameplaylist(playListID: playlistId, playlistName: playlistName, playlistType: playlistType))
        
        
        
        service.call(successHandler: { response in
            
            
            successHandler()
            
            
        } ,
                     errorHandler: { code , message in
                        errorHandler("Playlist",message)
                        
        } , internetErrorHandler: {code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
        
        
    }
    
    public class func deleteAllItemsInPlaylist(playListId:String,responseHandler:@escaping ((_ response: DataLoader<Bool>) -> Void))
    {
        let service = UserAPI(api:.DeletePlaylist(playListID: playListId))

        service.apiRequest { (response) in
            DefaultResponseHandler.defaultBoolResponseHandeler(response: response, responseHandler: responseHandler)
        }
    }

}
