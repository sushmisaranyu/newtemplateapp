//
//  ProfileService.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 19/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class ProfileService
{
    //MARK: PROFILE

    public class func addProfile(firstName:String,lastName:String,age:String,isChild:Bool,responseHandler: @escaping ((_ response: DataLoader<Bool>) -> Void))
    {
        let service = UserAPI(api:  .AddProfile(firstname: firstName, lastname: lastName, age: age, child: isChild))

        service.apiRequest { (response) in
            DefaultResponseHandler.defaultBoolResponseHandeler(response: response, responseHandler: responseHandler)
        }
    }
    public class func editProfile(firstName:String,lastName:String,age:String,isChild:Bool,profileID:String,responseHandler: @escaping ((_ response: DataLoader<Bool>) -> Void))
    {
        let service = UserAPI(api:  .EditProfile(firstname: firstName, lastname: lastName, age: age, child: isChild, profile_id: profileID))

        service.apiRequest { (response) in
            DefaultResponseHandler.defaultBoolResponseHandeler(response: response, responseHandler: responseHandler)
        }
    }

    public class func deleteProfile(profileID:String,responseHandler: @escaping ((_ response: DataLoader<Bool>) -> Void))
    {
        let service = UserAPI(api:  .DeleteProfile(profile_id : profileID))

        service.apiRequest { (response) in
            DefaultResponseHandler.defaultBoolResponseHandeler(response: response, responseHandler: responseHandler)
        }
    }

    public class func geAllProfiles(responseHandler: @escaping ((_ response: DataLoader<[ProfileUser]>) -> Void))
    {
        let service = UserAPI(api: .GetProfilesForAccount)

        service.apiRequest { (response) in

            switch response
            {
            case .success(let response):

                AppUserDefaults.ParseParentalControlFlag(json: response, dataKey: "data")

                let flag = OTTProfile.ParseArrays(jsonDictionary: response)
                if flag == false
                {
                    responseHandler(DataLoaderErrorHelper.getDefaultErrorDataLoader())
                }

                if let profileUsers = OTTProfile.getAllProfileUsers()
                {
                    responseHandler(DataLoader.success(response: profileUsers))
                }
                else
                {
                    responseHandler(DataLoaderErrorHelper.getDefaultErrorDataLoader())
                }
                break
            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }
        }
    }

    public class func bindProfile(profileID:String,responseHandler: @escaping ((_ response: DataLoader<Bool>) -> Void))
    {
        let service = UserAPI(api: .BindSessionToProfile(profile_id: profileID))

        service.apiRequest { (response) in
            switch response
            {
            case .success(let response):
                AppUserDefaults.profile_ID.setValue(profileID)
                responseHandler(DataLoader.success(response: true))
                break
            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }
        }
    }




    //MARK: END PROFILE
}
