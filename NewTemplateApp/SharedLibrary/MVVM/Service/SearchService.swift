//
//  SearchService.swift
//  SaraynuLibrary
//
//  Created by Ashley Dsouza on 26/06/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation


public class SearchService
{

    public class func getSearchResult(text:String,  responseHandler: @escaping ((_ response: DataLoader<[OTTVideoItem]>) -> Void))
    {
        var service = SearchAPI(api: .Search(text: text))

        service.apiRequest { response in
            switch response
            {
            case .success(let responseJson):
                guard let data:[String:Any] = responseJson.valueFor(key: "data", isMandatory: false)
                    else{
                        responseHandler(DataLoaderErrorHelper.getBadResponse())
                        return
                }

                guard let itemsJson:[[String:Any]] = data.valueFor(key: "items", isMandatory: false) else
                {
                    responseHandler(DataLoaderErrorHelper.getBadResponse())
                    return
                }

                var videoList = [OTTVideoItem]()
                for item in itemsJson
                {
                    guard let item =  OTTVideoItem.Parse(jsonDictionary: item) else { continue }
                    videoList.append(item)
                }


                responseHandler(DataLoader.success(response: videoList))

            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }
        }

    }
    


    //Archive
    public class  func GetAllVideosInGenre(genre:String,successHandler:@escaping ([OTTVideoItem])->Void,errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        
        
        var service = SearchAPI(api: .GetAllVideosInGenre(genre: genre))
        
        service.call(successHandler: {
            response in
            
            if let jsondictionary:[String:Any] = response.valueFor(key: "data", isMandatory: false)
            {
                
                var videoList = [OTTVideoItem]()
                
                
                if let cat:[[String:Any]] = jsondictionary.valueFor(key: "items", isMandatory: false)
                {
                    var index = 0
                    for item in cat
                    {
                        guard let item =  OTTVideoItem.Parse(jsonDictionary: item) else { continue }
                        videoList.append(item)
                    }
                    index+=1
                }
                successHandler(videoList)
                
            }
        }
            
            , errorHandler: {code, message in
                ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
        }, internetErrorHandler: {
            code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
        
    }

    
    public class  func SearchAutoComplete(textToAutoComplete:String,successHandler:@escaping ([String])->Void,errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        
       guard var textToSearch = textToAutoComplete.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        else
       {
            successHandler([String]())
            return
        }
        var service = SearchAPI(api:.SearchAutoComplete(textToAutoComplete: textToSearch))
        
        service.call(successHandler: {
            response in
            
            if let jsondictionary:[String:Any] = response.valueFor(key: "data", isMandatory: false)
            {
                
                var strings = [String]()
                
                
                if let itemsList:[String] = jsondictionary.valueFor(key: "items", isMandatory: false)
                {
                    strings = itemsList
                }
                
                successHandler(strings)
                
            }
        }
            
            , errorHandler: {code, message in
                ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
        }, internetErrorHandler: {
            code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
        
    }
    
    public class  func SearchVideos(textToSearch:String,successHandler:@escaping ([OTTVideoItem])->Void,errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        
        guard let  text = textToSearch.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            else{
                successHandler([OTTVideoItem]())
                return
        }
        
        var service = SearchAPI(api: .Search(text: text))
        
        service.call(successHandler: {
            response in
            
            if let jsondictionary:[String:Any] = response.valueFor(key: "data", isMandatory: false)
            {
                
                var videoList = [OTTVideoItem]()
                
                
                if let cat:[[String:Any]] = jsondictionary.valueFor(key: "items", isMandatory: false)
                {
                    var index = 0
                    for item in cat
                    {
                        guard let item =  OTTVideoItem.Parse(jsonDictionary: item) else { continue }
                        videoList.append(item)
                    }
                    index+=1
                }
                successHandler(videoList)
                
            }
        }
            
            , errorHandler: {code, message in
                ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
        }, internetErrorHandler: {
            code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
        
    }

}
