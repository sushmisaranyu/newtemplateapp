//
//  CategoryService.swift
//  SaraynuLibrary
//
//  Created by Ashley Dsouza on 16/05/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation

public class CommonCatalogResponseManager
{
    public class func CommonCatalogItemReseponseManager(response:DataLoader<[String:Any]>,responseHandler: @escaping ((_ response: DataLoader<CatalogListItem>) -> Void))
    {
        switch response
        {
        case .success(let responseJson):
            guard let jsondictionary:[String:Any] = responseJson.valueFor(key: "data", isMandatory: false) else {
                responseHandler(DataLoaderErrorHelper.getBadResponse())
                return
            }

            guard let catalogListItem  = CatalogListItem.Parse(jsonDictionary: jsondictionary) else
            {
                responseHandler(DataLoaderErrorHelper.getBadResponse())
                return
            }

            responseHandler(DataLoader.success(response: catalogListItem))

        default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
        }
    }

    public class func CommonVideoItemListReseponseManager(response:DataLoader<[String:Any]>,listItemKey:String,responseHandler: @escaping ((_ response: DataLoader<[OTTVideoItem]>) -> Void))
    {
        switch response
        {
        case .success(let responseJson):
            guard let jsondictionary:[String:Any] = responseJson.valueFor(key: "data", isMandatory: false) else {
                responseHandler(DataLoaderErrorHelper.getBadResponse())
                return
            }

            guard let catalogList:[[String:Any]] = jsondictionary.valueFor(key: listItemKey, isMandatory: false) else {responseHandler(DataLoaderErrorHelper.getBadResponse())
                return
            }

            var videoList = [OTTVideoItem]()

            for item in catalogList
            {
                guard let videoItem =  OTTVideoItem.Parse(jsonDictionary: item) else { continue }
                videoList.append(videoItem)
            }

            responseHandler(DataLoader.success(response: videoList))

        default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
        }
    }

}

public class CatalogService
{
   static let episodeTheme = "show_episode"

   
    fileprivate static let HomeScreenTTL: TimeInterval = 5 * 60


    public  class func getItemsInCatalogListWith(listID:String , responseHandler: @escaping ((_ response: DataLoader<[OTTVideoItem]>) -> Void))
    {
        let service = CatalogAPI(api: CatalogAPI.API.GetVideosForCatalogList(listId: listID))

        service.apiRequest { (response) in

            CommonCatalogResponseManager.CommonVideoItemListReseponseManager(response: response, listItemKey: "catalog_list_items", responseHandler: responseHandler)
        }
    }

    public  class func getItemsInCatalogListWithGenreFiltre(listID:String,genre:String , responseHandler: @escaping ((_ response: DataLoader<[OTTVideoItem]>) -> Void))
    {
        let service = CatalogAPI(api: CatalogAPI.API.GetCatalogsWithGenreFilter(catalogID: listID, genre: genre))

        service.apiRequest { (response) in
             CommonCatalogResponseManager.CommonVideoItemListReseponseManager(response: response, listItemKey: "items", responseHandler: responseHandler)
            }
    }


    public  class func getCatalogListItemWithKidsFilter(listID:String, responseHandler: @escaping ((_ response: DataLoader<CatalogListItem>) -> Void))
    {
        var service = CatalogAPI(api: CatalogAPI.API.GetVideosForCatalogListWithKidsFilter(listId: listID))

        service.apiRequest { (response) in
            CommonCatalogResponseManager.CommonCatalogItemReseponseManager(response: response, responseHandler: responseHandler)
        }
    }


    public  class func getCatalogListItemWith(listID:String, responseHandler: @escaping ((_ response: DataLoader<CatalogListItem>) -> Void))
    {
        var service = CatalogAPI(api: CatalogAPI.API.GetVideosForCatalogList(listId: listID))

        service.apiRequest { (response) in
        CommonCatalogResponseManager.CommonCatalogItemReseponseManager(response: response, responseHandler: responseHandler)
        }
    }

    public  class func getCatalogListItemWithContentCategory(listID:String,content_category:String, responseHandler: @escaping ((_ response: DataLoader<CatalogListItem>) -> Void))
    {
        var service = CatalogAPI(api: CatalogAPI.API.GetVideosForChannelsWithContentCategory(listId: listID,content_category: content_category))

        service.apiRequest { (response) in
           CommonCatalogResponseManager.CommonCatalogItemReseponseManager(response: response, responseHandler: responseHandler)
        }
    }


    public  class func getCatalogListItemWithTheme(listID:String,theme:String, responseHandler: @escaping ((_ response: DataLoader<CatalogListItem>) -> Void))
    {
        var service = CatalogAPI(api: CatalogAPI.API.GetVideosForChannelsWithTheme(listId: listID,theme: theme))

        service.apiRequest { (response) in
            CommonCatalogResponseManager.CommonCatalogItemReseponseManager(response: response, responseHandler: responseHandler)
        }
    }


    public  class func getCatalogListItemWithGenreFiltre(listID:String,genre:String, responseHandler: @escaping ((_ response: DataLoader<CatalogListItem>) -> Void))
    {
        var service = CatalogAPI(api: CatalogAPI.API.GetCatalogsWithGenreFilter(catalogID: listID, genre: genre))

        service.apiRequest { (response) in
            CommonCatalogResponseManager.CommonCatalogItemReseponseManager(response: response, responseHandler: responseHandler)
        }
    }

    public  class func getVideoFromParentCatalogForTV(catalogID:String, responseHandler: @escaping ((_ response: DataLoader<CatalogListItem>) -> Void))
    {
        let service = CatalogAPI(api: CatalogAPI.API.GetVideosForParentCatalogForTV(catalogID: catalogID))

        service.apiRequest { (response) in
            CommonCatalogResponseManager.CommonCatalogItemReseponseManager(response: response, responseHandler: responseHandler)
        }
    }

    public  class func getForListofPrograms(catalogID:String,contentId:String, responseHandler: @escaping ((_ response: DataLoader<LinearAndLiveModel>) -> Void))
    {
        let service = CatalogAPI(api: CatalogAPI.API.GetProgramDetails(contentId: contentId, catalogId: catalogID))

        service.apiRequest { (response) in
            switch response
            {
            case .success(let responseJson):
                guard let jsondictionary:[String:Any] = responseJson.valueFor(key: "data", isMandatory: false) else {
                    responseHandler(DataLoaderErrorHelper.getBadResponse())
                    return
                }

                guard let videoItem = OTTVideoItem.Parse(jsonDictionary: jsondictionary)
                    else
                {
                    responseHandler(DataLoaderErrorHelper.getBadResponse())
                    return
                }

                 var programList = OTTVideoItem.commonVideoListParser(jsonDictionary: jsondictionary, key: "items")


                let model = LinearAndLiveModel(item: videoItem, list: programList)

                responseHandler(DataLoader.success(response: model))

            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }
        }
    }

    public  class func getForProgramsForChannel(catalogID:String,contentId:String, responseHandler: @escaping ((_ response: DataLoader<[OTTVideoItem]>) -> Void))
    {
        let service = CatalogAPI(api: CatalogAPI.API.GetProgramDetails(contentId: contentId, catalogId: catalogID))

        service.apiRequest { (response) in
             CommonCatalogResponseManager.CommonVideoItemListReseponseManager(response: response, listItemKey: "items", responseHandler: responseHandler)
        }
    }
  
    public  func getHomeScreen(listID:String, responseHandler: @escaping ((_ response: DataLoader<[CatalogListItem]>) -> Void))
    {
        let service = CatalogAPI(api: CatalogAPI.API.GetHomePageWith(listID: listID))


        service.apiRequest(responseHandler: { [weak self] response in

            switch response
            {
            case .success(let responseJson):
                guard let jsondictionary:[String:Any] = responseJson.valueFor(key: "data", isMandatory: false) else {
                responseHandler(DataLoaderErrorHelper.getBadResponse())
                return
                }

                guard let catalogList:[[String:Any]] = jsondictionary.valueFor(key: "catalog_list_items", isMandatory: false) else {responseHandler(DataLoaderErrorHelper.getBadResponse())
                    return
                }

                var catalogItems = [CatalogListItem]()

                for item in catalogList
                {
                    guard let catalogListItem  = CatalogListItem.Parse(jsonDictionary: item) else {continue}
                    catalogItems.append(catalogListItem)
                }

                responseHandler(DataLoader.success(response: catalogItems))

            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }

        })

         
    }

    public  class func getCatalogListAndDBSave(listID:String, responseHandler: @escaping ((_ response: DataLoader<[CatalogListItem]>) -> Void))
    {
        let service = CatalogAPI(api: CatalogAPI.API.GetHomePageWith(listID: listID))
        service.apiRequest(responseHandler: { response in

            switch response
            {
            case .success(let responseJson):
                guard let jsondictionary:[String:Any] = responseJson.valueFor(key: "data", isMandatory: false) else {
                    responseHandler(DataLoaderErrorHelper.getBadResponse())
                    return
                }

                guard let catalogList:[[String:Any]] = jsondictionary.valueFor(key: "catalog_list_items", isMandatory: false) else {responseHandler(DataLoaderErrorHelper.getBadResponse())
                    return
                }

                var catalogItems = [CatalogListItem]()

                for item in catalogList
                {
                    guard let catalogListItem  = CatalogListItem.Parse(jsonDictionary: item) else {continue}
                    catalogItems.append(catalogListItem)
                }

                responseHandler(DataLoader.success(response: catalogItems))

            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }

        })


    }


    public class func getVideoItem(contentId:String, catalogId:String, responseHandler: @escaping ((_ response: DataLoader<OTTVideoItem>) -> Void))
    {
        let service = CatalogAPI(api: .GetMediaItemFor(contentId: contentId, catalogId: catalogId))

        service.apiRequest { response in
            switch response
            {
            case .success(let responseJson):
                guard let data:[String:Any] = responseJson.valueFor(key: "data", isMandatory: false)
                    else{
                        responseHandler(DataLoaderErrorHelper.getBadResponse())
                        return
                }

                guard let mediaItem = OTTVideoItem.Parse(jsonDictionary: data) else {
                    responseHandler(DataLoaderErrorHelper.getBadResponse())
                    return
                }
                responseHandler(DataLoader.success(response: mediaItem))

            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }
        }

    }

    public class func getEpisodeItem(contentId:String,subcategory_ID:String?,showID:String, responseHandler: @escaping ((_ response: DataLoader<OTTVideoItem>) -> Void))
    {
        var service = CatalogAPI(api: .GetEpisodeFor(contentId: contentId, showID: showID))

        if let subid = subcategory_ID
        {
            service =  CatalogAPI(api: .GetEpisodeWithSeasonFor(contentId: contentId, showID: showID, subcategoryID: subid))
        }

        service.apiRequest { response in
            switch response
            {
            case .success(let responseJson):
                guard let data:[String:Any] = responseJson.valueFor(key: "data", isMandatory: false)
                    else{
                        responseHandler(DataLoaderErrorHelper.getBadResponse())
                        return
                }

                guard let mediaItem = OTTVideoItem.Parse(jsonDictionary: data) else { responseHandler(DataLoaderErrorHelper.getBadResponse())
                    return }
                mediaItem.theme = episodeTheme
                mediaItem.itemType = ItemType.episode
                responseHandler(DataLoader.success(response: mediaItem))

            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }
        }
    }

    public class func getSharedVideoItem(contentURL:String, responseHandler: @escaping ((_ response: DataLoader<[String:String]>) -> Void))
    {
        let service = CatalogAPI(api: .GetSharedVideoItem(contentURL: contentURL))

        service.apiRequest { response in
            switch response
            {
            case .success(let responseJson):
                guard let data:[String:String] = responseJson.valueFor(key: "data", isMandatory: false)
                else{
                        responseHandler(DataLoaderErrorHelper.getBadResponse())
                        return
                }

                responseHandler(DataLoader.success(response: data))

            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }
        }

    }



    //Archive:

    
  public  func getExtras(itemId:String,catalogId:String,successHandler:@escaping ()->Void,errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        var service = CatalogAPI(api: CatalogAPI.API.GetExtras(itemId: itemId, catalogId: catalogId))
        
        service.call(successHandler: {
            response in
            
            if let jsondictionary:[String:Any] = response.valueFor(key: "data", isMandatory: false)
            {
                
                
               // OTTCatalogItem.DeleteAllCatalogItems()
                //OTTMediaItem.DeleteAllMediaItems()
                if let cat:[[String:Any]] = jsondictionary.valueFor(key: "items", isMandatory: false)
                {
                    var index = 0
                    for item in cat
                    {
                        _ = OTTCatalogItem.Parse(jsonDictionary: item,sortid:index,isMenuItem:true)
                    }
                    index+=1
                }
                successHandler()
                
            }
        }
            
            , errorHandler: {code, message in
                 ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
        }, internetErrorHandler: {
            code in
             ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
        
    }
    
    
    public class func getMediaItem(contentId:String, catalogId:String,theme:String, successHandler:@escaping (_ mediaItem:OTTMediaItem)->Void,errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        var service = CatalogAPI(api: .GetMediaItemFor(contentId: contentId, catalogId: catalogId))
        
        service.call(successHandler: {
            response in
            
            guard let data:[String:Any] = response.valueFor(key: "data", isMandatory: false)
                else{
                    ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
                    return
            }
            
            
            
            _ = OTTMediaItem.Parse(jsonDictionary: data,setCache:true,thm:theme )
            
            let mediaItem = OTTMediaItem.GetMediaItemWith(contentId: contentId)
            
            if mediaItem == nil
            {
                ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
                return
            }
            else
            {
                successHandler(mediaItem!)
            }
            
        }
            
            , errorHandler: {code, message in
               ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
        }, internetErrorHandler: {
            code in
            
             ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
    }
    
    public class func getVideoItem(contentId:String, catalogId:String, successHandler:@escaping (_ mediaItem:OTTVideoItem)->Void,errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        var service = CatalogAPI(api: .GetMediaItemFor(contentId: contentId, catalogId: catalogId))
        
        service.call(successHandler: {
            response in
            
            guard let data:[String:Any] = response.valueFor(key: "data", isMandatory: false)
                else{
                    ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
                    return
            }
            
            
            guard let mediaItem = OTTVideoItem.Parse(jsonDictionary: data)
                else{
                ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
                return
            }
            
            successHandler(mediaItem)
            
        }
            
            , errorHandler: {code, message in
                ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
        }, internetErrorHandler: {
            code in
            
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
    }
    
    
    public class func getEpisodeItem(contentId:String,subcategory_ID:String?,showID:String, successHandler:@escaping (_ mediaItem:OTTVideoItem)->Void,errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        var service = CatalogAPI(api: .GetEpisodeFor(contentId: contentId, showID: showID))
        
        if let subid = subcategory_ID
        {
            service =  CatalogAPI(api: .GetEpisodeWithSeasonFor(contentId: contentId, showID: showID, subcategoryID: subid))
        }
        
        service.call(successHandler: {
            response in
            
            guard let data:[String:Any] = response.valueFor(key: "data", isMandatory: false)
                else{
                    ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
                    return
            }
            
            
            guard let mediaItem = OTTVideoItem.Parse(jsonDictionary: data)      else{
                ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
                return
            }
            mediaItem.theme = episodeTheme
            mediaItem.itemType = ItemType.episode
            successHandler(mediaItem)
            
        }
            
            , errorHandler: {code, message in
                ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
        }, internetErrorHandler: {
            code in
            
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
    }
    
    public class  func getVideosFor(listId:String,isNoLimit:Bool = false,successHandler:@escaping ([OTTVideoItem])->Void,errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        var service = CatalogAPI(api: CatalogAPI.API.GetVideosForCatalogList(listId: listId))
        
        if isNoLimit == true
        {
             service = CatalogAPI(api: CatalogAPI.API.GetVideosForCatalogListNoLimit(listId: listId))
        }
        
        service.call(successHandler: {
            response in
            
            if let jsondictionary:[String:Any] = response.valueFor(key: "data", isMandatory: false)
            {
                
                var videoList = [OTTVideoItem]()
                
               
                if let cat:[[String:Any]] = jsondictionary.valueFor(key: "catalog_list_items", isMandatory: false)
                {
                    var index = 0
                    for item in cat
                    {
                        guard let videoItem =  OTTVideoItem.Parse(jsonDictionary: item) else { return }
                        videoList.append(videoItem)
                    }
                    index+=1
                }
                successHandler(videoList)
                
            }
        }
            
            , errorHandler: {code, message in
                ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
        }, internetErrorHandler: {
            code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
        
    }
    
    public class func getMenuCatalog(listId:String,successHandler:@escaping ()->Void,errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        var service = CatalogAPI(api: CatalogAPI.API.GetVideosForCatalogList(listId: listId))
        
            service.call(successHandler: {
            response in
            
            if let jsondictionary:[String:Any] = response.valueFor(key: "data", isMandatory: false)
            {
                
            
                OTTCatalog.DeleteAllCatalogs()
                if let cat:[[String:Any]] = jsondictionary.valueFor(key: "catalog_list_items", isMandatory: false)
                {
                    
                    var index = 0
                    for item in cat
                    {
                        
                       _ =   OTTCatalog.Parse(jsonDictionary: item)
                      
                    }
                    index+=1
                }
                successHandler()
                
            }
        }
            
            , errorHandler: {code, message in
                ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
        }, internetErrorHandler: {
            code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
    }

    
    public class func getAllEpisodeWithFilter(catalogID:String,showID:String,episodeTypes:[String]?,successHandler:@escaping ([OTTVideoItem])->Void,errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        
        var  filter = ""
        if let episode_types = episodeTypes
        {
            filter = filter + "&episode_type="
            var index = 0
            for type in episode_types
            {
                if index == 0
                {
                    filter = filter + "\(type)"
                }
                else
                {
                    filter = filter + ",\(type)"
                }
                index+=1
            }
        }
        
        var service = CatalogAPI(api: CatalogAPI.API.GetEpisodesWithType(catalogID: catalogID, showID: showID, filter: filter))
        
        service.call(successHandler: {
            response in
            var videoList = [OTTVideoItem]()
            if let jsondictionary:[String:Any] = response.valueFor(key: "data", isMandatory: false)
            {
                //OTTCatalog.DeleteAllCatalogs()
                if let cat:[[String:Any]] = jsondictionary.valueFor(key: "items", isMandatory: false)
                {
                    for item in cat
                    {
                        guard let video  =   OTTVideoItem.Parse(jsonDictionary: item) else { continue }
                        video.theme = episodeTheme
                        video.itemType = ItemType.episode
                        videoList.append(video)
                    }
                }
                
            }
             successHandler(videoList)
        }
            
            , errorHandler: {code, message in
                ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
        }, internetErrorHandler: {
            code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
    }
    
    public class  func getVideosForCatalog(catalogID:String,successHandler:@escaping ([OTTVideoItem])->Void,errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        var service = CatalogAPI(api: CatalogAPI.API.GetVideosForParentCatalog(catalogID: catalogID))
        
        service.call(successHandler: {
            response in
            
            if let jsondictionary:[String:Any] = response.valueFor(key: "data", isMandatory: false)
            {
                
                var videoList = [OTTVideoItem]()
                
                var theme = ""
                if let th:String = jsondictionary.valueFor(key: "theme", isMandatory: false)
                {
                    theme = th
                }
                
                if let cat:[[String:Any]] = jsondictionary.valueFor(key: "items", isMandatory: false)
                {
                    var index = 0
                    for item in cat
                    {
                        guard let item =  OTTVideoItem.Parse(jsonDictionary: item) else { continue }
                        
                        
                        if StringHelper.isNilOrEmpty(string: item.theme)
                        {
                            item.theme = theme
                        }
                        
                        
                        
                        videoList.append(item)
                    }
                    index+=1
                }
                successHandler(videoList)
                
            }
        }
            
            , errorHandler: {code, message in
                ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
        }, internetErrorHandler: {
            code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
        
    }
    
    public class func  GetCatalogsWithGenreFilter(catalogId:String,genre:String,successHandler:@escaping ([OTTVideoItem])->Void,errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
          var service = CatalogAPI(api: CatalogAPI.API.GetCatalogsWithGenreFilter(catalogID: catalogId, genre: genre))
        service.call(successHandler: {
            response in
            
            if let jsondictionary:[String:Any] = response.valueFor(key: "data", isMandatory: false)
            {
                var theme = ""
                var videoList = [OTTVideoItem]()
                if let th:String = jsondictionary.valueFor(key: "theme", isMandatory: false)
                {
                    theme = th
                }
                
                if let cat:[[String:Any]] = jsondictionary.valueFor(key: "items", isMandatory: false)
                {
                    var index = 0
                    for item in cat
                    {
                        guard let videoItem =  OTTVideoItem.Parse(jsonDictionary: item)
                            else{
                            ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
                            return
                        }
                        if StringHelper.isNilOrEmpty(string: videoItem.theme) == true
                        {
                            videoItem.theme = theme
                        }
                        
                        videoList.append(videoItem)
                    }
                    index+=1
                }
                successHandler(videoList)
                
            }
        }
            
            , errorHandler: {code, message in
                ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
        }, internetErrorHandler: {
            code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
    }
    
    
    public class func  Feedback(name:String,subject:String,gender:String,phone:String,city:String,email_id:String,successHandler:@escaping ()->Void,errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        var service = CatalogAPI(api: CatalogAPI.API.Feedback(name: name, subject: subject, gender: gender, phone: phone, city: city,email_id:email_id))
        service.call(successHandler: {
            response in
            
          
                successHandler()
                
           
        }, errorHandler: {code, message in
                ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
        }, internetErrorHandler: {
            code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
    }
    
   public class func getRelatedVideos(catalogId:String?,contentID:String?,successHandler:@escaping ([OTTVideoItem])->Void,errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        guard let catalog_ID = catalogId
        else
        {
             successHandler([OTTVideoItem]())
            return
        }
        
        guard let content_ID = catalogId
            else
        {
            successHandler([OTTVideoItem]())
            return
        }

        
        var service = CatalogAPI(api: CatalogAPI.API.Related(catalodId: content_ID, contentId: content_ID))
        service.call(successHandler: {
            response in
            
            if let jsondictionary:[String:Any] = response.valueFor(key: "data", isMandatory: false)
            {
                
                var videoList = [OTTVideoItem]()
                var theme = ""
                if let th:String = jsondictionary.valueFor(key: "theme", isMandatory: false)
                {
                    theme = th
                }
                
                if let cat:[[String:Any]] = jsondictionary.valueFor(key: "items", isMandatory: false)
                {
                    var index = 0
                    for item in cat
                    {
                        guard let videoItem =  OTTVideoItem.Parse(jsonDictionary: item) else { continue }
                        if StringHelper.isNilOrEmpty(string: videoItem.theme) == true
                        {
                            videoItem.theme = theme
                        }
                        videoList.append(videoItem)
                    }
                    index+=1
                }
                successHandler(videoList)
                
            }
        }
            
            , errorHandler: {code, message in
                ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
        }, internetErrorHandler: {
            code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
    }

    //TO ARCHIVE:

    public  class func getHomeScreen(successHandler:@escaping ()->Void,errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        var service = CatalogAPI(api: CatalogAPI.API.GetHomepage)


        print("Homescreen : \( OTTCatalogItem.hasHomeScreen())  ::: \(AppUserDefaults.hasValidHomeScreenCache()) ")

        if OTTCatalogItem.hasHomeScreen()
        {
            if AppUserDefaults.hasValidHomeScreenCache()
            {
                successHandler()
                return
            }
        }



        service.call(successHandler: {
            response in

            if let jsondictionary:[String:Any] = response.valueFor(key: "data", isMandatory: false)
            {


                OTTCatalogItem.DeleteAllCatalogItems()
                OTTMediaItem.DeleteAllMediaItems()
                if let cat:[[String:Any]] = jsondictionary.valueFor(key: "catalog_list_items", isMandatory: false)
                {
                    var index = 0
                    for item in cat
                    {
                        _ = OTTCatalogItem.Parse(jsonDictionary: item,sortid:index,isMenuItem:true)
                        index+=1
                    }

                }
                AppUserDefaults.updateHomeScreenLastCacheTime(cache: Date().timeIntervalSince1970 as Double)
                successHandler()

            }
        }

            , errorHandler: {code, message in
                ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
        }, internetErrorHandler: {  code in

            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
    }




}
