//
//  UserService.swift
//  SaraynuLibrary
//
//  Created by Ashley Dsouza on 15/05/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation



public class UserService
{
    static let UserDoesNotExistErrorCode = "1030"
    
    public class func EmailSignIn(emailId:String,password:String, responseHandler: @escaping ((_ response: DataLoader<Bool>) -> Void))
    {
        let service = UserAPI(api: .SignIn(email_id: emailId, password: password))
        
        service.apiRequest { (response) in
            
            switch response
            {
            case .success(let response):
                let (isValid,addProfile) = OTTUser.Parse(jsonDictionary:response)
                
                if isValid == false
                {
                    responseHandler(DataLoaderErrorHelper.getDefaultErrorDataLoader())
                    return
                }
                
                AppUserDefaults.login_type.setValue(LoginType.Email.key)
                responseHandler(DataLoader.success(response: addProfile))
                break
            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }
        }
    }
    
    public class func MobileSignIn(mobile:String,password:String,responseHandler: @escaping ((_ response: DataLoader<Bool>) -> Void))
    {
        let service = UserAPI(api:.SignInMobile(mobile: mobile, password: password))
        
        service.apiRequest { (response) in
            
            switch response
            {
            case .success(let response):
                let (isValid,addProfile) = OTTUser.Parse(jsonDictionary:response)
                
                if isValid == false
                {
                    responseHandler(DataLoaderErrorHelper.getDefaultErrorDataLoader())
                    return
                }
                
                AppUserDefaults.login_type.setValue(LoginType.Mobile.key)
                responseHandler(DataLoader.success(response: addProfile))
                break
            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }
        }
    }
    
    public class func MobileSignUp(mobile:String,name: String,password:String,responseHandler: @escaping ((_ response: DataLoader<UserModel>) -> Void))
    {
        let service = UserAPI(api:.SignUpUsingMobileNumber(user_id: mobile, firstName: name, lastName: "", password: password) )
        
        service.apiRequest { (response) in
            
            switch response
            {
            case .success(let response):
                if let user = UserModel.Parse(jsonDictionary:response, loginType: .Mobile)
                {
                    responseHandler(DataLoader.success(response: user))
                    //AppUserDefaults.login_type.setValue(LoginType.Mobile.key)
                    return
                }
                else
                {
                    responseHandler(DataLoaderErrorHelper.getDefaultErrorDataLoader())
                }
                break
            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }
        }
    }
    
    public class func EmailSignUp(emailId:String,name:String,password:String,responseHandler: @escaping ((_ response: DataLoader<UserModel>) -> Void))
    {
        let service = UserAPI(api: .SignUp(email_id: emailId, firstname: name, lastname: "", password: password))
        
        
        service.apiRequest { (response) in
            
            switch response
            {
            case .success(let response):
                
                //let user = OTTUser.Parse(jsonDictionary: <#T##[String : Any]#>)
                
                if let user = UserModel.Parse(jsonDictionary:response,loginType:.Email)
                {
                    
                    if let value = user.session
                    {
                        DispatchQueue.main.async {
                             user.logInUser(sessionID:value)
                            _ = OTTUser.getEmailID()
                        }

                        responseHandler(DataLoader.success(response: user))
                        AppUserDefaults.login_type.setValue(LoginType.Email.key)
                    }
                    else
                    {
                        responseHandler(DataLoaderErrorHelper.getDefaultErrorDataLoader())
                    }
                    
                    return
                }
                else
                {
                    responseHandler(DataLoaderErrorHelper.getDefaultErrorDataLoader())
                }
                
                
                break
            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }
        }
    }
    
    
    public class func MobileSignUp(mobileNo:String,name:String,password:String,responseHandler: @escaping ((_ response: DataLoader<MobileUser>) -> Void))
    {
        let service = UserAPI(api: .SignUpUsingMobileNumber(user_id: mobileNo, firstName: name, lastName: "", password: password))
        
        
        service.apiRequest { (response) in
            
            switch response
            {
            case .success(let response):
                
                if let mobileUser = MobileUser.Parse(jsonDictionary: response)
                {
                    responseHandler(DataLoader.success(response: mobileUser))
                }
                else
                {
                    responseHandler(DataLoaderErrorHelper.getDefaultErrorDataLoader())
                }
                
                break
            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }
        }
    }
    
    
    
    public class func resendOTPForVerification(mobileNos:String,responseHandler: @escaping ((_ response: DataLoader<Bool>) -> Void))
    {
        let service = UserAPI(api: .ResendOtp(mobileNo: mobileNos))
        service.apiRequest { (response) in
            DefaultResponseHandler.defaultBoolResponseHandeler(response: response, responseHandler: responseHandler)
        }
    }
    
    public class func validateOTP(otpString:String ,mobileNo:String,responseHandler: @escaping ((_ response: DataLoader<String>) -> Void))
    {
        let service = UserAPI(api: .ValidateOtp(otpString: otpString, mobileNo: mobileNo))
        
        
        service.apiRequest { (response) in
            
            switch response
            {
            case .success(let response):
                guard let data:[String:Any] = response.valueFor(key: "data", isMandatory: false) else { responseHandler(DataLoaderErrorHelper.getDefaultErrorDataLoader()); return; }
                guard let message:[[String:Any]] = data.valueFor(key: "messages", isMandatory: false) else
                { responseHandler(DataLoaderErrorHelper.getDefaultErrorDataLoader()); return; }
                guard let firstmessage = message.first else
                { responseHandler(DataLoaderErrorHelper.getDefaultErrorDataLoader()); return; }
                guard let sessionId:String = firstmessage.valueFor(key: "session", isMandatory: false)else
                { responseHandler(DataLoaderErrorHelper.getDefaultErrorDataLoader()); return; }
                
                responseHandler(DataLoader.success(response: sessionId))
                
            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }
        }
    }
    
    public class func verifyOTPForgotPassword(otpString:String ,responseHandler: @escaping ((_ response: DataLoader<String>) -> Void))
    {
        let service = UserAPI(api: .OTPVerificationForForgotPassword(otp: otpString))
        
        
        service.apiRequest { (response) in
            
            switch response
            {
            case .success(let response):
                
                
                responseHandler(DataLoader.success(response: "sessionID"))
                
            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }
        }
    }
    
    
    
    public class func ForgotPasswordEmail(emailId:String,responseHandler: @escaping ((_ response: DataLoader<Bool>) -> Void))
    {
        let service = UserAPI(api: .ForgotPassword(email_id: emailId))
        
        service.apiRequest { (response) in
            DefaultResponseHandler.defaultBoolResponseHandeler(response: response, responseHandler: responseHandler)
        }
    }
    
    public class func ForgotPasswordMobile(mobileNo:String,responseHandler: @escaping ((_ response: DataLoader<Bool>) -> Void))
    {
        let service = UserAPI(api: .ForgotPasswordForMobileNoUser(mobileNo: mobileNo))
        
        service.apiRequest { (response) in
            DefaultResponseHandler.defaultBoolResponseHandeler(response: response, responseHandler: responseHandler)
        }
    }
    
    public class func ResetPasswordForMobileNo(otpCode:String,newpassword:String,confirmPassword:String,responseHandler: @escaping ((_ response: DataLoader<Bool>) -> Void))
    {
        let service = UserAPI(api: .ResetPasswordForMobile(otp: otpCode, newPassword: newpassword, confirmPassword: confirmPassword))
        
        service.apiRequest { (response) in
            DefaultResponseHandler.defaultBoolResponseHandeler(response: response, responseHandler: responseHandler)
        }
    }
    
    public class func getUserDetails(responseHandler: @escaping ((_ response: DataLoader<OTTUserDetails>) -> Void))
    {
        let service = UserAPI(api: .GetUserDetails)
        
        service.apiRequest { (response) in
            
            switch response
            {
            case .success(let response):
                if let data:[String:Any] = response.valueFor(key: "data", isMandatory: false)
                {
                    AppUserDefaults.ParseParentalControlFlag(json: response, dataKey: "data")
                    let details = OTTUserDetails.Parse(response: data)
                   // OTTUser.Update(jsonDictionary: response)
                    responseHandler(DataLoader.success(response: details))
                }
                else
                {
                    responseHandler(DataLoaderErrorHelper.getDefaultErrorDataLoader())
                }
                break
            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }
        }
    }
    
    public class func GETConsolidatedItemState(catalogId:String,contentId:String,category:String,responseHandler: @escaping ((_ response: DataLoader<DetailsScreenStateModel>) -> Void))
    {
        let service = UserAPI(api: .ConsolidatedItemState(catalog: catalogId, contenId: contentId, category: category))
        
        
        service.apiRequest { (response) in
            
            switch response
            {
            case .success(let response):
                responseHandler(DataLoader.success(response: DetailsScreenStateModel.Parse(json: response, contentId: contentId,catalogId: catalogId)))
                break
            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }
        }
    }
    
    
    
    //    public class func getUserDetails(responseHandler: @escaping ((_ response: DataLoader<UserModel>) -> Void))
    //    {
    //        let service = UserAPI(api: .GetUserDetails)
    //        service.apiRequest { (response) in
    //
    //            switch response
    //            {
    //            case .success(let response):
    //                   if  let user = UserModel.Parse(jsonDictionary: response)
    //                   {
    //                    responseHandler(DataLoader.success(response: user))
    //                   }
    //                   else{
    //                    responseHandler(DataLoaderErrorHelper.getDefaultErrorDataLoader())
    //                   }
    //                break
    //            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
    //            }
    //        }
    //    }
    
    
    
    public class func Logout(responseHandler: @escaping ((_ response: DataLoader<Bool>) -> Void))
    {
        let service = UserAPI(api: .SignOut)
        service.apiRequest { (response) in
            DefaultResponseHandler.defaultBoolResponseHandeler(response: response, responseHandler: responseHandler)
        }
    }
    
    
    public class func UpdateEmailUserDetails(name:String,lastName:String,mobileNos:String,birthDate:String,responseHandler:@escaping ((_ response: DataLoader<Bool>) -> Void))
    {
        let service = UserAPI(api: .UpdateEmailUserDetails(firstname: name, lastname: lastName, mobilenos: mobileNos, birthdate: birthDate))
        
        service.apiRequest { (response) in
            DefaultResponseHandler.defaultBoolResponseHandeler(response: response, responseHandler: responseHandler)
        }
    }
    
    public class func UpdateMobileUserDetails(name:String,lastName:String,email:String,birthDate:String,responseHandler:@escaping ((_ response: DataLoader<Bool>) -> Void))
    {
        let service = UserAPI(api: .UpdateMobileUserDetails(firstname: name, lastname: lastName, email: email, birthdate: birthDate))
        
        service.apiRequest { (response) in
            DefaultResponseHandler.defaultBoolResponseHandeler(response: response, responseHandler: responseHandler)
        }
    }
    
    public class func ChangePassword(oldPassword:String,newPassword:String,responseHandler:@escaping ((_ response: DataLoader<Bool>) -> Void))
    {
        let service = UserAPI(api: .ChangePassword(current_password: oldPassword, password: newPassword, confirm_password: newPassword))
        
        service.apiRequest { (response) in
            DefaultResponseHandler.defaultBoolResponseHandeler(response: response, responseHandler: responseHandler)
        }
    }
    
    public class func VerifyPassword(password:String,responseHandler:@escaping ((_ response: DataLoader<Bool>) -> Void))
    {
        let service = UserAPI(api: .VerifyPassword(password: password))
        
        service.apiRequest { (response) in
            DefaultResponseHandler.defaultBoolResponseHandeler(response: response, responseHandler: responseHandler)
        }
    }
    
    
    public class func ChangeParentalPin(isParentalPinEnabled:Bool,pin:String,responseHandler:@escaping ((_ response: DataLoader<Bool>) -> Void))
    {
        let service = UserAPI(api: .SetParentalControl(isParentalControlEnabled: isParentalPinEnabled, pin: pin))
        
        service.apiRequest { (response) in
            DefaultResponseHandler.defaultBoolResponseHandeler(response: response, responseHandler: responseHandler)
        }
    }
    
    
    public class func GenerateLoginCode(responseHandler:@escaping ((_ response: DataLoader<String>) -> Void))
    {
        let service = UserAPI(api: .GenerateLoginCode)
        
        service.apiRequest { (response) in
            
            switch (response) {
                
            case .success(let response):
                
                if let token:String = response.valueFor(key: "token", isMandatory: false)
                {
                    responseHandler(DataLoader.success(response: token))
                }
                else
                {
                    responseHandler(DataLoaderErrorHelper.getDefaultErrorDataLoader())
                }
                
            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
                
            }
        }
    }
    
    
    public class func VerifyLoginToken(token:String, responseHandler:@escaping ((_ response: DataLoader<Bool>) -> Void))
    {
        let service = UserAPI(api: .VerifyLoginToken(token: token))
        
        service.apiRequest { (response) in
            
            switch response
            {
            case .success(let response):
                
                let is_verified : String? = response.valueFor(key: "is_verified", isMandatory: false)
                
                
                if is_verified == "true" {
                    
                    if let session_id:String = response.valueFor(key: "session_id", isMandatory: false)
                    {
                        AppUserDefaults.session_ID.setValue(session_id)
                        
                        responseHandler(DataLoader.success(response: true))
                    }
                    else
                    {
                        responseHandler(DataLoader.success(response: false))
                    }
                }else {
                    responseHandler(DataLoader.success(response: false))
                    return
                }
                
                break
            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }
        }
    }
    
    public class func getUserDetailsTV(responseHandler: @escaping ((_ response: DataLoader<Bool>) -> Void))
    {
        let service = UserAPI(api: .GetUserDetails)
        
        service.apiRequest { (response) in
            
            switch response
            {
            case .success(let response):
                
                if let data:[String:Any] = response.valueFor(key: "data", isMandatory: false)
                {
                    AppUserDefaults.ParseParentalControlFlag(json: response, dataKey: "data")

                    let user = OTTUserDetails.Parse(response:data)
                    
                    responseHandler(DataLoader.success(response: true))
                }
                else
                {
                    responseHandler(DataLoaderErrorHelper.getDefaultErrorDataLoader())
                }
                break
            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }
        }
    }
    
    
    public class func getPurchasedPlans(responseHandler: @escaping ((_ response: DataLoader<[PurchasedPlanModel]>) -> Void))
    {
        let service = UserAPI(api: .GetUserPurchasedPlans)
        service.apiRequest { (response) in
            
            switch response
            {
            case .success(let response):
                responseHandler(DataLoader.success(response: PurchasedPlanModel.Parse(response: response)))
            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }
        }
    }
    
    
    
    
    
    //Archive
    
    
    
    public class func resendOtpForVerification(mobileNos:String,sucessfullResendOtp:@escaping (()->Void),errorHandler :@escaping((String,String)->Void),internetErrorHandler:@escaping((String,String)->Void))
    {
        let service = UserAPI(api: .ResendOtp(mobileNo: mobileNos))
        service.call(successHandler: { (response) in
            AppUtilities.printLog(message: "\(response)")
            sucessfullResendOtp()
        }, errorHandler: { (code, message) in
            errorHandler(" resend otp " , message)
            
        }, internetErrorHandler: {code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
            
        })
    }
    
    
    public class func UpdateAddProfileStatus(successHandler:@escaping (()->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)  {
        let service = UserAPI(api: .UpdateAddProfileStatus)
        
        service.call(successHandler: { response in
            AppUtilities.printLog(message: "\(response)")
            successHandler()
        }, errorHandler: { code , message in
            
            errorHandler("Profile",message)
            
        } , internetErrorHandler: {code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
            
        })
    }
    
    
    
    public class func AssignProfile(profileId:String ,successHandler:@escaping (()->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)  {
        let service = UserAPI(api: .Assignprofile(profileId:profileId))
        service.call(successHandler: { response in
            AppUtilities.printLog(message: "\(response)")
            AppUserDefaults.profile_ID.setValue(profileId)
            successHandler()
        }, errorHandler: { code , message in
            
            errorHandler("Profile",message)
            
        } , internetErrorHandler: {code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
            
        })
    }
    
    
    
    
    public class func ForgotPasswordForMobileUser(mobileNo:String,successHandler:@escaping (()->Void),errorHandler:@escaping(_ errTitle:String,_ errMessage:String)->Void,internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        let service = UserAPI(api: .ForgotPasswordForMobileNoUser(mobileNo: mobileNo))
        service.call(successHandler: { (response) in
            AppUtilities.printLog(message: "\(response)")
            successHandler()
        }, errorHandler: { (code, message) in
            errorHandler("Forgot Password for mobile no user",message)
            
        }, internetErrorHandler: { (code) in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
        
    }
    
    
    
    public class func ResendVerificationEmail(emailId:String,successHandler:@escaping (()->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        let service = UserAPI(api: .ResendVerification(email: emailId))
        
        service.call(successHandler: { response in
            AppUtilities.printLog(message: "\(response)")
            successHandler()
        }, errorHandler: { code , message in
            
            errorHandler("Resend Verification Email",message)
            
        } , internetErrorHandler: {code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
        
        
    }
    
    
    public class func ChangePassword(oldPassword:String,newPassword:String,successHandler:@escaping (()->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        let service = UserAPI(api: .ChangePassword(current_password: oldPassword, password: newPassword, confirm_password: newPassword))
        
        service.call(successHandler: { response in
            
            successHandler()
        }, errorHandler: { code , message in
            
            
            errorHandler("Change Password",message)
            
        } , internetErrorHandler: {code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
        
        
    }
    
    public class func ThirdPartyLogin(provider:String,name:String?,uid:String,profile_pic:String?,emailId:String?,successHandler:@escaping (()->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        
        var sName = ""
        var sProfilePic = ""
        var sEmail = ""
        if let value = name { sName = value }
        if let value = profile_pic { sProfilePic = value}
        if let value = emailId { sEmail = value }
        
        
        let service = UserAPI(api: .ThirdPartyLogin(provider: provider, uid: uid, firstname: sName, ext_account_email_id: sEmail, profile_pic: sProfilePic))
        
        service.call(successHandler: { response in
            
            let (_,_) = OTTUser.Parse(jsonDictionary:response)
            
            //            if isValid == false
            //            {
            //                let (code,message) = ErrorMessageManager.getDefaultErrorMessage()
            //                errorHandler("Sign In",message)
            //                return
            //            }
            
            
            successHandler()
        }, errorHandler: { code , message in
            
            errorHandler("",message)
            
        } , internetErrorHandler: {code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
        
        
    }
    
    public class func Logout(successHandler:@escaping (()->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        let service = UserAPI(api: .SignOut)
        
        service.call(successHandler: { response in
            
            successHandler()
        }, errorHandler: { code , message in
            
            errorHandler("Sign Out",message)
            
        } , internetErrorHandler: {code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
        
        
    }
    
    
    public class func getUserDetails(successHandler:@escaping ((OTTUserDetails)->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        let service = UserAPI(api: .GetUserDetails)
        
        service.call(successHandler: { response in
            if let data:[String:Any] = response.valueFor(key: "data", isMandatory: false)
            {
                let details = OTTUserDetails.Parse(response: data)
                OTTUser.Update(jsonDictionary: response)
                successHandler(details)
            }
            else
            {
                errorHandler("","incorrect Json from server")
            }
            
            
        }, errorHandler: { code , message in
            
            errorHandler("Sign Out",message)
            
        } , internetErrorHandler: {code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
    }
    
    
    
    public class func UpdateProfileImage(profile_pic:String,successHandler:@escaping (()->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        let service = UserAPI(api: .UpdateProfilePicture(profile_pic:profile_pic))
        
        service.call(successHandler: { response in
            
            successHandler()
            
        }, errorHandler: { code , message in
            
            errorHandler("",message)
            
        } , internetErrorHandler: {code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
        })
        
    }
    
    
}
