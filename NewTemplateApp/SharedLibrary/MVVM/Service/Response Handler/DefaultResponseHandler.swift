//
//  DefaultResponseHandler.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 19/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class DefaultResponseHandler
{
    public class func defaultBoolResponseHandeler(response:DataLoader<[String:Any]>,responseHandler: @escaping ((_ response: DataLoader<Bool>) -> Void))
    {
        switch response
        {
        case .success(_):
            responseHandler(DataLoader.success(response: true))
            break
        default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
        }
    }
}
