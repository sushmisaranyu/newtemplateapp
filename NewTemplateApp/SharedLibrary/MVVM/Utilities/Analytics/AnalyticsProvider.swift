//
//  AnalyticsProvider.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 14/10/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import MatomoTracker
#if os(iOS)

import SharedLibrary
#elseif os(tvOS)

import SharedLibraryAppleTV
#endif


public protocol FirebaseAnalyticsPlugin:class
{
    func mediaActive(dict:[String:Any]?)
     func mediaViewTime(dict: [String : Any]?,viewTime:Double)
     func mediaView(dict: [String : Any]?)
    func mediaComplete(dict: [String : Any]?)
    func mediaStartToPlay(dict: [String : Any]?,startToPlayTime:Double)
    func mediaBufferTime(dict: [String : Any]?,bufferTime:Double)
     func sendMediaData(watchTime:Double,item:OTTVideoItem?)
}

public struct Events {
    public static let ACTIVE_SVOD       = "active_svod"
    public static let ACTIVE_REGISTERED = "active_registered"
    public static let MEDIA_ACTIVE      = "media_active"
    public static let BUFFER_TIME       = "buffer_time"
    public static let VIDEO_VIEW_TIME   = "video_view_time"
    public static let VIDEO_VIEW        = "video_view"
    public static let VIDEO_COMPLETE    = "video_complete"
    public static let START_TTP         = "start_ttp"

    public static let SIGNUP            = "sign_up"
    public static let LOGIN             = "login"
}

public struct Params {
    public static let START_TTP       = "start_ttp"
    public static let VIDEO_VIEW_TIME = "video_view_time"
    public static let ACTIVE_SVOD     = "active_svod"
    public static let MEDIA_ACTIVE    = "media_active"
    public static let BUFFER_TIME     = "buffer_time"
    public static let VIDEO_ID        = "video_id"
    public static let TITLE           = "title"
    public static let BITRATE         = "bitrate"
    public static let USER_STATE      = "user_state"
    public static let USER_PLAN       = "user_plan"
    public static let USER_PLAN_TYPE  = "user_plan_type"
    public static let CATEGORY        = "category"
    public static let CONTENT_TYPE    = "content_type"
    public static let GENRE           = "genre"
    public static let LANGUAGE        = "language"
    public static let USER_ID         = "user_id"

}


public enum Dimensions
{


    case application
    case media_language
    case media_category
    case media_content_type
    case media_genre
    case playlist
    case media_bitrate
    case user_age
    case user_gender
    case state_user
    case user_period
    case user_plan_type
    case user_custom_plan
    case user_id
    case video_id
    case app_launch
    case title
    case product_price
    case error_reason
    case currency_type


    var dimensionIndex:Int
    {
        switch self
        {

        case .application:          return 1
        case .media_language:       return 2
        case .media_category:       return 3
        case .media_content_type:   return 4
        case .media_genre:          return 5
        case .playlist:             return 6
        case .media_bitrate:        return 7
        case .user_age:             return 8
        case .user_gender:          return 9
        case .state_user:           return 10
        case .user_period:          return 11
        case .user_plan_type:       return 12
        case .user_custom_plan:     return 13
        case .user_id:              return 14
        case .video_id:             return 15
        case .app_launch:           return 17
        case .title:                return AppUtilities.selectPropertyForIOSorAppleTV(IOS: 16, AppleTV: 18) //IOS: 16
        case .product_price:        return AppUtilities.selectPropertyForIOSorAppleTV(IOS: 19, AppleTV: 21) //IOS : 19
        case .error_reason:         return AppUtilities.selectPropertyForIOSorAppleTV(IOS: 18, AppleTV: 20) //IOS:18
        case .currency_type:        return AppUtilities.selectPropertyForIOSorAppleTV(IOS: 17, AppleTV: 19) //IOS: 17

        }
    }

}



public class AnalyticsProvider
{


    public class func  getUserAgent() -> String
    {
        var useragent =  "Mozilla/5.0 (iPhone; CPU OS {OSVERSION} like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15F79"

        if let regex = try? NSRegularExpression(pattern: "\\((iPad|iPhone);", options: .caseInsensitive) {
            let deviceModel = Device.makeCurrentDevice().platform
            useragent = regex.stringByReplacingMatches(
                in: useragent,
                options: .withTransparentBounds,
                range: NSRange(location: 0, length: useragent.count),
                withTemplate: "(\(deviceModel);"
            )
        }

        var osversion = Device.makeCurrentDevice().osVersion
        osversion = osversion.replacingOccurrences(of: ".", with: "_")
        useragent = useragent.replacingOccurrences(of: "{OSVERSION}", with: osversion)


        return useragent.appending(" MatomoTracker SDK URLSessionDispatcher")
    }

    public static let mediaAnalyticsTracker = AnalyticsProvider()

    public static var shared: MatomoTracker = MatomoTracker(siteId: ID_SITE, baseURL: URL(string:SITE_URL)!,userAgent:getUserAgent())

    //static let SITE_URL = "https://matomo.apisaranyu.in/piwik/piwik.php"
    static let SITE_URL =  "http://18.205.63.219/matomo/matomo.php"


    #if os(iOS)
   // static let ID_SITE = "5"
    static let ID_SITE = "3"
    #elseif os(tvOS)
    static let ID_SITE = "10"
    #endif

    static let REC = "1"

    #if os(iOS)
    static let playerName = "InstaplayIOS"
    #elseif os(tvOS)
    static let playerName = "Apple TV Player"
    #endif
    



    private var _mPageStartTime:Double = 0
    private var _mVideoStartTime: Double = 0


    private var contentWatchTime:Double = 0

    private var _lastEventInterval: Double = 0

    public  var isBusy = false


    var bufferStartTime:Double = 0


    private var playClickedTime = 0.0

    public func bufferStart()
    {
        bufferStartTime = Date().timeIntervalSince1970
    }

    public func bufferStopTime()
    {
        if bufferStartTime == 0 {return}
        let bufferEndTIme = Date().timeIntervalSince1970

        let totalBufferTime = bufferEndTIme - bufferStartTime

        bufferStartTime = 0
        if totalBufferTime < 0 {return}


        guard let cd = customDimension else {return}

        let event = Event(tracker: AnalyticsProvider.shared, action: ["Activity"], eventCategory:"Buffer", eventAction:"Activity",eventName:"buff_time",eventValue:Float(totalBufferTime),dimensions: cd)
        AnalyticsProvider.shared.track(event)

        firebaseAnalyticsPlugin?.mediaBufferTime(dict: firebaseAnalyticsDictionary, bufferTime: totalBufferTime)

    }


    public func updateAppLaunch()
    {
        //         let tracker = ["app_launch":"1"]

        let event = Event(tracker: AnalyticsProvider.shared, action: [ "Activity"], eventCategory:"Users", eventAction:"Activity",eventName:"app_launch",eventValue:1.0)
        AnalyticsProvider.shared.track(event)
    }


    public func updateAppLaunchForActiveUser()
    {
        if OTTUser.hasLoggedInUser()
        {


            let tracker = ["user_active":"1"]
            let event = Event(tracker: AnalyticsProvider.shared, action: [ "Activity"], eventCategory:"Users", eventAction:"Activity", eventName:"user_active", eventValue:1.0, customTrackingParameters:tracker)
            AnalyticsProvider.shared.track(event)
        }

    }



    public func updateAppLaunchForActiveSVODUser()
    {
        if OTTUser.hasLoggedInUser()  && OTTUser.hasSubscription() == true
        {
            let tracker = ["svod_active":"1"]

            guard let userid = OTTUser.getUser()?.userId else {return}
            var dimension = [CustomDimension]()
            dimension.append(CustomDimension(index: Dimensions.user_id.dimensionIndex, value: getValue(property: userid)))

            let event = Event(tracker: AnalyticsProvider.shared, action: [ "Activity"], eventCategory:"Users", eventAction:"Activity",eventName:"svod_active",eventValue:1.0,customTrackingParameters:tracker,dimensions: dimension)
            AnalyticsProvider.shared.track(event)
        }

    }

    public func updateAppLaunchForActiveRegisteredUser()
    {
        if OTTUser.hasLoggedInUser() && OTTUser.hasSubscription() == false
        {
            let tracker = ["registered_active":"1"]
            guard let userid = OTTUser.getUser()?.userId else {return}
            var dimension = [CustomDimension]()
            dimension.append(CustomDimension(index: Dimensions.user_id.dimensionIndex, value: getValue(property: userid)))
            let event = Event(tracker: AnalyticsProvider.shared, action: [ "Activity"], eventCategory:"Users", eventAction:"Activity",eventName:"registered_active",eventValue:1.0,customTrackingParameters:tracker,dimensions: dimension)
            AnalyticsProvider.shared.track(event)
        }

    }

    public func mediaActiveTime()
    {
        if let cd = customDimension
        {
            let tracker = ["media_active":"1"]
            let event = Event(tracker: AnalyticsProvider.shared, action: [ "Activity"], eventCategory:"Users", eventAction:"Activity",eventName:"media_active",eventValue:1.0,dimensions: cd)
            AnalyticsProvider.shared.track(event)
        }
        firebaseAnalyticsPlugin?.mediaActive(dict: firebaseAnalyticsDictionary)
    }



    /**
     * @param ma_id   - A unique id that is always the same while playing a media. As soon as the played media changes (new video or audio started), this ID has to change.
     * @param ma_ti   - The name / title of the media.
     * @param ma_re   - The URL of the media resource.
     * @param ma_mt   - video or audio depending on the type of the media.
     * @param ma_pn   - The name of the media player, for example html5.
     * @param ma_st   - The time in seconds for how long a user has been playing this media. This number should typically increase when you send a media tracking request. It should be 0 if the media was only visible/impressed but not played. Do not increase this number when a media is paused.
     * @param ma_le   - The duration (the length) of the media in seconds. For example if a video is 90 seconds long, the value should be 90.
     * @param ma_ps   - The progress / current position within the media. Defines basically at which position within the total length the user is currently playing.
     * @param ma_ttp  - Defines after how many seconds the user has started playing this media. For example a user might have seen the poster of the video for 30 seconds before a user actually pressed the play button.
     * @param ma_w    - The resolution width of the media in pixels. Only recommended being set for videos.
     * @param ma_h    - The resolution height of the media in pixels. Only recommended being set for videos.
     * @param ma_fs   - Should be 0 or 1 and defines whether the media is currently viewed in full screen.
     * @param url
     * @param idsite
     * @param rec
     */


    public static func setupDebugging()
    {
        #if DEBUG
        shared.logger = DefaultLogger(minLevel: .verbose)
        #endif

    }

    var  mediaTrackerDict:[String:String]?
    var firebaseAnalyticsDictionary:[String:Any]?
    var customDimension:[CustomDimension]?
    weak var firebaseAnalyticsPlugin:FirebaseAnalyticsPlugin?

    public func initTracker(item:DetailsModel?,adaptiveURL:String,fireBaseAnalytics:FirebaseAnalyticsPlugin? = nil)
    {
        mediaTrackerDict = nil
        guard let videoItem  = item?.mediaitem else {return}
        guard let itemState  = item?.itemState else {return}
        guard let itemType   = videoItem.itemType else {return}
        guard let contentId  = videoItem.content_id else {return}
        guard let mediaTitle = videoItem.title else {return}
        //   if videoItem.getDurationInSeconds() == 0 {return}

        self.firebaseAnalyticsPlugin = fireBaseAnalytics
        setVideoStartTime()


        let newContentId = "\(Date().timeIntervalSince1970):\(contentId)".md5()

        print("New AnalyticsContentId: \(newContentId)")

        if itemType == .album || itemType == .tv_show {return}

        mediaTrackerDict =
            ["ma_id":newContentId,
             "ma_ti":mediaTitle,
             "ma_re":getModifiedProperties(item: videoItem, bitrate: "Auto") + "|" + getUserProperty(itemState: itemState),// TODO what url to use
                "ma_mt":"video",// TODO what url to use
                "ma_pn": AnalyticsProvider.playerName,
                "ma_st":"0", // What is initial value
                "ma_ps":"0",
                "ma_ttp":getVideoPlaybackInterval(),
                "ma_w":"1280",//TODO
                "ma_h":"720",//TODO
                "ma_fs":"0"
        ]

        customDimension = getDimensions(item: videoItem, itemState: itemState, playableURL: adaptiveURL)
        customDimension?.append(CustomDimension(index: Dimensions.media_bitrate.dimensionIndex, value: "Auto"))

        setupFirebaseAnalyticsDictionary(item: videoItem, itemState: itemState, playableURL: adaptiveURL, bitrate: "Auto")


        if StringHelper.isNilOrEmpty(string: itemState.analytics_user_id) == false
        {
            setUserId(userID: itemState.analytics_user_id)
        }
        else
        {
            removeUserId()
        }

    }

    /*
     Category = C
     Content-Type = CT
     ContentID = CID
     Theme = T
     Genre = G
     Language = L
     Bitrate = B

     User ID = UID
     UserState = US
     UserPlan UP
     UserPlanType = UPT
     UserCutomPlan = UCP

     */

    func getModifiedProperties(item:OTTVideoItem,bitrate:String) -> String
    {
        //  contentid | title | category | content type | genre | language |
        //   bitrate
        return "CID=\(AppUtilities.getValue(property: item.content_id))|C=\(AppUtilities.getValue(property: item.catalog_object?.plan_category_type))|T=\(AppUtilities.getValue(property: item.theme))|G=\(AppUtilities.getValue(property: item.getFirstGenre()))|L=\(AppUtilities.getValue(property: item.language))|B=\(bitrate)"
    }

    func getUserProperty(itemState:DetailsScreenStateModel) -> String
    {
        return "UID=\(AppUtilities.getValue(property: itemState.analytics_user_id))|US=\(AppUtilities.getValue(property: itemState.user_state))|UP=\(AppUtilities.getValue(property: itemState.user_period))|UPT=\(itemState.user_plan_type)|UCP=\(getValue(property: itemState.user_plan))))"
    }

    func getNewma_re(item:OTTVideoItem,bitrate:String) -> String
    {
        //  contentid | title | category | content type | genre | language |
        //   bitrate
        return "\(AppUtilities.getValue(property: item.content_id))|\(AppUtilities.getValue(property: item.catalog_object?.plan_category_type))|\(AppUtilities.getValue(property: item.theme))|\(AppUtilities.getValue(property: item.getFirstGenre()))|\(AppUtilities.getValue(property: item.language))|\(bitrate)"
    }

    func getNewvideourlValue(itemState:DetailsScreenStateModel) -> String
    {
        //user id | user state | user plan | user plan type

        return "\(AppUtilities.getValue(property: itemState.analytics_user_id))|\(AppUtilities.getValue(property: itemState.user_state))|\(AppUtilities.getValue(property: itemState.user_period))|\(itemState.user_plan_type)"
    }

    func setupFirebaseAnalyticsDictionary(item:OTTVideoItem,itemState:DetailsScreenStateModel, playableURL:String,bitrate:String)
    {
        var dict                         = [String:Any]()
        dict[Params.VIDEO_ID]            = AppUtilities.getValue(property: item.content_id)
        dict[Params.TITLE]               = AppUtilities.getValue(property: item.title)
        dict[Params.BITRATE]             = AppUtilities.getValue(property: bitrate)
        dict[Params.USER_STATE]          = AppUtilities.getValue(property: itemState.user_state)
        dict[Params.USER_PLAN]           = AppUtilities.getValue(property: itemState.user_period)
        dict[Params.CATEGORY]            = AppUtilities.getValue(property: item.catalog_object?.plan_category_type)
        dict[Params.CONTENT_TYPE]        = AppUtilities.getValue(property: item.theme)
        dict[Params.GENRE]               = AppUtilities.getValue(property: item.getFirstGenre())
        dict[Params.USER_ID]             = AppUtilities.getValue(property: itemState.analytics_user_id)
        dict[Params.USER_PLAN_TYPE]      = AppUtilities.getValue(property: itemState.user_plan_type)

        self.firebaseAnalyticsDictionary = dict
    }

    func getDimensions(item:OTTVideoItem,itemState:DetailsScreenStateModel, playableURL:String) -> [CustomDimension]
    {
        var dimension = [CustomDimension]()

        dimension.append(CustomDimension(index: Dimensions.application.dimensionIndex, value: "shemaroo-ios"))

        dimension.append(CustomDimension(index: Dimensions.media_content_type.dimensionIndex, value: getValue(property: item.theme)))
        dimension.append(CustomDimension(index: Dimensions.media_category.dimensionIndex, value: getValue(property: item.catalog_object?.plan_category_type)))
        dimension.append(CustomDimension(index: Dimensions.media_genre.dimensionIndex, value: getValue(property: item.getFirstGenre())))
        dimension.append(CustomDimension(index: Dimensions.media_language.dimensionIndex, value: getValue(property: item.language)))
        dimension.append(CustomDimension(index: Dimensions.playlist.dimensionIndex, value: getValue(property: "")))
        dimension.append(CustomDimension(index: Dimensions.state_user.dimensionIndex, value: getValue(property: itemState.user_state)))
        dimension.append(CustomDimension(index: Dimensions.user_age.dimensionIndex, value: getValue(property: itemState.age)))
        dimension.append(CustomDimension(index: Dimensions.user_custom_plan.dimensionIndex, value: getValue(property: itemState.user_plan)))
        dimension.append(CustomDimension(index: Dimensions.user_gender.dimensionIndex, value: getValue(property: itemState.gender)))
        dimension.append(CustomDimension(index: Dimensions.user_id.dimensionIndex, value: getValue(property: itemState.analytics_user_id)))
        dimension.append(CustomDimension(index: Dimensions.user_period.dimensionIndex, value: getValue(property: itemState.user_period)))
        dimension.append(CustomDimension(index: Dimensions.user_plan_type.dimensionIndex, value: getValue(property: itemState.user_plan_type)))
        dimension.append(CustomDimension(index: Dimensions.video_id.dimensionIndex, value: getValue(property: itemState.contentId)))
        dimension.append(CustomDimension(index: Dimensions.title.dimensionIndex, value: getValue(property: item.title)))
        return dimension

    }


    func getValue(property:String?) -> String
    {
        if StringHelper.isNilOrEmpty(string: property) == true
        {
            return ""
        }
        else
        {
            return property!
        }
    }

    public func setPageStartTime()
    {
        queue.async {[weak self] in
            self?._mPageStartTime = Date().timeIntervalSince1970
        }

    }

    public func setVideoStartTime()
    {
        queue.async {[weak self] in
            self?._mVideoStartTime = Date().timeIntervalSince1970
        }

    }

    func getVideoPlaybackInterval() -> String
    {
        let interval =  _mVideoStartTime - _mPageStartTime

        if interval < 0 {return "0"}

        return "\(Int(interval))"
    }

    func getVideoPlaybackIntervalInSeconds() -> Double
    {
        let interval =  _mVideoStartTime - _mPageStartTime

        if interval < 0 {return 0}

        return interval
    }


    func updateMediaEvent()
    {
        guard let tracker = mediaTrackerDict else {return}


    }


    func updateAnalytics(timeWatched:Double, timeDiff:Int)
    {
        guard let tracker = mediaTrackerDict else {return}

        var eventName = "seek"
        //The first eventName for the Video played needs to be play
        // remaining event names need to be seek
        if _hasPlayEventBeenSent == false
        {

            eventName = "play"
            let event = Event(tracker: AnalyticsProvider.shared, action: [ "Details Screen"], eventCategory:"MediaVideo", eventAction:eventName, customTrackingParameters: tracker)
            AnalyticsProvider.shared.track(event)
            _hasPlayEventBeenSent = true
            firebaseAnalyticsPlugin?.mediaView(dict: firebaseAnalyticsDictionary)
            firebaseAnalyticsPlugin?.mediaStartToPlay(dict: firebaseAnalyticsDictionary, startToPlayTime: getVideoPlaybackIntervalInSeconds())
            playbackStarted()

        }
        else
        {

            let event = Event(tracker: AnalyticsProvider.shared, action: [ "Details Screen"], eventCategory:"MediaVideo", eventAction:eventName, customTrackingParameters: tracker)
            AnalyticsProvider.shared.track(event)
            print("event fired")

            if timeDiff < 20
            {
                firebaseAnalyticsPlugin?.mediaViewTime(dict: firebaseAnalyticsDictionary, viewTime: Double(timeDiff))
            }
        }

        //      let eventCustom = Event(tracker: AnalyticsProvider.shared, action: ["Shemaroo-IOS"], eventCategory:"Media", //eventAction:"Activity",eventName:eventName,dimensions:customDimension ?? [])
        //        AnalyticsProvider.shared.track(eventCustom)




    }
    //
    //    * @param ma_st   - The time in seconds for how long a user has been playing this media. This number should typically increase when you send a media tracking request. It should be 0 if the media was only visible/impressed but not played. Do not increase this number when a media is paused.
    //     * @param ma_ps   - The progress / current position within the media. Defines basically at which position within the total length the user is currently playing.
    //     * @param ma_fs   - Should be 0 or 1 and defines whether the media is currently viewed in full screen.


    public func canFireAnalytics() -> Bool
    {

        if isBusy == true {return false}

        let currentTime = Date().timeIntervalSince1970
        let timeDiff = Int(currentTime - _lastEventInterval)

        print("Time Diff : \(timeDiff) ")

        if timeDiff > 0 && timeDiff < 10
        {
            return false
        }

        return true
    }



    var _hasPlayEventBeenSent:Bool = false
    var _lastma_st = 0


    private let queue = DispatchQueue(label: "com.shemaroo.shemaroome")


    public func updateMedia(ma_ps:String,ma_fs:String,totalDuration:String,height:String,width:String)
    {
        queue.async {[weak self] in
            self?.isBusy = true
            self?.updateMediaAsync(ma_ps:ma_ps,ma_fs:ma_fs,totalDuration:totalDuration,height:height,width:width)
            self?.isBusy = false
        }
    }

    public func updateMediaAsync(ma_ps:String,ma_fs:String,totalDuration:String,height:String,width:String)
    {

        //get current time
        let currentTime = Date().timeIntervalSince1970

        //get last tick differrence
        let timeDiff = Int(currentTime - _lastEventInterval)

        // if diff is less than 10 ignore else update media analytics
        if timeDiff > 0 && timeDiff < 10
        {
            return
        }

        _lastEventInterval = currentTime
        guard var tracker = mediaTrackerDict else {return}

        if timeDiff < 30
        {
            _lastma_st = _lastma_st + timeDiff
        }

        //lastma_st is that last seektime + the tick duration

        mediaTrackerDict?["ma_st"] = "\(_lastma_st)"
        mediaTrackerDict?["ma_ps"] = ma_ps
        mediaTrackerDict?["ma_fs"] = ma_fs
        mediaTrackerDict?["ma_h"]  = height
        mediaTrackerDict?["ma_w"]  = width
        if  mediaTrackerDict?["ma_le"] == nil
        {
            mediaTrackerDict?["ma_le"] = totalDuration
        }

        updateAnalytics(timeWatched: Double(_lastma_st), timeDiff: timeDiff)
        mediaActiveChecker()

    }



    func createDimensions()
    {
        var dimensions = [CustomDimension]()

        // CustomDimension(index: 1, value: <#T##String#>)
    }


    public func resetAnalytics()
    {
        self.mediaTrackerDict            = nil
        self._mPageStartTime             = 0
        self._mVideoStartTime            = 0
        self._hasPlayEventBeenSent       = false
        self._lastma_st                  = 0
        self.activePingCount             = 0
        self.isBusy                      = false
        self.firebaseAnalyticsPlugin     = nil
        self.firebaseAnalyticsDictionary = nil
        self.playClickedTime             = 0
    }


    var activePingCount = 0
    func mediaActiveChecker()
    {
        if activePingCount == AppUserDefaults.media_active_x_minForApp/15
        {
            print("Media Active fired")
             mediaActiveTime()
        }
        activePingCount+=1

    }

    var analyticsUserId:String?

    public func setUserId(userID:String)
    {

//        if let id = AnalyticsProvider.shared.forcedVisitorId
//        {
//            return
//        }
    //    AnalyticsProvider.shared.forcedVisitorId = userID
    }

    public func removeUserId()
    {
        analyticsUserId = nil
        AnalyticsProvider.shared.userId = nil

    }

    public func mediaComplete()
    {
        firebaseAnalyticsPlugin?.mediaComplete(dict: firebaseAnalyticsDictionary)
    }


    public  func appLaunched(time:Float)
    {

        //        var dimension = [CustomDimension]()
        //        dimension.append(CustomDimension(index: Dimensions.app_launch.dimensionIndex , value: "\(ceil(time*10)/10)"))

        //        let event = Event(tracker: AnalyticsProvider.shared, action: ["Activity"],   eventCategory: "App",eventAction:"Activity", eventName: "app_launch", eventValue: time)
        //        AnalyticsProvider.shared.track(event)
    }

    public func trackCampaign(name:String,keyword:String)
    {
        AnalyticsProvider.shared.trackCampaign(name: name, keyword: keyword)
    }

    public func setPlayClickedTime()
    {
        self.playClickedTime = Date().timeIntervalSince1970
    }

    public func playbackStarted()
    {
        if playClickedTime == 0 {return}

        //        guard let cd = customDimension else {return}
        //
        //        let newTime = Date().timeIntervalSince1970 - playClickedTime
        //
        //        playClickedTime = 0
        //
        //        let event = Event(tracker: AnalyticsProvider.shared, action: [],   eventCategory: "Users",eventAction:"Activity", eventName: "video_launch_time", eventValue: AppUtilities.roundOffToOneDecimalPoint(nos: newTime) ,dimensions: cd)
        //
        //        AnalyticsProvider.shared.track(event)
    }

    public func addToWatchList(item:DetailsModel?)
    {
        guard let videoItem  = item?.mediaitem else {return}
        guard let itemState  = item?.itemState else {return}

        let cd = getDimensions(item: videoItem, itemState: itemState, playableURL: "")

        let event = Event(tracker: AnalyticsProvider.shared, action: [],   eventCategory: "Users",eventAction:"Activity", eventName: "watchlist", eventValue: 1.0 ,dimensions: cd)

        AnalyticsProvider.shared.track(event)
    }

    public func itemShared(item:DetailsModel?)
    {

        guard let videoItem  = item?.mediaitem else {return}
        guard let itemState  = item?.itemState else {return}

        let cd = getDimensions(item: videoItem, itemState: itemState, playableURL: "")

        let event = Event(tracker: AnalyticsProvider.shared, action: [],   eventCategory: "Users",eventAction:"Activity", eventName: "share", eventValue: 1.0 ,dimensions: cd)

        AnalyticsProvider.shared.track(event)
    }


    public class func transactionSuccess(productName:String?,currencyType:String?,productPrice:String?)
    {
        guard  let name = productName, let price = productPrice, let currency = currencyType else {
            return
        }

        var cd = [CustomDimension]()
        cd.append(CustomDimension(index: Dimensions.currency_type.dimensionIndex, value: currency))
        cd.append(CustomDimension(index: Dimensions.product_price.dimensionIndex, value: price))


        let event = Event(tracker: AnalyticsProvider.shared, action: [],   eventCategory: "Transaction",eventAction:"Init", eventName: name, eventValue: 1.0,dimensions: cd)

        AnalyticsProvider.shared.track(event)
    }


    public class func transactionInit(productName:String?,currencyType:String?,productPrice:String?)
    {
        guard  let name = productName, let price = productPrice, let currency = currencyType else {
            return
        }

        var cd = [CustomDimension]()
        cd.append(CustomDimension(index: Dimensions.currency_type.dimensionIndex, value: currency))
        cd.append(CustomDimension(index: Dimensions.product_price.dimensionIndex, value: price))


        let event = Event(tracker: AnalyticsProvider.shared, action: [],   eventCategory: "Transaction",eventAction:"Init", eventName: name, eventValue: 1.0,dimensions: cd)

        AnalyticsProvider.shared.track(event)
    }


    public class func transactionFailure(productName:String?,error:String?)
    {
        guard  let name = productName, let err = error else {
            return
        }

        var cd = [CustomDimension]()
        cd.append(CustomDimension(index: Dimensions.error_reason.dimensionIndex, value: err))

        let event = Event(tracker: AnalyticsProvider.shared, action: [],   eventCategory: "Transaction",eventAction:"failure", eventName: productName, eventValue: 1.0,dimensions: cd)

        AnalyticsProvider.shared.track(event)
    }

    public class func transactionRetry(productName:String?,currencyType:String?,productPrice:String?)
    {
        guard  let name = productName, let price = productPrice, let currency = currencyType else {
            return
        }

        var cd = [CustomDimension]()
        cd.append(CustomDimension(index: Dimensions.currency_type.dimensionIndex, value: currency))
        cd.append(CustomDimension(index: Dimensions.product_price.dimensionIndex, value: price))


        let event = Event(tracker: AnalyticsProvider.shared, action: [],   eventCategory: "Transaction",eventAction:"retry", eventName: name, eventValue: 1.0,dimensions: cd)

        AnalyticsProvider.shared.track(event)
    }


    public class func login()
    {
        //AnalyticsProvider.shared.userId = OTTUser.getUser()?.userId

        guard let userid = OTTUser.getUser()?.userId else {return}
        var dimension = [CustomDimension]()
        dimension.append(CustomDimension(index: Dimensions.user_id.dimensionIndex, value: AppUtilities.getValue(property: userid)))


        let event = Event(tracker: AnalyticsProvider.shared, action: [],   eventCategory: "Users",eventAction:"Activity",eventName:"login", eventValue: 1.0, dimensions: dimension)

        AnalyticsProvider.shared.track(event)
        AnalyticsProvider.mediaAnalyticsTracker.updateAppLaunchForActiveUser()
        AnalyticsProvider.mediaAnalyticsTracker.updateAppLaunchForActiveRegisteredUser()
        AnalyticsProvider.mediaAnalyticsTracker.updateAppLaunchForActiveSVODUser()

    }

    public class func signUp()
    {
       // AnalyticsProvider.shared.userId = OTTUser.getUser()?.userId


        guard let userid = OTTUser.getUser()?.userId else {return}
        var dimension = [CustomDimension]()
        dimension.append(CustomDimension(index: Dimensions.user_id.dimensionIndex, value: AppUtilities.getValue(property: userid)))

        let event = Event(tracker: AnalyticsProvider.shared, action: [],   eventCategory: "Users",eventAction:"Activity", eventName:"sign_up" , eventValue: 1.0, dimensions: dimension)

        AnalyticsProvider.shared.track(event)
        AnalyticsProvider.mediaAnalyticsTracker.updateAppLaunchForActiveUser()
        AnalyticsProvider.mediaAnalyticsTracker.updateAppLaunchForActiveRegisteredUser()
        AnalyticsProvider.mediaAnalyticsTracker.updateAppLaunchForActiveSVODUser()

    }
}

extension AnalyticsProvider
{

}
