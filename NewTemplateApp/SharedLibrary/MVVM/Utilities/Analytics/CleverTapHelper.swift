//
//  CleverTapHelper.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 02/01/19.
//  Copyright © 2019 Saranyu. All rights reserved.
//

import Foundation



public struct CleverTapParams {
    public static let IDENTITY       = "Identity";
    public static let ACTIVE_PLANS   = "active_plans";
    public static let INACTIVE_PLANS = "inactive_plans";
    public static let IS_SUBSCRIBED  = "is_subscribed";
    public static let IS_LOGGEDIN    = "is_loggedIn";

}

public class CleverTapHelper
{
    public class func updateCleverTapProfile()
    {
//        var profile: [String:Any] = [String:Any]()
//        if OTTUser.hasLoggedInUser()
//        {
//            if let userID = OTTUser.getUser()?.userId
//            {
//                profile[CleverTapParams.IDENTITY] = userID
//            }
//
//            if let activePlans = OTTUser.getCommaSeperatedActivePlans()
//            {
//                profile[CleverTapParams.ACTIVE_PLANS] = activePlans
//            }
//
//            if let inactivePlans = OTTUser.getCommaSeperatedInActivePlans()
//            {
//                profile[CleverTapParams.INACTIVE_PLANS] = inactivePlans
//            }
//
//            profile[CleverTapParams.IS_SUBSCRIBED] = OTTUser.hasSubscription()
//            profile[CleverTapParams.IS_LOGGEDIN] = true
//
//        }
//        CleverTap.sharedInstance()?.profilePush(profile)
    }

    public class func setLocation()
    {
        /*
         Get the device location if available. Will prompt the user location permissions dialog.

         Please be sure to include the NSLocationWhenInUseUsageDescription key in your Info.plist.
         See https://developer.apple.com/library/ios/documentation/General/Reference/InfoPlistKeyReference/Articles/CocoaKeys.html#//apple_ref/doc/uid/TP40009251-SW26

         Uses desired accuracy of kCLLocationAccuracyHundredMeters.

         If you need background location updates or finer accuracy please implement your own location handling.
         Please see https://developer.apple.com/library/ios/documentation/CoreLocation/Reference/CLLocationManager_Class/index.html for more info.

         Optional.  You can use location to pass it to CleverTap via the setLocation API
         for, among other things, more fine-grained geo-targeting and segmentation purposes.
         */

        
//
//        CleverTap.getLocationWithSuccess({(location: CLLocationCoordinate2D) -> Void in
//            // do something with location here, optionally set on CleverTap for use in segmentation etc
//            CleverTap.setLocation(location)
//        }, andError: {(error: String?) -> Void in
//            if let e = error {
//                print(e)
//            }
//        })
    }




    public class func sendMediaData(watchTime:Double,item:OTTVideoItem?)
    {

        if AppUserDefaults.isCleverTapEventEnabledForApp == false {return}
        guard let videoItem = item else {return}

        var data: [String:Any]     = [String:Any]()
        data["watch_time"]         = Int(watchTime)
        data["media_name"]         = getValue(property: item?.title)
        data["media_content_type"] = getValue(property: item?.theme)
        data["media_genre"]        = getValue(property: item?.genres?.first)
        data["media_language"]     = getValue(property: item?.language)
        data["media_category"]     = getValue(property: item?.catalog_object?.layout_scheme)

       // CleverTap.sharedInstance()?.recordEvent("Media Event", withProps: data)

    }

    public class func getValue(property:String?) -> String
    {
        if StringHelper.isNilOrEmpty(string: property) == true
        {
            return ""
        }
        else
        {
            return property!
        }
    }
}
