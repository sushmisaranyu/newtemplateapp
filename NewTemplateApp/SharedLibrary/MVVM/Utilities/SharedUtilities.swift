//
//  SharedUtilities.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 10/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public class SharedUtilities
{

}

extension Bundle {
    public var versionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
   public  var buildNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
}

