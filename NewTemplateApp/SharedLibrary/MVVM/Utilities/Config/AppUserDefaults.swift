//
//  AppUserDefaults.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 24/02/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation




public enum AppUserDefaults {
    
    case min_ios_app_version
    case session_ID
    case profile_ID
    case login_type
    case social_accessToken
    case socialUserID
    case detectedRegion
    case termsOfConditionURL
    case aboutUsURl
    case privacyURL
    case contactUSURL
    case feedBackID
    case userEmailOrMobileNumber
    case callingCode
    case countryName
    case homeScreenLastCacheTime
    case recentSearches
    case parentalControlFlag
    case faq
    case profileCount
    case isCleverTapEventEnabled
    case isNotificationsEnabled
    case media_active_x_min
    case fireBaseEnabled


    
    var key: String {
        switch self {
            
        case .min_ios_app_version:          return "min_ios_app_version"
        case .session_ID:                   return "session_ID"
        case .profile_ID:                   return "profile_ID"
        case .login_type:                   return "login_type"
        case .social_accessToken:           return "social_accessToken"
        case .socialUserID:                 return "socialUser_ID"
        case .detectedRegion:               return "detectedRegion"
        case .termsOfConditionURL:          return "termsOfConditionURL"
        case .privacyURL:                   return "privacyURL"
        case .aboutUsURl:                   return "aboutUsURl"
        case .feedBackID:                   return "feedBackID"
        case .userEmailOrMobileNumber:      return "userMobileNumber"
        case .callingCode:                  return "calling_code"
        case .countryName:                  return "country_name"
        case .homeScreenLastCacheTime:      return "homeScreenLastCacheTime"
        case .recentSearches:               return "recentSearches"
        case .parentalControlFlag:          return "parentalControlFlag"
        case .faq:                          return "faq"
        case .profileCount:                 return "profileCount"
        case .isCleverTapEventEnabled:       return "isCleverTapEventEnabled"
        case .isNotificationsEnabled:       return "isNotificationsEnabled"
        case .contactUSURL   :               return "contactUSURL"
        case .media_active_x_min:            return "media_active_x_min"
        case .fireBaseEnabled:               return "fireBaseEnabled"

        }
    }
    
    var value: Any? {
        return UserDefaults.standard.object(forKey: key) as Any?
    }
    
    public func setValue(_ valueToSet: Any) {
        UserDefaults.standard.set(valueToSet, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    public func remove() {
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    var serviceURL:String
    {
        if let serviceURL = self.value as? String
        {
            return serviceURL
        }
        else
        {
            return AppConfiguration.serverURL.value()!
        }
    }
    
    public static var isSessionValid:Bool
    {
        AppUtilities.printLog(message: "\(AppUserDefaults.sessionID)")
        if AppUserDefaults.sessionID != ""
        {
            return true
        }
        else
        {
            return false
        }
    }

    public static func getParentalControlForApp() -> Bool
    {
        guard let parentalControl:Bool = AppUserDefaults.parentalControlFlag.value as? Bool
        else
        {
            return false
        }

        return parentalControl
    }

    public static func updateHomeScreenLastCacheTime(cache:Double)
    {
        AppUserDefaults.homeScreenLastCacheTime.setValue(cache)
    }

    public static func hasValidHomeScreenCache() -> Bool
    {
        let homeScreenTTL:Double = ( 60 * 5 )
        guard let homeScreenLastCacheTime = AppUserDefaults.homeScreenLastCacheTime.value as? Double
        else
        {
            return false
        }

        if homeScreenLastCacheTime == 0 { return false }

        let currentDate = Date().timeIntervalSince1970
        print("Cache : \(homeScreenLastCacheTime - currentDate)")

        if (homeScreenLastCacheTime + homeScreenTTL) > currentDate
        {
            return true
        }

        return false

    }
    
    public static var sessionID:String
    {
        if let id = AppUserDefaults.session_ID.value as? String
        {
            return id
        }
        else
        {
            return ""
        }
    }
    
    public static var detectedRegionForApp:String?
    {
        if let region = AppUserDefaults.detectedRegion.value as? String
        {
            return region
        }
        else
        {
            return  nil
        }
    }
    
    public static var boundProfileId:String?
    {
        if let id = AppUserDefaults.profile_ID.value as? String
        {
            return id
        }

        return nil
    }
    
    
    public static var loginType:String?
    {
        if let type = AppUserDefaults.login_type.value as? String
        {
            return type
        }
        
        return nil
    }

    public static var profileCountForApp:Int
    {
        if let count = AppUserDefaults.profileCount.value as? Int
        {
            return count
        }

        return 0
    }

    public static var firebaseEnabledForApp:Bool
    {
        if let count = AppUserDefaults.fireBaseEnabled.value as? Bool
        {
            return count
        }

        return true
    }


    public static var isCleverTapEventEnabledForApp:Bool
    {
        if let flag = AppUserDefaults.isCleverTapEventEnabled.value as? Bool
        {
            return flag
        }

        return true
    }

    public static var isNotificationsEnabledForApp:Bool
    {
        if let flag = AppUserDefaults.isNotificationsEnabled.value as? Bool
        {
            return flag
        }
        
        return true
    }

    public static var media_active_x_minForApp:Int
    {
        if let value = AppUserDefaults.media_active_x_min.value as? Int
        {
            return value * 60
        }

        return 2 * 60
    }


    public static func SetLoginType(type:LoginType,socialUserID:String,sessionID:String,accessToken:String?)
    {
        self.login_type.setValue(type.key)
        self.session_ID.setValue(sessionID)
        self.socialUserID.setValue(socialUserID)
        
        if let token = accessToken
        {
            self.social_accessToken.setValue(token)
        }
        
    }
    
    public static var socialUserIdForApp:String?
    {
        if let id = AppUserDefaults.socialUserID.value as? String
        {
            return id
        }
        
        return nil
    }

    public static var userEmailOrMobileNo:String?
    {
        if let mobileNo = AppUserDefaults.userEmailOrMobileNumber.value as?String
        {
            return mobileNo
        }
        return nil
    }

    
    
    
    public static var callingCodeOfCountry:String?
    {
        if let callingCode = AppUserDefaults.callingCode.value as?String
        {
            return callingCode
        }
        return nil
    }
    public static var userCountryName:String?
    {
        if let countryName = AppUserDefaults.countryName.value as? String
        {
            return countryName
        }
        return nil
    }
    
    public static func  clearUserCreds()
    {
        self.profile_ID.remove()
        self.social_accessToken.remove()
        self.login_type.remove()
        self.session_ID.remove()
        self.socialUserID.remove()
        self.parentalControlFlag.remove()

    }
    
    
    public static var feedbackIDForApp:String
    {
        if let id = AppUserDefaults.feedBackID.value as? String
        {
            return id
        }
        
        return ""
    }
    
    public static var aboutUsUrlForApp:String?
    {
        if let id = AppUserDefaults.aboutUsURl.value as? String
        {
            return id
        }
        
        return nil
    }
    
    public static var termsOfConditionForApp:String?
    {
        if let id = AppUserDefaults.termsOfConditionURL.value as? String
        {
            return id
        }
        
        return nil
    }
    
    public static var privacyPolicyForApp:String?
    {
        if let id = AppUserDefaults.privacyURL.value as? String
        {
            return id
        }
        
        return nil
    }

    public static var contanctUsForApp:String?
    {
        if let id = AppUserDefaults.contactUSURL.value as? String
        {
            return id
        }

        return nil
    }


    public static var faqForApp:String?
    {
        if let id = AppUserDefaults.faq.value as? String
        {
            return id
        }

        return nil
    }


    public static func ParseRegionJson(json:[String:Any]) -> Bool
    {
        guard let region:[String:Any] = json.valueFor(key: "region", isMandatory: false) else {return  false}

        guard let countryRegion:String = region.valueFor(key: "country_code2", isMandatory: false) else {return  false}
        AppUserDefaults.detectedRegion.setValue(countryRegion)

        guard let countryCode:String = region.valueFor(key: "calling_code") else {return  false}
        AppUserDefaults.callingCode.setValue(countryCode)

        guard let countryName:String = region.valueFor(key: "country_name") else {return  false}
        AppUserDefaults.countryName.setValue(countryName)

        return true
    }

    public static func ParseParentalControlFlag(json:[String:Any], dataKey:String,parentalControlKey:String = "parental_control")
    {
        guard let data:[String:Any] = json.valueFor(key: dataKey, isMandatory: false) else {return}

        guard let parentalControlFlag:String = data.valueFor(key: parentalControlKey, isMandatory: false) else {return}

        var flag = false

        if parentalControlFlag == true.description
        {
            flag = true
        }

        AppUserDefaults.parentalControlFlag.setValue(flag)
    }
    
}
