//
//  UserDefaults.swift
//  SaranyuLibrary
//
//  Created by Ash Dz on 23/02/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//


import Foundation

enum ServerUserDefaults {
    
    case catalogs_ServerURL
    case users_ServerURl
    case subscriptions_ServerURL
    case search_ServerURL  
    
    var key: String {
        switch self {
        case .catalogs_ServerURL:           return "catalogs_ServerURL"
        case .users_ServerURl:              return "users_ServerURl"
        case .subscriptions_ServerURL:      return "subscriptions_ServerURL"
        case .search_ServerURL:             return "search_ServerURL"

        }
    }


    public static func Parse(json:[String:Any]) -> Bool
    {
        guard let data:[String:Any] = json.valueFor(key: "data", isMandatory: false) else {return false}

        guard let services:[[String:Any]] = data.valueFor(key: "services", isMandatory: false) else {return false}

        for item in services
        {
            guard let name:String = item.valueFor(key: "name", isMandatory: false) else {continue}


            guard let dns:String = item.valueFor(key: "dns", isMandatory: false) else {continue}
            switch name
            {
            case "catalog": ServerUserDefaults.catalogs_ServerURL.setValue(dns)
            case "users": ServerUserDefaults.users_ServerURl.setValue(dns)
            case "subscriptions": ServerUserDefaults.subscriptions_ServerURL.setValue(dns)
            case "search": ServerUserDefaults.search_ServerURL.setValue(dns)
            default: break
            }
        }
        return true
    }
    
    var value: Any? {
        return UserDefaults.standard.object(forKey: key) as Any?
    }
    
    func setValue(_ valueToSet: Any) {
        UserDefaults.standard.set(valueToSet, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    func remove() {
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    var serviceURL:String
    {
        if let serviceURL = self.value as? String
        {
            return serviceURL
        }
        else
        {
            return AppConfiguration.serverURL.value()!
        }
    }
    
    
}
