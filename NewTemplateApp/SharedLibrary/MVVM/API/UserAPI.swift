//
//  CinestaanUserAPI.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 28/02/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation




public class UserAPI
{
    //    '{"auth_token":"hsByrxoM7TJjzhTETQMz", "user_profile":{"firstname":"somu","lastname":"H","age":"20","child":"false"}}'
    
    public enum API
    {
        case AddItemToList(playlistId:String,contentId:String,catalogId:String)
        case AddProfile(firstname:String,lastname:String,age:String,child:Bool)
        case Assignprofile(profileId:String)

        case BindSessionToProfile(profile_id:String)
        case ConsolidatedItemState(catalog:String,contenId:String,category:String)
        case ChangePassword(current_password:String,password:String,confirm_password:String)
        case CheckEmailAvalible(email_id:String)
        case CreatePlayList(playListName:String,playListType:String)
        case DeletePlaylist(playListID:String)
        case DeleteProfile(profile_id:String)
        case EditProfile(firstname:String,lastname:String,age:String,child:Bool,profile_id:String)
        case ForgotPassword(email_id:String)
        case ForgotPasswordForMobileNoUser(mobileNo:String) // recieve otp as response
        case GetAllPlayLists
        case GetPlaylistWith(playlistId:String)
        case GetProfilesForAccount
        case GetSubscriptionStatus
        case GetUserDetails
        case InitTransaction(parameters:[String:Any])
        case PurchaseSubscription(parameters:[String:Any])
        case RemoveItemFromPlayList(playlistId:String,playListItemId:String)
        case Renameplaylist(playListID:String,playlistName:String,playlistType:String)
        case ResendOtp(mobileNo:String)
        case ResendVerification(email:String)
        case ResetPasswordForMobile(otp:String,newPassword:String,confirmPassword:String)
        case SignIn(email_id:String,password:String)
        case SignInMobile(mobile:String,password:String)
        case SignOut
        case SignUp(email_id:String,firstname:String,lastname:String,password:String)
        case SignUpUsingMobileNumber(user_id:String,firstName:String,lastName:String,password:String)
        case ThirdPartyLogin(provider:String,uid:String,firstname:String,ext_account_email_id:String,profile_pic:String)
        case UpdateAddProfileStatus
        case UpdatePlaylistWithType(catalogId:String,contentId:String,playlistType:String)
        case UpdateProfilePicture(profile_pic:String)
        case VerifyPassword(password:String)
        
        // //{"auth_token":"Ts4XpMvGsB2SW7NZsWc3", "user": {"mobile_number":"234567890","user_email_id":"anki@yopmail.com","birthdate":"14/02/1994"}}
        
        case UpdateUserDetails(firstname:String,lastname:String,mobilenos:String,email:String,birthdate:String)
        case UpdateMobileUserDetails(firstname:String,lastname:String,email:String,birthdate:String)
        case UpdateEmailUserDetails(firstname:String,lastname:String,mobilenos:String,birthdate:String)
        case UpdateUserRating(catalogId:String,contentId:String,rating:String,playListType:String)
        case UpdateWatchHistory(catalogId:String,contentId:String,playbackTime:String)
        case ValidateOtp(otpString:String,mobileNo:String)
        case SetParentalControl(isParentalControlEnabled:Bool, pin:String)

        case OTPVerificationForForgotPassword(otp:String)

        
        //TVOS login
        case GenerateLoginCode
        case VerifyLoginToken(token:String)

        //Get Subscription Details
        case GetUserPurchasedPlans


        
    }
    
    var httpMethodForAPI:HTTPMethodForAPI
    {
        switch api
        {
            
            
        case  .SignInMobile:            return .POST
            
        case .AddItemToList:            return .POST
        case .AddProfile:               return .POST
        case .Assignprofile:            return .POST
        case .ConsolidatedItemState:    return .GET
        case .BindSessionToProfile:     return .POST
        case .ChangePassword:           return .POST
        case .CheckEmailAvalible:       return .POST
        case .CreatePlayList:           return .POST
        case .DeletePlaylist:           return .DELETE
        case .DeleteProfile:            return .DELETE
        case .EditProfile:              return .PUT
        case .ForgotPassword:           return .POST
        case .ForgotPasswordForMobileNoUser:     return .POST
        case .GetAllPlayLists:          return .GET
        case .GetPlaylistWith:          return .GET
        case .GetProfilesForAccount:    return .GET
        case .GetSubscriptionStatus:    return .GET
        case .GetUserDetails:           return .GET
        case .InitTransaction:          return .POST
        case .PurchaseSubscription:     return .POST
        case .RemoveItemFromPlayList:   return .DELETE
        case .Renameplaylist:           return .PUT
        case .ResendOtp :               return .POST
        case .ResendVerification:       return .POST
        case .ResetPasswordForMobile:   return .POST
        case .SignIn:                   return .POST
        case .SignOut:                  return .POST
        case .SignUp:                   return .POST
        case .SignUpUsingMobileNumber:  return .POST
        case .ThirdPartyLogin:          return .POST
        case .UpdateAddProfileStatus:   return .POST
        case .UpdatePlaylistWithType:   return .POST
        case .UpdateProfilePicture:     return .PUT
        case .UpdateUserDetails:        return .PUT
        case .UpdateEmailUserDetails:   return .PUT
        case .UpdateMobileUserDetails:  return .PUT
        case .UpdateUserRating:         return .POST
        case .UpdateWatchHistory:       return .POST
        case .ValidateOtp:              return .GET
        case .VerifyPassword:           return .POST
        case .SetParentalControl:       return .POST

        case .OTPVerificationForForgotPassword: return .POST
            
        //TVOS login
        case .GenerateLoginCode:       return .POST
        case .VerifyLoginToken:       return .GET

        case .GetUserPurchasedPlans:    return .GET
            
        }
    }
    
    var urlPath:String
    {
        switch api
        {
        case .AddItemToList(let playlistId,_,_):        return "/users/\(AppUserDefaults.sessionID)/playlists/\(playlistId)"
        case .AddProfile:                               return "/users/\(AppUserDefaults.sessionID)/profiles"
        case .Assignprofile:                            return "/users/\(AppUserDefaults.sessionID)/assign_profile?auth_token=\(AppConfiguration.authTokenForApp)"
        case .BindSessionToProfile:                     return "/users/\(AppUserDefaults.sessionID)/assign_profile"
        case .ChangePassword:                           return "/users/\(AppUserDefaults.sessionID)/change_password"
        case .CheckEmailAvalible:                       return "/users/check_available"
        case .ConsolidatedItemState(let catalogId, let contenId, let category): return "/users/\(AppUserDefaults.sessionID)/get_all_details.gzip" + (OTTServer.fixedGetParameters) + "&catalog_id=\(catalogId)&content_id=\(contenId)&category=\(category)"
        case .CreatePlayList:                           return "/users/\(AppUserDefaults.sessionID)/playlists"
        case .DeletePlaylist(let playListID):           return "/users/\(AppUserDefaults.sessionID)/playlists/\(playListID)/clear_all" + OTTServer.fixedGetParameters
        case .DeleteProfile(let profile_id):            return "/users/\(AppUserDefaults.sessionID)/profiles/\(profile_id)" + "?auth_token=\(AppConfiguration.authTokenForApp)"
        case .EditProfile( _, _, _, _, let profile_id): return "/users/\(AppUserDefaults.sessionID)/profiles/\(profile_id)"
        case .ForgotPassword:                           return "/users/forgot_password"
        case .ForgotPasswordForMobileNoUser:            return "/users/forgot_password"
        case .GetAllPlayLists:                          return "/v2/users/\(AppUserDefaults.sessionID)/playlists\(OTTServer.fixedGetParameters)"
        case .GetPlaylistWith(let playlistId):          return "/users/\(AppUserDefaults.sessionID)/playlists/\(playlistId)/listitems\(OTTServer.fixedGetParameters)&page=0&page_size=200"
        case .GetProfilesForAccount:                    return "/users/\(AppUserDefaults.sessionID)/profiles?auth_token=\(AppConfiguration.authTokenForApp)"
        case .GetSubscriptionStatus:                     return "/users/\(AppUserDefaults.sessionID)/user_plans\(OTTServer.fixedGetParameters)"
        case .GetUserDetails:                            return "/users/\(AppUserDefaults.sessionID)/account\(OTTServer.fixedGetParameters)"
        case .InitTransaction:                           return "/users/\(AppUserDefaults.sessionID)/transactions"
        case .PurchaseSubscription:                      return "/users/\(AppUserDefaults.sessionID)/purchases\(OTTServer.fixedGetParameters)"
        case .RemoveItemFromPlayList(let playlistId,let playListItemId) :return "/users/\(AppUserDefaults.sessionID)/playlists/\(playlistId)/listitems/\(playListItemId)\(OTTServer.fixedGetParameters)"
        case .Renameplaylist(let playListID, _,  _):    return "/users/\(AppUserDefaults.sessionID)/playlists/\(playListID)"
        case .ResendOtp :                               return "/users/resend_verification_link"
        case .ResendVerification( _) :                  return "/users/resend_verification_link.gzip" + OTTServer.fixedGetParameters
        case .ResetPasswordForMobile:                   return "/users/reset_password"
        case .SignIn:                                   return "/users/sign_in"
        case .SignInMobile:                             return "/users/sign_in"
        case .SignOut:                                  return "/users/\(AppUserDefaults.sessionID)/sign_out"
        case .SignUp:                                   return "/users"
        case .SignUpUsingMobileNumber:                  return "/users"
        case .ThirdPartyLogin:                          return "/users/external_auth/sign_in"
        case .UpdateAddProfileStatus:                   return "/users/\(AppUserDefaults.sessionID)/update_add_profile_status"
        case .UpdatePlaylistWithType(_, _, _):          return "/users/\(AppUserDefaults.sessionID)/playlists/playlistType"
        case .UpdateProfilePicture:                     return "/users/\(AppUserDefaults.sessionID)/account\(OTTServer.fixedGetParameters)"
        case .UpdateUserDetails:                        return "/users/\(AppUserDefaults.sessionID)/account\(OTTServer.fixedGetParameters)"
        case .UpdateMobileUserDetails:                        return "/users/\(AppUserDefaults.sessionID)/account\(OTTServer.fixedGetParameters)"
        case .UpdateEmailUserDetails:                        return "/users/\(AppUserDefaults.sessionID)/account\(OTTServer.fixedGetParameters)"
        case .UpdateUserRating(_,_,_,let playlisttype): return "/users/\(AppUserDefaults.sessionID)/playlists/\(playlisttype)"
        case .UpdateWatchHistory:                       return "/users/\(AppUserDefaults.sessionID)/playlists/watchhistory"
        case .ValidateOtp(let otpString,let mobileNo) :              return "/users/verification/\(otpString)?region=\(AppConfiguration.regionForApp)&auth_token=\(AppConfiguration.authTokenForApp)&type=msisdn&mobile_number=\(mobileNo)"
        case .VerifyPassword(let password):             return "/users/\(AppUserDefaults.sessionID)/verify_password"
        case .SetParentalControl: return "/users/\(AppUserDefaults.sessionID)/account"

        case .OTPVerificationForForgotPassword(let otp): return "/users/otp_verification"
            
        //TVOS login
        case .GenerateLoginCode:                return "/generate_token"
        case .VerifyLoginToken(let token):      return "/users/verify_tv_token\(OTTServer.fixedGetParameters)&token=\(token)"
            
        case .GetUserPurchasedPlans:            return "/users/\(AppUserDefaults.sessionID)/user_plans\(OTTServer.fixedGetParameters)"

        }
    }
    
    
    fileprivate  func createPlayListItemString(items:[String]) -> String
    {
        
        
        var string = ""
        
        for item in items
        {
            string = string + "\"content_id\":\"\(item)\","
        }
        
        string.remove(at: string.endIndex)
        
        AppUtilities.printLog(message: "String after removing last comma : \(string)")
        
        string = "{" + string + "}"
        
        return string
        
    }
    
    
    fileprivate var api:API
    
    var apiURL:String{
        return ServerUserDefaults.users_ServerURl.serviceURL + urlPath
    }
    
    public init(api:API)
    {
        self.api = api
    }
    
    
    
    
    var inputParameters:[String:Any]?
    {
        
        
        switch api
        {
            
        case .AddItemToList(_,let contentId,let catalogId):                     return ["auth_token": AppConfiguration.authTokenForApp,"listitem": ["content_id":contentId, "catalog_id":catalogId]]
            
        case .AddProfile(let firstname, let lastname, let age, let child):      return ["auth_token": AppConfiguration.authTokenForApp,"user_profile": ["firstname":firstname, "lastname":lastname, "age":age,"child":child]]
            
        case .Assignprofile(let profileId): return ["user_profile":["profile_id":profileId]]
            
        case .BindSessionToProfile(let profile_id): return ["auth_token": AppConfiguration.authTokenForApp,"user_profile": ["profile_id":profile_id]]
            
        case .ChangePassword(let current_password,let password, let confirm_password): return ["auth_token": AppConfiguration.authTokenForApp,"user": ["current_password":current_password,"password":password,"confirm_password":confirm_password, "region":AppConfiguration.regionForApp]]
            
        case .CheckEmailAvalible(let email_id): return ["auth_token": AppConfiguration.authTokenForApp,"user": ["email_id":email_id, "region":AppConfiguration.regionForApp]]
            
        case .CreatePlayList(let playListName, let playListType): return ["auth_token": AppConfiguration.authTokenForApp,"playlist": ["name":playListName, "type":playListType]]
            
        case .DeleteProfile: return nil
            
        case .EditProfile(let firstname, let lastname, let age, let child, _): return ["auth_token": AppConfiguration.authTokenForApp,"user_profile": ["firstname":firstname, "lastname":lastname, "age":age,"child":child]]
            
        case .ForgotPassword(let email_id): return ["auth_token": AppConfiguration.authTokenForApp,"user": ["user_id":email_id, "region":AppConfiguration.regionForApp, "type":"email"]]
            
        case .ForgotPasswordForMobileNoUser(let mobile): return ["auth_token":AppConfiguration.authTokenForApp, "user": ["user_id":mobile, "region":AppConfiguration.regionForApp,"type":"msisdn"]]
            
        case .GetAllPlayLists:              return nil
        case .GetPlaylistWith:              return nil
        case .GetProfilesForAccount:        return nil
        case .GetUserDetails:               return nil
            
        case .InitTransaction(let parameters): return parameters
            
        case .PurchaseSubscription(let parameters): return parameters
            
        case .RemoveItemFromPlayList :      return nil
            
        case .Renameplaylist(_, let playlistName, let playlistType): return ["auth_token": AppConfiguration.authTokenForApp,"playlist": ["name":playlistName, "type":playlistType]]
            
        case .ResendOtp(let mobileNo):  return ["auth_token": AppConfiguration.authTokenForApp,"user": ["email_id": mobileNo, "type":"msisdn"]]
            
        case .ResendVerification(let email_id): return ["auth_token": AppConfiguration.authTokenForApp,"user": ["email_id":email_id, "region":AppConfiguration.regionForApp,"type":"email"]]
            
        case .ResetPasswordForMobile(let otp,let newPassword,let confirmPassword): return ["auth_token":AppConfiguration.authTokenForApp, "user": ["key":otp,"password":newPassword,"confirm_password":confirmPassword,"region":AppConfiguration.regionForApp,"type":"msisdn"]]
            
        case .SignInMobile(let mobile,let password): return["auth_token":AppConfiguration.authTokenForApp,"user":["user_id":mobile,"password":password,"region":AppConfiguration.regionForApp,"type":"msisdn"]]
            
        case .SignUp(let email_id,let firstname,let lastname,let password): return ["auth_token": AppConfiguration.authTokenForApp,"user": ["email_id":email_id, "firstname":firstname, "lastname":lastname,"password":password,"region":AppConfiguration.regionForApp]]
            
        case .SignUpUsingMobileNumber(let mobileNo,let firstName ,let lastName ,let password): return ["auth_token":AppConfiguration.authTokenForApp,"user":["user_id":mobileNo,"firstname":firstName,"lastname":lastName,"password":password,"region":AppConfiguration.regionForApp,"type":"msisdn"]]
            
        case .ThirdPartyLogin(let provider,let uid,let firstname, let ext_account_email_id, let profile_pic): return ["auth_token": AppConfiguration.authTokenForApp,"user": ["provider":provider, "uid":uid,"firstname":firstname,"ext_account_email_id":ext_account_email_id,"region":AppConfiguration.regionForApp,"profile_pic":profile_pic]]
            
        case .UpdateAddProfileStatus:   return ["auth_token": AppConfiguration.authTokenForApp,"user": ["add_profile":false.description]]
            
        case .UpdatePlaylistWithType(let catalogId,let  contentId,_):return ["auth_token": AppConfiguration.authTokenForApp,"listitem": ["content_id":contentId, "catalog_id":catalogId]]
            
        case .UpdateProfilePicture(let profile_pic) :return ["auth_token": AppConfiguration.authTokenForApp,"user": ["profile_pic":profile_pic]]
            
            
            
        case .UpdateUserDetails(let firstname, let lastname, let mobilenos, let email, let birthdate): return ["auth_token": AppConfiguration.authTokenForApp,"user": ["firstname":firstname,"lastname":lastname, "user_email_id":email,"birthdate":birthdate]]
            
        case .UpdateMobileUserDetails(let firstname, let lastname, let email, let birthdate): return ["auth_token": AppConfiguration.authTokenForApp,"user": ["firstname":firstname,"lastname":lastname, "user_email_id":email,"birthdate":birthdate]]
            
        case .UpdateEmailUserDetails(let firstname, let lastname, let mobilenos, let birthdate): return ["auth_token": AppConfiguration.authTokenForApp,"user": ["firstname":firstname,"lastname":lastname, "mobile_number":mobilenos,"birthdate":birthdate]]
            
            
        case .UpdateUserRating(let catalogId,let contentId,let rating, _) :return ["auth_token": AppConfiguration.authTokenForApp,"listitem": ["content_id":contentId, "catalog_id":catalogId, "user_ratings":rating]]
            
        case .UpdateWatchHistory(let catalogId,let  contentId,let playbackTime):return ["auth_token": AppConfiguration.authTokenForApp,"listitem": ["content_id":contentId, "catalog_id":catalogId, "play_back_time":playbackTime]]
            
        case .ValidateOtp(_)            : return nil
        case.SignIn(let email_id,let password):         return ["auth_token": AppConfiguration.authTokenForApp,"user": ["email_id":email_id, "password":password,"region":AppConfiguration.regionForApp]]
            
        case .SignOut:                                  return ["auth_token": AppConfiguration.authTokenForApp]
            
        case .SetParentalControl(let isParentalControlEnabled, let pin) : return ["auth_token": AppConfiguration.authTokenForApp,"user": ["parental_control":isParentalControlEnabled.description, "parental_pin":pin]]

        case .VerifyPassword(let password): return ["auth_token": AppConfiguration.authTokenForApp,"user":["password":password]]
            
        case .OTPVerificationForForgotPassword(let otp): return ["auth_token": AppConfiguration.authTokenForApp,"user": ["key":otp,"region":AppConfiguration.regionForApp]]
            
        //TVOS login
        case .GenerateLoginCode:                                  return ["auth_token": AppConfiguration.authTokenForApp]
        case .VerifyLoginToken:                                  return nil
            
        default:                return nil
        }
        
    }
    
    
    public func call(successHandler: ((_ responseJSON: [String: Any]) -> Void)? = nil, errorHandler: ((_ code:String,_ message:String) -> Void)? = nil,internetErrorHandler:((Int)->Void)? = nil)
    {
        
        let service = OTTServer(serviceURL: apiURL, httpMethod: httpMethodForAPI,jsonParameters:inputParameters)
        AppUtilities.printLog(message:"\(String(describing: inputParameters))")
        AppUtilities.printLog(message:" url :\(apiURL)")
        
        service.call(successHandler: successHandler,
                     errorHandler: errorHandler,
                     internetErrorHandler: internetErrorHandler)
    }
    
    public func apiRequest(responseHandler: ((_ response: DataLoader<[String: Any]>) -> Void)? = nil) {
        
        
        let service = OTTServer(serviceURL: apiURL, httpMethod: httpMethodForAPI,jsonParameters:inputParameters)
        AppUtilities.printLog(message:"\(String(describing: inputParameters))")
        AppUtilities.printLog(message:" url :\(apiURL)")
        
      //  print("Service apiURL :\(apiURL)")
        service.callWithDataLoader(responseHandler: responseHandler)
        
    }
}
