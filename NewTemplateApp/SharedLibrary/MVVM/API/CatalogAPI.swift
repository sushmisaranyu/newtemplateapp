//
//  CinestaanCatalogAPI.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 28/02/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation



public class CatalogAPI
{
    
    
    public enum API
    {
        case GetHomepage
        case GetHomePageWith(listID:String)
        case GetMenuList        
        case GetExtras(itemId:String,catalogId:String)
        case GetMediaItemFor(contentId:String,catalogId:String)
        case GetSeasonsForShow(showId:String)
        case GetEpisodesForSeason(seasonId:String)
        case GetEpisodesForShow(showId:String)
        case GetVideosForCatalogList(listId:String)
        case GetVideosForCatalogListWithKidsFilter(listId:String)
        case GetVideosForChannelsWithTheme(listId:String,theme:String)
        case GetVideosForChannelsWithContentCategory(listId:String,content_category:String)
        case GetVideosForCatalogListNoLimit(listId:String)

        case GetSubscriptionDetails
        
        case GetEpisodeWithSeasonFor(contentId:String,showID:String,subcategoryID:String)
        case GetEpisodeFor(contentId:String,showID:String)
        
        case GetEpisodesWithType(catalogID:String,showID:String,filter:String)
        case GetVideosForParentCatalog(catalogID:String)
        case GetVideosForParentCatalogForTV(catalogID:String)
        case GetCatalogsWithGenreFilter(catalogID:String,genre:String)
        case Feedback(name:String,subject:String,gender:String,phone:String,city:String,email_id:String)
        case Related(catalodId:String,contentId:String)

        case GetProgramDetails(contentId:String,catalogId:String)
        case GetSharedVideoItem(contentURL:String)
    }


    var filter:String
    {
        if OTTProfile.isBoundProfileAChild() == true
        {
            return "&category=kids"
        }
        else
        {
            return ""
        }
    }

    var httpMethodForAPI:HTTPMethodForAPI
    {
        switch api
        {
            case .Feedback:             return .POST
            case .GetSharedVideoItem:   return .POST
            default:                     return .GET
        }
    }
    
    var urlPath:String
    {
        switch api
        {
        case .GetHomePageWith(let listId): return "/catalog_lists/\(listId).gzip" + OTTServer.fixedGetParameters
        case .GetHomepage:                                   return "/catalog_lists/epic-home-list-mobile-new.gzip" + OTTServer.fixedGetParameters
        case .GetMenuList: return "/catalog_lists.gzip" + OTTServer.fixedGetParameters
        case .GetExtras(let itemId, let catalogId): return "/catalogs/\(catalogId)/items/\(itemId)/videolists.gzip?auth_token=\(AppConfiguration.authTokenForApp)"
        case .GetMediaItemFor(let contentId,let catalogId): return "/catalogs/\(catalogId)/items/\(contentId).gzip\(OTTServer.fixedGetParameters)" 
        case .GetSeasonsForShow(let showId): return "/catalogs/shows/\(showId)/subcategories.gzip" + OTTServer.fixedGetParameters
            
        case .GetEpisodesForSeason(let seasonId) : return "/catalogs/shows/subcategories/\(seasonId)/episodes.gzip"  + OTTServer.fixedGetParameters + "&page=0&page_size=200"
            
        case .GetEpisodesForShow(let showId): return "/catalogs/shows/\(showId)/episodes.gzip"  + OTTServer.fixedGetParameters + "&page=0&page_size=200"
        
        case .GetVideosForCatalogList(let listId): return "/catalog_lists/\(listId).gzip" + OTTServer.fixedGetParameters + "&page=0&page_size=200"

        case .GetVideosForCatalogListWithKidsFilter(let listId): return "/catalog_lists/\(listId).gzip" + OTTServer.fixedGetParameters + "&page=0&page_size=200" + filter

        case .GetVideosForChannelsWithTheme(let listId,let theme): return "/catalog_lists/\(listId).gzip" + OTTServer.fixedGetParameters + "&theme=\(theme)" + filter

        case .GetVideosForChannelsWithContentCategory(let listId, let content_category): return "/catalogs/\(listId)/items.gzip" + OTTServer.fixedGetParameters +  "&status=any&content_category=\(content_category)&theme=live"
            
        case .GetVideosForCatalogListNoLimit(let listId): return "/catalog_lists/\(listId).gzip" + OTTServer.fixedGetParameters + "&page=0&page_size=150"
            
      //  case .GetMenuList: return "/catalog_lists/left-menu.gzip" + OTTServer.fixedGetParameters
        
         case .GetSubscriptionDetails: return "/catalogs/subscription/items.gzip" + OTTServer.fixedGetParameters
        
        case .GetEpisodeWithSeasonFor(let contentId, _, let subcategoryID): return "/catalogs/shows/subcategories/\(subcategoryID)/episodes/\(contentId).gzip" + OTTServer.fixedGetParameters +  "&page=0&page_size=200"
            
        case .GetEpisodeFor(let contentId, let showID): return "/catalogs/shows/\(showID)/episodes/\(contentId).gzip" + OTTServer.fixedGetParameters
            
            // http://23.21.228.227:3000/catalogs/5940d0cb99728d253700006b/items/5940d23e99728d253700006e/episode_list?auth_token=ncV4QsB7G8srXKRtwSUF&region=IN&episode_type=all
            
        case .GetEpisodesWithType(let catalogID,let showID,let filter):  return "/catalogs/\(catalogID)/items/\(showID)/episode_list.gzip" + OTTServer.fixedGetParameters+filter+"&page=0&page_size=200"
            
        case .GetVideosForParentCatalog(let catalogID): return "/catalogs/\(catalogID)/items.gzip" + OTTServer.fixedGetParameters + "&page=0&page_size=200"

        case .GetVideosForParentCatalogForTV(let catalogID): return "/catalogs/\(catalogID)/items.gzip" + OTTServer.fixedGetParameters + "&page=0&page_size=200&status=any"
            
         case .GetCatalogsWithGenreFilter(let catalogID,let genre): return "/catalogs/\(catalogID)/items.gzip?genre=\(genre)&auth_token=\(AppConfiguration.authTokenForApp)&region=\(AppConfiguration.regionForApp)" + "&page=0&page_size=200"
        //Hardcoding Feedback catalogID
        case .Feedback(_, _, _, _, _,_): return "/catalogs/\(AppUserDefaults.feedbackIDForApp)/items" + OTTServer.fixedGetParameters


        case .Related(let catalodId, let contentId): return "/catalogs/\(catalodId)/items/\(contentId)/related.gzip" + OTTServer.fixedGetParameters


           case .GetProgramDetails(let contentId, let catalogId): return "/catalogs/\(catalogId)/items/\(contentId)/programs.gzip" + OTTServer.fixedGetParameters + "&status=any"

        case .GetSharedVideoItem: return "/users/get_share_parameters.gzip" + OTTServer.fixedGetParameters

        }
    }
    
    fileprivate var api:API
    
    var apiURL:String{
        return ServerUserDefaults.catalogs_ServerURL.serviceURL + urlPath
    }
    
    public init(api:API)
    {
        self.api = api
    }
    
    
    
    
    var inputParameters:[String:Any]?
    {
        switch api
        {
        //'{"auth_token":"Mszhz6BxoxamWuSqvqhJ","content":{"title":"feedback", "status":"published", "name":"ankita", "subject":"this is first feedback","gender":"F","phone":"9740876169","city":"karwar","session_id":"9fc5dab67ceafd560073db326057abdd"}}'
        case .Feedback(let name, let subject, let gender, let phone, let city,let email_id):return ["auth_token": AppConfiguration.authTokenForApp,
                                                                                "content": ["title":"feedback", "status":"published" ,"name":name,"subject":subject,"gender":gender,"phone":phone,"city":city,"email_id":email_id,"session_id": (AppUserDefaults.sessionID)]]

            //{"auth_token":"Ts4XpMvGsB2SW7NZsWc3","data":{"content_url":"/kids-rhymes/finger-family-kid-voice-pop-rock-style"}}
        case .GetSharedVideoItem(let contentURL):return ["auth_token": AppConfiguration.authTokenForApp,"data": ["content_url":contentURL]]
        default: return nil
            
        }
    }
    
    public func call(successHandler: ((_ responseJSON: [String: Any]) -> Void)? = nil, errorHandler: ((_ code:String,_ message:String) -> Void)? = nil,internetErrorHandler:((Int)->Void)? = nil)
    {
        let service = OTTServer(serviceURL: apiURL, httpMethod: httpMethodForAPI,jsonParameters:inputParameters)
        service.call(successHandler: successHandler,
                     errorHandler: errorHandler,
                     internetErrorHandler: internetErrorHandler)
    }


    public func apiRequest(responseHandler: ((_ response: DataLoader<[String: Any]>) -> Void)? = nil) {


        let service = OTTServer(serviceURL: apiURL, httpMethod: httpMethodForAPI,jsonParameters:inputParameters)
        AppUtilities.printLog(message:"\(String(describing: inputParameters))")
        AppUtilities.printLog(message:" url :\(apiURL)")

        service.callWithDataLoader(responseHandler: responseHandler)

    }

}
