//
//  SearchAPI.swift
//  SaraynuLibrary
//
//  Created by Ashley Dsouza on 26/06/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation


public class SearchAPI
{
    
    public enum API
    {
        case GetAllVideosInGenre(genre:String)
        case SearchAutoComplete(textToAutoComplete:String)
        case Search(text:String)

       
        
    }
    
    var httpMethodForAPI:HTTPMethodForAPI
    {
        switch api
        {
        default: return .GET
        }
    }
    
    // http://23.21.228.227:3000/search?q=devotion&search_in=genres&region=IN&auth_token=Mszhz6BxoxamWuSqvqhJ
    var urlPath:String
    {
        switch api
        {
        case .GetAllVideosInGenre(let genre):                                   return "/search.gzip?q=\(genre)&search_in=genres&region=\(AppConfiguration.regionForApp)&auth_token=\(AppConfiguration.authTokenForApp)"+"&page=0&page_size=200"
        
       // return "/catalog_lists.gzip" + OTTServer.fixedGetParameters + "&page=0&page_size=200"
            
            
        case .SearchAutoComplete(let textToAutoComplete): return "/search/autocomplete\(OTTServer.fixedGetParameters)&&filters=category%2Call&q=\(textToAutoComplete)"
            
            case .Search(let text): return "/search.gzip\(OTTServer.fixedGetParameters)&filters=category%2Call&q=\(text)" + "&page=0&page_size=200\(filter)"
            

        }
    }


    var filter:String
    {
        if OTTProfile.isBoundProfileAChild() == true
        {
            return "&category=kids"
        }
        else
        {
            return ""
        }
    }



    fileprivate var api:API
    
    var apiURL:String{
        return ServerUserDefaults.search_ServerURL.serviceURL + urlPath
    }
    
    public init(api:API)
    {
        self.api = api
    }
    
    
    
    
    var inputParameters:[String:Any]?
    {
        switch api
        {
            
        default: return nil
            
        }
    }
    
    public func call(successHandler: ((_ responseJSON: [String: Any]) -> Void)? = nil, errorHandler: ((_ code:String,_ message:String) -> Void)? = nil,internetErrorHandler:((Int)->Void)? = nil)
    {
        let service = OTTServer(serviceURL: apiURL, httpMethod: httpMethodForAPI)
        service.call(successHandler: successHandler,
                     errorHandler: errorHandler,
                     internetErrorHandler: internetErrorHandler)
    }

    public func apiRequest(responseHandler: ((_ response: DataLoader<[String: Any]>) -> Void)? = nil) {


        let service = OTTServer(serviceURL: apiURL, httpMethod: httpMethodForAPI,jsonParameters:inputParameters)
        AppUtilities.printLog(message:"\(String(describing: inputParameters))")
        AppUtilities.printLog(message:" url :\(apiURL)")

        service.callWithDataLoader(responseHandler: responseHandler)

    }
    
}
