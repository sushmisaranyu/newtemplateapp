//
//  DefaultAPI.swift
//  SaraynuLibrary
//
//  Created by Ashley Dsouza on 26/06/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation





public class DefaultAPI
{
    
    public enum API
    {
        case GetRegion
        case GetServerURLs
        case GetConfig(version:String)
        case CountryCode
        
    }
    
    var httpMethodForAPI:HTTPMethodForAPI
    {
        switch api
        {
        default: return .GET
        }
    }
    
    var urlPath:String
    {
        switch api
        {
        case .GetRegion:                                   return "/regions/autodetect/ip" + "?auth_token=\(AppConfiguration.authTokenForApp)"
        case .GetServerURLs:                               return "/services" + "?auth_token=\(AppConfiguration.authTokenForApp)"
        case .GetConfig(let version):                      return "/catalogs/message/items/app-config-params" + "\(OTTServer.fixedGetParameters)" + "&current_version=\(version)"
        
        case .CountryCode:  return "/regions?auth_token=\(AppConfiguration.authTokenForApp)"
            
        }
    }
    
    fileprivate var api:API
    
    var apiURL:String{
        return AppConfiguration.serverURL.value()! + urlPath
    }
    
    public init(api:API)
    {
        self.api = api
    }
    
    
    
    
    var inputParameters:[String:Any]?
    {
        switch api
        {
            
        default: return nil
            
        }
    }
    
    public func call(successHandler: ((_ responseJSON: [String: Any]) -> Void)? = nil, errorHandler: ((_ code:String,_ message:String) -> Void)? = nil,internetErrorHandler:((Int)->Void)? = nil)
    {
        let service = OTTServer(serviceURL: apiURL, httpMethod: httpMethodForAPI)
        service.call(successHandler: successHandler,
                     errorHandler: errorHandler,
                     internetErrorHandler: internetErrorHandler)
    }
    

    public func apiRequest(responseHandler: ((_ response: DataLoader<[String: Any]>) -> Void)? = nil) {


        let service = OTTServer(serviceURL: apiURL, httpMethod: httpMethodForAPI,jsonParameters:inputParameters)
        AppUtilities.printLog(message:"\(String(describing: inputParameters))")
        AppUtilities.printLog(message:" url :\(apiURL)")

        service.callWithDataLoader(responseHandler: responseHandler)

    }
    
    public func callforCountryCode(successHandler: ((_ responseJSON: [String: Any]) -> Void)? = nil,successHandlerWithArray:@escaping ((_ responseJsonArray : [[String:Any]]) ->Void), errorHandler: ((_ code:String,_ message:String) -> Void)? = nil,internetErrorHandler:((Int)->Void)? = nil)
    {
                let service = OTTServer(serviceURL:apiURL, httpMethod: httpMethodForAPI , jsonParameters:nil,hasArrayInResponse:true)
                service.call(successHandler: successHandler, successHandlerwithArray: successHandlerWithArray, errorHandler: errorHandler, internetErrorHandler: internetErrorHandler)
        
    }
   
}
