//
//  BaseViewModel.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 06/08/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift

public enum ViewStateError: Error
{
    case NoError
    case NoData
    case ErrorFromServer(title:String,message:String)
    case InternetConnectivityError

}



open class BaseViewModel
{
    open let isLoading = BehaviorSubject<Bool>(value: false)
    open let viewStateError = BehaviorSubject<ViewStateError>(value:ViewStateError.NoError)
}
