//
//  ViewStateProtocol.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 03/08/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


protocol ViewStateProtocol {

    /// Setup Observer  for handling view state change
    func setupObservers(viewModel:BaseViewModel)

    /// Setup default UI for the viewcontroller
    func setupUI()

    /// Add and customize error view
    func setupErrorView()

    /// Add and customize activity indicator
    func setupLoader()

    /// Show activity indicator
    func showLoader()

    /// Hide activity Indicator
    func hideLoader()


    /// Show error view
    ///
    /// - Parameters:
    ///   - title: title to be show
    ///   - message: message to be shown
    func showErrorViewWithMessage(title:String,message:String)

    /// Show No internet error message
    func showNoInternetErroView()

    /// Hide error view
    func hideErrorView()
}
