//
//  BaseViewController.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 03/08/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import NVActivityIndicatorView
import SnapKit
import RxSwift
import RxCocoa
import RxDataSources




protocol EmptyDataViewDependancyProtocol
{
    func setupErrorView(view:UIView)
    func showErrorView()
    func hideErrorView()
}



open class BaseViewController:UIViewController
{
    var emptyDataView:EmptyDataSetView?

    open let bag = DisposeBag()

    var activityIndicatorView:AppActivityIndicatorHelper?

    static var activityIndicatorType:NVActivityIndicatorType = NVActivityIndicatorType.audioEqualizer

    static var activityBackgroundColor: UIColor = UIColor.black.withAlphaComponent(0.25)

    static var activityColor:UIColor = UIColor.white

    static var emptyDataViewTitleFont =  "HelveticaNeue-Bold"

    static var emptyDataViewTitleFontSize:CGFloat =  AppUtilities.selectPropertyForDevice(iPhone: 20, iPad: 25.5)

    static var emptyDataViewMessageFont = "HelveticaNeue"

    static var emptyDataViewMessageFontSize:CGFloat =  AppUtilities.selectPropertyForDevice(iPhone: 14, iPad: 17.85)

    static var emptyDataViewTint:UIColor = UIColor.white

    static var emptyDataViewRefreshImageName = "Refresh"

    static var emptyDataViewErrorImageName   = ""

    static var emptyDataViewNoInternetImageName = "No Connection"

    open override func viewDidLoad() {
        super.viewDidLoad()
        print("Base View controller View did load")

    }




}

extension BaseViewController: ViewStateProtocol
{
    open func setupUI() {
        setupLoader()
        setupErrorView()
    }

   open func setupObservers(viewModel:BaseViewModel) {

    viewModel.isLoading
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { [weak self] isLoading in
            AppUtilities.printLog(message: "OnNext Loader \(isLoading)")
            if isLoading == true
            {
                self?.showLoader()
            }
            else
            {
                self?.hideLoader()
            }


        }, onError: {  (error) in

        }, onCompleted: {
            AppUtilities.printLog(message: "Loader Completed")
        }, onDisposed: {
            AppUtilities.printLog(message: "Loader Disposed")
        }).disposed(by: bag)


        viewModel.viewStateError
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] error in
                AppUtilities.printLog(message: "OnNext Error \(error)")


                switch error
                {
                    case .NoError: self?.hideErrorView()
                    case .ErrorFromServer(let title, let message): self?.showErrorViewWithMessage(title: title, message: message)
                    case .InternetConnectivityError: self?.showNoInternetErroView()
                    case .NoData: self?.showNoDataView()
                }


                }, onError: {  (error) in

            }, onCompleted: {
                AppUtilities.printLog(message: "Loader Completed")
            }, onDisposed: {
                AppUtilities.printLog(message: "Loader Disposed")
            }).disposed(by: bag)

    }

   open func setupErrorView() {

        if self.emptyDataView != nil { return }

        DispatchQueue.main.async {

            self.emptyDataView = EmptyDataSetView()
            self.view.addSubview(self.emptyDataView!)
            self.emptyDataView?.refreshDelegate = self

            self.emptyDataView?.setupEmptyView(titleFont: BaseViewController.emptyDataViewTitleFont, titleFontSize: BaseViewController.emptyDataViewTitleFontSize, msgFont: BaseViewController.emptyDataViewMessageFont, msgFontSize: BaseViewController.emptyDataViewMessageFontSize, refreshTint: BaseViewController.emptyDataViewTint)

            self.emptyDataView?.snp.makeConstraints { (make) in
                make.top.equalTo(self.view.snp.top)
                make.left.equalTo(self.view.snp.left)
                make.right.equalTo(self.view.snp.right)
                make.bottom.equalTo(self.view.snp.bottom)
                make.center.equalTo(self.view.center)

            }

            self.emptyDataView?.isHidden = true
        }


    }

   open func setupLoader() {

        if activityIndicatorView != nil {return}

        activityIndicatorView =  AppActivityIndicatorHelper(viewController: self, isUserInteractionDisabled: true, onMainQueue: true, backgroundColor: BaseViewController.activityBackgroundColor ,indicatorColor:BaseViewController.activityColor, activityIndicatorType: BaseViewController.activityIndicatorType)
        activityIndicatorView?.setupActivityIndicator()

    }

    open func showLoader() {
        self.activityIndicatorView?.startActivityIndicator()
    }

   open func hideLoader() {
        self.activityIndicatorView?.stopActivityIndicator()
    }

   open func showErrorViewWithMessage(title: String, message: String) {
        self.emptyDataView?.isHidden = false
        self.emptyDataView?.CreateEmptyDataSetView(BaseViewController.emptyDataViewErrorImageName , refreshImageName: BaseViewController.emptyDataViewRefreshImageName, message:message,title:title)

    }

    open func showNoDataView()
    {
        self.emptyDataView?.isHidden = false
        self.emptyDataView?.CreateEmptyDataSetView(BaseViewController.emptyDataViewErrorImageName , refreshImageName: BaseViewController.emptyDataViewRefreshImageName, message:"",title:"")

    }

    open func showNoInternetErroView() {
        DispatchQueue.main.async {
            self.emptyDataView?.isHidden = false
            self.emptyDataView?.CreateEmptyDataSetView(BaseViewController.emptyDataViewErrorImageName , refreshImageName: BaseViewController.emptyDataViewRefreshImageName, message:"Please check your internet connection and try again.",title:"No internet")
        }

    }

    open func hideErrorView() {
        DispatchQueue.main.async {
            self.emptyDataView?.isHidden = true
        }
    }



}


extension BaseViewController:EmptyDataSetViewDelegate
{
      @objc open func Refresh() {

    }


}
