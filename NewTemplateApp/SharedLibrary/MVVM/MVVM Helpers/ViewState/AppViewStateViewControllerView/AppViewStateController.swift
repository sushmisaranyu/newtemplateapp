//
//  AppViewStateController.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 05/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import UIKit

open class AppViewStateController:UIViewController, BaseAppViewState
{
    public func showToastMessage(message: String, state: ToastState) {
        self.toastViewDependancy?.showToast(message: message, state: state)
    }

    public var loadingDependancy: LoaderDependancy? { get {return loader} }

    public var emptyDataViewDependancy: EmptyDataViewDependancy? { get {return emptyDataView} }

    public var toastViewDependancy: ToastViewDependancy? { get {return toastView} }


    var loader:LoaderDependancy?
    var emptyDataView:EmptyDataViewDependancy?
    var toastView:ToastViewDependancy?

    open let bag = DisposeBag()

    public func setup(loader:LoaderDependancy?,emptyDataView:EmptyDataViewDependancy?,toastView:ToastViewDependancy?,isLoadingDriver:Driver<Bool>?)
    {
        self.loader = loader
        self.emptyDataView = emptyDataView
        self.toastView = toastView

        isLoadingDriver?.distinctUntilChanged().drive(onNext: { [weak self] (isLoading) in
            if isLoading == true{
                self?.view.endEditing(true)
                self?.loader?.showLoader()
            }
            else
            {
                self?.loader?.hideLoader()
            }
        }).disposed(by: bag)

        setupUI()
    }

    public func setupUI() {
        self.emptyDataView?.setupErrorView(view: self.view)
        self.loader?.setupLoader(view: self.view)
        self.toastView?.setupView(parentView: self.view)
    }



    public func showErrorViewWithMessage(title: String, message: String,contentView:UIView?,refresh:(()->Void)?) {
        emptyDataView?.showErrorViewWithMessage(title: title, message: message, contentView: contentView, refresh: refresh)
    }

    public func showNoInternetErroView(contentView:UIView?,refresh:(()->Void)?) {
        emptyDataView?.showNoInternetErroView(contentView:contentView, refresh: refresh)
    }

    public func hideErrorView(contentView:UIView?) {
        emptyDataView?.hideErrorView(contentView: contentView)
    }


}
