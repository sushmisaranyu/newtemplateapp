//
//  ToastViewDependancy.swift
//  Shemaroo
//
//  Created by Ashley Dsouza on 09/11/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import UIKit

public enum ToastState
{
    case success
    case error
}

public protocol ToastViewDependancy
{
    func getToastView() -> UIView?
    func setupView(parentView:UIView)
    func showToast(message:String,state:ToastState)
}

