//
//  EmptyDataViewDependancy.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 05/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import UIKit

public protocol EmptyDataViewDependancy: class
{

    /// Add and customize error view
    func setupErrorView(view:UIView)

    /// Show error view
    ///
    /// - Parameters:
    ///   - title: title to be show
    ///   - message: message to be shown
    func showErrorViewWithMessage(title:String,message:String,contentView:UIView?,refresh:(()->Void)?)

    /// Show No internet error message
    func showNoInternetErroView(contentView:UIView?,refresh:(()->Void)?)

    /// Hide error view
    func hideErrorView(contentView:UIView?)
}
