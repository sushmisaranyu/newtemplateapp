//
//  LoaderDepedancy.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 05/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import UIKit

public protocol LoaderDependancy
{
    /// Add and customize activity indicator
    func setupLoader(view:UIView)

    /// Show activity indicator
    func showLoader()

    /// Hide activity Indicator
    func hideLoader()
}
