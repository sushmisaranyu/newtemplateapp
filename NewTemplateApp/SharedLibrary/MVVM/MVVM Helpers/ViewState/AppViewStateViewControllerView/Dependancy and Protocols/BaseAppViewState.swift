//
//  BaseAppViewController.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 05/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

public protocol BaseAppViewState {


    var loadingDependancy:LoaderDependancy? {get}
    var emptyDataViewDependancy:EmptyDataViewDependancy? {get}

    /// Setup Observer and ui  for handling view state change
    func setup(loader:LoaderDependancy?,emptyDataView:EmptyDataViewDependancy?,toastView:ToastViewDependancy?,isLoadingDriver:Driver<Bool>?)

    /// Setup default UI for the viewcontroller
    func setupUI()

    /// Show error view
    ///
    /// - Parameters:
    ///   - title: title to be show
    ///   - message: message to be shown
    func showErrorViewWithMessage(title:String,message:String,contentView:UIView?,refresh:(()->Void)?)

    /// Show No internet error message
    func showNoInternetErroView(contentView:UIView?,refresh:(()->Void)?)

    /// Hide error view
    func hideErrorView(contentView:UIView?)

    /// Show Toast Message
    func showToastMessage(message:String,state:ToastState)
}

