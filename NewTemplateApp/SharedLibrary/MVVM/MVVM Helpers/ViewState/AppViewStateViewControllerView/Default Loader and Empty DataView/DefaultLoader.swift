//
//  DefaultLoader.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 05/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import NVActivityIndicatorView
import SnapKit
import UIKit

public class DefaultLoader:LoaderDependancy
{

    public init()
    {
        
    }

    var holderView:UIView?
    var activityIndicator:NVActivityIndicatorView?

    var imgview : UIImageView?
    
    public func setupLoader(view: UIView) {
        if holderView != nil { return }

        holderView = UIView()
        holderView?.backgroundColor = UIColor.white.withAlphaComponent(0.3)

        view.addSubview(holderView!)
        view.bringSubview(toFront: holderView!)

        holderView?.snp.remakeConstraints({ (make) in
            make.edges.equalTo(view)
            make.height.equalTo(view)
            make.width.equalTo(view)
        })
        self.holderView?.isHidden = true
      
        #if os(iOS)
            activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        #else
            activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
        #endif

        activityIndicator?.color = UIColor(hexString: "#d51f53")!
        activityIndicator?.type = .ballRotate
        holderView?.addSubview(activityIndicator!)
        
        activityIndicator?.snp.remakeConstraints({ (make) in
            make.center.equalTo(holderView!)
        })
       

       

    }

    public func showLoader() {
       
        DispatchQueue.main.async {
            self.holderView?.isHidden = false
            self.activityIndicator?.startAnimating()
        }
       
    }

    public func hideLoader() {

        DispatchQueue.main.async {
            self.holderView?.isHidden = true
            self.activityIndicator?.stopAnimating()
        }
       
       
    }
}
