//
//  DefaultEmptyDataView.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 05/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import UIKit

public class DefaultEmptyDataView:EmptyDataViewDependancy
{
    public init()
    {

    }

    var reload:(()->Void)?

    var dataView:EmptyDataSetView?

    public func setupErrorView(view: UIView) {
        if dataView != nil {return}

        dataView?.refreshDelegate = self

        dataView = EmptyDataSetView(frame: view.frame)
        view.addSubview(dataView!)

        dataView?.snp.remakeConstraints({ (make) in
            make.edges.equalTo(view)
        })

        dataView?.setupEmptyView(titleFont: "HelveticaNeue-Bold", titleFontSize: 16, msgFont: "Helvetica", msgFontSize: 12, refreshTint: .black)
        dataView?.titleColor = UIColor.black
        dataView?.messageColor = UIColor.black
        dataView?.isHidden = true

    }

    public func showErrorViewWithMessage(title: String, message: String,contentView:UIView?,refresh:(()->Void)?) {
        DispatchQueue.main.async {
            self.dataView?.CreateEmptyDataSetView("", refreshImageName: "Refresh", message: message, title: title)
             self.dataView?.isHidden = false
             contentView?.isHidden = true
        }
    }

    public func showNoInternetErroView(contentView:UIView?,refresh:(()->Void)?) {
        DispatchQueue.main.async {
             self.dataView?.CreateEmptyDataSetView("", refreshImageName: "Refresh", message: "Please check your internet connection and try again", title: "No Internet")
            self.dataView?.isHidden = false
            contentView?.isHidden = true
        }

    }

    public func hideErrorView(contentView:UIView?) {
        DispatchQueue.main.async {
             self.dataView?.isHidden = true
             contentView?.isHidden = false
        }

    }


}

extension DefaultEmptyDataView:EmptyDataSetViewDelegate
{
    public func Refresh() {
        reload?()
    }


}
