//
//  DataLoader.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 17/08/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


public enum DataLoader<T>
{
    case success(response:T)
   // case serverError(message:String)
    case serverErrorWithCode(code:String,message:String)
    case connectivityError
    case None
}

public class DataLoaderHelper<T,U>
{
    public class func convert(responseHandler: @escaping ((_ response: DataLoader<T>) -> Void), dataLoader:DataLoader<U>)
    {
        switch dataLoader {
        case .connectivityError: responseHandler(.connectivityError)
        case .None: responseHandler(.None)
        //case .serverError(let message): responseHandler(DataLoader.serverError(message: message))
        case.serverErrorWithCode(let code, let message): responseHandler(DataLoader.serverErrorWithCode(code: code, message: message))
    
        default: return

        }
    }
}

public struct DataLoaderError
{
    public static let defaultErrorCode:String = "-9000"
     public static let badResponseErrorCode:String = "-8000"
}

public class DataLoaderErrorHelper<T>
{
    public class func getDefaultErrorDataLoader() -> DataLoader<T>
    {
        return DataLoader.serverErrorWithCode(code: DataLoaderError.badResponseErrorCode, message: "Bad Response from server")
    }

    public class func getBadResponse() -> DataLoader<T>
    {
        return DataLoader.serverErrorWithCode(code: DataLoaderError.defaultErrorCode, message: "Opps something went wrong")
    }
}
