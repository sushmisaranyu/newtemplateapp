//
//  BaseShowAllViewModelProtocol.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 10/08/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


protocol  BaseShowAllViewModelProtocol:class {

    func makeAPIRequest(successHandler:@escaping (([OTTVideoItem])->Void),errorHandler:@escaping ((String,String)->Void),internetHandler:@escaping ((String,String)->Void))

}
