//
//  EmptyDataSetView.swift
//  Plash News
//
//  Created by Ash Dz on 12/05/16.
//  Copyright © 2016 Plash Digital Labs. All rights reserved.
//

import Foundation
import UIKit

public protocol EmptyDataSetViewDelegate
{
   func Refresh()
}

public class EmptyDataSetView: UIView
{
    
    @IBOutlet weak var RefreshButton: UIButton!
    @IBOutlet weak var EmptyLabel: UILabel!
   
    @IBOutlet weak var EmptyLabelHeader: UILabel!
    @IBOutlet weak var EmptyImageView: UIImageView!
    var view: UIView!
    public var refreshDelegate:EmptyDataSetViewDelegate!
    
    
    var titleColor:UIColor          = UIColor.white
    var messageColor:UIColor        = UIColor.white
   // var backGroundColor: UIColor    = UIColor.lightGray
    var titleFont:UIFont!
    var messageFont:UIFont!
    var refreshButtonTint:UIColor!
    var imageTintColor:UIColor      = UIColor.gray.withAlphaComponent(0.5)
    
    public override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    public func setupEmptyView(titleFont:Font,titleFontSize:CGFloat,msgFont:Font,msgFontSize:CGFloat,refreshTint:UIColor)
    {
         self.titleFont            = UIFont(name:titleFont.fontName, size: titleFontSize)!
         self.messageFont          = UIFont(name:msgFont.fontName, size: msgFontSize)!
         self.refreshButtonTint    = refreshTint
        self.imageTintColor        = refreshTint
     
         
    }

    public func setupEmptyView(titleFont:String,titleFontSize:CGFloat,msgFont:String,msgFontSize:CGFloat,refreshTint:UIColor)
    {
        self.titleFont            = UIFont(name:titleFont, size: titleFontSize)!
        self.messageFont          = UIFont(name:msgFont, size: msgFontSize)!
        self.refreshButtonTint    = refreshTint
        self.imageTintColor        = refreshTint
        
    }
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "EmptyDataSetView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    public func CreateEmptyDataSetView(_ imageName:String,refreshImageName:String,message:String,title:String)
    {
        let img = UIImage(named: imageName)
        EmptyImageView.tintColor    = imageTintColor
        EmptyImageView.image = img
        
        let refreshImg = UIImage(named: refreshImageName)
        
       // RefreshButton.imageView?.image    = refreshImg
       
      
        EmptyLabel.text             = message
        EmptyLabel.font             = messageFont
        EmptyLabel.textColor        = messageColor
        
        EmptyLabelHeader.text       = title
        
        EmptyLabelHeader.textColor  = titleColor

        RefreshButton.tintColor     = refreshButtonTint
        
        RefreshButton.setImage(refreshImg, for: .normal)
        
        RefreshButton.setTitleColor(refreshButtonTint, for: UIControlState.selected)       
        
    }
    
    @IBAction func RefreshButtonClicked(_ sender: AnyObject) {
        refreshDelegate?.Refresh()
    }
}
