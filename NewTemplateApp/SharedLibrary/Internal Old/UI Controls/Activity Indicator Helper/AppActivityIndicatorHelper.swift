//
//  AppActivityIndicatorHelper.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 09/03/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation
import NVActivityIndicatorView

public class AppActivityIndicatorHelper: NSObject {
    
    private var activityIndicator:          NVActivityIndicatorView!
    private var taggedViewConttoller:       UIViewController!
    private let viewForActivityIndicator  = UIView()
    
    private var isFromTableView           = false
    private var isUserInteractionDisabled = false
    private var isOnMainQueue             = false
    private var backgroundColor: UIColor!
    
    
    
    required public init(viewController:UIViewController, isUserInteractionDisabled: Bool, onMainQueue: Bool, backgroundColor:UIColor, indicatorColor:UIColor = UIColor.black , activityIndicatorType: NVActivityIndicatorType = NVActivityIndicatorType.ballPulse  ) {
        isOnMainQueue = onMainQueue
        self.isUserInteractionDisabled = isUserInteractionDisabled
        self.taggedViewConttoller      = viewController
        self.activityIndicator         = NVActivityIndicatorView(frame:CGRect(x:0,y:0,width: AppUtilities.selectPropertyForDevice(iPhone: 40.0, iPad: 51),height:AppUtilities.selectPropertyForDevice(iPhone: 40.0, iPad: 51)))
        
        
        //        if viewController is UICollectionViewController ||  viewController is UITableViewController
        //        {
        //            self.isFromTableView       = true
        //        }
        
        self.backgroundColor           = backgroundColor
        self.activityIndicator.type    = activityIndicatorType
        self.activityIndicator.color   = indicatorColor
        
    }
    
    
    
    deinit { viewForActivityIndicator.removeFromSuperview() }
    
    public func setupActivityIndicator() {
        if isOnMainQueue == true {
            DispatchQueue.main.async { self.setupActivityIndicatorLocal() }
        } else { setupActivityIndicatorLocal() }
    }
    
    public func UpdateLayouts()
    {
         viewForActivityIndicator.frame  = taggedViewConttoller.view.frame
           viewForActivityIndicator.center = taggedViewConttoller.view.center
         activityIndicator.center        = viewForActivityIndicator.center
    }
    
    private func setupActivityIndicatorLocal() {
        viewForActivityIndicator.frame  = taggedViewConttoller.view.frame
        viewForActivityIndicator.addSubview(activityIndicator)
        viewForActivityIndicator.backgroundColor = backgroundColor
        //viewForActivityIndicator.alpha  = 0.50
        activityIndicator.center        = viewForActivityIndicator.center
        viewForActivityIndicator.isHidden = true
        taggedViewConttoller.view.addSubview(viewForActivityIndicator)
    }
    
    public func startActivityIndicator() {
        if isOnMainQueue == true {
            DispatchQueue.main.async{ self.startActivityIndicatorLocal() }
        } else { startActivityIndicatorLocal() }
    }
    
    private func startActivityIndicatorLocal() {
        activityIndicator.startAnimating()
        viewForActivityIndicator.isHidden = false
        viewForActivityIndicator.frame  =  CGRect(x:0, y:0, width:taggedViewConttoller.view.frame.width, height:taggedViewConttoller.view.frame.height)
        if isFromTableView == true {
            let tableViewContorller = taggedViewConttoller as! UITableViewController
            viewForActivityIndicator.frame = CGRect(x:tableViewContorller.tableView.contentOffset.x, y:tableViewContorller.tableView.contentOffset.y, width:UIScreen.main.bounds.width, height:UIScreen.main.bounds.height)
            if isUserInteractionDisabled == true { tableViewContorller.tableView.isUserInteractionEnabled = false }
        }
        viewForActivityIndicator.bringSubview(toFront: activityIndicator)
        if isUserInteractionDisabled == true { taggedViewConttoller?.view.isUserInteractionEnabled = false }
    }
    
    private func stopActivityIndicatorLocal() {
        if isUserInteractionDisabled == true { taggedViewConttoller.view.isUserInteractionEnabled = true }
        viewForActivityIndicator.isHidden     = true
        activityIndicator.stopAnimating()
        if isFromTableView == true && isUserInteractionDisabled == true{
            let tableViewContorller = taggedViewConttoller as! UITableViewController
            tableViewContorller.tableView.isUserInteractionEnabled = true
        }
    }
    
    public func stopActivityIndicator() {
        if isOnMainQueue == true {
            DispatchQueue.main.async{ self.stopActivityIndicatorLocal() }
        } else { stopActivityIndicatorLocal() }
    }
    
    func setColor(color:UIColor) { activityIndicator.color = color }
}
