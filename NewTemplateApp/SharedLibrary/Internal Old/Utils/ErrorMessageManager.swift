//
//  ErrorMessageManager.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 06/04/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation


public class ErrorMessageManager
{
    static let defaultErrorCode = "-9000"
    
    public class func getErrorMessagefor(code:NSNumber) -> String
    {
        return "Error"
    }
    
    public class func getServerErrorMessagefor(json:[String:Any]) -> (String,String)
    {
        guard let error:[String:Any] = json.valueFor(key: "error", isMandatory: false) else
        {
            return getDefaultErrorMessage()
        }
        
        guard let code:String = error.valueFor(key: "code", isMandatory: false) else {
             return getDefaultErrorMessage()
        }
        
        guard let message:String = error.valueFor(key: "message", isMandatory: false) else {
            return getDefaultErrorMessage()
        }

        return (code,message)
    }
    
    public class func getInternetErrorMessage(code:Int) -> (String,String)
    {
        var message = ""
        switch code{
            case -1009 : message    = "No Internet connection detected"
            case -1001: message     = "Request timeout"
            default: return getDefaultErrorMessage()
        }
        
        return (String(code),message)
    }
    
    public class func getDefaultErrorMessage() -> ( String , String )   {
        return (defaultErrorCode,"Opps something went wrong")
    }


    
    public class func getDefaultErrorMessage(errorHandler:((String,String)->Void))
    {
        let (title,message) = getDefaultErrorMessage()
        errorHandler(title,message)
    }
    
    public class func GetInternetErrorMessage(code:Int , internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        let (_,message) = ErrorMessageManager.getInternetErrorMessage(code: code)
        internetErrorHandler("Opps",message)
        
    }
}

