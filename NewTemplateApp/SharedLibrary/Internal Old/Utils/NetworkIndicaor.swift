//
//  NetworkIndicaor.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 23/02/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation


import Foundation
import UIKit

/** Puts on the network activity indicator when a network access is in progress. */
class NetworkActivityIndicator {
    
    private static var numberOfActiveNetworkCalls = 0
    
    // The sync lock mechanism is needed in the form of a serial queue, as download image network calls are being made on separate threads from NSOperationQueue. The numberOfNetworkCalls counter update was not atomic without this sync lock and was causing the assert in the stopNetworkActivityIndicator to fail.
    private static let networkCallCounterSyncQueue = DispatchQueue(label: "com.saranyu.networkCallCounterView")
    
    class func start() {
        networkCallCounterSyncQueue.sync
            {
                DispatchQueue.main.async {
                    self.numberOfActiveNetworkCalls += 1
                    AppUtilities.printLog(message: "Number of active network calls: \(self.numberOfActiveNetworkCalls)")

                    #if os(iOS)
                    // compiles for IOS
                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
                    #endif
                }
               
        }
    }
    
    class func stop() {
        networkCallCounterSyncQueue.sync
            {
                  DispatchQueue.main.async {
                self.numberOfActiveNetworkCalls -= 1
                AppUtilities.printLog(message:"Number of active network calls: \(self.numberOfActiveNetworkCalls)")
                assert(self.numberOfActiveNetworkCalls >= 0)
                if (self.numberOfActiveNetworkCalls == 0) {
                    #if os(iOS)
                    // compiles for IOS
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    #endif
                }
                }
        }
    }
    
}
