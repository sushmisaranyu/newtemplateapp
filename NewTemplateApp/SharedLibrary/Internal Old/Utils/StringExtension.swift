//
//  StringExtension.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 12/04/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation

public class StringHelper{
    
    
   public class func isNilOrEmpty(string:String?) -> Bool
    {
        if string == nil { return true }
        
        if string?.count == 0 { return true }
        
        if string?.trimmingCharacters(in: .whitespaces).count == 0 { return true }
        
        return false
    }
    
    public class func hasSpace(string:String?) -> Bool
    {
        guard let str = string else { return false }
        
        if str.contains(" ")
        {
            return true
        }
        
        return false
    }
    
}
