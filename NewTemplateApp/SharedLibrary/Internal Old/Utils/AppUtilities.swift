//
//  AppUtilities.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 10/03/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import PINRemoteImage

public class AppUtilities {
    
    
    
    // MARK: To find the Label Height
    public class func heightForView(text: String, font:UIFont, width:CGFloat) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x:0,y:0, width:width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
         label.sizeToFit()
        return label.frame.height
    }


    // MARK: To find the Label Height
    public class func widthForView(text: String, font:UIFont) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x:0,y:0, width:CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 1
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.width
    }
    
    public class func heightSingleLineOfTextForView(text: String, font:UIFont, width:CGFloat) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x:0,y:0, width:width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return label.frame.height / (CGFloat(label.numberOfLines))
    }
    
    
    public class func UIColorFromRGB(colorCode: Int, alpha: Float = 1.0) -> UIColor {
        // colorCode is stored as Int with R, G and B as the last three bytes.
        let mask = 0xff
        let r = CGFloat((colorCode >> 16) & mask) / 255.0
        let g = CGFloat((colorCode >>  8) & mask) / 255.0
        let b = CGFloat( colorCode        & mask) / 255.0
        // print("r is \(r) g is \(g) b is \(b)")
        return UIColor(red: r, green: g, blue: b, alpha: CGFloat(alpha))
    }
    
    public class func printFonts() {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName )
            print("Font Names = [\(names)]")
        }
    }
    
    public class func isValidEmail(_ emailID: String?) -> Bool {
        guard let emailID = emailID else { return false }
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,22}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: emailID)
    }
    
    public class func isValidMobileNos(_ mobileNos: String?) -> Bool {
        //Epic didnt want validation
//        guard let nos = mobileNos else { return false }
//        let nosRegex = "^(\\+\\d{1,3}[- ]?)?\\d{2,15}$"
//        let nosTest = NSPredicate(format: "SELF MATCHES %@", nosRegex)
//        return nosTest.evaluate(with: nos)
        return true
    }
    
    public class func isValidCountryCode(code:String?) -> Bool
    {
        // country code is sent from backend
//        guard let countrycode = code else {return false}
//        let codeRegex = "[0-9]{4}$"
//        let codeTest = NSPredicate(format:"SELF MATCHES %@",codeRegex)
//        return codeTest.evaluate(with:countrycode)
        
        return true
    }
    
    public class func textContainsOlyDigits(code:String?) -> Bool
    {
        guard let mobileOrEmail = code else {return false}
        let mobileOrEmailRegex = "[0-9]{7}$"
        let mobileOrEmailTest = NSPredicate(format:"SELF MATCHES %@",mobileOrEmailRegex)
        return mobileOrEmailTest.evaluate(with:mobileOrEmail)
    }
    
    public class func isMobile(string:String?) ->Bool
    {

        guard let value = string else {return false}

        let stringCharset = CharacterSet.init(charactersIn: value)
        let nosCharset = CharacterSet.decimalDigits

        if value.contains(".")
        {
            return false
        }

        if nosCharset.isSuperset(of: stringCharset) == true
        {
            return true
        }
        
        return false
    }
    
    
    
    public class func validatetextContainsOlyDigits(code:String?) -> Bool
    {
        print("\(String(describing: code))")

        guard let mobile = code else {return false}
        let mobileRegex = "[0-9]{10}$"
        let mobileTest = NSPredicate(format:"SELF MATCHES %@",mobileRegex)
        return mobileTest.evaluate(with:mobile)
    }
    
    #if DEBUG
    public class func printLog(message:String,file: String = #file, function: String = #function, line: Int = #line )
    {

       // if LibaryConfig.isLoggingEnabled == false { return }

        let patharray = file.components(separatedBy: "/")

       print("LOG :: \(patharray.last)::\(function)::\(line) -> \(message)")

    }
    #else
    public class func printLog(message:String)
    {

        // if LibaryConfig.isLoggingEnabled == false { return }
//


      //  print("LOG ::  \(message)")

    }
    #endif


    public class func selectPropertyForDevice<T>(iPhone:T,iPad:T) -> T
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone
        {
            return iPhone
        }
        else
        {
            return iPad
        }
    }


    public class func selectPropertyForIOSorAppleTV<T>(IOS:T,AppleTV:T) -> T
    {
        #if os(iOS)
            return IOS
        #else
            return AppleTV
        #endif
    }


    public class func epocTimeToNormalDateForSubscription(timeInterval:Double?) -> String
    {

        guard let interval = timeInterval else {return ""}

        if interval == 0 {return ""}

        let date = Date(timeIntervalSince1970: interval)

        let localDate = toLocalTime(date: date)
        //  let localDate = date


        let calendar = Calendar.current

        return getDate(date: localDate)

    }


    public class func epocTimeToNormalDate(timeInterval:Double?) -> String
    {

        guard let interval = timeInterval else {return ""}

        if interval == 0 {return ""}

        let date = Date(timeIntervalSince1970: interval)

        let localDate = toLocalTime(date: date)
     //  let localDate = date


        let calendar = Calendar.current

        return getTime(date: localDate)

    }

    public class func getLocalDateForInterval(interval:Double?) -> Date?
    {
        guard let i = interval else {return nil}

        if i == 0 {return nil}

        let date = Date(timeIntervalSince1970: i)

        let localDate = toLocalTime(date: date)

        return localDate
    }

    class func getTime(date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        return dateFormatter.string(from: date)
    }

    class func getDate(date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMM yyyy"
        return dateFormatter.string(from: date)
    }


    // Convert UTC (or GMT) to local time
    class func toLocalTime(date:Date) -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT())
        return Date(timeInterval: -seconds, since: date)
    }

    
    
    public class func  getSecondsFrom_HH_MM_SS(timeString:String?) -> Int
    {
        guard let string = timeString else {return 0}

        if  StringHelper.isNilOrEmpty(string: string) == true {return 0}

        if string == "0" {return 0}

        let array = string.components(separatedBy: ":")

        if array.count  != 3 {return 0}

        var total = 0
        
        if let hour = Int(array[0])
        {
            total = hour * 60
        }
        
        if let min = Int(array[1])
        {
            total = total + min
            total = total * 60
        }
        
        
        if let sec = Int(array[2])
        {
            total = total + sec
        }
        
        return total
    }
    
    public class func formatSecondsToString(_ seconds: Double) -> String {
        
        let Sec = Int(seconds.truncatingRemainder(dividingBy: 60))
        let m   = seconds/60
        let Min = Int(m.truncatingRemainder(dividingBy: 60))
        let Hr  = Int(m/60)

        return String(format: "%02d:%02d:%02d",Hr, Min, Sec)
    }
    
    
    
    public class func convertDateFormater(date: String) -> Double {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        //dateFormatter.timeZone = TimeZone(identifier: "UTC")
        
        guard let converteddate = dateFormatter.date(from: date) else {
            return 0
        }
        
        print("\(date) :: \(converteddate)")
        
      return converteddate.timeIntervalSince1970
        
    }
    
    public class func resizeImage(image: UIImage) -> UIImage? {
        let maxwidth:CGFloat = 512
        let maxheight:CGFloat = 512
        
        var newHeight = image.size.height
        var newWidth  = image.size.width
        
        if newWidth < maxwidth && newHeight < maxheight
        {
            return image
        }
        
        
        if newHeight > maxheight
        {
            newHeight   = maxheight
            newWidth    = maxheight/image.size.height * image.size.width
        }
        
        if newWidth > maxwidth
        {
            newHeight   = maxwidth/newWidth * newHeight
            newWidth    = maxwidth
        }
        
        let targetSize = CGSize(width: newWidth, height: newHeight)
        
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width:size.width * heightRatio, height:size.height * heightRatio)
        } else {
            newSize = CGSize(width:size.width * widthRatio,  height:size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x:0, y:0, width:newSize.width, height:newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }

    public class func getVersionNumber(version:String) -> Int
    {
        var _:[Int]  = [Int]()

        let versionCharset = CharacterSet(charactersIn: version)

        let charset = CharacterSet(charactersIn: "1234567890.")

        if charset.isSuperset(of: versionCharset) == false
        {
            return 0
        }

        var arrayString = version.components(separatedBy: ".")

        if arrayString.count == 0 {return 0}

        var total = 0

        for i in 0...arrayString.count - 1
        {
            if let value = Int(arrayString[i])
            {
                //AppUtilities.printLog(message: "Appversion Calc : \(value) * \(pow(Double(10), Double(4-i)))")
                total = total + value * Int(pow(Double(10), Double(4-i)))
             //   AppUtilities.printLog(message: "Appversion Calc Totla : \(total)")

            }
        }

        AppUtilities.printLog(message: "Appversion Calc : \(total)")
        return total

    }

    public class func versionChecker(appVersion:String,minVersion:String,maxVersion:String) -> AppUpdateState
    {
        //var _:AppUpdateState = .noUpdate


        let currentVersion = AppUtilities.getVersionNumber(version: appVersion)
        let minSupported = AppUtilities.getVersionNumber(version: minVersion)
        let maxSupported = AppUtilities.getVersionNumber(version: maxVersion)


        if minSupported == 0 || maxSupported == 0 {return .noUpdate}

        if currentVersion >= maxSupported {
           return .noUpdate
        }

       if currentVersion < minSupported
       {
            return .forcedUpdate
       }

       return .softUpdate
    }


    public class func getFirstCharactersInString(name:String?,charCount:Int) -> String
    {

        guard let tmpString = name else {return ""}

        let substring = tmpString.components(separatedBy: " ")

        var firstCharStrings = ""

        for string in substring
        {
            let newString = string.trimmingCharacters(in: .whitespaces)

            if newString.count == 0 {continue}

            let prefix = newString.prefix(1)

            firstCharStrings = firstCharStrings + prefix
        }


        if firstCharStrings.count < charCount {return firstCharStrings}

        return String(firstCharStrings.prefix(charCount))


    }

    public class func setupImage(imageView:UIImageView?, imageURL:String?,placeholderImage:UIImage?)
    {
        guard let url = imageURL else {return}

        if #available(iOS 11.0, *)
        {
            imageView?.pin_setImage(from:  URL(string:url), placeholderImage: placeholderImage)
        }
        else
        {
            imageView?.sd_setImage(with: URL(string:url), placeholderImage: placeholderImage)
        }
      }

    public class func getValue(property:String?) -> String
    {
        if StringHelper.isNilOrEmpty(string: property) == true
        {
            return ""
        }
        else
        {
            return property!
        }
    }

    //
    public class func roundOffToOneDecimalPoint(nos:Double) -> Float
    {
        var newnos = Float(nos) * 10
        newnos = floor(newnos)
        newnos = newnos/10

        return newnos
    }
   
}


