//
//  DictionaryExtension.swift
//  SaranyuLibrary
//
//  Created by Ash Dz on 23/02/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation



extension Dictionary {
    
    // MARK: Extract Field Values
    // Used to extract values from a server response. This logs to LogEntries in case of an error.
    public  func valueFor<T>( key: String, isMandatory: Bool = true) -> T? {
        if let value: Value = self[key as! Key] {
            if let value = value as? T {
                return value
            } else {
                
                return nil
            }
        } else {
            if isMandatory {
                
            }
            return nil
        }
    }
    
}
