//
//  NSManagedObjectExtension.swift
//  RuralKing
//
//  Created by Ash Dz on 16/09/16.
//  Copyright © 2016 TruNext. All rights reserved.
//

import Foundation
import CoreData

extension NSManagedObject {
    
    // MARK: Factory Methods
    public class func managedObject(managedObjectContext: NSManagedObjectContext) -> AnyObject {
        let className = NSStringFromClass(self)
        return NSEntityDescription.insertNewObject(forEntityName: className, into: managedObjectContext)
    }
    
    // MARK: Purge
    public class func purge() {
        let managedObjectContext = CoreDataStorage.shared.context
        let managedObjects = getManagedObjects(managedObjectContext: managedObjectContext, includesPropertyValues: false)
        for managedObject in managedObjects {
            managedObjectContext.delete(managedObject as! NSManagedObject)
        }
        try! managedObjectContext.save()
    }
    
    // MARK: Database Query Methods
   public class func first(managedObjectContext: NSManagedObjectContext) -> AnyObject? {
        return getManagedObjects(managedObjectContext: managedObjectContext).first
    }
    
    // Set either predicateString or predicate. predicateString is used if both predicateString and predicate are set.
    public class func getManagedObjects(managedObjectContext: NSManagedObjectContext,predicateString: String? = nil, predicate: NSPredicate? = nil, sortKeys: [String]? = nil, isAscending: Bool = true, propertiesToFetch: [String]? = nil, returnsDistinctResults: Bool = false, includesPropertyValues: Bool = true) -> [AnyObject] {
        let className = NSStringFromClass(self)

            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: className)
//        fetchRequest.entity = NSEntityDescription.entity(forEntityName: NSStringFromClass(self), in: managedObjectContext)
        fetchRequest.predicate = predicateString != nil ? NSPredicate(format: predicateString!) : predicate
        setSortKeysInFetchRequest(fetchRequest: fetchRequest, isAscending: isAscending, sortKeyNames: sortKeys)
        setPropertiesToFetchInFetchRequest(fetchRequest: fetchRequest, propertyNames: propertiesToFetch)
        fetchRequest.returnsDistinctResults = returnsDistinctResults
        fetchRequest.includesPropertyValues = includesPropertyValues
        do
        {
            return try managedObjectContext.fetch(fetchRequest) as [AnyObject]
            
        }
        catch
        {
            return [AnyObject]()
        }
    }
    
    private class func setSortKeysInFetchRequest(fetchRequest: NSFetchRequest<NSFetchRequestResult>, isAscending: Bool, sortKeyNames: [String]?) {
        if let sortKeyNames = sortKeyNames {
            fetchRequest.sortDescriptors = [NSSortDescriptor]()
            for sortKeyName in sortKeyNames {
                fetchRequest.sortDescriptors!.append(NSSortDescriptor(key: sortKeyName, ascending: isAscending))
            }
        }
    }
    
    // Using this option will change the resultType to Dictionary
    private class func setPropertiesToFetchInFetchRequest(fetchRequest: NSFetchRequest<NSFetchRequestResult>, propertyNames: [String]?) {
        if let propertyNames = propertyNames {
            let propertiesInEntity = fetchRequest.entity!.propertiesByName
            fetchRequest.propertiesToFetch = [NSPropertyDescription]()
            for propertyName in propertyNames {
                if let property = propertiesInEntity[propertyName] {
                    fetchRequest.propertiesToFetch!.append(property)
                } else { assertionFailure("Property \(propertyName) does not exist in the entity.") }
            }
            fetchRequest.resultType = NSFetchRequestResultType.dictionaryResultType
        }
    }
    
    // Set either predicateString or predicate. predicateString is used if both predicateString and predicate are set.
    public class func fetchedResultsController(managedObjectContext: NSManagedObjectContext, sortKeys: [String], isAscending: Bool = true, predicateString: String? = nil, predicate: NSPredicate? = nil, batchSize: Int = 20, delegate: NSFetchedResultsControllerDelegate? = nil) -> NSFetchedResultsController<NSFetchRequestResult> {
        let className = NSStringFromClass(self)
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: className)
        fetchRequest.fetchBatchSize = batchSize
        if let predicateString = predicateString {
            AppUtilities.printLog(message: "PREDICATE STRING:\(predicateString)")
            fetchRequest.predicate = NSPredicate(format: predicateString)
        } else if let predicate = predicate {
            fetchRequest.predicate = predicate
        }
        fetchRequest.sortDescriptors = [NSSortDescriptor]()
        for sortKey in sortKeys {
            fetchRequest.sortDescriptors?.append(NSSortDescriptor(key: sortKey, ascending: isAscending))
        }
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController.delegate = delegate
        
        try! fetchedResultsController.performFetch()
        return fetchedResultsController
    }
    
    public class func count(predicateString: String? = nil) -> Int {
        let managedObjectContext = NSManagedObjectContext.newObjectContext()
        let className = NSStringFromClass(self)
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: className)
        if let predicateString = predicateString {
            fetchRequest.predicate = NSPredicate(format: predicateString)
        }
        fetchRequest.includesPropertyValues = false
        fetchRequest.includesSubentities    = false
        
        do
        {
         let count = try managedObjectContext.count(for: fetchRequest)
         return count

        }
        catch let error as NSError{
            print(error)
            return -1
        }
    }
}

extension NSManagedObjectContext
{
    public func Save()
    {
       
        do
        {
            try self.save()
        }
        catch {
            print("HWSave Failed!!!!!!!!!!!!!!! !!!!! !!! !!")
        }
    }
    
    public class func newObjectContext() -> NSManagedObjectContext
    {
        return CoreDataStorage.shared.context
    }
}

