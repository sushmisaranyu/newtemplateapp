//
//  SmartURLFetcher.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 30/03/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation
import CryptoSwift


public class SmartURLFetcher
{
//    
//    private String getFinalURL(String smartUrl) {
//    String serviceId = "1";
//    String protocol = this.protocol;
//    final String playUrl = "yes";
//    String initialUrl = smartUrl +
//    "?service_id=" + serviceId + "&protocol=" + protocol +
//    "&play_url=" + playUrl + "&us=";
//    String finalUrl = initialUrl + md5("JU5CsFAyWqQzXRo8dyx6" + initialUrl);
//    return finalUrl;
//    }
    
    
    public var playback_url:String?
    public var vwidth:Int = 0
    public var vheight:Int = 0
    public var fileSize:Float = 0

    
    public class func MD5(string:String) -> String
    {
       let hash = string.md5()
         AppUtilities.printLog(message: "\(hash)")
       return hash        
    }
    
   public  class func getAdaptiveURL(smartURL:String,successHandler: @escaping ((_ adaptiveURL: String) -> Void), errorHandler: @escaping (() -> Void),internetErrorHandler:@escaping ((Int)->Void))
    {
        let serviceId = "1"
        let smartProtocol = "hls"
        let playURL = "yes"
        let initialURL = smartURL + "?service_id=" + serviceId + "&protocol=" + smartProtocol +  "&play_url=" + playURL + "&us=";
        let finalURL   = initialURL + MD5(string: AppConfiguration.smartURLMD5KeyForApp + initialURL)
        
        
        
        
        var service = OTTServer(serviceURL:finalURL,httpMethod:HTTPMethodForAPI.GET,hasArrayInResponse:true)
        
        service.call(successHandlerwithArray: { (responseJson) in
            if let adaptive_urls = Parse(responseJSON: responseJson)
            {
                successHandler(adaptive_urls)
            }
            else
            {
                errorHandler()
            }
           
        }, errorHandler: { code, message in
            errorHandler()
        }, internetErrorHandler: { code in
            internetErrorHandler(code)
        })
    }



    public class func getAdaptiveURL(smartURL:String, responseHandler: @escaping ((_ response: DataLoader<[String:String]>) -> Void))
    {
        let serviceId = "1"
        let smartProtocol = "hls"
        let playURL = "yes"
        let initialURL = smartURL + "?service_id=" + serviceId + "&protocol=" + smartProtocol +  "&play_url=" + playURL + "&us=";
        let finalURL   = initialURL + MD5(string: AppConfiguration.smartURLMD5KeyForApp + initialURL)

        var service = OTTServer(serviceURL:finalURL,httpMethod:HTTPMethodForAPI.GET,hasArrayInResponse:false)

        service.callWithDataLoader { (response) in

            switch response
            {
            case .success(let response):
                print("Smart URL \(response)")
                guard let adaptiveURL:[[String:Any]] = response.valueFor(key: "adaptive_urls", isMandatory: false) else {
                    responseHandler(DataLoaderErrorHelper.getBadResponse())
                    return
                }

                if let urlList =  ParseV2(responseJSON: adaptiveURL)
                {
                    responseHandler(.success(response: urlList))
                }
                else
                {
                    responseHandler(DataLoaderErrorHelper.getBadResponse())
                }


                break
            default: DataLoaderHelper.convert(responseHandler: responseHandler, dataLoader: response)
            }
        }
    }
    
    public  class func getDownloadURL(smartURL:String,successHandler: @escaping ((_ adaptiveURL: [SmartURLFetcher]) -> Void), errorHandler: @escaping (() -> Void),internetErrorHandler:@escaping ((Int)->Void))
    {
        let serviceId = "1"
        let smartProtocol = "hls"
        let playURL = "yes"
        let initialURL = smartURL + "?service_id=" + serviceId + "&protocol=" + smartProtocol +  "&play_url=" + playURL + "&us=";
        let finalURL   = initialURL + MD5(string: AppConfiguration.smartURLMD5KeyForApp + initialURL)
        
        
        
        
        var service = OTTServer(serviceURL:finalURL,httpMethod:HTTPMethodForAPI.GET,hasArrayInResponse:true)
        
        service.call(successHandlerwithArray: { (responseJson) in
            if let array = ParseDownloadArray(responseJson: responseJson)
            {
                successHandler(array)
            }
            else
            {
                errorHandler()
            }
            
        }, errorHandler: { code, message in
            errorHandler()
        }, internetErrorHandler: { code in
            internetErrorHandler(code)
        })
    }
    
    
    class func ParseDownloadArray(responseJson:[[String:Any]]) -> [SmartURLFetcher]?
    {
        var list = [SmartURLFetcher]()
        
        for dictionary in responseJson
        {
            guard let playback_url:String = dictionary.valueFor(key:"playback_url", isMandatory: false)
            else
            {
                continue
            }
            
            
            var item = SmartURLFetcher()
            item.playback_url = playback_url
            
 
            if let value:Int = dictionary.valueFor(key:"vwidth", isMandatory: false)
            {
                item.vwidth = value
            }
            
            
            if let value:Int = dictionary.valueFor(key:"vheight", isMandatory: false)
            {
                item.vheight = value
            }
            
            if let value:Int64 = dictionary.valueFor(key: "size", isMandatory: false)
            {
                item.fileSize = Float(value)
            }
                       
            list.append(item)
        }
        
        
        if list.count == 0 { return nil }
        
        
        let sortedArray = list.sorted(by: {$0.vheight < $1.vheight})
        
        return sortedArray
        
    }
    
   class func Parse(responseJSON: [[String: Any]]) -> String?
    {
    
       for dictionary in responseJSON
       {
            guard let adaptive_url:[[String:Any]] = dictionary.valueFor(key:"adaptive_urls", isMandatory: false) else {return nil}

        
       }
        
        return nil
    }

    class func ParseV2(responseJSON: [[String: Any]]) -> [String:String]?
    {

        var adaptiveurl:[String:String] = [String:String]()

        for dictionary in responseJSON
        {
            guard let adaptiveList:String = dictionary.valueFor(key:"playback_url", isMandatory: false) else {continue}
            guard let label:String = dictionary.valueFor(key:"label", isMandatory: false) else {continue}

            adaptiveurl[label] = adaptiveList

        }

        if adaptiveurl.count == 0 {return  nil}

        return adaptiveurl
    }


}
