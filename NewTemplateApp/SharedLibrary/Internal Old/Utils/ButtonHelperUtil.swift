//
//  ButtonHelperUtil.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 27/04/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation
import UIKit

public class ButtonHelperUtil
{
    public class func SetupButton(button:UIButton,title:String,font:UIFont,foreground:UIColor,background:UIColor,state:UIControlState)
    {
        let mySelectedAttributedTitle = NSAttributedString(string: title,
                                                           attributes: [NSAttributedStringKey.foregroundColor : foreground, NSAttributedStringKey.font:font])
        button.setAttributedTitle(mySelectedAttributedTitle, for: state)
        button.backgroundColor = background
        button.setNeedsLayout()
    }

    public class func SetupButton(button:UIButton,title:String,templateFont:TemplateFontProperties,background:UIColor,state:UIControlState)
    {
        var string = title
        if templateFont.isAllUpperCase
        {
            string = string.uppercased()
        }
        
        SetupButton(button:button,title:title,font:UIFont(name:templateFont.font.fontName,size:templateFont.fontSize)!,foreground:templateFont.fontColor,background:background,state:state)
        
    }
    
    public class func SetupButton(button:UIButton,title:String,templateFont:TemplateFontProperties,background:UIColor,borderWidth:CGFloat,borderColor:UIColor,state:UIControlState)
    {
        SetupButton(button:button,title:title,templateFont:templateFont,background:background,state:state)
        button.layer.borderColor = borderColor.cgColor
        button.layer.borderWidth = borderWidth
        
        
    }
}
