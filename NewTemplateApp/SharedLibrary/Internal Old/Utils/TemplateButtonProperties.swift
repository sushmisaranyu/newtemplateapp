//
//  TemplateButtonProperties.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 13/03/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation
import UIKit

class TemplateButtonProperties
{
    var backgroundColor:UIColor         = UIColor.white
    var borderColor:UIColor             = UIColor.clear
    var tintColor:UIColor               = UIColor.black
    var title:String                    = ""
    var fontName:String                     = "Helvetica"
    
    var selectedFontColor:UIColor       = UIColor.white
    var isSelecttemplateStateEnabled    = false
    var borderWidth:CGFloat             = 0
    var selectedBackgroundColor:UIColor = UIColor.white
    var imageName:String?
  
//    public  init(font:fontName , fontSize:CGFloat, fontColor:UIColor,isAllUpperCase:Bool = false, backgroundColor:UIColor,borderColor:UIColor = UIColor.clear,borderWidth:CGFloat = 0)
//    {
//        super.init(font: font, fontSize: fontSize, lineCount: 1, fontColor: fontColor,isAllUpperCase: isAllUpperCase)
//        self.backgroundColor    = backgroundColor
//        self.borderColor        = borderColor
//        self.borderWidth        = borderWidth
//        
//        
//        
//    }
//    
//    public convenience init(font:Font , fontSize:CGFloat, lineCount:Int,fontColor:UIColor,isAllUpperCase:Bool = false, backgroundColor:UIColor,selectedFontColor:UIColor,selectedBackgroundColor:UIColor,borderColor:UIColor = UIColor.clear,borderWidth:CGFloat = 0)
//    {
//        self.init(font:font, fontSize:fontSize, lineCount:lineCount,fontColor:fontColor,isAllUpperCase:isAllUpperCase, backgroundColor:backgroundColor,borderColor:borderColor,borderWidth:borderWidth)
//        self.backgroundColor                = backgroundColor
//        self.borderColor                    = borderColor
//        self.borderWidth                    = borderWidth
//        self.selectedFontColor              = selectedFontColor
//        self.selectedBackgroundColor        = selectedBackgroundColor
//        self.isSelecttemplateStateEnabled   = true
//        
//    }
}

