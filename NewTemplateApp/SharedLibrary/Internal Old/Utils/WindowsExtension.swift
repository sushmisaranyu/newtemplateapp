//
//  WindowsExtension.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 22/03/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation



extension UIWindow {
    
   public func visibleViewController() -> UIViewController? {
        if let rootViewController: UIViewController = self.rootViewController {
            return UIWindow.getVisibleViewController(rootViewController)
        } else {
            return nil
        }
    }
    
    public class func getVisibleViewController(_ startingViewController: UIViewController) -> UIViewController? {
        if let navigationController = startingViewController as? UINavigationController {
            return navigationController.visibleViewController
        } else if let tabBarController = startingViewController as? UITabBarController {
            if let selectedViewController = tabBarController.selectedViewController {
                return getVisibleViewController(selectedViewController)
            }
        } else if let presentedViewController = startingViewController.presentedViewController {
            return getVisibleViewController(presentedViewController)
        }
        return startingViewController
    }
    
}
