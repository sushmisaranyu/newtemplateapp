//
//  AppLogging.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 15/03/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation


public class LibaryConfig
{

    public static var isLoggingEnabled:Bool
    {
        #if DEBUG
        return true
        #else
        return false
        #endif
    }
    
 
}
