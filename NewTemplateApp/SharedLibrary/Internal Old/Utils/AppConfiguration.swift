//
//  AppConfiguration.swift
//  SaranyuLibrary
//
//  Created by Ash Dz on 23/02/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation


public enum AppConfiguration {
    case serverURL
    case authToken
    case region
    case coreDataName
    case storyBoardName
    case storyBoardNameIPAD
    case smartURLMD5Key
    case paymentSecretToken
    case gaKeyForApp
       
    
    func value<T>() -> T? {
        if let value: T = Bundle.main.object(forInfoDictionaryKey: key()) as? T {
              AppUtilities.printLog(message: "\(value)")
            return value
        } else {
            let errorMessage = "Configuration setting not found for key \(key())."
            print(errorMessage)
            assertionFailure(errorMessage)
            return nil     // This will never execute. But the code is added to silence a compiler error message.
        }
    }
    
    fileprivate func key() -> String {
        switch self {
        case .serverURL:              return "Saranyu OTT Server Url"
        case .authToken:              return "Saranyu OTT Auth Token"
        case .region:                 return "Saranyu OTT Region"
        case .coreDataName:           return "Saranyu CoreData Name"
        case .storyBoardName:         return "Saranyu StoryBoard Name"
        case .storyBoardNameIPAD:     return "Saranyu StoryBoard Name IPAD"
        case .smartURLMD5Key:         return "Saranyu Smart URL MD5 Key"
        case .paymentSecretToken:     return "Saranyu OTT Payment Secret Key"
        case .gaKeyForApp:              return "Saranyu GA Key"
        
        }
    }
    
    
    public static var authTokenForApp:String
    {
        return  AppConfiguration.authToken.value()!
    }

    public static var regionForApp:String
    {
        if let region = AppUserDefaults.detectedRegionForApp
        {
            return region
        }
        else
        {
            return AppConfiguration.region.value()!
        }
    }

    #if os(iOS)
    // compiles for OS X
    public static var storyBoardNameForApp:String
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone {
            return AppConfiguration.storyBoardName.value()!
        }
        else
        {
            return AppConfiguration.storyBoardNameIPAD.value()!
        }
    }
    #endif
 
    public static var smartURLMD5KeyForApp:String
    {
        return AppConfiguration.smartURLMD5Key.value()!
    }
    
    public static var paymentSecretTokenForApp:String
    {
        return AppConfiguration.paymentSecretToken.value()!
    }
    
    public static var GAKeyForApp:String
    {
        return AppConfiguration.gaKeyForApp.value()!
    }
}
