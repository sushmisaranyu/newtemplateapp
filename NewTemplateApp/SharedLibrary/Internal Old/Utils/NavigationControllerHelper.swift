//
//  NavigationControllerHelper.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 03/04/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation
import UIKit

public class NavigationControllerHelper
{
    
    public class func transparentNavBarOnViewWillDisappear(navigationController:UINavigationController?)
    {
        if let navigation_Controller = navigationController
        {
            navigation_Controller.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
            navigation_Controller.navigationBar.shadowImage = nil
            navigation_Controller.navigationBar.tintColor = nil
            navigation_Controller.navigationBar.isTranslucent = false
        }
    }


    
    public class func setupTransparentNavBar(navigationController:UINavigationController?)
    {
        navigationController?.navigationBar.tintColor           = .white
        navigationController?.navigationBar.backgroundColor     = nil

        #if os(iOS)
        // compiles for IOS
        navigationController?.isToolbarHidden                   = true
        #endif

        navigationController?.isNavigationBarHidden             = false
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage    = UIImage()
        navigationController?.navigationBar.isTranslucent  = true

    }

    
    public class func removeTextFromBackButton(navigationItem:UINavigationItem?)
    {
        #if os(iOS)
        // compiles for IOS
        navigationItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        #endif

    }
    
    public class func Push(navigationController:UINavigationController?, viewController:UIViewController, animated:Bool = false)
    {
        guard let navController = navigationController else { return }
        
        DispatchQueue.main.async {
            navController.pushViewController(viewController, animated: animated)
        }
    }
}
