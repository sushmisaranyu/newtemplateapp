//
//  TemplateFont.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 10/03/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation
import UIKit

public class TemplateFontProperties
{
    
    public var font:Font
    public var templateFont : UIFont
    public var fontSize:CGFloat
    var fontColor:UIColor
    var lineCount:Int
    var minSize:CGFloat = 0
    var isAllUpperCase:Bool
    
    
//    
//    init()
//    {
//        fontSize = 0.0
//        fontColor = UIColor.blue
//        lineCount = 0
//        minSize = 0
//        font = Font.HelveticaNeue
//        var fontfamily  = UIFont(name: font.fontName , size: fontSize)
//        templateFont    = fontfamily!
//        isAllUpperCase = false
//
//    }
    
    public init(font:Font , fontSize:CGFloat, lineCount:Int,fontColor:UIColor,isAllUpperCase:Bool = false)
    {
        //self.init()
        self.font           = font
        self.fontSize       = fontSize
        self.lineCount      = lineCount
        self.isAllUpperCase = isAllUpperCase
        let fontname        = self.font.fontName
        var fontfamily      = UIFont(name: fontname , size: fontSize)
        templateFont        = fontfamily!
        self.fontColor      = fontColor
        let text            = "random text for"
        var size            = AppUtilities.heightSingleLineOfTextForView(text: text, font:fontfamily!, width:667.0)
        
        if lineCount > 0
        {
            minSize =  size * (CGFloat(integerLiteral: lineCount) + 4)
        }
    }
    
    
    
    
    
//    convenience init(font:Fonts.Font , fontSize:CGFloat, fontColor:String , lineCount:Int, needsTagColor:Bool)
//    {
//        self.init(font:font , fontSize:fontSize, lineCount:lineCount, needsTagColor:needsTagColor)
//        FontColor = UIColor(hexString: fontColor)
//    }
    
    
    
    
    //Attributes
//    
//    convenience init(font:Fonts.Font , fontSize:CGFloat, fontColor:String , lineCount:Int, needsTagColor:Bool,linespacing:CGFloat,lineheightmultiple:CGFloat)
//    {
//        self.init(font:font , fontSize:fontSize, fontColor: fontColor,lineCount:lineCount, needsTagColor:needsTagColor)
//        
//        hasAttributeProperties  = true
//        lineSpacing             = linespacing
//        lineHeightMultiple      = lineheightmultiple
//    }
//    
    
    
   public func CalculateHeight(text:String? , width:CGFloat) -> CGFloat
    {
        var temp_txt = "Blank"
        if text != nil && (text?.characters.count)! > 0 {temp_txt = text!}
        var font = UIFont(name: self.font.fontName, size: fontSize)
        var size = AppUtilities.heightForView(text: temp_txt, font:font!, width:width)
        
        // print("Minsize:\(MinSize) Size:\(size)")
        
        if lineCount > 0
        {
            return size > minSize ? minSize : size
        }
        
        return size
        
    }
    

    
    func GetColor() -> UIColor
    {
        return fontColor
    }
    
    
}


extension  UILabel
{
    
  public  func SetupLabelTemplate(template:TemplateFontProperties ,  text:String!)
    {
        
        var text = text
        if text == nil
        {
            text = " "
        }
        
        if template.isAllUpperCase == true
        {
            text = text?.uppercased()
        }
        
        self.numberOfLines      = template.lineCount
        self.textColor          = template.GetColor()
        self.font               = template.templateFont
        self.text               = text
        self.lineBreakMode      = .byWordWrapping
       // self.sizeToFit()

       
    }
    
    
    
    
    
}


extension UIButton
{
    func SetupButtonwithLabel(templateNormal:TemplateFontProperties,backgroundColor:UIColor,selectedBackgroundColor:UIColor,text:String)
    {
        var text = text
        if text == nil
        {
            text = " "
        }
        
        
        self.setTitle(text, for: .normal)
    }
}
