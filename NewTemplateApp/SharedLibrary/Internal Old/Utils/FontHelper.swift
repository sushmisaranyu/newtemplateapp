//
//  FontHelper.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 09/03/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation
import UIKit


public enum Font
{
        case HelveticaNeue
        case HelveticaNeueBold
        case HelveticaNeueLight
        case HelveticaNeueItalic
    
    
    public var fontName: String
    {
        
        switch( self )
        {
        case .HelveticaNeue:                    return "HelveticaNeue"
        case .HelveticaNeueBold:                return "HelveticaNeue-Bold"
        case .HelveticaNeueLight:               return "HelveticaNeue-Light"
        case .HelveticaNeueItalic:              return "HelveticaNeue-Italic"
            
            
        }
    }
    
    

    
    //Used for debugging correct Font names
    static func printFonts() {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName )
            print("Font Names = [\(names)]")
        }
    }

}

