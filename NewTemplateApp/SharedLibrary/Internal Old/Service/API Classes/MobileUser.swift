//
//  MobileUser.swift
//  SaraynuLibrary
//
//  Created by Ashley Dsouza on 28/03/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import CoreData

public class MobileUser
{

    var firstName:String?
    var lastName:String?
    var user_id:String?
    var mobileNos:String?

    public class func Parse(jsonDictionary:[String:Any]) -> MobileUser?
    {
        let user = MobileUser()
        guard let data:[String:Any] = jsonDictionary.valueFor(key:"data", isMandatory: false) else
        {
            return nil
        }

        guard let userId:String = data.valueFor(key:"user_id", isMandatory: false) else
        {
            return nil
        }

        user.firstName = ""
        user.lastName = ""
        user.mobileNos = ""

        if let value:String  = data.valueFor(key:"email_id", isMandatory: false)
        {
            user.mobileNos = value
        }



        if let value:String  = data.valueFor(key:"firstname", isMandatory: false)
        {
            user.firstName = value
        }

        if let value:String  = data.valueFor(key:"lastname", isMandatory: false)
        {
            user.lastName = value
        }

        return user
    }

    public func createOTTUser(session:String)
    {
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
        let user                    = OTTUser.InitOTTUser(managedObjectContext:managedObjectContext)

        user.userId = self.user_id
        user.mobile_number = self.mobileNos
        user.firstName = self.firstName
        user.lastName = self.lastName

        AppUserDefaults.login_type.setValue(LoginType.Mobile.key)
        AppUserDefaults.session_ID.setValue(session)
        managedObjectContext.Save()
    }

}
