//
//  OTTPeopleRoleTypes.swift
//  SaraynuLibrary
//
//  Created by Ashley Dsouza on 11/05/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation


public enum OTTPeopleRoleTypes
{
    case Actor
    case Director
    
    public var key:String{
        
        switch(self)
        {
        case .Actor: return "Actor"
        case .Director: return "Director"
        }
    }
}
