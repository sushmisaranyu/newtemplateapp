//
//  OTTMediaItem+CoreDataProperties.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 02/03/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation
import CoreData


extension OTTMediaItem {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<OTTMediaItem> {
        return NSFetchRequest<OTTMediaItem>(entityName: "OTTMediaItem");
    }

    @NSManaged public var catalogId: String?
    @NSManaged public var catalogName: String?
    @NSManaged public var contentId: String?
    @NSManaged public var mediadescription: String?
    @NSManaged public var duration: String?
    @NSManaged public var friendlyId: String?
    @NSManaged public var genres: [String]?
    @NSManaged public var keywords: [String]?
    @NSManaged public var language: String?
    @NSManaged public var people: [OTTPeople]?
    @NSManaged public var playURL: OTTPlayURL?
    @NSManaged public var playURLType: String?
    @NSManaged public var rating: NSNumber?
    @NSManaged public var releaseDate: String?
    @NSManaged public var theme: String?
    @NSManaged public var thumbnails: OTTImageURLs?
    @NSManaged public var title: String?
    @NSManaged public var censorRating :String?
    @NSManaged public var cacheTime:OTTCache?
    @NSManaged public var episodeFlag:Bool
    @NSManaged public var subcategoryFlag:Bool
    @NSManaged public var episode_count:NSNumber?
    @NSManaged public var share_url:String?
    @NSManaged public var show_theme_id:String?
    @NSManaged public var subcategory_id:String?
    @NSManaged public var subcategory_name:String?
    @NSManaged public var no_of_user_rated:NSNumber?
    @NSManaged public var average_user_rating:NSNumber?
    @NSManaged public var access_control:OTTAccessControl?
    @NSManaged public var last_episode:OTTVideoItem?
//    @NSManaged public var parentCatalogItem:OTTCatalogItem?
//    @NSManaged public var parentListId:String?
    
    @NSManaged public var cache:OTTCache?
    
    
    class func InitOTTMediaItem(contentId:String , managedObjectContext: NSManagedObjectContext) -> OTTMediaItem {
        
         if let item = GetMediaItemWith(contentId: contentId)
         {
            return item
         }
        
        return  OTTMediaItem.managedObject(managedObjectContext: managedObjectContext) as! OTTMediaItem
        
    }
    
    
   class public  func GetMediaItemWith(contentId:String?) -> OTTMediaItem?
    {
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
        guard let id = contentId else{
            return nil
        }
        
       
        
        let item = OTTMediaItem.getManagedObjects(managedObjectContext: managedObjectContext,predicate: NSPredicate(format: "contentId = '\(id)'")) as? [OTTMediaItem]
        if item == nil || item?.count == 0 { return nil}
        return item?.first
    }

    
    public class func DeleteAllMediaItems() {
        
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
        
        let list = OTTMediaItem.getManagedObjects(managedObjectContext: managedObjectContext) as? [OTTMediaItem]
        
        guard let tempList = list else{ return }
        
        for item in tempList
        {
            managedObjectContext.delete(item)
        }
         managedObjectContext.Save()
        
    }
    
    
    public func getReleaseDateYear() -> String?
    {        
        
        guard let releaseDate = self.releaseDate else {
            return nil
        }
        
        guard releaseDate.count >= 4 else { return nil }
        
        let startIndex  = releaseDate.index(releaseDate.startIndex, offsetBy: 4)
        let year        = releaseDate.substring(to: startIndex)
        
        return year
    }
    
    public  func getDurationInSeconds() -> Int
    {
        
        guard let durationinMinutes = duration else {return 0}
        
        if let durationInt = Int(durationinMinutes)
        {
            return durationInt * 60
        }
        else
        {
            return 0
        }
       
    }
    
    
    public  func getDurationInMinutes() -> Int
    {
        
        guard let durationinMinutes = duration else {return 0}
        
        if let durationInt = Int(durationinMinutes)
        {
            return durationInt
        }
        else
        {
            return 0
        }
        
    }
    
    public  func getDurationInHHmm() -> (hr:Int,min:Int)
    {
        
        guard let durationinMinutes = duration else {return (0,0)}
        
        guard let durationInt = Int(durationinMinutes)
        else
        {
            return (0,0)
        }
        
        let hr = durationInt/60
        let min = durationInt%60
        
        return (hr,min)
    }
    
    public func isShow() -> Bool
    {
        if self.theme == "show"
        {
            return true
        }
        
        return false
    }
    
    
    public func getPeopleWithRoles(type:OTTPeopleRoleTypes?) -> [OTTPeople]?
    {
        guard let roleType = type else
        {
            return self.people
        }
        
        guard let people = self.people else {  return nil }
        
        var list = [OTTPeople]()
        
        for person in people
        {
            if let role = person.roles
            {
               if role.contains(roleType.key)
               {
                    list.append(person)
               }
            }
        }
        
        
        if list.count == 0
        {
            return nil
        }
        
        return list
    }
    
}
