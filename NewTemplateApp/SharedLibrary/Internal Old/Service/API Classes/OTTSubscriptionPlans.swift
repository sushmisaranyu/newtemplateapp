//
//  OTTSubscriptionPlans.swift
//  SaraynuLibrary
//
//  Created by Ashley Dsouza on 19/06/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation


public class OTTSubscriptionPlans
{
    public var id:String?
    public var title:String?
    public var region:String?
    public var currency:String?
    public var duration:String?
    public var period:String?
    public var description:String?
    public var apple_product_id:String?
    public var local_price:String?
    public var plan_category:String?
    public var plan_subscription_catalog_id:String?
    public var plan_category_pack_id:String?
    public var priceInDecimalFormat:NSDecimalNumber = 0.0
    public var discounted_price:String?
    public var actual_price:String?
    public var currencyType:String?
    public var currencySymbol:String?
    public var priceWithoutSymbol:String?
    public var sortId:Int = 0

    
    
    
    
    public class func Parse(plan_category:String,plan_subscription_catalog_id:String,plan_category_pack_id:String,resposne:[String:Any],sortId:Int) -> OTTSubscriptionPlans
    {
        var plan = OTTSubscriptionPlans()
        
        plan.id                                 = resposne.valueFor(key: "id", isMandatory: false)
        plan.title                              = resposne.valueFor(key: "title", isMandatory: false)
//        plan.region                             = resposne.valueFor(key: "region", isMandatory: false)
        plan.currency                           = resposne.valueFor(key: "currency", isMandatory: false)
        plan.duration                           = resposne.valueFor(key: "duration", isMandatory: false)
        plan.period                             = resposne.valueFor(key: "period", isMandatory: false)
        plan.discounted_price                   = resposne.valueFor(key: "discounted_price", isMandatory: false)
        plan.description                        = resposne.valueFor(key: "description", isMandatory: false)
        plan.apple_product_id                   = resposne.valueFor(key: "apple_product_id", isMandatory: false)
        plan.plan_category                      = plan_category
        plan.plan_subscription_catalog_id       = plan_subscription_catalog_id
        plan.plan_category_pack_id              = plan_category_pack_id
        plan.sortId                             = sortId
        plan.actual_price                       = resposne.valueFor(key: "price", isMandatory: false)
        return plan
    }
    

    public func getProductnameForSubscription() -> String
    {
        var name = ""
        guard let category = self.plan_category else {return name}
        name = category

        guard let duration = self.duration else {return name}

         name = name + " - " + duration

        if let period = self.period
        {
            name = name + " " + period
        }

        return name

    }
}
