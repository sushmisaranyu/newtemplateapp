//
//  OTTSubscriptionStatus.swift
//  SaraynuLibrary
//
//  Created by Ashley Dsouza on 19/06/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation
import CoreData


@objc(OTTSubscriptionStatus)
public class OTTSubscriptionStatus: NSManagedObject
{
    @nonobjc public class func fetchRequest() -> NSFetchRequest<OTTSubscriptionStatus> {
        return NSFetchRequest<OTTSubscriptionStatus>(entityName: "OTTSubscriptionStatus")
    }
    
    @NSManaged public var id: String?
    @NSManaged public var plan_id: String?
    @NSManaged public var category: String?
    @NSManaged public var valid_till: String?
    @NSManaged public var start_date: String?
    @NSManaged public var currentServerDate:String?
    @NSManaged public var sortId:Int


    class func InitOTTSubscriptionStatus(managedObjectContext:NSManagedObjectContext) -> OTTSubscriptionStatus {
        
        return  OTTSubscriptionStatus.managedObject(managedObjectContext: managedObjectContext) as! OTTSubscriptionStatus
    }
    
    public  class func Parse(response:[String:Any],currentServerDate:String,sortId:Int)
    {
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
        
        let status = OTTSubscriptionStatus.InitOTTSubscriptionStatus(managedObjectContext: managedObjectContext)
        
        status.id = response.valueFor(key: "id", isMandatory: false)
        status.plan_id = response.valueFor(key: "plan_id", isMandatory: false)
        status.category = response.valueFor(key: "category", isMandatory: false)
        status.valid_till = response.valueFor(key: "valid_till", isMandatory: false)
        status.start_date = response.valueFor(key: "start_date", isMandatory: false)
        status.currentServerDate    = currentServerDate
        status.sortId = sortId
        
        managedObjectContext.Save()
        
    }
    
    public class func  hasValidSubscription() -> Bool
    {
      let (timeRemainging,totalSubsriptionTime) =  subscriptionTimeLeft()
      if timeRemainging == 0 && totalSubsriptionTime == 0 { return false }
      return true

    }
    
    
    //Subscription Time left (timeRemainging,totalSubsriptionTime)
    public class func  subscriptionTimeLeft() -> (Double,Double)
    {
     
        
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
        
        guard let list = OTTSubscriptionStatus.getManagedObjects(managedObjectContext: managedObjectContext) as? [OTTSubscriptionStatus]
            else {
                 return (0,0)
        }
        
        let sortedList = list.sorted(by: {$0.sortId < $1.sortId})
        
        let currentDate = sortedList.first?.currentServerDate
        let startDate   = sortedList.first?.start_date
        let validTill   = sortedList.last?.valid_till
        
        
        if currentDate == nil || startDate == nil || validTill == nil { return (0,0) }
        
        let timeRemaininginSecs = AppUtilities.convertDateFormater(date: validTill!) - AppUtilities.convertDateFormater(date: currentDate!)
        
        let totalSubsriptionTimeinSecs = AppUtilities.convertDateFormater(date: validTill!) - AppUtilities.convertDateFormater(date: startDate!)
        
        
        let timeRemainging   = timeRemaininginSecs / 60 / 60 / 24
        let totalSubsriptionTime = totalSubsriptionTimeinSecs / 60 / 60 / 24
        
        print(timeRemainging)
        print(totalSubsriptionTime)
        return (timeRemainging,totalSubsriptionTime)
        
    }

    
    public class func DeleteAll() {
        
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
        
        guard let list = OTTSubscriptionStatus.getManagedObjects(managedObjectContext: managedObjectContext) as? [OTTSubscriptionStatus]
            else {
                return
        }
        
        for item in list
        {
            managedObjectContext.delete(item)
        }
         managedObjectContext.Save()
        
    }

}
