//
//  OTTCatalog+CoreDataProperties.swift
//  
//
//  Created by Ashley Dsouza on 20/06/17.
//
//

import Foundation
import CoreData


extension OTTCatalog {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<OTTCatalog> {
        return NSFetchRequest<OTTCatalog>(entityName: "OTTCatalog")
    }

    @NSManaged public var catalog_id: String?
    @NSManaged public var sequence_no: NSNumber
    @NSManaged public var catalog_type: String?
    @NSManaged public var genres: [String]?
    @NSManaged public var name: String?
    @NSManaged public var friendly_id: String?

}
