//
//  OTTAccessControl.swift
//  SaraynuLibrary
//
//  Created by Ashley Dsouza on 26/06/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation


public class AdvertModel
{
    public var adURL:String
    public var midRollPosition:[Int]

    init(url:String,midrollPosition:[Int])
    {
        self.adURL = url
        self.midRollPosition = midrollPosition
    }

    public class func Parse(key:String,json:[String:Any]) -> AdvertModel?
    {
        guard let ad:[String:Any] = json.valueFor(key: key) else {return nil}
        guard let adURl:String = ad.valueFor(key: "ads_url", isMandatory: false) else {return nil}

        var midroll = [Int]()

        if let value:[Int] = ad.valueFor(key: "midroll_position", isMandatory: false)
        {
            midroll = value
        }

        return AdvertModel(url: adURl, midrollPosition: midroll)
    }

}

public class OTTAccessControl:NSObject,NSCoding
{
    public var is_free:Bool?
    public var login_required:Bool?
    public var ads_available:Bool?
    public var premium_tag:Bool?

    public var post_role_settings:AdvertModel?
    public var mid_role_settings:AdvertModel?
    public var pre_role_settings:AdvertModel?



    public class func Parse(json:[String:Any]) -> OTTAccessControl
    {
        var accessControl = OTTAccessControl()
        accessControl.is_free = json.valueFor(key: "is_free", isMandatory: false)
        accessControl.premium_tag = json.valueFor(key: "premium_tag", isMandatory: false)

        accessControl.login_required = json.valueFor(key: "login_required", isMandatory: false)
        accessControl.ads_available = json.valueFor(key: "ads_available", isMandatory: false)

        accessControl.post_role_settings = AdvertModel.Parse(key: "post_role_settings", json: json)
        accessControl.mid_role_settings = AdvertModel.Parse(key: "mid_role_settings", json: json)
        accessControl.pre_role_settings = AdvertModel.Parse(key: "pre_role_settings", json: json)
        
        return accessControl
    }
    
    public func encode(with aCoder: NSCoder){
        
        aCoder.encode(is_free, forKey: "is_free")
        aCoder.encode(premium_tag, forKey: "premium_tag")

        aCoder.encode(login_required, forKey: "login_required")
        aCoder.encode(ads_available,forKey: "ads_available")
    }
    
    required convenience public init(coder aDecoder: NSCoder) {
        
        self.init()
        self.is_free          = (aDecoder.decodeObject(forKey: "is_free") as? Bool)
        self.login_required   = (aDecoder.decodeObject(forKey: "login_required") as? Bool)
        self.ads_available    = (aDecoder.decodeObject(forKey: "ads_available") as? Bool)
        self.premium_tag      = (aDecoder.decodeObject(forKey: "premium_tag") as? Bool)
    }

    func getPreRollAd() -> String? {
        return self.pre_role_settings?.adURL
    }

    
}
