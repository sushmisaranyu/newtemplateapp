//
//  OTTPlayListItem.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 21/04/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation


public class OTTPlayListItem:NSObject,NSCoding
{
     public var catalogId: String?
     public var catalogName:String?
     public var contentId: String?
     public var mediadescription: String?
     public var duration: String?
     public var friendlyId: String?
     public var genres: [String]?
     public var keywords: [String]?
     public var language: String?
     public var sortId: NSNumber?
     public var people: [OTTPeople]?
     public var playURL: OTTPlayURL?
     public var playURLType: String?
     public var rating: NSNumber?
     public var releaseDate: String?
     public var theme: String?
     public var thumbnails: OTTImageURLs?
     public var title: String?
     public var censorRating :String?
     public var id:NSNumber?
     public var play_back_time:String?
     public var userRatings:NSNumber?
     public var listitem_id:String?
     public var view_count:NSNumber?
     public var like_count:NSNumber?
    public var playlist_id:String?
    public var show_name:String?
    public var subcategory_name:String?
    public var share_url:String?
       public override init()
    {
        sortId = 0
    }
    
    public func encode(with aCoder: NSCoder){
        
        aCoder.encode(catalogId, forKey: "catalogId")
        aCoder.encode(contentId, forKey: "contentId")
        aCoder.encode(mediadescription, forKey:"mediadescription")
        aCoder.encode(duration, forKey: "duration")
        aCoder.encode(friendlyId, forKey: "friendlyId")
        aCoder.encode(genres, forKey:"genres")
        aCoder.encode(keywords, forKey: "keywords")
        aCoder.encode(language, forKey: "language")
        aCoder.encode(sortId, forKey:"sortId")
        aCoder.encode(people, forKey: "people")
        aCoder.encode(playURL, forKey: "playURL")
        aCoder.encode(playURLType, forKey:"playURLType")
        aCoder.encode(rating, forKey: "rating")
        aCoder.encode(releaseDate, forKey: "releaseDate")
        aCoder.encode(theme, forKey:"theme")
        aCoder.encode(thumbnails, forKey: "thumbnails")
        aCoder.encode(releaseDate, forKey: "title")
        aCoder.encode(censorRating, forKey:"censorRating")
        aCoder.encode(id, forKey: "id")
        aCoder.encode(play_back_time, forKey: "play_back_time")
        aCoder.encode(userRatings, forKey: "userRatings")
        aCoder.encode(listitem_id,forKey:"listitem_id")
        aCoder.encode(view_count,forKey:"view_count")
        aCoder.encode(like_count,forKey:"like_count")
        aCoder.encode(show_name,forKey:"show_name")
        aCoder.encode(subcategory_name,forKey:"subcategory_name")
        
    }
    
    required convenience public init(coder aDecoder: NSCoder) {
        
        self.init()
        self.catalogId          = aDecoder.decodeObject(forKey: "catalogId") as? String
        self.catalogName        = aDecoder.decodeObject(forKey: "catalogName") as? String
        self.contentId          = aDecoder.decodeObject(forKey: "contentId") as? String
        self.mediadescription   = aDecoder.decodeObject(forKey: "mediadescription") as? String
        self.duration           = aDecoder.decodeObject(forKey: "duration") as? String
        self.friendlyId         = aDecoder.decodeObject(forKey: "friendlyId") as? String
        self.genres             = aDecoder.decodeObject(forKey: "genres") as? [String]
        self.genres             = aDecoder.decodeObject(forKey: "genres") as? [String]
        self.keywords           = aDecoder.decodeObject(forKey: "keywords") as? [String]
        self.sortId             = aDecoder.decodeObject(forKey: "sortId") as? NSNumber
        self.people             = aDecoder.decodeObject(forKey: "people") as? [OTTPeople]
        self.playURL            = aDecoder.decodeObject(forKey: "playURL") as? OTTPlayURL
        self.playURLType        = aDecoder.decodeObject(forKey: "playURLType") as? String
        self.rating             = aDecoder.decodeObject(forKey: "rating") as? NSNumber
        self.releaseDate        = aDecoder.decodeObject(forKey: "releaseDate") as? String
        self.theme              = aDecoder.decodeObject(forKey: "theme") as? String
        self.thumbnails         = aDecoder.decodeObject(forKey: "thumbnails") as? OTTImageURLs
        self.title              = aDecoder.decodeObject(forKey: "title") as? String
        self.censorRating       = aDecoder.decodeObject(forKey: "censorRating") as? String
        self.id                 = aDecoder.decodeObject(forKey: "id") as? NSNumber
        self.play_back_time     = aDecoder.decodeObject(forKey: "play_back_time") as? String
        
        
        self.userRatings        = aDecoder.decodeObject(forKey: "userRatings") as? NSNumber
        
        self.listitem_id        = aDecoder.decodeObject(forKey: "listitem_id") as? String
        self.view_count         = aDecoder.decodeObject(forKey: "view_count") as? NSNumber
        self.like_count         = aDecoder.decodeObject(forKey: "like_count") as? NSNumber
        self.show_name          = aDecoder.decodeObject(forKey: "show_name") as? String
        self.subcategory_name   = aDecoder.decodeObject(forKey: "subcategory_name") as? String
      
        
    }
    
  
    
    public class func Parse(jsonDictionary:[String:Any],sortid:NSNumber = 0) -> OTTPlayListItem
    {
        let object                  = OTTPlayListItem()
        
        object.catalogId            = jsonDictionary.valueFor(key:"catalog_id", isMandatory: false)
        object.catalogName          = jsonDictionary.valueFor(key:"catalog_name", isMandatory: false)
        object.contentId            = jsonDictionary.valueFor(key:"content_id", isMandatory: false)
        object.mediadescription     = jsonDictionary.valueFor(key:"description", isMandatory: false)
        object.duration             = jsonDictionary.valueFor(key:"duration_string", isMandatory: false)
        object.friendlyId           = jsonDictionary.valueFor(key:"friendly_id", isMandatory: false)
        object.genres               = jsonDictionary.valueFor(key:"genres", isMandatory: false)
        object.keywords             = jsonDictionary.valueFor(key:"keywords", isMandatory: false)
        object.language             = jsonDictionary.valueFor(key:"language", isMandatory: false)
        object.sortId               = sortid as NSNumber?
        object.rating               = jsonDictionary.valueFor(key:"rating", isMandatory: false)
        object.releaseDate          = jsonDictionary.valueFor(key:"release_date_string", isMandatory: false)
        object.theme                = jsonDictionary.valueFor(key:"theme", isMandatory: false)
        object.title                = jsonDictionary.valueFor(key:"title", isMandatory: false)
        object.censorRating         = jsonDictionary.valueFor(key:"censor_rating", isMandatory: false)
        object.id                   = jsonDictionary.valueFor(key:"id", isMandatory: false)
        object.play_back_time       = jsonDictionary.valueFor(key:"play_back_time", isMandatory: false)
      
        object.listitem_id          = jsonDictionary.valueFor(key: "listitem_id", isMandatory:false)
        object.view_count           = jsonDictionary.valueFor(key: "view_count", isMandatory:false)
        object.like_count           = jsonDictionary.valueFor(key: "like_count", isMandatory:false)
        object.show_name            = jsonDictionary.valueFor(key: "show_name", isMandatory:false)
        object.subcategory_name     = jsonDictionary.valueFor(key: "subcategory_name", isMandatory:false)
       
          object.userRatings          = 0
        
        if let value:String = jsonDictionary.valueFor(key:"user_ratings", isMandatory: false)
        {
            
            if let dnos = Double(value)
            {
                object.userRatings = NSNumber(value:dnos)
            }
            
           
        }
      
        
        if let dict:[[String:Any]]  = jsonDictionary.valueFor(key:"people", isMandatory: false)
        {
            var people = [OTTPeople]()
            var index = 0
            for obj in dict
            {
                people.append(OTTPeople.Parse(jsonDictionary: obj,sortid: Int64(index)))
                index+=1
            }
            
            object.people = people
        }
        
        
        if let value:String = jsonDictionary.valueFor(key:"play_url_type", isMandatory: false)
        {
            object.playURLType  = value
            if let dict:[String:Any] = jsonDictionary.valueFor(key:"play_url", isMandatory: false)
            {
                if object.theme == "movie"
                {
                    object.playURL = OTTPlayURL.Parse(jsonDictionary: dict, playURLTypeString: value)
                }
                
                if object.theme == "video"
                {
                    object.playURL = OTTPlayURL.ParseFallbackForVideoTheme(jsonDictionary: dict, playURLTypeString: value)
                }
            }
        }
        
        
        
        if let thumbnailjson:[String:Any] = jsonDictionary.valueFor(key:"thumbnails", isMandatory: false)
        {
            object.thumbnails   = OTTImageURLs.Parse(jsonDictionary: thumbnailjson)
        }
        
       return object
    }
    
    
    public func getWatchHistory() -> Int
    {
        if self.play_back_time == nil { return 0 }
        
        let time = AppUtilities.getSecondsFrom_HH_MM_SS(timeString: self.play_back_time!)
        
        return time
    }

}
