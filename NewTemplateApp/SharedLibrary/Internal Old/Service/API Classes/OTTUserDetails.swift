//
//  OTTUserDetails.swift
//  SaraynuLibrary
//
//  Created by Ashley Dsouza on 19/06/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation
import CoreData

public class OTTUserDetails
{
     public var email_id: String?

    
    public var login_type: String?
    public var firstName: String?
    public var mobile_number:String?
    public var lastName: String?
    public var user_id: String?
    public var primary_id:String?
    public var profile_pic:String?
    public var state:String?
    public var district:String?
    public var city:String?
    public var address:String?
    public var uploadImg:uploadImage?
    public var gender:String?
    public var ext_account_email_id:String?
    public var user_email_id:String?
    public var birthdate:String?
    public var parental_pin:String?
    public var is_subscribed:Bool = false
    public var sub_category:[String]?
    public var active_plans:[String]?
    public var inactive_plans:[String]?
    
    

    
    public func saveTODB()
    {
//        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
//        
//        let user = OTTUser.InitOTTUser(managedObjectContext: managedObjectContext)
//        
//        user.emailId                = self.email_id
//        user.ext_account_email_id   = self.ext_account_email_id
//        user.loginType               = self.login_type
//        user.firstName               = self.firstName
//        user.mobile_number           = self.mobile_number
//        user.lastName               = self.lastName
//        user.userId                  = self.user_id
//       
//        managedObjectContext.Save()
    }
    public class func Parse(response:[String:Any]) -> OTTUserDetails
    {
        let userdetails = OTTUserDetails()
        
        userdetails.email_id = response.valueFor(key: "email_id", isMandatory: false)
        userdetails.login_type = response.valueFor(key: "login_type", isMandatory: false)
        userdetails.mobile_number = response.valueFor(key: "mobile_number", isMandatory: false)
        userdetails.firstName = response.valueFor(key: "firstname", isMandatory: false)
        userdetails.lastName = response.valueFor(key: "lastname", isMandatory: false)
        userdetails.profile_pic = response.valueFor(key: "profile_pic", isMandatory: false)
        userdetails.state = response.valueFor(key: "state", isMandatory: false)
        userdetails.district = response.valueFor(key: "district", isMandatory: false)
        userdetails.city = response.valueFor(key: "city", isMandatory: false)
        userdetails.address = response.valueFor(key: "address", isMandatory: false)
        userdetails.gender = response.valueFor(key: "gender", isMandatory: false)
        userdetails.user_id = response.valueFor(key: "user_id", isMandatory: false)
        userdetails.ext_account_email_id = response.valueFor(key: "ext_account_email_id", isMandatory: false)
        userdetails.user_email_id = response.valueFor(key: "user_email_id", isMandatory: false)
        userdetails.birthdate = response.valueFor(key: "birthdate", isMandatory: false)
        userdetails.parental_pin = response.valueFor(key: "parental_pin", isMandatory: false)
        userdetails.sub_category = response.valueFor(key: "sub_category", isMandatory: false)
        userdetails.inactive_plans = response.valueFor(key: "inactive_plans", isMandatory: false)
        userdetails.active_plans = response.valueFor(key: "active_plans", isMandatory: false)
        userdetails.is_subscribed = false

        if let value:Bool = response.valueFor(key: "is_subscribed", isMandatory: false)
        {
             userdetails.is_subscribed = value
        }


        AppUserDefaults.ParseParentalControlFlag(json: response, dataKey: "data")

        

        if let json:[String:Any]  = response.valueFor(key: "upload_image", isMandatory: false)
        {
            userdetails.uploadImg = uploadImage.Parse(response: json)
        }
        
         OTTUser.DeleteAllUsers()
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()


        let user = OTTUser.InitOTTUser(managedObjectContext: managedObjectContext)

        user.emailId              = userdetails.email_id
        user.ext_account_email_id = userdetails.ext_account_email_id
        user.loginType            = userdetails.login_type
        user.firstName            = userdetails.firstName
        user.mobile_number        = userdetails.mobile_number
        user.lastName             = userdetails.lastName
        user.userId               = userdetails.user_id
        user.is_subscribed        = userdetails.is_subscribed
        user.sub_category         = userdetails.sub_category
        user.active_plans         = userdetails.active_plans
        user.inactive_plans       = userdetails.inactive_plans
        
        #if os(tvOS)
        //Shemaroo specific logic for Parental Control handling
        if let value:[String:Any] =  response.valueFor(key:"profile_obj", isMandatory: false)
        {
            if let profileID:String = value.valueFor(key: "profile_id", isMandatory: false)
            {
                //  if AppUserDefaults.getParentalControlForApp() == false
                // {
                AppUserDefaults.profile_ID.setValue(profileID)
                //    }
            }
            
        }
        #endif


        managedObjectContext.Save()

        return userdetails
    
    }
    
   
    
}

public class uploadImage
{
    public var access_key:String?
    public var key:String?
    public var policy:String?
    public var signature:String?
    public var sas:String?
    public var bucket:String?
    public var acl:String?
    public var domain_name:String?
    public var base_url:String?
    
    public class func Parse(response:[String:Any]) -> uploadImage
    {
        let uploadimg = uploadImage()
        
        uploadimg.access_key  = response.valueFor(key: "access_key", isMandatory: false)
        uploadimg.key         = response.valueFor(key: "key", isMandatory: false)
        uploadimg.policy      = response.valueFor(key: "policy", isMandatory: false)
        uploadimg.signature   = response.valueFor(key: "signature", isMandatory: false)
        uploadimg.sas         = response.valueFor(key: "sas", isMandatory: false)
        uploadimg.bucket      = response.valueFor(key: "bucket", isMandatory: false)
        uploadimg.acl         = response.valueFor(key: "acl", isMandatory: false)
        uploadimg.domain_name = response.valueFor(key: "domain_name", isMandatory: false)
        uploadimg.base_url    = response.valueFor(key: "base_url", isMandatory: false)

        return uploadimg
    }
   
    
}

