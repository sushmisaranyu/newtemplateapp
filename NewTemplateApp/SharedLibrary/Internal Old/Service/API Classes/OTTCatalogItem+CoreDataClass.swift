//
//  OTTCatalogItem+CoreDataClass.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 02/03/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData

@objc(OTTCatalogItem)
public class OTTCatalogItem: NSManagedObject {

//    func getRegions() -> [String]
//    {
//        return region as! [String]
//    }
//    
//    
//    func setRegion(regions:[String])
//    {
//        region = regions as NSObject
//    }
//    
//    func setThumbnails(thumbnails:OTTImageURLs)
//    {
//        self.thumbnails = thumbnails
//    }
    
    
    
    
    
    /**
     Parses jsonDictionary
     - Parameter jsonDictionary: json dictionary from server
     - Parameter sortid: sorting Id used to order resultswhen displaying items
     - Parameter isMenuItem: flag used to identify if a catalog item is a menuList Item or is a first level item
     - Returns: OTTCatalogItem
     */
    public class func Parse(jsonDictionary:[String:Any], sortid:Int = 0, isMenuItem:Bool = false, parentCatalog:OTTCatalogItem? = nil,parentListId:String? = nil) -> String?
    {
        
        
        if let listid:String = jsonDictionary.valueFor(key: "list_id", isMandatory: false)
        {
            
            let managedObjectContext    = NSManagedObjectContext.newObjectContext()
            let object                  = OTTCatalogItem.InitOTTCatalogItem(listId: listid, parentListId:parentListId,managedObjectContext: managedObjectContext)
       
            object.displayTitle = jsonDictionary.valueFor(key:"display_title", isMandatory: false)
            object.friendlyId   = jsonDictionary.valueFor(key:"friendly_id", isMandatory: false)
            object.listId       = jsonDictionary.valueFor(key:"list_id", isMandatory: false)
            object.listType     = jsonDictionary.valueFor(key:"list_type", isMandatory: false)
            object.region       = jsonDictionary.valueFor(key:"region", isMandatory: false)
            object.tags         = jsonDictionary.valueFor(key:"tags", isMandatory: false)
            object.total_items_count = jsonDictionary.valueFor(key:"total_items_count", isMandatory: false)
            object.sortId       = sortid as NSNumber?
            object.theme        = jsonDictionary.valueFor(key:"theme", isMandatory: false)
            object.isMenuItem   = isMenuItem
            object.layout_type  = jsonDictionary.valueFor(key: "layout_type", isMandatory: false)
            object.home_link    = jsonDictionary.valueFor(key: "home_link", isMandatory: false)
            
            
            if let imageDictionary:[String:Any] = jsonDictionary.valueFor(key:"thumbnails", isMandatory: false)
            {
                object.thumbnails = OTTImageURLs.Parse(jsonDictionary: imageDictionary)
            }
            
            
            
            object.catalogItemList = [OTTItem]()
            
            if let items:[[String:Any]] = jsonDictionary.valueFor(key:"catalog_list_items", isMandatory: false)
            {
                var catalogType = OTTCatalogType.Empty
                var index = 0
                
                          
                
                for item in items
                {
                    
                    if  let theme:String = item.valueFor(key:"theme", isMandatory: false)
                    {
                        //print(theme)
                        if theme == "movie" || theme == "video" || theme == "show" || theme == "show_episode"
                        {
                            //print("movie")
                            if let itemId = OTTMediaItem.Parse(jsonDictionary: item)
                            {
                                catalogType = .MediaList
                                object.catalogItemList?.append(OTTItem(id:itemId,type: OTTItemType.Media, sortId:NSNumber(value:index)))
                            }
                        }
                        
                        
                        if theme == "catalog_list"
                        {
                    
                            //print("catalog_list")
                            if let itemId = OTTCatalogItem.Parse(jsonDictionary: item,sortid: index,parentCatalog: object,parentListId: object.listId)
                            {
                                catalogType = .CatalogList
                                object.catalogItemList?.append(OTTItem(id:itemId,type: OTTItemType.Catalog, sortId:NSNumber(value:index)))
                            }
                        }
                    }
                    index+=1
                }
                object.catalogType = catalogType.key
            }
            

            
            
            managedObjectContext.Save()
            
            let item = OTTCatalogItem.GetOTTCatalogItemWith(listId:object.listId,managedObjectContext:managedObjectContext)
            return object.listId
        }
        
        return nil
    }

    
}
