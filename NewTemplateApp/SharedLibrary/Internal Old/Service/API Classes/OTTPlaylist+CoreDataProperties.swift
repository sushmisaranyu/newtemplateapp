//
//  OTTPlaylist+CoreDataProperties.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 21/04/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension OTTPlaylist {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<OTTPlaylist> {
        return NSFetchRequest<OTTPlaylist>(entityName: "OTTPlaylist");
    }
    
    @NSManaged public var name: String?
    @NSManaged public var playlist_type: String?
    @NSManaged public var playlist_id: String?
    @NSManaged public var list: [OTTPlayListItem]?
    @NSManaged public var profileId:String?
    @NSManaged public var sortId:NSNumber?

}
