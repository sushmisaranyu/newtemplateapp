//
//  OTTItem.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 19/03/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation
import CoreData

public enum OTTItemType{
    case Media
    case Catalog
    
    
    public var key:String
    {
        switch self
        {
            case .Media         : return "media"
            case .Catalog   : return "catalog"
           
            
        }
    }
    
    static func GetTypeFor(value:String?) -> OTTItemType?
    {
        guard let key = value else{
            return nil
        }
        
        switch key
        {
        case OTTItemType.Media.key:         return .Media
        case OTTItemType.Catalog.key:       return .Catalog
        
        default: return nil
        }
    }
}


public class OTTItem: NSObject,NSCoding
{
    public var id: String = ""
    public var itemType: OTTItemType
    public var sortId:NSNumber = 0
    
//    init() {
//        
//    }
//    
    
    public init(id:String,type:OTTItemType,sortId:NSNumber)
    {
        
        self.id         = id
        self.itemType   = type
        self.sortId     = sortId
    }
    
    required convenience public init(coder aDecoder: NSCoder) {
        
      //  self.init()
        let itemType           = OTTItemType.GetTypeFor(value: (aDecoder.decodeObject(forKey: "itemType") as? String)!)!
        let id                 = aDecoder.decodeObject(forKey: "id") as! String
        let sortId             = aDecoder.decodeObject(forKey: "sortId") as! NSNumber
        
        self.init(id:id,type:itemType,sortId:sortId)
        
    }
    
    public func encode(with aCoder: NSCoder){
        aCoder.encode(itemType.key, forKey: "itemType")
        aCoder.encode(id, forKey: "id")
        aCoder.encode(sortId,forKey:"sortId")
    }

    
    

    
}
