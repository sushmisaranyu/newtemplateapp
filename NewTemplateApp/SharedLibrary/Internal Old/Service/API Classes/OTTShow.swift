//
//  OTTShow.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 03/05/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation


public class OTTShow
{
    public var show:OTTMediaItem
    public var seasons:[OTTSeasons]?
    public var episodes:[OTTVideoItem]?
    
    public init(showMediaItem:OTTMediaItem)
    {
        show = showMediaItem
    }
    
    
    public var hasSeasons:Bool
    {
        if show.subcategoryFlag == true
        {
            return true
        }
        return false
    }
    
    
    public var hasSeasonsBeenDownloaded:Bool
        {
            if seasons == nil || seasons?.count == 0
            {
                return false
            }
        
            return true
    }
    
    public var showId:String?
    {
        return show.contentId
    }
    
    public var hasEpisodesForShow:Bool
    {
        if episodes == nil || (episodes?.count)! == 0
        {
            return false
        }
        
        return true
    }
    
    public func getSeasonForIndex(seasonIndex:Int) -> OTTSeasons
    {
        if seasonIndex >= (seasons?.count)!
        {
            return (seasons?.first)!
        }
        
        return seasons![seasonIndex]
    }
    
    func getEpisodesForSeasonIndex(index:Int) -> [OTTVideoItem]
    {
        
        if index >= (seasons?.count)!
        {
            return (seasons?.first!.episodes)!
        }
        
        return seasons![index].episodes!
    }
    
    var seasonCount:Int
    {
        guard let list = seasons
            else {
                return 0
        }
        
        return list.count
    }
    
    var showEpisodeCount:Int{
        guard let list = episodes
        else {
            return 0
        }
        
        return list.count
    }
    
}
