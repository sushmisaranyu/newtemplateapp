//
//  OTTCatalogTheme.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 03/03/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation

public enum OTTCatalogType
{
    case MediaList
    case CatalogList
    case Empty
    case ContinueWatching
    
    public var key:String
    {
        switch self
        {
            case .MediaList             : return "MediaList"
            case .CatalogList           : return "CatalogList"
            case .Empty                 : return "Empty"
            case .ContinueWatching      : return "ContinueWatching"
            
        }
    }
    
    static func GetTypeFor(value:String?) -> OTTCatalogType?
    {
        guard let key = value else{
            return nil
        }
        
        switch key
        {
            case OTTCatalogType.MediaList.key:         return OTTCatalogType.MediaList
            case OTTCatalogType.CatalogList.key:       return OTTCatalogType.CatalogList
            case OTTCatalogType.ContinueWatching.key:      return OTTCatalogType.ContinueWatching
            default: return nil
        }
    }
}


