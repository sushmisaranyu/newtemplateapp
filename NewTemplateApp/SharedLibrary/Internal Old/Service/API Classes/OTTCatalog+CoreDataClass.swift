//
//  OTTCatalog+CoreDataClass.swift
//  
//
//  Created by Ashley Dsouza on 20/06/17.
//
//

import Foundation
import CoreData

@objc(OTTCatalog)
public class OTTCatalog: NSManagedObject {
    public  class func Parse(jsonDictionary:[String:Any], setCache:Bool = false)
    {
        
        if let catalog_id:String = jsonDictionary.valueFor(key:"catalog_id", isMandatory: false)
        {
            guard let theme:String = jsonDictionary.valueFor(key:"theme", isMandatory: false) else {
                return
            }
            
            if theme != "catalog" { return }
            
            let managedObjectContext    = NSManagedObjectContext.newObjectContext()
            let object                  = OTTCatalog.managedObject(managedObjectContext: managedObjectContext) as! OTTCatalog
            
            object.catalog_id            = catalog_id
            object.catalog_type          = jsonDictionary.valueFor(key:"catalog_name", isMandatory: false)
            object.friendly_id            = jsonDictionary.valueFor(key:"friendly_id", isMandatory: false)
            
            object.name                 = jsonDictionary.valueFor(key:"name", isMandatory: false)
            
            object.sequence_no          = 0
            if let sequence:NSNumber = jsonDictionary.valueFor(key:"sequence_no", isMandatory: false)
            {
                 object.sequence_no  = sequence
            }
            
            if let value:[[String:Any]] = jsonDictionary.valueFor(key:"genres", isMandatory: false)
            {
                var genre = [String]()
                for item in value
                {
                    if let name:String = item.valueFor(key: "name", isMandatory: false)
                    {
                        genre.append(name)
                    }
                }
                object.genres = genre
            }
            
            managedObjectContext.Save()
            
        }
        
       
    }
    
    public class func DeleteAllCatalogs() {
        
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
        
        let list = OTTCatalog.getManagedObjects(managedObjectContext: managedObjectContext) as? [OTTCatalog]
        
        guard let tempList = list else{ return }
        
        for item in tempList
        {
            managedObjectContext.delete(item)
        }
        
        managedObjectContext.Save()
    }
    
    public class func GetAllCatalogs() -> [OTTCatalog]? {
        
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
        
        let list = OTTCatalog.getManagedObjects(managedObjectContext: managedObjectContext) as? [OTTCatalog]
        
       return list
        
    }

  
    
    public class func GetCatalogForID(listID:String) -> OTTCatalog?
    {
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
        guard  let list = OTTCatalog.getManagedObjects(managedObjectContext: managedObjectContext,predicate: NSPredicate(format:"list_id = '\(listID)'")) as? [OTTCatalog]
            else
        {
            return nil
        }
        
        return list.first
    }
}
