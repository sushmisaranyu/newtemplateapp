//
//  OTTPeople.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 28/02/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation


public class OTTPeople:NSObject,NSCoding{
   public var catalogId:String?
   public var characterName:String?
   public  var displayImage:String?
   public var name:String?
   public var personId:String?
   public var roles:[String]?
   public var sortId:Int64?
    
    override init() {
        catalogId       = ""
        characterName = ""
        displayImage = ""
        name = ""
        personId = ""
        roles = [String]()
        sortId = 0
    }
    
   public class func Parse(jsonDictionary:[String:Any], sortid:Int64 = 0) -> OTTPeople
    {
    
        let object = OTTPeople()
        object.catalogId        = jsonDictionary.valueFor(key:"catalog_id", isMandatory: false)
        object.characterName    = jsonDictionary.valueFor(key:"character_name", isMandatory: false)
        object.displayImage     = jsonDictionary.valueFor(key:"display_image", isMandatory: false)
        object.roles            = jsonDictionary.valueFor(key:"roles", isMandatory: false)
        object.sortId           = sortid
        object.personId         = jsonDictionary.valueFor(key:"person_id", isMandatory: false)
        object.name             = jsonDictionary.valueFor(key: "name", isMandatory: false)
        return object
    }
    
    required convenience public init(coder aDecoder: NSCoder) {
        
       self.init()
        self.catalogId           = aDecoder.decodeObject(forKey: "catalogId") as? String
        self.characterName       = aDecoder.decodeObject(forKey: "characterName") as? String
        self.displayImage        = aDecoder.decodeObject(forKey: "displayImage") as? String
        self.name                = aDecoder.decodeObject(forKey: "name") as? String
        self.personId            = aDecoder.decodeObject(forKey: "personId") as? String
        self.roles               = aDecoder.decodeObject(forKey: "roles") as? [String]
        self.sortId              = aDecoder.decodeObject(forKey: "sortId") as? Int64
      
       
    }
    
    public func encode(with aCoder: NSCoder){
        aCoder.encode(catalogId, forKey: "catalogId")
        aCoder.encode(characterName, forKey: "characterName")
        aCoder.encode(displayImage, forKey: "displayImage")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(personId, forKey: "personId")
        aCoder.encode(roles, forKey: "roles")        
        aCoder.encode(sortId, forKey: "sortId")
    }
    
   
    
   
}
