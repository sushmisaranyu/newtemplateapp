//
//  OTTUser+CoreDataProperties.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 06/04/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData

public enum LoginType
{
    case Email
    case Mobile
    case Facebook
    case GooglePlus
    case Twitter
    
    
    public  var key:String{
        switch self
        {
            case .Email: return "email"
            case .Mobile: return "mobile"
            case .Facebook: return "facebook"
            case .GooglePlus: return "google"
            case .Twitter: return "twitter"
        }
    }
}

extension OTTUser {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<OTTUser> {
        return NSFetchRequest<OTTUser>(entityName: "OTTUser");
    }

    
    class func InitOTTUser(managedObjectContext:NSManagedObjectContext) -> OTTUser {
       
        return  OTTUser.managedObject(managedObjectContext: managedObjectContext) as! OTTUser
    }
    
    public class func DeleteAllUsers() {
        
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
        
        let list = OTTUser.getManagedObjects(managedObjectContext: managedObjectContext) as? [OTTUser]
        
        guard let tempList = list else{ return }
        
        for item in tempList
        {
            managedObjectContext.delete(item)
        }
        
        managedObjectContext.Save()
    }
    
    /**
    Delete User all user objects in Core data and remove saved session ID
     */
    public class func Logout()
    {
        DeleteAllUsers()
        OTTProfile.DeleteAllProfiles()
        OTTPlaylist.DeleteAllPlaylist()
        OTTSubscriptionStatus.DeleteAll()
       // OTTCatalogItem.DeleteAllCatalogItems()
        AppUserDefaults.session_ID.remove()
        AppUserDefaults.profile_ID.remove()
        AppUserDefaults.login_type.remove()
        AppUserDefaults.clearUserCreds()
        
        
    }
    
    public class func getEmailID() -> String
    {
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()

        let list = OTTUser.getManagedObjects(managedObjectContext: managedObjectContext) as? [OTTUser]

        print("OTTUser Debug  \(list?.first) :: \(list?.first?.emailId) :: \(list?.first?.ext_account_email_id)")

        guard let email = list?.first?.emailId
        else
        {
            return ""
        }
        
        return email       
    }
    
    public class func getMobileNumber() -> String
    {
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
        
        let list = OTTUser.getManagedObjects(managedObjectContext: managedObjectContext) as? [OTTUser]
        
        guard let email = list?.first?.mobile_number
            else
        {
            return ""
        }
        
        return email
    }
    
    public class func getUser() -> OTTUser?
    {
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
        
        let list = OTTUser.getManagedObjects(managedObjectContext: managedObjectContext) as? [OTTUser]
        
        
        return list?.first
    }

    public class func hasParentalControl() -> Bool
    {
        return false
    }
    
    
    /**
    Check if a user is logged in or not
    Returns:  true is sessionID is present , false if sessionID is not avalible
    */
    public class func hasLoggedInUser()->Bool
    {
        return AppUserDefaults.isSessionValid
    }

    /**
     Check if a user has subscription or not
     Returns:  true if user has a subscription else false
     */
    public class func hasSubscription()->Bool
    {
        guard let user = OTTUser.getUser() else {return false}

        return user.is_subscribed
    }

    public class func getCommaSeperatedActivePlans()-> String?
    {
        guard let user = OTTUser.getUser() else {return nil}

        guard let plans = user.active_plans else {return nil}

        if plans.count == 0 {return nil}

        return plans.joined(separator: ",")
    }

    public class func getCommaSeperatedInActivePlans()-> String?
    {
        guard let user = OTTUser.getUser() else {return nil}

        guard let plans = user.inactive_plans else {return nil}

        if plans.count == 0 {return nil}

        return plans.joined(separator: ",")
    }
    
}
