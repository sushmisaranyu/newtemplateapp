//
//  OTTUser+CoreDataClass.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 06/04/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData

@objc(OTTUser)
public class OTTUser: NSManagedObject {
    
    @NSManaged public var emailId: String?
    @NSManaged public var session: String?
    @NSManaged public var loginType: String?
    @NSManaged public var firstName: String?
    @NSManaged public var lastName: String?
    @NSManaged public var profileImage: String?
    @NSManaged public var userId: String?
    @NSManaged public var ext_account_email_id: String?
    @NSManaged public var mobile_number:String?
    @NSManaged public var first_time_login:Bool
    @NSManaged public var parental_control:Bool
    @NSManaged public var is_subscribed:Bool
    @NSManaged public var sub_category:[String]?
    @NSManaged public var active_plans:[String]?
    @NSManaged public var inactive_plans:[String]?
    
    public  class func Parse(jsonDictionary:[String:Any]) -> (isValid:Bool,addProfile:Bool)
    {
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
        var addProfile = false
        var first_time_login = false
        guard let data:[String:Any] = jsonDictionary.valueFor(key:"data", isMandatory: false) else
        {
            return (false,addProfile)
        }
        
        
        guard let session:String = data.valueFor(key:"session", isMandatory: false) else
        {
            return (false,addProfile)
        }
        
        guard let userId:String = data.valueFor(key:"user_id", isMandatory: false) else
        {
            return (false,addProfile)
        }
        
        if let value:Bool = data.valueFor(key: "first_time_login", isMandatory: false)
        {
            first_time_login = value
        }
        
        let user                = InitOTTUser(managedObjectContext:managedObjectContext)
        user.userId             = userId
        user.session            = session
        user.first_time_login   = first_time_login
        
        
        if let value:String  = data.valueFor(key:"email_id", isMandatory: false)
        {
            user.emailId = value
        }
        
        if let value:String = data.valueFor(key:"ext_account_email_id",isMandatory: false)
        {
            user.ext_account_email_id = value
        }
        
        if let value:String  = data.valueFor(key:"login_type", isMandatory: false)
        {
            user.loginType = value
        }
        
        if let value:String  = data.valueFor(key:"firstname", isMandatory: false)
        {
            user.firstName = value
        }
        
        if let value:String  = data.valueFor(key:"lastname", isMandatory: false)
        {
            user.lastName = value
        }
        
        if let value:String  = data.valueFor(key:"mobile_number", isMandatory: false)
        {
            user.mobile_number = value
        }
        
        
        if let value:String  = data.valueFor(key:"profile_pic", isMandatory: false)
        {
            user.profileImage = value
        }
        
        if let value:Bool  = data.valueFor(key:"add_profile", isMandatory: false)
        {
            addProfile = value
        }

        user.is_subscribed = false

        if let value:Bool  = data.valueFor(key:"is_subscribed", isMandatory: false)
        {
             user.is_subscribed = value
        }

        user.sub_category = data.valueFor(key:"sub_category", isMandatory: false)
        user.active_plans = data.valueFor(key: "active_plans", isMandatory: false)
         user.inactive_plans = data.valueFor(key: "inactive_plans", isMandatory: false)

        AppUserDefaults.ParseParentalControlFlag(json: jsonDictionary, dataKey: "data")
        AppUserDefaults.session_ID.setValue(session)
        
        
        //Shemaroo specific logic for Parental Control handling
        if let value:[String:Any] =  data.valueFor(key:"profile_obj", isMandatory: false)
        {
            if let profileID:String = value.valueFor(key: "profile_id", isMandatory: false)
            {
                //  if AppUserDefaults.getParentalControlForApp() == false
                // {
                AppUserDefaults.profile_ID.setValue(profileID)
                //    }
            }
            
        }
        
        managedObjectContext.Save()
        return (true,addProfile)
        
    }
    
    public  class func Update(jsonDictionary:[String:Any])
    {
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
        
        var user:OTTUser?
        
        if let value    = OTTUser.getUser()
        {
            user = value
        }
        else
        {
            user = OTTUser.InitOTTUser(managedObjectContext: managedObjectContext)
        }
        
        var addProfile = false
        guard let data:[String:Any] = jsonDictionary.valueFor(key:"data", isMandatory: false) else
        {
            return
            // return (false,addProfile)
        }
        
        
        
        
        guard let userId:String = data.valueFor(key:"user_id", isMandatory: false) else
        {
            return
        }
        
        user?.userId     = userId
        
        
        if let value:String  = data.valueFor(key:"email_id", isMandatory: false)
        {
            user?.emailId = value
        }
        
        if let value:String = data.valueFor(key:"ext_account_email_id",isMandatory: false)
        {
            user?.ext_account_email_id = value
        }
        
        if let value:String  = data.valueFor(key:"login_type", isMandatory: false)
        {
            user?.loginType = value
        }
        
        if let value:String  = data.valueFor(key:"firstname", isMandatory: false)
        {
            user?.firstName = value
        }
        
        if let value:String  = data.valueFor(key:"mobile_number", isMandatory: false)
        {
            user?.mobile_number = value
        }
        
        if let value:String  = data.valueFor(key:"lastname", isMandatory: false)
        {
            user?.lastName = value
        }
        
        if let value:String  = data.valueFor(key:"profile_pic", isMandatory: false)
        {
            user?.profileImage = value
        }
        
        AppUserDefaults.ParseParentalControlFlag(json: jsonDictionary, dataKey: "data")
        
        if let value:Bool  = data.valueFor(key:"add_profile", isMandatory: false)
        {
            addProfile = value
        }
        
        managedObjectContext.Save()
    }
    
    
    public class func InitUserWithUserModel(userModel:UserModel,sessionId:String)
    {
        OTTUser.DeleteAllUsers()
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
        let user                    = OTTUser.InitOTTUser(managedObjectContext:managedObjectContext)
        user.userId             = userModel.userId
        user.emailId            = userModel.emailId

        user.mobile_number      = userModel.mobile_number
        user.firstName          = userModel.firstName
        user.lastName           = userModel.lastName
        user.loginType          = userModel.loginType
        user.profileImage       = userModel.profileImage
        user .mobile_number     = userModel.mobile_number
        user.session            = sessionId
        
        if let value = userModel.profile_id
        {
            AppUserDefaults.profile_ID.setValue(value)
        }
        print("")
        
        managedObjectContext.Save()
    }
    
}
