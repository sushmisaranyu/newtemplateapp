//
//  OTTCatalogItem+CoreDataProperties.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 02/03/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension OTTCatalogItem {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<OTTCatalogItem> {
        return NSFetchRequest<OTTCatalogItem>(entityName: "OTTCatalogItem");
    }

    @NSManaged public var displayTitle: String?
    @NSManaged public var friendlyId: String?
    @NSManaged public var isMenuItem: Bool
    @NSManaged public var listId: String?
    @NSManaged public var listType: String?
    @NSManaged public var region: [String]?
    @NSManaged public var sortId: NSNumber?
    @NSManaged public var tags: [String]?
    @NSManaged public var theme: String?
    @NSManaged public var thumbnails: OTTImageURLs?
    @NSManaged public var catalogType:String?
    @NSManaged public var total_items_count:NSNumber?
    @NSManaged public var layout_type:String?
    @NSManaged public var home_link:String?
    
    @NSManaged public var catalogItemList:[OTTItem]?
    @NSManaged public var cache:OTTCache?
    
    
    class func InitOTTCatalogItem(listId:String,parentListId:String?, managedObjectContext: NSManagedObjectContext) -> OTTCatalogItem {
        
        let item = GetOTTCatalogItemWith(listId: listId, parentListId: parentListId, managedObjectContext: managedObjectContext)
        
        if let catalog = item
        {
            return catalog
        }
        
        return  OTTCatalogItem.managedObject(managedObjectContext: managedObjectContext) as! OTTCatalogItem
        
    }
    
    
    
   public class func DeleteAllCatalogItems() {
        
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
        
        let list = OTTCatalogItem.getManagedObjects(managedObjectContext: managedObjectContext) as? [OTTCatalogItem]
        
        guard let tempList = list else{ return }
        
        for item in tempList
        {
            managedObjectContext.delete(item)
        }
    
        managedObjectContext.Save()
        
    }
    
    public class func GetOTTCatalogItemWith(listId:String?,managedObjectContext:NSManagedObjectContext) -> OTTCatalogItem?
    {
        guard let id = listId else{
            return nil
        }
        
        
        let list = OTTCatalogItem.getManagedObjects(managedObjectContext: managedObjectContext , predicate:NSPredicate(format:"listId = '\(id)'")) as? [OTTCatalogItem]
        
        if list != nil  && (list?.count)! > 0
        {
            return list!.first!
        }
        
        return  nil
    }
    
    public class func GetOTTCatalogItemWith(listId:String?,parentListId:String?,managedObjectContext:NSManagedObjectContext) -> OTTCatalogItem?
    {
        guard let id = listId else{
            return nil
        }
        
        var parentId = ""
        
        if parentListId != nil
        {
            parentId = parentListId!
        }
        
        let list = OTTCatalogItem.getManagedObjects(managedObjectContext: managedObjectContext , predicate:NSPredicate(format:"listId = '\(id)' AND parentListId = '\(parentId)'")) as? [OTTCatalogItem]
        
        if list != nil  && (list?.count)! > 0
        {
            return list!.first!
        }
        
        return  nil
    }
    
    public class func getHomeScreenCatalog(managedObjectContext:NSManagedObjectContext) -> OTTCatalogItem?
    {
       
        
        let list = OTTCatalogItem.getManagedObjects(managedObjectContext: managedObjectContext , predicate:NSPredicate(format:"isMenuItem = true"),sortKeys: ["sortId"] ,isAscending:true) as? [OTTCatalogItem]
        
        if list != nil  && (list?.count)! > 0
        {
            return list!.first!
        }
        
        return  nil
    }

    
    public func  getOTTItemContent() -> [OTTItemContent]?
    {
        guard let catalogList = self.catalogItemList else {
            return nil
        }
        
        var itemList = [OTTItemContent]()
        
        var index = 0
        for item in catalogList
        {
            var tempItem = OTTItemContent(item: item)
            switch item.itemType
            {
            case OTTItemType.Catalog:
                if tempItem.getCatalogItem(managedObjectContext: managedObjectContext!) == nil
                {
                    continue
                }
                
                itemList.append(tempItem)
                
                break
            case OTTItemType.Media:
                if tempItem.getMediaItem(managedObjectContext: managedObjectContext!) == nil
                {
                    continue
                }
                
                itemList.append(tempItem)
                
                break
            }
        }
        
        return itemList
    }

    public static func hasHomeScreen() -> Bool
    {
        let managedObjectContext = NSManagedObjectContext.newObjectContext()

        if let _ = getHomeScreenCatalog(managedObjectContext: managedObjectContext)
        {
            return true
        }
        else
        {
            return false
        }
    }

}
