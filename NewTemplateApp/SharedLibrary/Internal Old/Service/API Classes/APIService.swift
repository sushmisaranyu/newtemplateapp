//
//  APIService.swift
//  SaranyuLibrary
//
//  Created by Ash Dz on 23/02/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation


class APIService
{
    var name:String?
    var dns:String?
    var avalible:Bool?
    
    
    class func Parse(jsonDictionary:[String:Any]) ->APIService
    {
        let service = APIService()
        
        service.name        = jsonDictionary.valueFor(key:"name", isMandatory: false)
        service.dns         = jsonDictionary.valueFor(key:"dns", isMandatory: false)
        service.avalible    = jsonDictionary.valueFor(key:"avalible", isMandatory: false)
        
        return service
       
    }
}
