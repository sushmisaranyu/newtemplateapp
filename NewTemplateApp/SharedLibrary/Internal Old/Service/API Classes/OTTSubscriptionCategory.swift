//
//  OTTSubscriptionCategory.swift
//  SaraynuLibrary
//
//  Created by Ashley Dsouza on 29/06/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation


public class OTTSubscriptionCategory
{
    public var plan_category:String
    public var plan_subscription_catalog_id:String
    public var plan_category_pack_id:String
    
    
    public init(plan_category:String,plan_subscription_catalog_id:String,plan_category_pack_id:String)
    {
        self.plan_category = plan_category
        self.plan_subscription_catalog_id = plan_subscription_catalog_id
        self.plan_category_pack_id = plan_category_pack_id
    }
    
    
}
