//
//  OTTPlaylist+CoreDataClass.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 21/04/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData

@objc(OTTPlaylist)
public class OTTPlaylist: NSManagedObject {

    public class func DeleteAllPlaylist() {
        
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
        
        let list = OTTPlaylist.getManagedObjects(managedObjectContext: managedObjectContext) as? [OTTPlaylist]
        
        guard let tempList = list else{ return }
        
        for item in tempList
        {
            managedObjectContext.delete(item)
        }
          managedObjectContext.Save()
    }
    
    class func InitOTTPlaylist(managedObjectContext:NSManagedObjectContext,playListId:String) -> OTTPlaylist {
        
        
        if let playList = GetPlayListWith(playListId:playListId, managedObjectContext:managedObjectContext)
        {
            return playList
        }
        
        
        return  OTTPlaylist.managedObject(managedObjectContext: managedObjectContext) as! OTTPlaylist
    }
    
    
    class public  func GetPlayListWith(playListId:String?,managedObjectContext:NSManagedObjectContext) -> OTTPlaylist?
    {
        
        guard let id = playListId else{
            return nil
        }
        
        let item = OTTPlaylist.getManagedObjects(managedObjectContext: managedObjectContext,predicate: NSPredicate(format: "playlist_id = '\(id)'")) as? [OTTPlaylist]
        if item == nil || item?.count == 0 { return nil}
        return item?.first
    }
    
    
    class public  func GetPlayList(playlistType:String,profileId:String,managedObjectContext:NSManagedObjectContext) -> OTTPlaylist?
    {
              if PlayListManagerConfig.hasMultipleProfile == true {
                let item = OTTPlaylist.getManagedObjects(managedObjectContext: managedObjectContext,predicate: NSPredicate(format: "playlist_type = '\(playlistType)' AND profileId  =='\(profileId)'")) as?
                    [OTTPlaylist]
                if item == nil || item?.count == 0 { return nil}
                return item?.first
        }
        else
              {
                let item = OTTPlaylist.getManagedObjects(managedObjectContext: managedObjectContext,predicate: NSPredicate(format: "playlist_type = '\(playlistType)'")) as? [OTTPlaylist]
                if item == nil || item?.count == 0 { return nil}
                return item?.first
        }
        
       
    }
    
    public class func Parse(jsonDictionary:[String:Any],managedObjectContext:NSManagedObjectContext)
    {
            guard let playListId:String = jsonDictionary.valueFor(key:"playlist_id", isMandatory: false) else { return }
        
        
            let object                  = OTTPlaylist.InitOTTPlaylist(managedObjectContext: managedObjectContext,playListId: playListId)
        
            object.name                 = jsonDictionary.valueFor(key:"name", isMandatory: false)
            object.playlist_id          = jsonDictionary.valueFor(key:"playlist_id", isMandatory: false)
            object.playlist_type        = jsonDictionary.valueFor(key:"playlist_type", isMandatory: false)
        
            if PlayListManagerConfig.hasMultipleProfile == true
            {
                object.profileId            = jsonDictionary.valueFor(key:"profile_id", isMandatory: false)
            }
            else
            {
                  object.profileId            = ""
            }
        
            var playListItems = [OTTPlayListItem]()
            if let list:[[String:Any]] = jsonDictionary.valueFor(key:"listitems", isMandatory: false)
            {
                
                for item in list{
                    var playlistitem = OTTPlayListItem.Parse(jsonDictionary: item)
                    playListItems.append(playlistitem)
                }                
               
            }
        
            object.list = playListItems
        // print(object)
        //CoreDataStorage.shared.save()
           managedObjectContext.Save()
       
        
    }
    
    public class func GetPlayListFor(profileId:String,withplayListType:String?,managedObjectContext:NSManagedObjectContext) -> OTTPlaylist?
    {
        if withplayListType == nil { return nil }
        
        guard let p_String = withplayListType
            else{
                return nil
        }
        //print(p_String)
        
        guard  let playList = OTTPlaylist.getManagedObjects(managedObjectContext: managedObjectContext,predicate: NSPredicate(format:"playlist_type = '\(p_String)'")) as? [OTTPlaylist]
        else
        {
            return nil
        }
        return playList.first
        
    }
    
    public func getPlayListItemIdFor(contentId:String,catalogId:String,managedObjectContext:NSManagedObjectContext) -> String?
    {
        guard  let list = self.list else
        {
            return nil
        }
        
        if list.count == 0 { return nil}
        
        let predicate = NSPredicate(format: "contentId == '\(contentId)'")
        let filteredArray = list.filter { predicate.evaluate(with: $0) };
        
        guard let itemId = filteredArray.first?.listitem_id
        else
        {
            return nil
        }
        
        return itemId
    }
    
    public func getPlayListItemFor(contentId:String,catalogId:String) -> OTTPlayListItem?
    {
        guard  let list = self.list else
        {
            return nil
        }
        
        if list.count == 0 { return nil}
        
        for item in list
        {
            //AppUtilities.printLog(message: "List of playlist items : \(item.contentId)  == \(item.listitem_id)")
            if item.contentId == contentId
            {
                return item
            }
        }
        
//        if let index = list.index(where: { (item) -> Bool in
//            item.contentId == contentId // test if this is the item you're looking for
//        })
//        {
//            return list[index]
//        }
        
        return nil
    }
    
    public func getPlayListItemFor(listItemId:String?) -> OTTPlayListItem?
    {
        if listItemId == nil { return nil }
        
        guard  let list = self.list else
        {
            return nil
        }
        
        if list.count == 0 { return nil}
        
        for item in list
        {
            //AppUtilities.printLog(message: "List of playlist items : \(item.contentId)  == \(item.listitem_id)")
            if item.listitem_id == listItemId
            {
                return item
            }
        }
//
//
//        
//        guard  let index = list.index(where: { (item) -> Bool in
//            item.listitem_id == listItemId // test if this is the item you're looking for
//        })
//            else
//        {
//            return nil
//        }
        
        return nil
    }
    
    
    public class func getPlayListItemFor(contentId:String,catalogId:String,playlistType:String,managedObjectContext:NSManagedObjectContext) -> OTTPlayListItem?
    {
       
        
        var profileId = ""
        
        if let pId = AppUserDefaults.boundProfileId
        {
            profileId = pId
        }
        else
        {
            if PlayListManagerConfig.hasMultipleProfile == true { return nil }
        }
        
        guard let playlist = OTTPlaylist.GetPlayList(playlistType: playlistType, profileId: profileId,managedObjectContext:managedObjectContext)
            else
        {
            return nil
        }
        
        
        if let playlistItem = playlist.getPlayListItemFor(contentId: contentId, catalogId: catalogId)
        {
            return playlistItem
        }
        
        
        return nil
    }
    
    public class func updatePlayListItemFromPlaylistwith(playlistId:String,item:OTTPlayListItem,managedObjectContext:NSManagedObjectContext)
    {
      
        guard let playList =  GetPlayListWith(playListId: playlistId,managedObjectContext:managedObjectContext)
            else
        {
            return
        }
        
       
        
        if let item = playList.getPlayListItemFor(listItemId: item.listitem_id)
        {
            return
        }
        
//        let newPlayList = OTTPlaylist.managedObject(managedObjectContext: managedObjectContext) as! OTTPlaylist
//        
//        newPlayList.name = playList.name
//        newPlayList.playlist_id = playList.playlist_id
//        newPlayList.profileId = playList.profileId
//        newPlayList.playlist_type = playList.playlist_type
        
        
        var itemList = [OTTPlayListItem]()
        
        if let list = playList.list
        {
            for item in list
            {
                itemList.append(item)
            }
        }
        
        itemList.append(item)
        playList.list = itemList
//        managedObjectContext.delete(playList)
//        managedObjectContext.insert(newPlayList)
        
    
        //CoreDataStorage.shared.save()
        managedObjectContext.Save()
        
    }
    
    
    public class func updatePlayListItemFromWatchHistory(type:String,contentId:String,catalogId:String,watchTime:String,response:[String:Any],managedObjectContext:NSManagedObjectContext)
    {
        
        var profileId = ""
        
        if let pId = AppUserDefaults.boundProfileId
        {
            profileId = pId
        }
        else
        {
            if PlayListManagerConfig.hasMultipleProfile == true { return }
        }
        
        
        guard let playList =  GetPlayList(playlistType: type, profileId: profileId, managedObjectContext: managedObjectContext)
            else
        {
            return
        }
        
        var pItem = OTTPlayListItem.Parse(jsonDictionary: response)
        pItem.play_back_time = watchTime
       
        
        AppUtilities.printLog(message: "Updated Playback time ; \(pItem.play_back_time)")
//        let newPlayList = OTTPlaylist.managedObject(managedObjectContext: managedObjectContext) as! OTTPlaylist
//        
//        newPlayList.name = playList.name
//        newPlayList.playlist_id = playList.playlist_id
//        newPlayList.profileId = playList.profileId
//        newPlayList.playlist_type = playList.playlist_type
        
        
        var itemList = [OTTPlayListItem]()
        
        if let list = playList.list
        {
            for item in list
            {
                if item.catalogId == catalogId && item.contentId == contentId
                {
                    continue
                }
                itemList.append(item)
            }
        }
        
        itemList.append(pItem)
        playList.list = itemList

        
        
        managedObjectContext.Save()
        
    }
    
    public class func updatePlayListItemWithType(type:String,contentId:String,catalogId:String,response:[String:Any],managedObjectContext:NSManagedObjectContext)
    {
        
        var profileId = ""
        
        if let pId = AppUserDefaults.boundProfileId
        {
            profileId = pId
        }
        else
        {
            if PlayListManagerConfig.hasMultipleProfile == true { return }
        }
        
        
        guard let playList =  GetPlayList(playlistType: type, profileId: profileId, managedObjectContext: managedObjectContext)
            else
        {
            return
        }
        
        var pItem = OTTPlayListItem.Parse(jsonDictionary: response)
        
        
        

        
        
        var itemList = [OTTPlayListItem]()
        
        if let list = playList.list
        {
            for item in list
            {
                if item.catalogId == catalogId && item.contentId == contentId
                {
                    continue
                }
                itemList.append(item)
            }
        }
        
        itemList.append(pItem)
        playList.list = itemList
        
        
        
        managedObjectContext.Save()
        
    }
    
    public class func removePlayListItemFromPlaylistwith(playlistId:String,itemId:String,managedObjectContext:NSManagedObjectContext)
    {
        
        guard let playList =  GetPlayListWith(playListId: playlistId,managedObjectContext:managedObjectContext)
            else
        {
            return
        }
        
//        let newPlayList = OTTPlaylist.managedObject(managedObjectContext: managedObjectContext) as! OTTPlaylist
//        
//        newPlayList.name = playList.name
//        newPlayList.playlist_id = playList.playlist_id
//        newPlayList.profileId = playList.profileId
//        newPlayList.playlist_type = playList.playlist_type
        
        var itemList = [OTTPlayListItem]()
        
        if let list = playList.list
        {
            for item in list
            {
                if itemId != item.listitem_id
                {
                    itemList.append(item)
                }
            }
        }

        playList.list = itemList
        managedObjectContext.Save()
        
        
    }
    
    public class func multipleItemsWithSameName(playlistName:String,managedObjectContext:NSManagedObjectContext) -> Bool
    {
        let playlst =  playlistName.uppercased()
        let list1 = OTTPlaylist.getManagedObjects(managedObjectContext: managedObjectContext) as? [OTTPlaylist]

        for lis in  list1!
        {
              AppUtilities.printLog(message: "\(lis.name)")
        }
        
        guard  let list = OTTPlaylist.getManagedObjects(managedObjectContext: managedObjectContext,predicate: NSPredicate(format:"name = '\(playlst)'")) as? [OTTPlaylist]
            else
        {
            return false
        }
        
        if list.count == 0 { return false }
        
        return true
    }
}
