//
//  OTTExtras.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 21/04/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation

class OTTExtraItems
{
    var videoId:String?
    var catalogId:String?
    var description:String?
    var duration:String?
    var genere:[String]?
    var language:String?
    var playURL: OTTPlayURL?
    var playURLType: String?
    var thumbnails:OTTImageURLs?
    var title:String?
    var categoryName:String?
    
    class func Parse(json:[String:Any]) -> OTTExtraItems
    {
        var item = OTTExtraItems()
        
        item.videoId        = json.valueFor(key: "videolist_id", isMandatory: false)
        item.catalogId      = json.valueFor(key: "catalog_id", isMandatory: false)
        item.description    = json.valueFor(key: "description", isMandatory: false)
        item.duration       = json.valueFor(key: "duration", isMandatory: false)
        item.genere         = json.valueFor(key: "genres", isMandatory: false)
        item.language       = json.valueFor(key: "language", isMandatory: false)
       
        item.playURLType    = json.valueFor(key: "smart_url", isMandatory: false)
        
        item.title          = json.valueFor(key: "title", isMandatory: false)
        item.categoryName   = json.valueFor(key: "category", isMandatory: false)
        
        
        if StringHelper.isNilOrEmpty(string: item.playURLType) == false
        {
            if let dict:[String:Any] = json.valueFor(key:"play_url", isMandatory: false)
            {
               item.playURL = OTTPlayURL.Parse(jsonDictionary: dict, playURLTypeString: item.playURLType!)
            }
        }
        
        
        if let thumbnails:[String:Any] = json.valueFor(key: "thumbnails", isMandatory: false)
        {
            item.thumbnails = OTTImageURLs.Parse(jsonDictionary: thumbnails)
        }
        
        return item
        
    }
}

class OTTExtras
{
    var category:String?
    var items:[OTTExtraItems]?
//    
//    public class func Parse(response:[String:Any]?) -> [OTTExtras]?
//    {
//        guard let data:[String:Any] = response?.valueFor(key: "data", isMandatory: false)
//        else
//        {
//            return nil
//        }
//        
//        guard let items:[[String:Any]] = data.valueFor(key: "items", isMandatory: false) else
//        {
//            return nil
//        }
//        
//        if items.count == 0 { return nil }
//        
//        let extrasList = [OTTExtras]()
//        
//        for item in items
//        {
//           
//            
//        }
//        
//    }
    
}
