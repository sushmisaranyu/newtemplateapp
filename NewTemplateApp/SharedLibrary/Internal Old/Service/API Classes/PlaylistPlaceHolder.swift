//
//  PlaylistPlaceHolder.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 24/04/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation


public class PlaylistPlaceHolder
{
    var content_id:String
    var catalog_id:String
    
    public init(contentId:String,catalogId:String)
    {
        self.content_id = contentId
        self.catalog_id = catalogId
    }
    
    
    public func json() -> String? {
        
        var json = "{\"content_id\":\"\(content_id)\",\"catalog_id\":\"\(catalog_id)\"}"
              
        AppUtilities.printLog(message: "JSON:\(json)")
        return  json
    }
    
    
    
}
