//
//  OTTSeasons.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 03/05/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation


public class OTTSeasons
{
    public var title:String?
    public var content_id:String?
    public var catalog_id:String?
    public var language:String?
    public var genres:[String]?
    public var description:String?
    public var thumbnails:OTTImageURLs?
    public var episode_flag:Bool?
    public var subcategory_flag:Bool?
    public var catalog_name:String?
    public var show_name:String?
    public var sequence_no:NSNumber = 0
    public var episodes:[OTTVideoItem]?
    public var episode_count:Int = 0
    
    public var hasEpisodes:Bool{
        if episodes == nil || episodes?.count == 0
        {
            return false
        }
        
        return true
    }
    
    var episodeCount:Int{
        guard let list = episodes
            else {
                return 0
        }
        
        return list.count
    }
    
    public class func Parse(jsonDictionary:[String:Any]) -> OTTSeasons
    {
        var season = OTTSeasons()
        
        season.title = jsonDictionary.valueFor(key: "title", isMandatory: false)
        
        season.content_id       = jsonDictionary.valueFor(key: "content_id", isMandatory: false)
        season.catalog_id       = jsonDictionary.valueFor(key: "catalog_id", isMandatory: false)
        season.language         = jsonDictionary.valueFor(key: "language", isMandatory: false)
        season.genres           = jsonDictionary.valueFor(key: "genres", isMandatory: false)
        season.description      = jsonDictionary.valueFor(key: "description", isMandatory: false)
        season.episode_flag     = jsonDictionary.valueFor(key: "episode_flag", isMandatory: false)
        season.subcategory_flag = jsonDictionary.valueFor(key: "subcategory_flag", isMandatory: false)
        season.catalog_name     = jsonDictionary.valueFor(key: "catalog_name", isMandatory: false)
        season.show_name        = jsonDictionary.valueFor(key: "show_name", isMandatory: false)
        
        if let value:NSNumber  = jsonDictionary.valueFor(key: "sequence_no", isMandatory: false)
        {
            season.sequence_no  = value
        }
        
        if let count:NSNumber  = jsonDictionary.valueFor(key: "episode_count", isMandatory: false)
        {
            let epcount = count.intValue
        
            if epcount > 0
            {
            
                season.episode_count = epcount
            
                if StringHelper.isNilOrEmpty(string: season.title)
                {
                    season.title = "Season" + " - \(count) Episodes"
                }
                else
                {
                    season.title =  season.title! + " - \(count) Episodes"
                }
                
            }
        }

        
        
        
        if let thumbnailjson:[String:Any] = jsonDictionary.valueFor(key:"thumbnails", isMandatory: false)
        {
            season.thumbnails   = OTTImageURLs.Parse(jsonDictionary: thumbnailjson)
        }
        
        return season
        
    }
    
    
}
