//
//  OTTImage.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 28/02/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation


public class OTTImageURLs:NSObject,NSCoding
{

    var thumbnails:[String:String]?

    override init() {

    }
    
    required convenience public init(coder aDecoder: NSCoder) {
        self.init()
        self.thumbnails           = aDecoder.decodeObject(forKey: "thumbnails") as? [String:String]
    }
    
    public func encode(with aCoder: NSCoder){
        aCoder.encode(thumbnails, forKey: "thumbnails")
    }
    
    
    /**
     Parses jsonDictionary
     - Parameter jsonDictionary: json dictionary from server
     - Returns: OTTImageURl
     */
   public class func Parse(jsonDictionary:[String:Any]) -> OTTImageURLs
    {
        let object = OTTImageURLs()
        var dictionary = [String:String]()

        for item in jsonDictionary
        {
            guard let dictvalue = item.value as? [String:String] else {continue}

            guard let url = dictvalue["url"] else {continue}

            if StringHelper.isNilOrEmpty(string: url) == true  {continue}

            dictionary[item.key] = url
        }

        object.thumbnails = dictionary

        return object
    }
    
    /// Return Thumbnail image with the specified key
    ///
    /// - Parameter key: key for thumbnail
    /// - Returns: url for thumbnail if avalible
    public func getThumbnailFor(key:String) -> String?
    {
        return self.thumbnails?[key]
    }
}
