//
//  OTTMediaItem+CoreDataClass.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 02/03/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation
import CoreData

@objc(OTTMediaItem)
public class OTTMediaItem: NSManagedObject {

    
    
    /**
     Parses jsonDictionary
     - Parameter jsonDictionary: json dictionary from server
     */
    public  class func Parse(jsonDictionary:[String:Any], setCache:Bool = false,thm:String? = nil) -> String?
    {
        
        if let contentId:String = jsonDictionary.valueFor(key:"content_id", isMandatory: false)
        {
            
            let managedObjectContext    = NSManagedObjectContext.newObjectContext()
            let object                  = OTTMediaItem.InitOTTMediaItem(contentId: contentId, managedObjectContext: managedObjectContext)
            
            object.catalogId            = jsonDictionary.valueFor(key:"catalog_id", isMandatory: false)
            object.catalogName          = jsonDictionary.valueFor(key:"catalog_name", isMandatory: false)
            object.contentId            = jsonDictionary.valueFor(key:"content_id", isMandatory: false)
            object.mediadescription     = jsonDictionary.valueFor(key:"description", isMandatory: false)
            object.duration             = jsonDictionary.valueFor(key:"duration_string", isMandatory: false)
            object.friendlyId           = jsonDictionary.valueFor(key:"friendly_id", isMandatory: false)
            object.genres               = jsonDictionary.valueFor(key:"genres", isMandatory: false)
            object.keywords             = jsonDictionary.valueFor(key:"keywords", isMandatory: false)
            object.language             = jsonDictionary.valueFor(key:"language", isMandatory: false)
            
            
            
            var rating:NSNumber = 0
            
            if let avgrating:String = jsonDictionary.valueFor(key:"average_user_rating", isMandatory: false)
            {
                if  let number = Double(avgrating)
                {
                    rating = NSNumber(value: number)
                }
            }
            
            object.rating               = rating
            object.releaseDate          = jsonDictionary.valueFor(key:"release_date_string", isMandatory: false)
            
            if let tm:String = jsonDictionary.valueFor(key:"theme", isMandatory: false)
            {
                object.theme                = jsonDictionary.valueFor(key:"theme", isMandatory: false)
            }
            else
            {
                if let hardcodedtm = thm
                {
                    object.theme = hardcodedtm
                }
            }
//            object.theme                = jsonDictionary.valueFor(key:"theme", isMandatory: false)
            object.title                = jsonDictionary.valueFor(key:"title", isMandatory: false)
            object.censorRating         = jsonDictionary.valueFor(key:"censor_rating", isMandatory: false)
            object.episodeFlag          = false
            object.subcategoryFlag      = false
            object.share_url            = jsonDictionary.valueFor(key:"share_url", isMandatory: false)
            object.show_theme_id        = jsonDictionary.valueFor(key:"show_theme_id", isMandatory: false)
            object.subcategory_id       = jsonDictionary.valueFor(key:"subcategory_id", isMandatory: false)
            object.subcategory_name     = jsonDictionary.valueFor(key:"subcategory_name", isMandatory: false)
            object.no_of_user_rated     = jsonDictionary.valueFor(key: "no_of_user_rated", isMandatory: false)
            
            if let value:[String:Any]  = jsonDictionary.valueFor(key: "access_control", isMandatory: false)
            {
                object.access_control       = OTTAccessControl.Parse(json: value)
            }
            
            if let value:String = jsonDictionary.valueFor(key:"episode_flag", isMandatory: false)
            {
                if value == "yes"
                {
                     object.episodeFlag = true
                }
                
            }
            
            object.episode_count = 0
            
            if let value:NSNumber = jsonDictionary.valueFor(key: "episode_count", isMandatory: false)
            {
                object.episode_count = value
            }
            
            if let value:String = jsonDictionary.valueFor(key:"subcategory_flag", isMandatory: false)
            {
                if value == "yes"
                {
                    object.subcategoryFlag = true
                }
                
            }
                       
            if let value:Bool = jsonDictionary.valueFor(key:"subcategory_flag", isMandatory: false)
            {
                object.subcategoryFlag      = value
            }
            
            if let value:Bool = jsonDictionary.valueFor(key:"episode_flag", isMandatory: false)
            {
                object.episodeFlag      = value
            }
            
            if setCache == true{
             object.cache               = OTTCache.initCache()
            }
            
            
            if let dict:[[String:Any]]  = jsonDictionary.valueFor(key:"people", isMandatory: false)
            {
                var people = [OTTPeople]()
                var index:Int64 = 0
                for obj in dict
                {
                    people.append(OTTPeople.Parse(jsonDictionary: obj,sortid: index))
                    index+=1
                }
                
                object.people = people
            }
            
            
            if let value:String = jsonDictionary.valueFor(key:"play_url_type", isMandatory: false)
            {
                object.playURLType  = value
                if let dict:[String:Any] = jsonDictionary.valueFor(key:"play_url", isMandatory: false)
                {
                    if object.theme == "movie" 
                    {
                        object.playURL = OTTPlayURL.Parse(jsonDictionary: dict, playURLTypeString: value)
                    }
                    
                    if object.theme == "video"
                    {
                        object.playURL = OTTPlayURL.ParseFallbackForVideoTheme(jsonDictionary: dict, playURLTypeString: value)
                    }
                }
            }
            
            
            if let lastEpidsode:[String:Any] = jsonDictionary.valueFor(key: "last_episode", isMandatory: false)
            {
                if lastEpidsode.count > 0
                {
                    object.last_episode = OTTVideoItem.Parse(jsonDictionary: lastEpidsode)
                }
            }
            
            if let thumbnailjson:[String:Any] = jsonDictionary.valueFor(key:"thumbnails", isMandatory: false)
            {
                object.thumbnails   = OTTImageURLs.Parse(jsonDictionary: thumbnailjson)
            }
  
            //CoreDataStorage.shared.save()
            managedObjectContext.Save()
            
           
            
           
            
            return object.contentId
        }
        
        return nil
    }
    
}
