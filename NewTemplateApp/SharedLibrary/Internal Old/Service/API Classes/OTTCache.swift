//
//  OTTCache.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 19/03/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation



public class OTTCache:NSObject,NSCoding
{
    var lastUpdatedTime:TimeInterval? = 0.0
    
    
    
    required convenience public init(coder aDecoder: NSCoder) {
        
        self.init()
        self.lastUpdatedTime   = (aDecoder.decodeObject(forKey: "lastUpdatedTime") as? TimeInterval)
    
    }
    
    public func encode(with aCoder: NSCoder){
        aCoder.encode(lastUpdatedTime, forKey: "lastUpdatedTime")
    }
    
    
    fileprivate func setCache()
    {
        lastUpdatedTime = Date().timeIntervalSince1970
    }
    
    
    fileprivate func isCacheValid() -> Bool
    {
        guard let lastCachedTime = lastUpdatedTime else {
            return false
        }
        
        let currentTime = Date().timeIntervalSince1970
        
        let diff  = currentTime - lastCachedTime
        
        if diff  > 5 * 60 { return false }
        
        return true
    }
    
    public class func initCache() -> OTTCache
    {
        let cache = OTTCache()
        cache.setCache()
        return  cache
    }
    
    public class func isCacheValid(cache:OTTCache?) -> Bool{
        
        guard let tcache = cache
        else
        {
            return false
        }
        
        return tcache.isCacheValid()
    }
    
}
