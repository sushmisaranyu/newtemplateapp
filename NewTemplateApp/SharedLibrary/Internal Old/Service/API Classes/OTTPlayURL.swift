//
//  OTTPlayURL.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 01/03/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation

public enum OTTPlayURLType
{
    case Saranyu
    case Youtube
    case DailyMotion
    
   public var key:String
    {
        switch self
        {
            case .Saranyu       : return "saranyu"
            case .Youtube       : return "youtube"
            case .DailyMotion   : return "dailymotion"
        }
    }
    
    static func GetTypeFor(value:String?) -> OTTPlayURLType?
    {
        guard let key = value else{
            return nil
        }
        
       switch key
       {
            case OTTPlayURLType.Saranyu.key: return OTTPlayURLType.Saranyu
            case OTTPlayURLType.Youtube.key: return OTTPlayURLType.Youtube
            case OTTPlayURLType.DailyMotion.key: return OTTPlayURLType.DailyMotion
            default: return nil
        }
    }
}

public class OTTPlayURL:NSObject,NSCoding
{
    public var urlType:OTTPlayURLType?
    public var url:String?
    
    
   public  override init()
    {
        
    }
    
    required convenience public init(coder aDecoder: NSCoder) {
        
        self.init()
        self.urlType           = OTTPlayURLType.GetTypeFor(value: (aDecoder.decodeObject(forKey: "urlType") as? String)!)
        self.url               = aDecoder.decodeObject(forKey: "url") as? String
        
        
    }
    
    public func encode(with aCoder: NSCoder){
        aCoder.encode(urlType?.key, forKey: "urlType")
        aCoder.encode(url, forKey: "url")
    }
    
    
   public class func Parse(jsonDictionary:[String:Any], playURLTypeString:String) -> OTTPlayURL?
    {
        if let type = OTTPlayURLType.GetTypeFor(value: playURLTypeString)
        {
           if let playurl =  GetURL(jsonDictionary: jsonDictionary, type: type)
           {
                return playurl
            }
            else
           {
                 return ParseFallbackForVideoTheme(jsonDictionary:jsonDictionary, playURLTypeString:OTTPlayURLType.Saranyu.key)
            }
        }
        
        return nil
    }
    
    public class func ParseFallbackForVideoTheme(jsonDictionary:[String:Any], playURLTypeString:String) -> OTTPlayURL?
    {
        if let type = OTTPlayURLType.GetTypeFor(value: playURLTypeString)
        {
            if let url:String = jsonDictionary.valueFor(key:"url", isMandatory: false)
            {
                let obj = OTTPlayURL()
                obj.url = url
                obj.urlType = type
                return obj
            }
        }
        
        return nil
    }
    
   public class func GetURL(jsonDictionary:[String:Any],type:OTTPlayURLType) -> OTTPlayURL?
    {
        if let dict:[String:Any] = jsonDictionary.valueFor(key:type.key, isMandatory: false)
        {
            if let url:String = dict.valueFor(key:"url", isMandatory: false)
            {
                let obj = OTTPlayURL()
                obj.url = url
                obj.urlType = type
                return obj
            }
        }
        
        return nil
        
    }
    
    
    
}
