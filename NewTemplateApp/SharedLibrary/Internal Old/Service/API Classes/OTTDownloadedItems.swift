//
//  OTTDownloadedItems.swift
//  SaraynuLibrary
//
//  Created by Ashley Dsouza on 03/10/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation
import CoreData


@objc(OTTDownloadedItems)
public class OTTDownloadedItems: NSManagedObject {
    @NSManaged public var id: String?
    @NSManaged public var saveDate: Double
    @NSManaged public var savedItem: OTTVideoItem?
    @NSManaged public var downloaded:Bool
    
    
    public class func InitOTTDownloadedItems(contentID:String,managedObjectContext:NSManagedObjectContext) -> OTTDownloadedItems {
        
        
        if let item = GetOTTDownloadedItems(contentId:contentID)
        {
            return item
        }
        
        return  OTTDownloadedItems.managedObject(managedObjectContext: managedObjectContext) as! OTTDownloadedItems
    }
    
    
    
    class public  func GetOTTDownloadedItems(contentId:String?) -> OTTDownloadedItems?
    {
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
        guard let id = contentId else{
            return nil
        }

        let item = OTTDownloadedItems.getManagedObjects(managedObjectContext: managedObjectContext,predicate: NSPredicate(format: "id = '\(id)'")) as? [OTTDownloadedItems]
        if item == nil || item?.count == 0 { return nil}
        return item?.first
    }
    
    
    class public func DeleteItemWith(contentId:String,managedObjectContext:NSManagedObjectContext)
    {
        guard let item = GetOTTDownloadedItems(contentId: contentId)
        else
        {
            return
        }
        
        managedObjectContext.delete(item)
        managedObjectContext.Save()
    
    }
    
    
    public class func DeleteAllDownloadedItems() {
        
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
        
        let list = OTTDownloadedItems.getManagedObjects(managedObjectContext: managedObjectContext) as? [OTTDownloadedItems]
        
        guard let tempList = list else{ return }
        
        for item in tempList
        {
            managedObjectContext.delete(item)
        }
        managedObjectContext.Save()
        
    }
    
    
    public class func GetListOfExpiredDownloads() -> [OTTDownloadedItems]?
    {
        
        var expiredList = [OTTDownloadedItems]()
        
        let managedObjectContext    = NSManagedObjectContext.newObjectContext()
        
        guard  let list = OTTDownloadedItems.getManagedObjects(managedObjectContext: managedObjectContext) as? [OTTDownloadedItems]
        else
        {
            return nil
        }
        
        for item in list
        {
            if item.downloaded == false{
                continue
            }
            
            let expiry = Date().timeIntervalSince1970 - item.saveDate
            
            if DownloadConfig.expiryTime < expiry
            {
                expiredList.append(item)
            }
        }
        
        if expiredList.count == 0 {return nil}
        
        return expiredList
    }
    
}
