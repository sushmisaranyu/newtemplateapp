//
//  OTTItemContent.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 20/03/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation
import CoreData

public class OTTItemContent:OTTItem
{
    public var mediaItem:OTTMediaItem?
    public var catalogItem:OTTCatalogItem?
    
    
    public init(item:OTTItem)
    {
        super.init(id: item.id, type: item.itemType, sortId: item.sortId)
    }
    
    required convenience public init(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    

    public func getMediaItem(managedObjectContext:NSManagedObjectContext) -> OTTMediaItem?
    {
        if mediaItem != nil { return mediaItem }
        
        let item = OTTMediaItem.GetMediaItemWith(contentId: self.id)
        mediaItem = item
        
        return item
    }
    
    public func getCatalogItem(managedObjectContext:NSManagedObjectContext) -> OTTCatalogItem?
    {
        if catalogItem != nil { return catalogItem }
        
        let item    = OTTCatalogItem.GetOTTCatalogItemWith(listId: self.id, managedObjectContext: managedObjectContext)
        catalogItem = item
        return item
    }
    
    
    
}
