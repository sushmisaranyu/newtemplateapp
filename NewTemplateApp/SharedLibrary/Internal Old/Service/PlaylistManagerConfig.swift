//
//  PlaylistManagerConfig.swift
//  SaraynuLibrary
//
//  Created by Ashley Dsouza on 12/05/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation



public class PlayListManagerConfig
{
    public static var addToListPlaylistName = "my favourite"
    public static var addToListPlaylistType = "my favourite"
    public static var watchHistoryPlaylistType  = "watchhistory"
    public static var hasMultipleProfile = true
}
