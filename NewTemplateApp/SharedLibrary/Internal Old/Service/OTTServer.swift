//
//  ServerInterface.swift
//  SaraynuLibrary
//
//  Created by Ash Dz on 23/02/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation
import Alamofire

public enum HTTPMethodForAPI
{
    case GET
    case POST
    case DELETE
    case PUT
}

public class OTTServer
{
    public static let notification_SessionInvalidNotificationName = Notification.Name("SessionInvalidNotification")

    fileprivate var  httpMethod:HTTPMethod
    fileprivate var  jsonParameters:[String:Any]?
    fileprivate var  serviceURL:String
    fileprivate var  hasArrayInResponse:Bool
    
    static var fixedGetParameters:String{
        return "?auth_token=\(AppConfiguration.authTokenForApp)&region=\(AppConfiguration.regionForApp)"
    }
    
    init(serviceURL:String,httpMethod:HTTPMethodForAPI, jsonParameters:[String:Any]? = nil,hasArrayInResponse:Bool = false)
    {
        switch httpMethod
        {
            case .POST:         self.httpMethod = .post
            case .GET:          self.httpMethod = .get
            case .DELETE:       self.httpMethod = .delete
            case .PUT:          self.httpMethod = .put
        }
        self.serviceURL         = serviceURL
        self.hasArrayInResponse = hasArrayInResponse
        self.jsonParameters     = jsonParameters
    }
          
    
    func call(successHandler: ((_ responseJSON: [String: Any]) -> Void)? = nil,successHandlerwithArray: ((_ responseJSON: [[String: Any]]) -> Void)? = nil, errorHandler: ((_ code:String,_ message:String) -> Void)? = nil,internetErrorHandler:((Int)->Void)? = nil) {
        

         URLCache.shared = URLCache(memoryCapacity: 0, diskCapacity: 0, diskPath: nil)
        
        var requestHTTP = URLRequest(url: URL(string: self.serviceURL)!)
        requestHTTP.httpMethod = self.httpMethod.rawValue
        requestHTTP.cachePolicy = .reloadRevalidatingCacheData
        if jsonParameters != nil
        {
           requestHTTP.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let jsonData = try? JSONSerialization.data(withJSONObject: jsonParameters as Any)
           requestHTTP.httpBody = jsonData
           
            let string  = NSString(data: jsonData!, encoding: String.Encoding.utf8.rawValue)
            AppUtilities.printLog(message: "\(String(describing: string))")
        }

        DispatchQueue.global().async {
            NetworkActivityIndicator.start()
            
            request(requestHTTP).responseJSON{ response in
                    if (LibaryConfig.isLoggingEnabled)
                    {
                        AppUtilities.printLog(message: "ServiceURL : \(self.serviceURL)")
                        AppUtilities.printLog(message: "Response Data \(String(describing: response.data))")
                        AppUtilities.printLog(message: "Response Response \(String(describing: response.response))")
                         AppUtilities.printLog(message: "Response Description \(response.description)")
                        //print("Response Result \(response.result.value)")
                      
                    }
                    //  print("Response Description \(response.result.value as! [String:AnyObject])")
                    NetworkActivityIndicator.stop()
                    if  let statusCode = response.response?.statusCode
                    {
                        //Server error code
                        if statusCode == 422
                        {
                            guard let errorjson: [String:Any] = response.result.value as? [String:Any]
                                else{
                                    let (code,message) = ErrorMessageManager.getDefaultErrorMessage()
                                    errorHandler?(code,message)
                                    return
                            }
                            AppUtilities.printLog(message: "\(errorjson)")
//                            let val = errorjson["error"]
//                            let val1 = val!["code"]
//                            AppUtilities.printLog(message: " ******* \(val)")

                            let (code,message) = ErrorMessageManager.getServerErrorMessagefor(json: errorjson)
                             AppUtilities.printLog(message: " ############ \(code)")
                            
                            //Invalid Session Id
                            if code == "1016"
                            {
                                //Cancel all network calls and logout.
                                // Post notification for Reload
                                NotificationCenter.default.post(name: OTTServer.notification_SessionInvalidNotificationName, object: nil)
                                return
                            }
                            else if message == "Invalid Session Id"
                            {
                                NotificationCenter.default.post(name: OTTServer.notification_SessionInvalidNotificationName, object: nil)
                                return
                            }
                            
                            
                            errorHandler?(code,message)
                            return
                        }
//                        else
//                        {
//                            let (code,message) = ErrorMessageManager.getDefaultErrorMessage()
//                            errorHandler?(code,message)
//                            return
//                        }
                        
                    }
                    
                    
                    switch response.result {
                    case .success(let theResponseJSON) :
                        
                        if self.hasArrayInResponse == true{
                            if let responseDictionaryArray = theResponseJSON     as? [[String :AnyObject]]
                            {
                                //print(" Response Data :\(responseDictionary)")
                                successHandlerwithArray?(responseDictionaryArray)
                            }
                            else
                            {
                                AppUtilities.printLog(message:"Server did not return an appropriate dictionary object. \(theResponseJSON)")
                                let (code,message) = ErrorMessageManager.getDefaultErrorMessage()
                                errorHandler?(code,message)
                            }
                        }
                        else
                        {
                            if let responseDictionary = theResponseJSON     as? [String :AnyObject]
                            {
                                //print(" Response Data :\(responseDictionary)")
                                successHandler?(responseDictionary)
                            } else {
                                AppUtilities.printLog(message:"Server did not return an appropriate dictionary object. \(theResponseJSON)")
                                let (code,message) = ErrorMessageManager.getDefaultErrorMessage()
                                errorHandler?(code,message)
                            }
                        }
                        
                        
                      
                    case let .failure(error):
                        if error._code != -999 {        // -999 is when we cancel the Alamofire request. So there is no need to log this                         if errorType!._code == -1009
                            
                            
                            
                            if error._code == -1009
                            {
                                AppUtilities.printLog(message:"No Internet connection detected!!!!  \(error) : Status Code :\(error._code)")
                                internetErrorHandler?(error._code)
                            }
                            else if error._code == -1001
                            {
                                AppUtilities.printLog(message:"Request timeout   \(error) : Status Code :\(error._code)")
                                internetErrorHandler?(error._code)
                            }
                            else
                            {
                                let (code,message) = ErrorMessageManager.getDefaultErrorMessage()
                                errorHandler?(code,message)
                            }
                        }
                        else
                        {
                            AppUtilities.printLog(message:"Error:: \(error)")
                            let (code,message) = ErrorMessageManager.getDefaultErrorMessage()
                            errorHandler?(code,message)
                        }
                    }
                   
            }
        }
    }


    func callWithDataLoader(responseHandler: ((_ response: DataLoader<[String: Any]>) -> Void)? = nil) {


        URLCache.shared = URLCache(memoryCapacity: 0, diskCapacity: 0, diskPath: nil)

        guard let url = URL(string: self.serviceURL) else {
            responseHandler?(DataLoaderErrorHelper.getDefaultErrorDataLoader())
            return
        }

        var requestHTTP = URLRequest(url: url)
        requestHTTP.httpMethod = self.httpMethod.rawValue
        requestHTTP.cachePolicy = .reloadRevalidatingCacheData
        if jsonParameters != nil
        {
            requestHTTP.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let jsonData = try? JSONSerialization.data(withJSONObject: jsonParameters as Any)
            requestHTTP.httpBody = jsonData

            let string  = NSString(data: jsonData!, encoding: String.Encoding.utf8.rawValue)
            AppUtilities.printLog(message: "\(String(describing: string))")
        }

        DispatchQueue.global().async { [weak self] in
            NetworkActivityIndicator.start()

            request(requestHTTP).responseJSON{ [weak self] response in

                //guard let strongSelf = self else {return}

                if (LibaryConfig.isLoggingEnabled)
                {
                    AppUtilities.printLog(message: "ServiceURL : \(response.request?.url?.absoluteString)")
                    AppUtilities.printLog(message: "Response Data \(String(describing: response.data))")
                   // AppUtilities.printLog(message: "Response Response \(String(describing: response.response))")
                    // AppUtilities.printLog(message: "Response Description \(response.description)")
                    //print("Response Result \(response.result.value)")

                }
                //  print("Response Description \(response.result.value as! [String:AnyObject])")
                NetworkActivityIndicator.stop()
                if  let statusCode = response.response?.statusCode
                {
                    //Server error code
                    if statusCode == 422
                    {
                        guard let errorjson: [String:Any] = response.result.value as? [String:Any]
                            else{
                                responseHandler?(DataLoaderErrorHelper.getDefaultErrorDataLoader())
                                return
                        }

                        if LibaryConfig.isLoggingEnabled == true
                        {
                            AppUtilities.printLog(message: "\(errorjson)")
                        }

                        let (code,message) = ErrorMessageManager.getServerErrorMessagefor(json: errorjson)
                        AppUtilities.printLog(message: " ############ \(code)")

                        //Invalid Session Id
                        if code == "1016"
                        {
                            //Cancel all network calls and logout.
                            // Post notification for Reload
                            NotificationCenter.default.post(name: OTTServer.notification_SessionInvalidNotificationName, object: nil)
                            return
                        }
                        else if message == "Invalid Session Id"
                        {
                            NotificationCenter.default.post(name: OTTServer.notification_SessionInvalidNotificationName, object: nil)
                            return
                        }

                        responseHandler?(DataLoader.serverErrorWithCode(code: code, message: message))
                        return
                    }
                    //                        else
                    //                        {
                    //                            let (code,message) = ErrorMessageManager.getDefaultErrorMessage()
                    //                            errorHandler?(code,message)
                    //                            return
                    //                        }

                }


                switch response.result {
                case .success(let theResponseJSON) :

                    if self?.hasArrayInResponse == true{
                        if let responseDictionaryArray = theResponseJSON     as? [[String :AnyObject]]
                        {
                            let arrayDict:[String:Any] =  ["data":responseDictionaryArray]
                            responseHandler?(DataLoader.success(response: arrayDict))
                            return
                        }
                        else
                        {
                            AppUtilities.printLog(message:"Server did not return an appropriate dictionary object. \(theResponseJSON)")

                            responseHandler?(DataLoaderErrorHelper.getDefaultErrorDataLoader())
                            return
                        }
                    }
                    else
                    {
                        if let responseDictionary = theResponseJSON     as? [String :AnyObject]
                        {

                            responseHandler?(DataLoader.success(response: responseDictionary))
                            return
                        } else {
                            AppUtilities.printLog(message:"Server did not return an appropriate dictionary object. \(theResponseJSON)")

                            responseHandler?(DataLoaderErrorHelper.getDefaultErrorDataLoader())
                        }
                    }



                case let .failure(error):
                    if error._code != -999 {        // -999 is when we cancel the Alamofire request. So there is no need to log this                         if errorType!._code == -1009



                        if error._code == -1009
                        {
                            AppUtilities.printLog(message:"No Internet connection detected!!!!  \(error) : Status Code :\(error._code)")
                            responseHandler?(DataLoader.connectivityError)
                        }
                        else if error._code == -1001
                        {
                            AppUtilities.printLog(message:"Request timeout   \(error) : Status Code :\(error._code)")
                            responseHandler?(DataLoader.connectivityError)
                        }
                        else
                        {
                            let (code,message) = ErrorMessageManager.getDefaultErrorMessage()
                            responseHandler?(DataLoader.connectivityError)
                        }
                    }
                    else
                    {
                        AppUtilities.printLog(message:"Error:: \(error)")

                        responseHandler?(DataLoaderErrorHelper.getDefaultErrorDataLoader())
                    }
                }

            }
        }
    }
    
   
    
}
