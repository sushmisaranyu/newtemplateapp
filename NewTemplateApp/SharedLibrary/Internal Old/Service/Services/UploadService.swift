//
//  UploadService.swift
//  SaraynuLibrary
//
//  Created by Ashley Dsouza on 06/07/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation
import Alamofire


public class UploadService
{
    
   public class func alamofireMultipartUpload(_ data: Data,key:String,acl:String,AWSAccessKeyId:String,Policy:String,Signature:String,userID:String ,baseURl:String, successHandler: @escaping (_ response:String ) -> Void, errorHandler: @escaping (String,String) -> Void ) {
        
       
    _ = "image/jpeg"
    
    _ = key + "701264862.jpg"
    
    let parameterHeader: [String: String] = [
            "key" :   key + "\(userID).jpg",
            "acl"   :   acl,
            "AWSAccessKeyId"   :   AWSAccessKeyId,
            "policy": Policy,
            "Signature":Signature,
            "Content-Type":"image/jpeg"
            ]

        let serviceURL: String = baseURl

        upload(multipartFormData:
            {
                multipartFormData in
                for (key, value) in parameterHeader {
                    
                    multipartFormData.append( value.data(using: String.Encoding.utf8)!, withName: key)
                    //multipartFormData.appendBodyPart(data: value.dataUsingEncoding(String.Encoding.utf8)!, name: key)
                }
                multipartFormData.append(data, withName: "file", fileName:"\(userID).jpg",  mimeType: "image/jpeg")
                
              
                
                
        }, to: serviceURL,method : .post, encodingCompletion: {encodingResult in
            
            switch encodingResult {
            case .success(let upload, _, _):
                upload.response(completionHandler: { response in
//                    print(response.data)
//                    print(response.error.debugDescription)
//                    print(response.request)
//                    
//                    print(response.response?.statusCode)
                    
                    if let statusCode = response.response?.statusCode
                    {
                        if statusCode > 220
                        {
                            errorHandler("","")
                            return
                        }
                    }
                    
                    
                    if let error = response.error {
                        if response.error?._code != -999 {
                             AppUtilities.printLog(message: "\(response)")
                            
                            if response.error?._code == -1009 || response.error?._code == -1001
                            {
                                let (title,message)  = ErrorMessageManager.getInternetErrorMessage(code: (response.error?._code)!)
                                print("\(title) : \(message)")
                                errorHandler(title,message)
                            }
                            else
                            {
                                AppUtilities.printLog(message:"Error while uploading data. \(error as NSError)")
                                errorHandler("Upload Failed","Failed to upload image.")
                            }
                            
                            
                        }
                    } else {
                        debugPrint("Image Uploaded Successfully")
                        let hexcode = String(data: response.data!, encoding: String.Encoding.utf8)!
                         AppUtilities.printLog(message: hexcode)
                         AppUtilities.printLog(message: "\(response)")
                        
                        successHandler(hexcode)
                    }
                    
                })
            case .failure(let encodingError):
                
                print(encodingError._code)
                if encodingError._code == -1009 || encodingError._code == -1001
                {
                    //let (title,message)  = ErrorMessageManager.getInternetErrorMessage(code: (encodingError._code)!)
                    //print("\(title) : \(message)")
                    errorHandler("","")
                }
                else
                {
                    errorHandler("Upload Failed","Failed to upload image.")
                }
            }
            
        })
        
    }

    
    public class func Upload(data:Data,key:String,acl:String,AWSAccessKeyId:String,Policy:String,signature:String)
    {
        
        
        let headers = [
            "content-type": "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"]
        let parameters = [
            [
                "name": "key",
                "value": key + "@701264862.jpg"
            ],
            [
                "name": "acl",
                "value": acl
            ],
            [
                "name": "AWSAccessKeyId",
                "value": AWSAccessKeyId
            ],
            [
                "name": "Policy",
                "value": Policy
            ],
            [
                "name": "Signature",
                "value": signature
            ],
            [
                "name": "Content-Type",
                "value": "image/jpeg"
            ],
            [
                "name": "file",
                "value": "@701264862.jpg"
            ]
        ]
        let boundary = "----WebKitFormBoundary7MA4YWxkTrZu0gW"
        var body = ""
        let error: NSError? = nil
        for param in parameters {
            let paramName = param["name"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if let filename = param["fileName"] {
                let contentType = param["content-type"]!
                 let fileContent = data.base64EncodedString(options: [])
                
                if (error != nil) {
                    AppUtilities.printLog(message: "\(String(describing: error))")
                }
                body += "; filename=\"\(filename)\"\r\n"
                body += "Content-Type: \(contentType)\r\n\r\n"
                body += fileContent
            } else if let paramValue = param["value"] {
                body += "\r\n\r\n\(paramValue)"
            }
        }
        let request = NSMutableURLRequest(url: NSURL(string: "https://tcr-cf-test.s3.amazonaws.com/")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = body.data(using: .utf8)
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                AppUtilities.printLog(message: "\(String(describing: error))")
            } else {
                let httpResponse = response as? HTTPURLResponse
                AppUtilities.printLog(message: "\(String(describing: httpResponse))")
            }
        })
        dataTask.resume()
        
    }
    
}




