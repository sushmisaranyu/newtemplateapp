//
//  GDPRAPI.swift
//  SaraynuLibrary
//
//  Created by Ashley Dsouza on 14/06/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation


class GDPRAPI
{
    public enum API
    {
        case GetGDPRStatusForUser(type:String,userID:String)
        case UpdateGDPRStatus(type:String,userID:String,cookieState:Bool)

    }

    var httpMethodForAPI:HTTPMethodForAPI
    {
        switch api
        {
        case .GetGDPRStatusForUser: return .GET
        case .UpdateGDPRStatus: return .POST

        }
    }

    var urlPath:String
    {
        switch api
        {
        case .GetGDPRStatusForUser(let type, let userId):  return "/users/consent_data?auth_token=\(AppConfiguration.authTokenForApp)&user_id=\(userId)&type=\(type)"
        case .UpdateGDPRStatus:                               return "/users/gdpr_data"


        }
    }

    fileprivate var api:API

    var apiURL:String{
        return AppConfiguration.serverURL.value()! + urlPath
    }

    public init(api:API)
    {
        self.api = api
    }

    var inputParameters:[String:Any]?
    {
        switch api
        {
        case .UpdateGDPRStatus(let type,let userid,let cookieState):   return  ["auth_token": AppConfiguration.authTokenForApp,
                                                          "user": ["user_id":userid, "gdpr_consent_data":true.description,"gdpr_cookies_data":cookieState.description,"type":type]]
        default: return nil

        }
    }

    public func call(successHandler: ((_ responseJSON: [String: Any]) -> Void)? = nil, errorHandler: ((_ code:String,_ message:String) -> Void)? = nil,internetErrorHandler:((Int)->Void)? = nil)
    {
        let service = OTTServer(serviceURL: apiURL, httpMethod: httpMethodForAPI,jsonParameters:inputParameters)
        service.call(successHandler: successHandler,
                     errorHandler: errorHandler,
                     internetErrorHandler: internetErrorHandler)
    }


 
}
