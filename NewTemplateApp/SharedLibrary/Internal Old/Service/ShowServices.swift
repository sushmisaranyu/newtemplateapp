//
//  ShowServices.swift
//  cinestaan
//
//  Created by Ash Dz on 03/05/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation


public class TVShows
{

    



    //Archive

    public class func getEpisodesForShow(showId:String,successHandler:@escaping (([OTTVideoItem])->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        let service = CatalogAPI(api: .GetEpisodesForShow(showId: showId))

        service.call(successHandler: { response in

            if let episodeList = CommonEpisodeParser(response: response)
            {
                successHandler(episodeList)
            }
            else
            {
                ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
                return
            }

        }, errorHandler: { code , message in
            errorHandler("Show",message)

        } , internetErrorHandler: {code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)

        })
    }

    public class func getSeasonsForShow(showId:String,successHandler:@escaping (([OTTSeasons])->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        let service = CatalogAPI(api: .GetSeasonsForShow(showId: showId))
        
        service.call(successHandler: { response in
            //Basic Json Validation
            guard let dataJson:[String:Any] = response.valueFor(key: "data", isMandatory: false)
                else{
                    ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
                    return
            }
            
            guard let itemJson:[[String:Any]] = dataJson.valueFor(key: "items", isMandatory: false)
                else{
                    ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
                    return
            }
            
            //Parsing Season Item
            var list = [OTTSeasons]()
            for item in itemJson
            {
                let videoItem = OTTSeasons.Parse(jsonDictionary: item)
                list.append(videoItem)
            }
            
            
            //Sorting Video Items by Sequence No
            let sortedArray = list.sorted(by: { $0.sequence_no.intValue < $1.sequence_no.intValue })
            successHandler(sortedArray)
          
        }, errorHandler: { code , message in
            
            errorHandler("Show",message)
            
        } , internetErrorHandler: {code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
            
        })

        
    }
    
    
    public class func getEpisodesForSeason(seasonId:String,successHandler:@escaping (([OTTVideoItem])->Void),errorHandler:@escaping (_ title:String,_ message:String)->Void, internetErrorHandler:@escaping (_ title:String,_ message:String)->Void)
    {
        let service = CatalogAPI(api: .GetEpisodesForSeason(seasonId: seasonId))
        
        service.call(successHandler: { response in
            
            if let episodeList = CommonEpisodeParser(response: response)
            {
                successHandler(episodeList)
            }
            else
            {
                ErrorMessageManager.getDefaultErrorMessage(errorHandler: errorHandler)
                return
            }
            
        }, errorHandler: { code , message in
            
            errorHandler("Show",message)
            
        } , internetErrorHandler: {code in
            ErrorMessageManager.GetInternetErrorMessage(code: code, internetErrorHandler: internetErrorHandler)
            
        })
    }
    

    public class func CommonEpisodeParser(response:[String:Any]) -> [OTTVideoItem]?
    {
        //Basic Json Validation
        guard let dataJson:[String:Any] = response.valueFor(key: "data", isMandatory: false) else { return nil  }

        guard let itemJson:[[String:Any]] = dataJson.valueFor(key: "items", isMandatory: false) else { return nil }

        //Parsing Video Item
        var videoList = [OTTVideoItem]()
        for item in itemJson
        {
            guard let _:String = item.valueFor(key: "content_id", isMandatory: false)
                else
            {
                continue
            }

            guard let videoItem = OTTVideoItem.Parse(jsonDictionary: item) else { continue }
            videoItem.theme = CatalogService.episodeTheme
            videoList.append(videoItem)
        }


        //Sorting Video Items by Sequence No
        let sortedArray = videoList.sorted(by: { ($0.sequence_no?.intValue)! < ($1.sequence_no?.intValue)! })
        return sortedArray
    }
    

    
}
