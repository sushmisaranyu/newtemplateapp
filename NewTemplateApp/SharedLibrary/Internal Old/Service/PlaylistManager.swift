//
//  PlaylistManager.swift
//  SaraynuLibrary
//
//  Created by Ashley Dsouza on 11/05/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation
import CoreData

public class PlaylistManager
{
    //Get playlist 
    public class func getPlayList(playlistName:String, playlistType:String,managedObjectContext:NSManagedObjectContext, successHandler:@escaping ((_ playList:OTTPlaylist)->Void),errorHandler:@escaping ((_ title:String,_ message:String)-> Void) , internetErrorHandler:@escaping ((_ title:String,_ message:String)-> Void))
    {
        
        var profileId = ""
        
        if let pId = AppUserDefaults.boundProfileId
        {
            profileId = pId
        }
        else
        {
            if PlayListManagerConfig.hasMultipleProfile == true {
                let (title,message) = ErrorMessageManager.getDefaultErrorMessage()
                errorHandler(title,message)
                return
            }
        }
        
        
      
        
        //1. Check local DB for id
        if let playlist = OTTPlaylist.GetPlayList(playlistType: playlistType, profileId: profileId,managedObjectContext:managedObjectContext)       {
            
            successHandler(playlist)
        }
        else
        {
            //2. If playlist not found in DB Get All Playlists from server
            PlaylistService.GetAllPlayList(managedObjectContext:managedObjectContext,successHandler: {
                
                //2. a Recheck For playlist
                
                if let playlist = OTTPlaylist.GetPlayList(playlistType: playlistType, profileId: profileId,managedObjectContext:managedObjectContext)
                {
                    successHandler(playlist)
                }
                else
                {
                    
                    // If playlist is not there in server response create playlist
                    CreatePlayList(playlistName: playlistName, playlistType: playlistType,managedObjectContext:managedObjectContext, profileId: profileId, successHandler: successHandler, errorHandler: errorHandler, internetErrorHandler: internetErrorHandler)
                    
                }
                
            }, errorHandler: errorHandler, internetErrorHandler: internetErrorHandler)
        }
    }
    
    
    //Create PlayList for item
    public class  func CreatePlayList(playlistName:String,playlistType:String,managedObjectContext:NSManagedObjectContext,profileId:String,successHandler:@escaping ((_ playlist:OTTPlaylist)->Void),errorHandler:@escaping ((_ title:String,_ message:String)-> Void) , internetErrorHandler:@escaping ((_ title:String,_ message:String)-> Void))
    {
        let name = playlistName.trimmingCharacters(in: .whitespaces)
        let type = playlistType.trimmingCharacters(in: .whitespaces)

        
        PlaylistService.CreatePlayList(playlistName: name, playlistType: type,managedObjectContext:managedObjectContext, successHandler: {
            if let playlist = OTTPlaylist.GetPlayList(playlistType: playlistType, profileId: profileId,managedObjectContext: managedObjectContext)
            {
                successHandler(playlist)
            }
            else
            {
                AppUtilities.printLog(message: "Failed to create Playlist Type : \(playlistType) Name : \(playlistName) : profileId :\(profileId)")
                let (title,message) = ErrorMessageManager.getDefaultErrorMessage()
                errorHandler(title,message)
            }
            
        }, errorHandler: errorHandler, internetErrorHandler: internetErrorHandler)
    }
    
    
    public class func additemToPlaylist(catalogId:String,contentId:String,playlistName:String,playlistType:String,errortitle:String,errormsg:String,managedObjectContext:NSManagedObjectContext,successHandler:@escaping (()->Void),errorHandler:@escaping ((_ title:String,_ message:String)-> Void) , internetErrorHandler:@escaping ((_ title:String,_ message:String)-> Void))
    {
        let errorTitle = errortitle
        let errorMessage = errormsg
        
        //1. Get PlayList
        PlaylistManager.getPlayList(playlistName:playlistName,playlistType: playlistType, managedObjectContext:managedObjectContext,successHandler: {
            playList in
            
            guard let playlistId = playList.playlist_id
                else
            {
                errorHandler(errorTitle,errorMessage)
                return
            }
            
            
            
            //Add item to PlayList
            PlaylistService.AddItemsToPlayList(playListId: playlistId, contentId: contentId, catalogId: catalogId, managedObjectContext:managedObjectContext,successHandler: successHandler, errorHandler: { title,message in
                AppUtilities.printLog(message: "Add item to playlist \(title) :: \(message)")
                errorHandler(errorTitle,errorMessage)
            }, internetErrorHandler: internetErrorHandler)
            
            
        }, errorHandler:errorHandler, internetErrorHandler: internetErrorHandler)
    }
    
    public class func additemToPlaylistWithType(catalogId:String,contentId:String,playlistName:String,playlistType:String,errortitle:String,errormsg:String,managedObjectContext:NSManagedObjectContext,successHandler:@escaping (()->Void),errorHandler:@escaping ((_ title:String,_ message:String)-> Void) , internetErrorHandler:@escaping ((_ title:String,_ message:String)-> Void))
    {
        let errorTitle = errortitle
        let errorMessage = errormsg
        
        //1. Get PlayList
        
        PlaylistService.UpdatePlaylistWithType(catalogId: catalogId, contentId: contentId, playListType: playlistType, managedObjectContext: managedObjectContext, successHandler: successHandler, errorHandler: errorHandler, internetErrorHandler: internetErrorHandler)
    }
    
    
    public class func removeitemToPlaylistWithType(catalogId:String,contentId:String,playlistName:String,playlistType:String,errortitle:String,errormsg:String,managedObjectContext:NSManagedObjectContext,successHandler:@escaping (()->Void),errorHandler:@escaping ((_ title:String,_ message:String)-> Void) , internetErrorHandler:@escaping ((_ title:String,_ message:String)-> Void))
    {
        let errorTitle = errortitle
        let errorMessage = errormsg
        
        //1. Get PlayList
        
        PlaylistService.UpdatePlaylistWithType(catalogId: catalogId, contentId: contentId, playListType: playlistType, managedObjectContext: managedObjectContext, successHandler: successHandler, errorHandler: errorHandler, internetErrorHandler: internetErrorHandler)
    }
    
    public class func removeitemFromPlaylist(catalogId:String,contentId:String,playlistName:String,playlistType:String,errortitle:String,errormsg:String,managedObjectContext:NSManagedObjectContext,successHandler:@escaping (()->Void),errorHandler:@escaping ((_ title:String,_ message:String)-> Void) , internetErrorHandler:@escaping ((_ title:String,_ message:String)-> Void))
    {
        let errorTitle = "Opps"
        let errorMessage = "Failed to remove item from the list."
        //Get Playlist
        PlaylistManager.getPlayList(playlistName:playlistName, playlistType: playlistType,managedObjectContext:managedObjectContext, successHandler: {
            playList in
            
            guard let playlistId = playList.playlist_id
                else
            {
                errorHandler(errorTitle,errorMessage)
                return
            }
            
            //AppUtilities.printLog(message: "Remove Items From Playlist with Id: \(playlistId)")
           
            guard  let playlistItem = OTTPlaylist.getPlayListItemFor(contentId: contentId, catalogId: catalogId, playlistType: playlistType,managedObjectContext:managedObjectContext)
                else
            {
                errorHandler(errorTitle,errorMessage)
                return
            }
            
            //Remove playlist item
            PlaylistService.RemoveItemFromPlayList(playListId: playlistId, playListItemId: playlistItem.listitem_id!, managedObjectContext: managedObjectContext,successHandler: successHandler, errorHandler: { _,_ in errorHandler(errorTitle,errorMessage)}, internetErrorHandler: internetErrorHandler)
            
            
        }, errorHandler:{
            title,message in
            AppUtilities.printLog(message: "Remove Item from playlist \(title) :: \(message)")
            errorHandler(errorTitle,errorMessage)
        }, internetErrorHandler: internetErrorHandler)
    }

    
    public class func getaItemStateInPlayList(contentId:String?,catalogId:String?,inPlaylistType:String,managedObjectContext:NSManagedObjectContext) -> Bool
    {
        guard let contentid = contentId else { return false }
        guard let catalogid = catalogId else { return false }
        
        if let playlistItem = OTTPlaylist.getPlayListItemFor(contentId: contentid, catalogId: catalogid, playlistType: inPlaylistType,managedObjectContext:managedObjectContext)
        {
            AppUtilities.printLog(message: "\(playlistItem.listitem_id)")
            return true
        }
        else
        {
            return false
        }
    }

    public class func getUserRating(contentId:String?,catalogId:String?,inPlaylistType:String,managedObjectContext:NSManagedObjectContext) -> NSNumber?
    {
        guard let contentid = contentId else { return false }
        guard let catalogid = catalogId else { return false }
        
        if let playlistItem = OTTPlaylist.getPlayListItemFor(contentId: contentid, catalogId: catalogid, playlistType: inPlaylistType,managedObjectContext:managedObjectContext)
        {
           
            return playlistItem.userRatings
        }
        else
        {
            return nil
        }
    }
    
    
    

}


