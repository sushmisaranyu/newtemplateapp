//
//  ErrorEnums.swift
//  SaraynuLibrary
//
//  Created by Ashley Dsouza on 15/05/17.
//  Copyright © 2017 Saranyu. All rights reserved.
//

import Foundation


public enum EmailValidationError
{
    case InvalidEmailId
    case EmailIdIsNilOrEmpty
    case EmailIdHasSpaces
}

public enum MobileNosValidationError
{
    case InvalidMobileNos
    case MobileNosIsNilOrEmpty
    case MobileNosHasSpaces
}

public enum CountryCodeValidationError
{
    case InvalidCountryCode
    case CountrycodeIsNilOrEmpty
    case CountryCodeHasSpaces
}
public enum OtpValidationError
{
    case InvalidOtp
    case OtpIsNilOrEmpty
    case OtpHasSpaces
}
public enum EmailCharsetError
{
    case SpaceNotAllowedInEmail
}
public enum PhoneCharsetError
{
    case SpaceNotAllowedInMobile
    case hasCharactersOtherThanNumbers

}


public enum PasswordValidationError
{
    case EmptyOrNil
    case SizeLessThanMiniumCharCount
    case HasSpace
}

public enum NameValidationError
{
    case hasSpaces
    case isEmpty
    case hasSpecialCharacter
    case hasNumbers
}

public enum NameCharsetError
{
    case cannotStartWithSpace
    case issSpace
    case issSpecialCharacter
    case issNumbers
}


public enum PlaylistNameCharsetValidationError
{
    case isEmpty
    case hasSpecialCharacter
    case maxLengthReached
    case multiplePlaylistSameName
}

public enum PlaylistNameValidationError
{
    case isEmpty
    case hasSpecialCharacter
    case maxLengthReached
}
