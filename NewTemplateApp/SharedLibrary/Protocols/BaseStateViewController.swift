//
//  BaseStateViewController.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 03/09/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import UIKit

public protocol LoaderDependancyProtocol:class
{
    func setupLoader()
    func startLoader()
    func stopLoader()
}

public protocol EmptyDataViewProtocol:class
{
    func setupEmptyDataView(view:UIView)
    func showNoItemErrorView(title:String,message:String)
    func showErrorView(title:String,message:String)
    func showNoInternetErrorView()
}


public protocol BaseViewStateManager: class
{


}
