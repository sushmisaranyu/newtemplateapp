//
//  BaseCollectionViewProtocol.swift
//  SharedLibrary
//
//  Created by Ashley Dsouza on 27/08/18.
//  Copyright © 2018 Saranyu. All rights reserved.
//

import Foundation
import UIKit

public protocol  BaseCollectionViewProtocol:class {
    func getCellIdentifier() -> String
    func registerNibForCell(collectionView:UICollectionView?)
    func getSizeForCell(collectionViewRect:CGRect,direction:UICollectionViewScrollDirection,catalogItem:CatalogListItem?) -> CGSize
    func setupCell(collectionView:UICollectionView,indexPath:IndexPath,item:OTTVideoItem,catalogItem:CatalogListItem?) -> UICollectionViewCell
    func selectionSupported(indexPath:IndexPath,item:OTTVideoItem?) -> Bool
}


