//
//  T_Font.swift
//  NewTemplateApp
//
//  Created by Apple  on 15/03/19.
//  Copyright © 2019 Apple . All rights reserved.
//

import Foundation
class T_Font:NSObject
{
    public var fontName:String?
    public var fontSize:T_Size?
   

    
    
    public class func parseFontsFromJsonData(fonts:[[String:Any]]) -> [[String:T_Font]]?
    {
        var fontsArray = [[String:T_Font]]()
        for fontInRow in fonts
        {
                if let keyFont:String = fontInRow["key"] as? String
                {
                    if let template:[String:Any] = fontInRow["template"] as? [String:Any]
                    {
                        let fontObj = getTfontFromDictionary(template: template)
                        fontsArray.append([keyFont : fontObj])
                    }
                    
                    
                }
            
        }
        return fontsArray
    }
    
    class func getTfontFromDictionary(template:[String:Any]) -> T_Font
    {
        let myFont = T_Font()
        myFont.fontName = template["font_Name"] as? String
        myFont.fontSize = T_Size.parseSizeFromDictionary(template:template)
        return myFont
    }
    
    public class func  getUIFontForTfont(tcolor:T_Font) -> UIFont
    {
        
        return UIFont.init(name: tcolor.fontName!, size: CGFloat(tcolor.fontSize!.fontSizeIphone!))!
        
    }
    
}
