//
//  T_Color.swift
//  NewTemplateApp
//
//  Created by Apple  on 12/03/19.
//  Copyright © 2019 Apple . All rights reserved.
//

import UIKit

class T_Color: NSObject {

    public var hexValue:String?
    public var alpha:Float?
    

    
    
    public class func parseColorsFromJsonData(colors:[[String:Any]]) -> [[String:T_Color]]?
    {
          var colorsArray = [[String:T_Color]]()
        for colorInRow in colors
        {
                if let keyColor:String = colorInRow["key"] as? String
                {
                    if let template:[String:Any] = colorInRow["template"] as? [String:Any]
                    {
                        let colorObj:T_Color = getTColorFromDictionary(template: template)
                        colorsArray.append([keyColor : colorObj])
                    }
                    
                    
                }
            
        }
        return colorsArray
    }
    
    class func getTColorFromDictionary(template:[String:Any]) ->T_Color
    {
        let mycolor = T_Color()
        mycolor.hexValue = template["hex"] as? String
        mycolor.alpha = template["alpha"] as? Float
        return mycolor
    }
    
    
    
    
    
    
    public class func  getUIcolorForTcolor(tcolor:T_Color) -> UIColor
    {
        return UIColor.init(hexString: tcolor.hexValue!, alpha: tcolor.alpha!)!
        
    }
    
}

