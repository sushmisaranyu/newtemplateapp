//
//  T_Size.swift
//  NewTemplateApp
//
//  Created by Apple  on 18/03/19.
//  Copyright © 2019 Apple . All rights reserved.
//

import Foundation
class T_Size :NSObject
{
    public var fontSizeIphone:Float?
    public var fontSizeIpad:Float?
    public var fontSizeTv:Float?
    
    
   public class func parseSizeFromDictionary(template:[String:Any]) -> T_Size
    {
        let sizeObj = T_Size()
        sizeObj.fontSizeIphone = template["fontSize_Iphone"] as? Float
        sizeObj.fontSizeIpad = template["fontSize_Pad"] as? Float
        sizeObj.fontSizeTv = template["fontSize_Tv"] as? Float
        return sizeObj
    }
}
