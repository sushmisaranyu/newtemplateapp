//
//  TemplateManager.swift
//  NewTemplateApp
//
//  Created by Apple  on 12/03/19.
//  Copyright © 2019 Apple . All rights reserved.
//

import Foundation
import UIKit
import SharedLibrary


class TemplateManager: NSObject {
    
    var appColors = [[String:T_Color]]()
    var appFonts = [[String:T_Font]]()

    public static let shared = TemplateManager()
    
    override init() {
        super .init()
       
    }
   //MARK: PARSE
    func parseColorsAndFontsDataFromFile()
    {
        let jsonData:[String:Any] =  self.parseDataFromFile()!
        self.parseColors(jsonResult: jsonData)
        self.parseFonts(jsonResult:jsonData)
    
    }
    public func parseDataFromFile() -> [String:Any]?
    {
        
        guard let path = Bundle.main.path(forResource: "ColorFont", ofType: "json") else {
            return nil
        }
        guard let data  = try? Data.init(contentsOf: URL.init(fileURLWithPath: path)) else
        {
            return nil
        }
        guard let jsonResult:[String:Any] = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String : Any] else
        {
            return nil
        }
        
        return jsonResult
        
    }
    func parseColors(jsonResult:[String:Any])
    {
        if let colors:[[String:Any]] = jsonResult["Colors"] as? [[String:Any]]
        {
            guard let result:[[String:T_Color]] =  T_Color.parseColorsFromJsonData(colors: colors) else
            {
                return
            }
           
            appColors = result
            let color:T_Color =  self.getColorForString(colorString: "grey")!
            let uicolor:UIColor =  T_Color.getUIcolorForTcolor(tcolor:color)
            print(color)
             print(uicolor)
            
        }
    }
    
    func parseFonts(jsonResult:[String:Any])
    {
        if let fonts:[[String:Any]] = jsonResult["Fonts"] as? [[String:Any]]
        {
            guard let result:[[String:T_Font]] =  T_Font.parseFontsFromJsonData(fonts: fonts) else
            {
                return
            }
            
            appFonts =  result
        }
    }
    
    //MARK: GET OBJECTS FROM STRINGS
    func getColorForString(colorString:String) -> T_Color?
    {
               for colorInRow in appColors
               {
                        if colorInRow.keys.first == colorString
                        {
                            if let extractedColor:T_Color = colorInRow[colorString]
                            {
                                return extractedColor
                            }
                            return nil
                        }
                }
        
                  return nil
    }
    
    func getFontForString(fontString:String) -> T_Font?
    {
        for fontInRow in appFonts
        {
            if fontInRow.keys.first == fontString
            {
                if let extractedFont:T_Font = fontInRow[fontString]
                {
                    return extractedFont
                }
                return nil
            }
        }

        return nil
    }
    
    
}
